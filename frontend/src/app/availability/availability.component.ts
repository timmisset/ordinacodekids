import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from "../services/authentication/authentication.service";
import {AvailabilityService} from "../services/availability/availability.service";
import {MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import {CalendarService} from "../services/calendar/calendar.service";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {ToastrService} from "ngx-toastr";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {EmployeeService} from "../services/employee/employee.service";
import {SchoolService} from "../services/school/school.service";
import {BookingService} from "../services/booking/booking.service";

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css']
})
export class AvailabilityComponent implements OnInit {


  constructor(private authenticationService: AuthenticationService,
              private calendarService:CalendarService,
              private availabilityService: AvailabilityService,
              private employeeService:EmployeeService,
              private schoolService: SchoolService,
              private toastr: ToastrService,
              private bookingService: BookingService
              ) { }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.availabilityService.subjectAvailabilities.subscribe(availabilities => {
      this.load(availabilities);
    });
    this.load(this.availabilityService.availabilities);
    this.employeeService.subjectEmployees.subscribe(value => { if(value) {this.employees = value } });
    this.employees = this.employeeService.employees;

    this.authenticationService.currentUserSubject.subscribe(user => {
      if(user) {
        if(user.role === "ROLE_ADMIN") {
          this.isAdmin = true;
          this.employee = undefined;
        } else{
          this.isAdmin = false;
          this.employee = this.employees.filter(employee=> {
            return employee.email === user.email
          })[0];
        }
        this.isAdminOrEmployee = ["ROLE_ADMIN", "ROLE_EMPLOYEE"].indexOf(user.role) > -1;
      }
    });
  }

  load(availabilities: Availability[])
    {

      availabilities = this.filterToOwner(availabilities);
      this.dataSource = new MatTableDataSource<Availability>(availabilities);
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (availability, columnDef) => {

        switch(columnDef) {
          case "date": return availability.date;
          case "employee": return this.padSortingValue(`${this.getEmployeeAttribute(availability.employeeId, "firstName")} ${this.getEmployeeAttribute(availability.employeeId, "lastName")}`, this.sort.direction);
          case "booking-school-name": return this.padSortingValue(this.getSchoolAttribute(availability.bookingId, "name"), this.sort.direction);
          case "booking-school-contact": return this.padSortingValue(this.getSchoolAttribute(availability.bookingId, "contactName"), this.sort.direction);
          case "booking-school-contact-email": return this.padSortingValue(this.getSchoolAttribute(availability.bookingId, "contactEmail"), this.sort.direction);
          default:
            return "";
        }
      };
      this.dataSource.filterPredicate = ((availability, filter) => {
        filter = filter.trim().toLowerCase();
        let match = false;
        const employeeAttributes = [this.getEmployeeAttribute(availability.employeeId, "firstName"), this.getEmployeeAttribute(availability.employeeId, "lastName"), this.getEmployeeAttribute(availability.employeeId, "email")];
        employeeAttributes.forEach((value:string) => {
          if(value) {
            value = value.toLowerCase().trim();
            if(value.indexOf(filter) > -1 || filter.indexOf(value) > -1) { match = true; return; }
          }
        });
        const schoolAttributes = [this.getSchoolAttribute(availability.bookingId, "name"), this.getSchoolAttribute(availability.bookingId, "contactName"), this.getSchoolAttribute(availability.bookingId, "contactEmail")];
        schoolAttributes.forEach((value:string) => {
          if(value) {
            value = value.toLowerCase().trim();
            if(value.indexOf(filter) > -1 || filter.indexOf(value) > -1) { match = true; return; }
          }
        });

        return match;
      });

      this.dataSource.paginator = this.paginator;



  }

  displayedColumns: string[] = ['select', 'date', 'employee', 'booking-school-name', 'booking-school-contact', 'booking-school-contact-email'];
  dataSource = new MatTableDataSource<Availability>();
  selection = new SelectionModel<Availability>(true, []);
  calendarDate: Date;

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  ignoreFilter = false;
  selectEmployeeBadge: string = "";
  isAdmin: boolean = false;
  isAdminOrEmployee:boolean = false;
  employees: Employee[];
  employee: Employee;

  /**
   * The date filter used by the date picker to disable certain dates
   * @param date
   */
  dateFilter = (date: Date): boolean => {
    const day = date.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6 && date > new Date() &&
      this.dataSource.data.find(value => value.date.toLocaleDateString() === date.toLocaleDateString() && value.employeeId === this.employee.id) === undefined
  };

  filterToOwner(availabilities:Availability[]) {
    if(this.ignoreFilter || this.authenticationService.currentUserIsAdmin || !availabilities) { return availabilities; }
    else {
      return availabilities.filter(value =>
        this.employeeService.mappedEmployees[value.employeeId].email === this.authenticationService.currentUserSubject.getValue().email
        ||
        ( this.bookingService.mappedBookings[value.bookingId] &&
          this.schoolService.mappedSchools[this.bookingService.mappedBookings[value.bookingId].schoolId].contactEmail ===
          this.authenticationService.currentUserSubject.getValue().email
      ));
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue;
  }

  /**
   * This method will pad the value with prefix 'ZZZ' or '000' in case the value is '' or undefined
   * to make sure that sorting on column values will never list the empty ones on top
   * @param value
   * @param direction
   */
  padSortingValue(value, direction) {
    const padWith = direction === 'asc' ? 'zzz' : '000';
    return !(value) ? padWith : value.toLowerCase();
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle($event: MatCheckboxChange) {
    $event.checked ?
      this.dataSource.connect().getValue().forEach(row => this.selection.select(row)) :
      this.selection.clear()

  }

  getBooking(bookingId:number):Booking {
    return this.bookingService.mappedBookings[bookingId];
  }


  getSchoolAttribute(bookingId: any, attribute: string) {
     const booking:Booking = this.getBooking(bookingId);
     if(booking) {
       const school:School = this.schoolService.mappedSchools[booking.schoolId];
       return school[attribute];
     }
  }

  getEmployeeAttribute(employeeId:number, attribute:string ) {
    const employee:Employee = this.employeeService.mappedEmployees[employeeId];
    if(employee) {
      return employee[attribute];
    }

  }

  getAvailabilityDate(availability: Availability) {
    return availability.date.toLocaleDateString();
  }

  createAvailability(date: Date) {
    const availability:Availability= {
      date: date,
      employeeId: this.employee.id
    };
    this.availabilityService.createAvailability(availability);
  }

  dateSelected($event: any) {
    this.calendarDate = $event.value;
    this.createAvailability($event.value)
  }


  openPicker(picker: any) {
    if(this.isAdmin && !this.employee) {
      // throw error:
      this.selectEmployeeBadge = "!";
      this.toastr.warning("You must first select an employee to add availability for");
      this.calendarDate = undefined;
      setTimeout(() => { this.selectEmployeeBadge = ""}, 2500);
      picker.close();
    }
    else{
      picker.open();
    }
  }

  async remove() {
    const confirmed = this.availabilityService.removeAvailabilities( this.selection.selected);
    if(confirmed) { this.selection.clear(); }
  }
}
