import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { AvailabilityComponent } from './availability.component';
import {AngularMaterialModule} from "../resources/angular-material.module";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {CalendarService} from "../services/calendar/calendar.service";
import {AvailabilityService} from "../services/availability/availability.service";
import {EmployeeService} from "../services/employee/employee.service";
import {SchoolService} from "../services/school/school.service";
import {ToastrService} from "ngx-toastr";
import {BookingService} from "../services/booking/booking.service";
import {User} from "../model/User";
import {BehaviorSubject, Subject} from "rxjs";
import {Test} from "tslint";
import {By, HAMMER_LOADER} from "@angular/platform-browser";
import {OrdinaCalendarEvent} from "../model/OrdinaCalendarEvent";
import {SelectionModel} from "@angular/cdk/collections";
import {MatTableDataSource} from "@angular/material/table";
import {OrdinaDateAdapter} from "../util/ordina-date-adapter";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {Platform} from "@angular/cdk/platform";

class MockAuthenticationService {

  currentUserSubject: BehaviorSubject<User> = new BehaviorSubject({ email: "admin", role: "ROLE_ADMIN"});
}
class MockEmployeeService {
  subjectEmployees: Subject<Employee[]> = new Subject<Employee[]>();
}
class MockAvailabilityService {
  subjectAvailabilities: Subject<Availability[]> = new Subject<Availability[]>();
  createAvailability() {}
  removeAvailabilities() {}
}

describe('AvailabilityComponent', () => {
  let component: AvailabilityComponent;
  let fixture: ComponentFixture<AvailabilityComponent>;
  let mockEmployee:Employee;
  let mockSchool:School;
  let mockAvailability:Availability;
  let mockBooking:Booking;
  let mockEvent:OrdinaCalendarEvent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule],
      declarations: [ AvailabilityComponent ],
      providers: [
        { provide: OrdinaDateAdapter, useClass: OrdinaDateAdapter, deps: [ MAT_DATE_LOCALE, Platform ] },
        { provide: AuthenticationService, useClass: MockAuthenticationService },
        { provide: CalendarService, useValue: { }},
        { provide: AvailabilityService, useClass: MockAvailabilityService},
        { provide: EmployeeService, useClass: MockEmployeeService},
        { provide: SchoolService, useValue: { }},
        { provide: ToastrService, useValue: { warning() {} }},
        { provide: BookingService, useValue: { }},
        { provide: HAMMER_LOADER, useValue: () => new Promise(() => {}) } // dummy to prevent warnings
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    const mockDate = new Date();
    mockSchool = { id: 3, contactName: "John Doe", contactEmail: "johndoe@email.com", name: "School Awesome", description: "My awesome school", password: "978wkkj62ASSAL!!@$" }
    mockEmployee = { id: 1, firstName: "Jane", lastName: "Doe", educationPackages: [], email: "janedoe@email.com", password: "$(*!Jqoe9872lkj!@#$#" }
    mockBooking = { id: 2, schoolId:  3, availabilityId: 4 }
    mockAvailability = { id: 4, employeeId: 1, bookingId: 2, date: mockDate }
    mockEvent =  {start: mockDate, title: "Hello!", school: mockSchool, employee: mockEmployee, availability: mockAvailability, booking: mockBooking}

    TestBed.get(EmployeeService).mappedEmployees = {};
    TestBed.get(BookingService).mappedBookings = {};
    TestBed.get(SchoolService).mappedSchools = {};
    TestBed.get(EmployeeService).mappedEmployees[mockEmployee.id] = mockEmployee;
    TestBed.get(BookingService).mappedBookings[mockBooking.id] = mockBooking;
    TestBed.get(SchoolService).mappedSchools[mockSchool.id] = mockSchool;
    TestBed.get(EmployeeService).employees = [mockEmployee];
    TestBed.get(BookingService).bookings = [mockBooking];
    TestBed.get(SchoolService).schools = [mockSchool];

    fixture = TestBed.createComponent(AvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should respond to an admin login', () => {
    // arrange
    const authService: AuthenticationService = TestBed.get(AuthenticationService);
    const adminUser: User = { email: "admin" , role: "ROLE_ADMIN" }

    // act
    authService.currentUserSubject.next(adminUser);

    // assert
    expect(component.isAdmin).toBeTruthy();
    expect(component.isAdminOrEmployee).toBeTruthy();
  })
  it('should respond to an employee login', () => {
    // arrange
    const authService: AuthenticationService = TestBed.get(AuthenticationService);
    const employeeUser: User = { email: "employee" , role: "ROLE_EMPLOYEE" }
    component.employees = [mockEmployee];

    // act
    authService.currentUserSubject.next(employeeUser);

    // assert
    expect(component.isAdmin).toBeFalsy();
    expect(component.isAdminOrEmployee).toBeTruthy();
  })
  it('should respond to a school login', () => {
    // arrange
    const authService: AuthenticationService = TestBed.get(AuthenticationService);
    const schoolUser: User = { email: "school" , role: "ROLE_SCHOOL" }

    // act
    authService.currentUserSubject.next(schoolUser);

    // assert
    expect(component.isAdmin).toBeFalsy();
    expect(component.isAdminOrEmployee).toBeFalsy();
  })
  it('should respond to an update in availabilities', () => {
    // arrange
    const availabilitiesService: AvailabilityService = TestBed.get(AvailabilityService);
    const spyOnLoadAvailabilities = spyOn(component, "load").and.callThrough();
    component.ignoreFilter = true;
    const mockAvailabilities = [mockAvailability];

    // act
    availabilitiesService.subjectAvailabilities.next(mockAvailabilities);

    // assert
    expect(spyOnLoadAvailabilities).toHaveBeenCalledWith(mockAvailabilities);

  })
  it('should respond to an update in employees', () => {
    // arrange
    const employeeService: EmployeeService = TestBed.get(EmployeeService);
    component.employees = [];

    // act
    employeeService.subjectEmployees.next([mockEmployee]);

    // assert
    expect(component.employees).toEqual([mockEmployee]);
  })
  it('should extract the right information when sorting', () => {
    // arrange

    // act
    const date = component.dataSource.sortingDataAccessor(mockAvailability, "date");
    const employee = component.dataSource.sortingDataAccessor(mockAvailability, "employee");
    const bookingSchoolName = component.dataSource.sortingDataAccessor(mockAvailability, "booking-school-name");
    const bookingSchoolContact = component.dataSource.sortingDataAccessor(mockAvailability, "booking-school-contact");
    const bookingSchoolContactEmail = component.dataSource.sortingDataAccessor(mockAvailability, "booking-school-contact-email");
    const defaultReturnsWhitespace = component.dataSource.sortingDataAccessor(mockAvailability, "Non-existing-header");

    // assert
    expect(new Date(date)).toEqual(new Date(mockAvailability.date));
    expect(employee).toEqual((mockEmployee.firstName  + " " + mockEmployee.lastName).toLowerCase());
    expect(bookingSchoolName).toEqual(mockSchool.name.toLowerCase());
    expect(bookingSchoolContact).toEqual(mockSchool.contactName.toLowerCase());
    expect(bookingSchoolContactEmail).toEqual(mockSchool.contactEmail.toLowerCase());
    expect(defaultReturnsWhitespace).toEqual("");
  })
  it('should filter the right records', () => {
    // arrange
    const expectMatchesToBeTrue = [];
    const expectMatchToBeFalse = [];

    // act
    expectMatchesToBeTrue.push(component.dataSource.filterPredicate(mockAvailability, mockEmployee.firstName));
    expectMatchesToBeTrue.push(component.dataSource.filterPredicate(mockAvailability, mockEmployee.lastName));
    expectMatchesToBeTrue.push(component.dataSource.filterPredicate(mockAvailability, mockEmployee.email));
    expectMatchesToBeTrue.push(component.dataSource.filterPredicate(mockAvailability, mockEmployee.firstName.toLowerCase().substr(0, 2)));
    expectMatchesToBeTrue.push(component.dataSource.filterPredicate(mockAvailability, mockSchool.contactEmail));
    expectMatchesToBeTrue.push(component.dataSource.filterPredicate(mockAvailability, mockSchool.contactName));
    expectMatchesToBeTrue.push(component.dataSource.filterPredicate(mockAvailability, mockSchool.name));

    expectMatchToBeFalse.push(component.dataSource.filterPredicate(mockAvailability, mockEmployee.password));
    expectMatchToBeFalse.push(component.dataSource.filterPredicate(mockAvailability, mockSchool.password));

    // assert
    expectMatchesToBeTrue.forEach(match => expect(match).toBeTruthy());
    expectMatchToBeFalse.forEach(match => expect(match).toBeFalsy());
  })
  it('Validate all selected', () => {
    // arrange
    component.dataSource = new MatTableDataSource<Availability>([mockAvailability]);
    component.selection.select(mockAvailability);

    // act
    const expectAllSelectedToBeTrue = component.isAllSelected();
    component.selection.deselect(mockAvailability);
    const expectAllSelectedToBeFalse = component.isAllSelected();

    // assert
    expect(expectAllSelectedToBeTrue).toBeTruthy();
    expect(expectAllSelectedToBeFalse).toBeFalsy();
  })
  it('Should filter out calendar days when in weekend', () => {
    // arrange
    const dateAdapter:OrdinaDateAdapter = TestBed.get(OrdinaDateAdapter);

    let saturday = new Date();
    while(dateAdapter.getDayOfWeek(saturday) != 6) { saturday = dateAdapter.addCalendarDays(saturday, 1)}
    let sunday = dateAdapter.addCalendarDays(saturday, 1);
    let monday = dateAdapter.addCalendarDays(sunday, 1);

    // act
    const displaySaturday = component.dateFilter(saturday);
    const displaySunday = component.dateFilter(sunday);
    const displayMonday = component.dateFilter(monday);

    // assert
    expect(displaySaturday).toBeFalsy();
    expect(displaySunday).toBeFalsy();
    expect(displayMonday).toBeTruthy();
  })
  it('should filter out the date when this is already booked for the employee', () => {
    // arrange
    const dateAdapter:OrdinaDateAdapter = TestBed.get(OrdinaDateAdapter);
    let workDay = new Date();
    while(dateAdapter.getDayOfWeek(workDay) < 1 || dateAdapter.getDayOfWeek(workDay) > 5) { workDay = dateAdapter.addCalendarDays(workDay, 1); }
    workDay = dateAdapter.addCalendarDays(workDay, 7); // add another week or it will be filtered out for being the current day / in the past

    mockAvailability.date = workDay;
    component.employee = mockEmployee;
    component.dataSource = new MatTableDataSource<Availability>([mockAvailability]);

    // act
    const displayDate = component.dateFilter(mockAvailability.date);

    // assert
    expect(displayDate).toBeFalsy()
  })
  it('should show future days and filter out historical or current day', () => {
    // arrange
    const dateAdapter:OrdinaDateAdapter = TestBed.get(OrdinaDateAdapter);
    let workDay = new Date();
    while(dateAdapter.getDayOfWeek(workDay) < 1 || dateAdapter.getDayOfWeek(workDay) > 5) { workDay = dateAdapter.addCalendarDays(workDay, 1); }

    const currentDay = new Date();
    const workDayInFuture = dateAdapter.addCalendarDays(workDay, 7);
    const workDayInPast = dateAdapter.addCalendarDays(workDay, -7);

    // act
    const displayCurrentDay = component.dateFilter(currentDay);
    const displayWorkDayInFuture = component.dateFilter(workDayInFuture);
    const displayWorkDayInPast = component.dateFilter(workDayInPast);

    // assert
    expect(displayCurrentDay).toBeFalsy();
    expect(displayWorkDayInFuture).toBeTruthy();
    expect(displayWorkDayInPast).toBeFalsy();
  })
  it('should filter the availabilities for the employee', () => {
    // arrange
    component.ignoreFilter = false;
    const authService:AuthenticationService = TestBed.get(AuthenticationService);
    authService.currentUserIsAdmin = false;
    authService.currentUserSubject.next({ email: mockEmployee.email, role: "ROLE_EMPLOYEE"})

    // act
    const filtered:Availability[] = component.filterToOwner([mockAvailability]);

    // assert
    expect(filtered).toEqual([mockAvailability]);
  })
  it('should filter the availabilities for the school', () => {
    // arrange
    component.ignoreFilter = false;
    const authService:AuthenticationService = TestBed.get(AuthenticationService);
    authService.currentUserIsAdmin = false;
    authService.currentUserSubject.next({ email: mockSchool.contactEmail, role: "ROLE_SCHOOL"})

    // act
    const filtered:Availability[] = component.filterToOwner([mockAvailability]);

    // assert
    expect(filtered).toEqual([mockAvailability]);
  })
  it('should apply the filter upon keystroke', () => {
    // arrange
    const spyOnApplyFilter = spyOn(component, "applyFilter").and.callThrough();
    const searchText = 'search text';

    // act
    fixture.debugElement.query(By.css("input[data-input-id=filter]")).triggerEventHandler('keyup', {target: { value: searchText }})

    // assert
    expect(spyOnApplyFilter).toHaveBeenCalledWith(searchText);
    expect(component.dataSource.filter).toEqual(searchText);
  })
  it('should apply sort padding for missing values', () => {
    // sort padding is used to make sure that the sorting is performed on data only. When a column has missing items in the list they should not be listed on top in any situation
    // arrange

    // act
    const sortValueDesc = component.padSortingValue("", "desc");
    const sortValueAsc = component.padSortingValue("", "asc");
    const sortValueWithValue = component.padSortingValue("Some value", "asc");

    // assert
    expect(sortValueAsc).toEqual("zzz");
    expect(sortValueDesc).toEqual("000");
    expect(sortValueWithValue).toEqual("Some value".toLocaleLowerCase());

  })
  it('should select and deselect all items using the master toggle', () => {
    // arrange
    component.dataSource = new MatTableDataSource<Availability>([mockAvailability]);

    // act
    component.masterToggle({ checked: true, source: undefined })
    const selectionAfterCheckedTrue = component.selection.selected;
    component.masterToggle({ checked: false, source: undefined })
    const selectionAfterCheckedFalse = component.selection.selected;


    // assert
    expect(selectionAfterCheckedTrue).toEqual([mockAvailability]);
    expect(selectionAfterCheckedFalse).toEqual([]);

  })
  it('should return the date for the availability', () => {
    // arrange

    // act
    const dateAsLocalString = component.getAvailabilityDate(mockAvailability);

    // assert
    expect(dateAsLocalString).toEqual(mockAvailability.date.toLocaleDateString());
  })
  it('should create a new availability slot', () => {
    // arrange
    const spyOnAvailabilityService = spyOn(<AvailabilityService>TestBed.get(AvailabilityService), "createAvailability").and.stub();
    const expectedAvailablity = {
      employeeId: mockEmployee.id,
      date: new Date()
    }
    component.employee = mockEmployee;

    // act
    // the datePicker dateSelected codebehind is triggered which receives the event with the date as value
    component.dateSelected({ value: expectedAvailablity.date} );

    // assert
    expect(spyOnAvailabilityService).toHaveBeenCalledWith(expectedAvailablity);

  })
  it('should show a warning to the admin user when opening the date picker without selecting an employee first', fakeAsync(() => {
    // arrange
    component.isAdmin = true;
    component.employee = undefined;
    const mockPicker = { close() { }, open() { }}
    const spyOnToastr = spyOn(TestBed.get(ToastrService), "warning");
    const spyOnPickerClose = spyOn(mockPicker, "close").and.stub();
    const spyOnPickerOpen = spyOn(mockPicker, "open").and.stub();

    // act
    // call the open picker with a dummy picker with just the open and close methods
    component.openPicker(mockPicker);

    // assert
    expect(spyOnToastr).toHaveBeenCalledTimes(1);
    expect(component.calendarDate).toBeUndefined();
    expect(component.selectEmployeeBadge).toEqual("!");
    tick(2600);
    expect(component.selectEmployeeBadge).toEqual("");
    expect(spyOnPickerClose).toHaveBeenCalledTimes(1);
    expect(spyOnPickerOpen).toHaveBeenCalledTimes(0);


  }));
  it('should open the picker when an employee is selected or the employee is logged in', () => {
    // arrange
    component.isAdmin = true;
    component.employee = mockEmployee;
    const mockPicker = { close() { }, open() { }}
    const spyOnPickerClose = spyOn(mockPicker, "close").and.stub();
    const spyOnPickerOpen = spyOn(mockPicker, "open").and.stub();

    // act
    // call the open picker with a dummy picker with just the open and close methods
    component.openPicker(mockPicker);

    // assert
    expect(spyOnPickerClose).toHaveBeenCalledTimes(0);
    expect(spyOnPickerOpen).toHaveBeenCalledTimes(1);
  })
  it('should remove the availability and clear the selection', () => {
    // arrange
    const availabilityService:AvailabilityService = TestBed.get(AvailabilityService);
    const spyOnAvailabilityService = spyOn(availabilityService, "removeAvailabilities").and.returnValue(Promise.resolve(true));
    component.selection.select(mockAvailability);

    // act
    component.remove();

    // assert
    expect(spyOnAvailabilityService).toHaveBeenCalledWith([mockAvailability])
    expect(component.selection.selected).toEqual([]);

  })

});
