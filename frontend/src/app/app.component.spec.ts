import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {AngularMaterialModule} from "./resources/angular-material.module";
import {DemobarComponent} from "./demobar/demobar.component";
import {AnimatedIconComponent} from "./animated-icon/animated-icon.component";
import {AuthenticationService} from "./services/authentication/authentication.service";
import {BehaviorSubject} from "rxjs";
import {User} from "./model/User";
import {Route, Router, Routes} from "@angular/router";
import {MatTabChangeEvent} from "@angular/material/tabs";
import {ChangeDetectorRef} from "@angular/core";

class MockAuthenticationService {
  mockUser:User = { email: "admin", role: "ROLE_ADMIN" };
  currentUserSubject:BehaviorSubject<User> = new BehaviorSubject<User>(this.mockUser);
  login(email, password) {  }
}
const mockRouteForAdmin:Route = {component: AppComponent, path: "adminRole", data: { index: 1, roles: ["ROLE_ADMIN"]}};
const mockSecondRouteForAdmin:Route = {component: AppComponent, path: "anotherAdminRole", data: { index: 2, roles: ["ROLE_ADMIN"]}};
const mockRouterForEmployeeAndSchool:Route = {component: AppComponent, path: "employeeSchoolRole", data: { index: 1, roles: ["ROLE_EMPLOYEE", "ROLE_SCHOOL"]}};


describe('AppComponent', () => {
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([ mockRouteForAdmin, mockSecondRouteForAdmin, mockRouterForEmployeeAndSchool ]),
        AngularMaterialModule
      ],
      declarations: [
        AppComponent, DemobarComponent, AnimatedIconComponent
      ],
      providers: [
        { provide: AuthenticationService, useClass: MockAuthenticationService},
        { provide: ChangeDetectorRef, useValue: { detectChanges: { } }}
      ]
    }).compileComponents()
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Ordina CodeKids'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Ordina CodeKids');
  });

  it('should filter the router for the correct roles', () => {
    // arrange
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const spyOnChangeDetection = spyOn(fixture.componentInstance, "detectChanges").and.stub();
    const authService:AuthenticationService = TestBed.get(AuthenticationService);

    app.ngOnInit();
    spyOnChangeDetection.calls.reset();

    // act
    authService.currentUserSubject.next({role: "ROLE_ADMIN", email: "email"});
    const routesForAdmin:Routes = app.routes;

    authService.currentUserSubject.next({role: "ROLE_EMPLOYEE", email: "email"});
    const routesForEmployee:Routes = app.routes;

    authService.currentUserSubject.next({role: "ROLE_SCHOOL", email: "email"});
    const routesForSchool:Routes = app.routes;

    // assert
    expect(routesForAdmin).toEqual([mockRouteForAdmin, mockSecondRouteForAdmin]);
    expect(routesForEmployee).toEqual([mockRouterForEmployeeAndSchool]);
    expect(routesForSchool).toEqual([mockRouterForEmployeeAndSchool]);
    expect(spyOnChangeDetection).toHaveBeenCalledTimes(3);
  })

  it('should capture the naviation of the router', async ()=> {
    // arrange
    const fixture = TestBed.createComponent(AppComponent);
    const app:AppComponent = fixture.debugElement.componentInstance;
    const router:Router = TestBed.get(Router);

    app.ngOnInit(); // subscribe to event emitter of router is done in the ngOnInit

    // act
    await router.navigate(["/anotherAdminRole"])

    // assert
    expect(app.selectedRouteIndex).toEqual(mockSecondRouteForAdmin.data.index);
  })

  it('should navigate to the correct route when tab is changed', async () => {
    // arrange
    const fixture = TestBed.createComponent(AppComponent);
    const app:AppComponent = fixture.debugElement.componentInstance;
    const router:Router = TestBed.get(Router);

    const matTabChangeEvent: MatTabChangeEvent = {
      index: mockSecondRouteForAdmin.data.index,
      tab: undefined
    }

    app.ngOnInit();

    // act
    const confirmed = await app.tabChanged(matTabChangeEvent);
    expect(confirmed).toBeTruthy();
    expect(router.url).toEqual("/" + mockSecondRouteForAdmin.path)

  })

});
