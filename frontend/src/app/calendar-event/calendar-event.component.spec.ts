import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { CalendarEventComponent } from './calendar-event.component';
import {AngularMaterialModule} from "../resources/angular-material.module";
import {CalendarService} from "../services/calendar/calendar.service";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {ToastrService} from "ngx-toastr";
import {SchoolService} from "../services/school/school.service";
import {BookingService} from "../services/booking/booking.service";
import {BehaviorSubject, Subject} from "rxjs";
import {User} from "../model/User";
import {OrdinaCalendarEvent} from "../model/OrdinaCalendarEvent";

class MockCalendarService {

}
class MockAuthenticationService {
  mockUser:User = { email: "admin", role: "ROLE_ADMIN" }
  currentUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(this.mockUser);
}
class MockSchoolService {
  subjectSchools: Subject<School> = new Subject<School>();
}
class MockBookingService {
  removeBooking() {}
  createBooking() {}
}
describe('CalendarEventComponent', () => {
  let component: CalendarEventComponent;
  let fixture: ComponentFixture<CalendarEventComponent>;
  let mockSchool: School;
  let mockEvent: OrdinaCalendarEvent;
  let mockEmployee: Employee;
  let mockAvailability: Availability;
  let mockBooking: Booking;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule],
      declarations: [ CalendarEventComponent ],
      providers: [
        {provide: AuthenticationService, useClass: MockAuthenticationService },
        {provide: CalendarService, useClass: MockCalendarService},
        {provide: BookingService, useClass: MockBookingService},
        {provide: SchoolService, useClass: MockSchoolService},
        {provide: ToastrService, useValue: { success() {}, error() {}}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const mockDate = new Date();
    mockSchool = { id: 3, contactName: "John Doe", contactEmail: "johndoe@email.com", name: "School Awesome", description: "My awesome school", password: "" }
    mockEmployee = { id: 1, firstName: "Jane", lastName: "Doe", educationPackages: [], email: "janedoe@email.com" }
    mockBooking = { id: 2, schoolId:  3, availabilityId: 4 }
    mockAvailability = { id: 4, employeeId: 1, bookingId: 2, date: mockDate }
    mockEvent =  {start: mockDate, title: "Hello!", school: mockSchool, employee: mockEmployee, availability: mockAvailability, booking: mockBooking}

    fixture = TestBed.createComponent(CalendarEventComponent);
    component = fixture.componentInstance;
    component.ordinaCalendarEvent =mockEvent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should respond to a school subject change', () => {
    // arrange
    const schoolService: SchoolService = TestBed.get(SchoolService);
    const schoolsAtStart: School[] = component.schools;
    const schoolsToEmit: School[] = [mockSchool];

    // act
    schoolService.subjectSchools.next(schoolsToEmit);

    // assert
    expect(schoolsAtStart).not.toEqual(schoolsToEmit);
    expect(component.schools).toEqual(schoolsToEmit);
  })

  it('should return TRUE for isOwner if the current user IS the owner of the event', () => {
    // arrange
    mockSchool.contactEmail = "myEmail";
    component.currentUser = { email: "myEmail" }

    // act
    const isOwner = component.isOwner();

    // assert
    expect(isOwner).toBeTruthy();
  })
  it('should return FALSE for isOwner if the current user IS NOT the owner of the event', () => {
    // arrange
    mockSchool.contactEmail = "myEmail";
    component.currentUser = { email: "anotherEmail" }

    // act
    const isOwner = component.isOwner();

    // assert
    expect(isOwner).toBeFalsy();
  })
  it('should return TRUE for canCancel if the current user IS THE ADMIN', () => {
    // arrange
    component.isAdmin = true;

    // act
    const canCancel = component.canCancel();

    // assert
    expect(canCancel).toBeTruthy();
  })
  it('should return TRUE for canCancel if the current user IS THE OWNER', () => {
    // arrange
    component.isAdmin = false;
    component.isOwner = () => { return true; }

    // act
    const canCancel = component.canCancel();

    // assert
    expect(canCancel).toBeTruthy();
  })
  it('should return FALSE for canCancel if the current user IS NOT THE ADMIN AND NOT THE OWNER', () => {
    // arrange
    component.isAdmin = false;
    component.isOwner = () => { return false; }

    // act
    const canCancel = component.canCancel();

    // assert
    expect(canCancel).toBeFalsy();
  })
  it('should return TRUE for canBook if the current user IS A SCHOOL AND the event IS NOT BOOKED', () => {
    // arrange
    component.isAdmin = false;
    component.currentUser = { email: "school", role: "ROLE_SCHOOL" }
    component.hasOwner = () => { return false; }

    // act
    const canBook = component.canBook();

    // assert
    expect(canBook).toBeTruthy();
  })
  it('should return TRUE for canBook if the current user IS AN ADMIN AND the event IS NOT BOOKED', () => {
    // arrange
    component.isAdmin = true;
    component.hasOwner = () => { return false; }

    // act
    const canBook = component.canBook();

    // assert
    expect(canBook).toBeTruthy();
  })
  it('should close the window and emit the event', () => {
    // arrange
    const spyOnEmit = spyOn(component.closeClicked, "emit");

    // act
    component.close();

    // assert
    expect(spyOnEmit).toHaveBeenCalled();

  })
  it('should cancel the booking', () => {
    // arrange
    const bookingService: BookingService = TestBed.get(BookingService);
    const spyOnBookingService = spyOn(bookingService, "removeBooking");

    // act
    component.cancel();

    // assert
    expect(spyOnBookingService).toHaveBeenCalledWith(component.ordinaCalendarEvent.booking);

  })
  it('should book the slot for a school user', () => {
    // arrange
    const bookingService: BookingService = TestBed.get(BookingService);
    const spyOnBookingService = spyOn(bookingService, "createBooking");
    component.isAdmin = false;

    // act
    component.book();

    // assert
    expect(spyOnBookingService).toHaveBeenCalledWith(component.ordinaCalendarEvent.availability);
  })
  it('should book the slot for an admin user with a school selected', () => {
    // arrange
    const bookingService: BookingService = TestBed.get(BookingService);
    const spyOnBookingService = spyOn(bookingService, "createBooking");
    component.isAdmin = true;
    component.targetSchool = mockSchool;

    // act
    component.book();

    // assert
    expect(spyOnBookingService).toHaveBeenCalledWith(component.ordinaCalendarEvent.availability);
  })
  it('should show a warning when the admin tries to book a slot without selecting a school first', fakeAsync(() => {
    // arrange
    const bookingService: BookingService = TestBed.get(BookingService);
    const spyOnBookingService = spyOn(bookingService, "createBooking");
    component.isAdmin = true;
    component.targetSchool = undefined;

    // act
    component.book();

    // assert
    expect(component.selectSchoolBadge).toEqual("!");
    tick(2600);
    expect(component.selectSchoolBadge).toEqual("");
    expect(spyOnBookingService).not.toHaveBeenCalled();
  }));
});
