import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OrdinaCalendarEvent} from "../model/OrdinaCalendarEvent";
import {CalendarService} from "../services/calendar/calendar.service";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {User} from "../model/User";
import {ToastrService} from "ngx-toastr";
import {SchoolService} from "../services/school/school.service";
import {BookingService} from "../services/booking/booking.service";

@Component({
  selector: 'app-calendar-event',
  templateUrl: './calendar-event.component.html',
  styleUrls: ['./calendar-event.component.css']
})
export class CalendarEventComponent implements OnInit {

  constructor(private calendarService:CalendarService,
              private authenticationService:AuthenticationService,
              private toastr:ToastrService,
              private schoolService: SchoolService,
              private bookingService: BookingService

  ) { }

  @Input()
  ordinaCalendarEvent:OrdinaCalendarEvent;

  @Output()
  closeClicked: EventEmitter<any> = new EventEmitter<any>();

  targetSchool: School;
  schools: School[];
  isAdmin:boolean = false;
  currentUser: User;
  selectSchoolBadge: any;

  ngOnInit() {
    this.schoolService.subjectSchools.subscribe(value => {
      this.schools = value;
    });
    this.schools = this.schoolService.schools;

    this.authenticationService.currentUserSubject.subscribe(value => {
      this.currentUser = value;
      this.isAdmin = value && value.role === "ROLE_ADMIN";
    });
  }

  book() {
    if(this.isAdmin) {
      if(!this.targetSchool) {
        this.toastr.error("As administrator, you must select a school to link to this booking", "", {positionClass:"toast-top-center"});
        this.selectSchoolBadge = "!";
        setTimeout(() => { this.selectSchoolBadge = ""}, 2500);
        return;
      }
      this.currentUser.school = this.targetSchool;
    }
    this.bookingService.createBooking(this.ordinaCalendarEvent.availability);
  }
  cancel() {
    this.bookingService.removeBooking(this.ordinaCalendarEvent.booking);
  }

  close() {
    this.closeClicked.emit();
  }
  canBook() {
    return !this.hasOwner() && (this.isAdmin || (this.currentUser && this.currentUser.role === "ROLE_SCHOOL"));
  }

  canCancel() {
    return this.hasOwner() && (this.isAdmin || this.isOwner())
  }
  isOwner() {
    return  this.currentUser &&
            this.hasOwner() &&
            this.currentUser.email === this.ordinaCalendarEvent.school.contactEmail
  }
  hasOwner() {
    return !!this.ordinaCalendarEvent.school;
  }
}
