import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NavigationEnd, NavigationExtras, Route, Router, Routes} from "@angular/router";
import {MatTabChangeEvent} from "@angular/material/tabs";
import {AuthenticationService} from "./services/authentication/authentication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit(): void {
    this.router.events.subscribe(value => {
      value instanceof NavigationEnd ? this.setTab(value) : ''
    });
    this.authenticationService.currentUserSubject.subscribe(user => {
      const role = user ? user.role : "ROLE_NONE";
      this.routes = this.router.config.filter(value => value.data && value.data.roles.indexOf(role) > -1).sort((a, b) => a.data.index > b.data.index ? 1 : -1);
      this.setTabForUrl(this.router.url);
      this.detectChanges();
    })

    this.authenticationService.login("admin", "admin" );
  }

  detectChanges() {
    this._changeRef.detectChanges(); // call a change detection for the tabs
  }

  routes:Routes = [];
  selectedRouteIndex = 0;
  showToolbar: boolean = true;
  title: string = "Ordina CodeKids";
  constructor(private router: Router, private authenticationService: AuthenticationService, private _changeRef: ChangeDetectorRef) {
     }

  tabChanged($event: MatTabChangeEvent) {
    const selectedRoute = this.routes.find(value => value.data.index === $event.index);
    const navigationExtras: NavigationExtras = { queryParams: selectedRoute.data.queryParams }
    return this.router.navigate([selectedRoute.path], navigationExtras);
  }

  // make sure that the initial route is reflected in the selected tab
  setTab(value: NavigationEnd): void {
    this.setTabForUrl(value.urlAfterRedirects);
  }

  private setTabForUrl(url:string) {
    url = url.startsWith("/") ? url.substr(1) : url;
    const selectedRoute = this.routes.find(route => route.path === url);
    if(selectedRoute) {
      this.selectedRouteIndex = selectedRoute.data.index;
    }
  }
}
