import { Injectable } from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthenticationService} from "../services/authentication/authentication.service";
import {User} from "../model/User";
import {catchError} from "rxjs/operators";
import {ToastrService} from "ngx-toastr";


@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private toastr: ToastrService) { }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with basic auth credentials if available
        const user:User = this.authenticationService.currentLoginUser;

        if(user) {
          request = request.clone({
            setHeaders: {
              Authorization: user.authData
            }
          });
        }


        return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
          this.toastr.error(error.error.message);
          return throwError(error);
        }));
    }
}
