import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-animated-icon',
  templateUrl: './animated-icon.component.html',
  styleUrls: ['./animated-icon.component.css']
})
export class AnimatedIconComponent implements OnInit {
  animatedPosition: boolean = false;

  @Input()
  defaultIcon: string;

  @Input()
  animatedIcon: string;

  constructor() { }

  ngOnInit() {
  }

  buttonClick() {
    this.animatedPosition = !this.animatedPosition;
  }

}
