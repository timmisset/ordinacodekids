import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimatedIconComponent } from './animated-icon.component';
import {AngularMaterialModule} from "../resources/angular-material.module";
import {By} from "@angular/platform-browser";

describe('AnimatedIconComponent', () => {
  let component: AnimatedIconComponent;
  let fixture: ComponentFixture<AnimatedIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule],
      declarations: [ AnimatedIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimatedIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should switch state', () => {
    // arrange
    const button = fixture.debugElement.query(By.css('button'));
    let icon = fixture.debugElement.query(By.css('mat-icon'));
    const isDefaultIconBeforeClick = icon.classes.defaultIcon;


    // act
    button.triggerEventHandler('click', undefined);
    fixture.detectChanges();
    const isDefaultIconAfterClick = icon.classes.defaultIcon;

    // assert
    expect(isDefaultIconAfterClick).toEqual(!isDefaultIconBeforeClick);
  })
});
