export interface EducationPackage {
  id: number,
  name: string,
  description: string
}
