interface Employee {
  id?:number;
  firstName:string;
  lastName:string;
  email:string;
  educationPackages:string[];
  password?:string;
}
