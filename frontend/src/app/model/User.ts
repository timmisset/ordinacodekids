export interface User {
  id?: number,
  email: string,
  password?: string
  role?: string,
  authData?: string,
  school?:School,
  employee?:Employee
}
