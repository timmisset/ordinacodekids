export interface DialogConfirm {
  title?: string;
  description?: string;
  showCancelButton?: boolean;
  cancelButtonText?: string;
  onOkButton?():any;
}
