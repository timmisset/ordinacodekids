interface Availability {
  id?:number;
  date:Date;
  employeeId:number;
  bookingId?:number;
}
