import {CalendarEvent} from "calendar-utils";

export interface OrdinaCalendarEvent extends CalendarEvent {
  availability: Availability;
  school: School;
  booking: Booking;
  employee: Employee;
}
