interface School {
  id?:number;
  name:string;
  description:string;
  contactName:string;
  contactEmail:string;
  password?:string;
}
