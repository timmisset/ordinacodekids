import {Component, OnInit} from '@angular/core';
import {CalendarEvent, CalendarView} from 'angular-calendar';
import {MonthViewDay} from 'calendar-utils'
import {CalendarService} from "../services/calendar/calendar.service";
import {OrdinaCalendarEvent} from "../model/OrdinaCalendarEvent";
import {OrdinaDateAdapter} from "../util/ordina-date-adapter";



@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  constructor(private calendarService:CalendarService, private dateAdapter:OrdinaDateAdapter) { }

  view: CalendarView = CalendarView.Month;
  calendarHoursInDay: number = 8;

  viewDate: Date = new Date();
  excludeDays: number[] = [0, 6];


  CalendarView = CalendarView;

  events: CalendarEvent[] = this.calendarService.calendarEvents;
  possibleToBook:boolean;
  selectedEvent:OrdinaCalendarEvent;
  title: string;



  offset(stepSize = 1) {
    this.viewDate = this.view == CalendarView.Month ?
      this.dateAdapter.addCalendarMonths(this.viewDate, stepSize) :
      this.dateAdapter.addCalendarDays(this.viewDate, stepSize * 7)
    this.updateTitle();
  }

  showEvent() {
    return this.view == CalendarView.Week && this.selectedEvent
  }

  ngOnInit() {
    this.calendarService.subjectCalendarEvents.subscribe(value => {
      this.events = value
    });
    this.calendarService.subjectCalendarHoursInDay.subscribe(value=> {
      this.calendarHoursInDay = value});
    this.updateTitle();
  }

  dayClicked(day: MonthViewDay<any>) {
    if(day.isFuture) {
      // jump to the week view for the specified day
      this.viewDate = day.date;
      this.view = CalendarView.Week;
      this.updateTitle();
    }
  }

  eventClick($event: { event: CalendarEvent; sourceEvent: MouseEvent | KeyboardEvent }) {
    const event = $event.event as OrdinaCalendarEvent;
    this.possibleToBook = !event.booking;
    this.selectedEvent = event;
  }


  current() {
    this.viewDate = new Date()
  }

  toggleView() {
    this.view = this.view === CalendarView.Month ? CalendarView.Week :CalendarView.Month
    this.updateTitle();

  }

  updateTitle() {
    this.title = this.view === CalendarView.Month ? this.getMonthTitle() : this.getWeekTitle();
  }
  getMonthTitle():string {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return this.viewDate.getFullYear() + " - " + months[this.viewDate.getMonth()];
  }
  getWeekTitle():string {
    return "Week# " + this.dateAdapter.getWeekOfYear(this.viewDate);
  }
}
