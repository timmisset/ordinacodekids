import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendarComponent} from './calendar.component';
import {CalendarService} from "../services/calendar/calendar.service";
import {OrdinaDateAdapter} from "../util/ordina-date-adapter";
import {AngularMaterialModule} from "../resources/angular-material.module";
import {OrdinaCalendarEvent} from "../model/OrdinaCalendarEvent";
import {Subject} from "rxjs";
import {
  CalendarModule,
  CalendarMonthModule,
  CalendarView,
  CalendarWeekModule,
  DateAdapter as CalendarDateAdapter,
} from "angular-calendar";
import {CalendarEventComponent} from "../calendar-event/calendar-event.component";
import {DateAdapter, MAT_DATE_LOCALE} from "@angular/material/core";
import {Platform} from "@angular/cdk/platform";
import {adapterFactory} from "angular-calendar/date-adapters/date-fns";
import {By} from "@angular/platform-browser";

class MockCalendarService {
  subjectCalendarEvents: Subject<OrdinaCalendarEvent[]> = new Subject<OrdinaCalendarEvent[]>()
  subjectCalendarHoursInDay: Subject<number> = new Subject<number>();
}

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule, CalendarModule.forRoot({
        provide: CalendarDateAdapter,
        useFactory: adapterFactory
      }), CalendarWeekModule, CalendarMonthModule ],
      declarations: [ CalendarComponent, CalendarEventComponent ],
      providers: [
        { provide: CalendarService, useClass: MockCalendarService },
        { provide: OrdinaDateAdapter, useClass: OrdinaDateAdapter, deps: [ MAT_DATE_LOCALE, Platform ] },
        DateAdapter
      ]
    })
      // .overrideComponent(CalendarComponent, { set: { changeDetection: ChangeDetectionStrategy.Default }})
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the title based on the viewMode', () => {
    // arrange
    const weekTitle = component.getWeekTitle();
    const monthTitle = component.getMonthTitle();

    // act
    component.view = CalendarView.Week;
    component.updateTitle();
    fixture.detectChanges();
    const returnedTitleForWeek = fixture.debugElement.query(By.css("h3")).nativeElement.textContent.trim();

    component.view = CalendarView.Month;
    component.updateTitle();
    fixture.detectChanges();
    const returnedTitleForMonth = fixture.debugElement.query(By.css("h3")).nativeElement.textContent.trim();

    // assert
    expect(returnedTitleForWeek).toEqual(weekTitle);
    expect(returnedTitleForMonth).toEqual(monthTitle);

  })
  it('should navigate (offset) months when clicked', () => {
    // arrange
    const currentDate = new Date();
    component.viewDate = currentDate;
    component.view = CalendarView.Month;

    let expectedMonth = currentDate.getMonth() + 1;
    expectedMonth = expectedMonth > 11 ? 0 : expectedMonth;

    // act
    component.offset(1);

    // assert

    expect(component.viewDate.getMonth()).toEqual(expectedMonth);

  })
  it('should navigate (offset) weeks when clicked', () => {
    // arrange
    const currentDate = new Date();
    currentDate.setHours(0);
    currentDate.setMinutes(0);
    currentDate.setSeconds(0);
    currentDate.setMilliseconds(0);
    component.viewDate = currentDate;
    component.view = CalendarView.Week;

    let expectedDifference = 7 * 24 * 60 * 60 * 1000; // 7 days expressed in milliseconds

    // act
    component.offset(); // <-- test default true branch

    // assert
    expect(component.viewDate.getTime() - currentDate.getTime()).toEqual(expectedDifference);

  })
  it('should navigate back to the current date', () => {
    // arrange
    const offsetDate = new Date(2020, 1, new Date().getDay() === 0 ? 1 : 0);
    const currentDate = new Date();
    component.viewDate = offsetDate;

    // act
    component.current();

    // assert
    expect(offsetDate).not.toEqual(currentDate);
    expect(component.viewDate).toEqual(currentDate);
  })
  it('should respond to subscription changes', () => {
    // arrange
    const calendarService: CalendarService = TestBed.get(CalendarService);
    const events: OrdinaCalendarEvent[] = [];
    const calendarHoursInDay: number = 10;

    // act
    calendarService.subjectCalendarEvents.next(events);
    calendarService.subjectCalendarHoursInDay.next(calendarHoursInDay);

    // assert
    expect(component.events).toBe(events);
    expect(component.calendarHoursInDay).toEqual(calendarHoursInDay);
  })
  it('should toggle view modes and update the title', () => {
    // arrange
    const currentViewMode = component.view;
    const spyOnUpdateTitle = spyOn(component, "updateTitle");

    // act
    component.toggleView();
    component.toggleView();
    component.toggleView();

    // assert
    expect(currentViewMode).not.toEqual(component.view);
    expect(spyOnUpdateTitle).toHaveBeenCalledTimes(3);

  })
  it('should switch to week-view mode when a day is clicked', () => {
    // arrange
    component.view = CalendarView.Month;
    component.viewDate = new Date();
    const dayToView = new Date();
    dayToView.setDate(dayToView.getDate() + 10);

    // act
    component.dayClicked({isFuture: true, inMonth: true, backgroundColor: undefined, badgeTotal: undefined, cssClass: undefined, events: undefined, date: dayToView, day: undefined, isPast: false, isToday:false, isWeekend: undefined})

    // assert
    expect(component.viewDate).toEqual(dayToView);
    expect(component.view).toEqual(CalendarView.Week);
  })
  it('should ignore dates that are not in the future when they are clicked', () => {
    // arrange
    component.view = CalendarView.Month;
    const currentDate = new Date();
    component.viewDate = currentDate;
    const dayToView = new Date();
    dayToView.setDate(dayToView.getDate() + 10);

    // act
    component.dayClicked({isFuture: false, inMonth: true, backgroundColor: undefined, badgeTotal: undefined, cssClass: undefined, events: undefined, date: dayToView, day: undefined, isPast: false, isToday:false, isWeekend: undefined})

    // assert
    expect(component.viewDate).toEqual(currentDate);
    expect(component.view).toEqual(CalendarView.Month);
  })
  it('should set the current event when clicked', () => {
    // arrange
    const mockEvent: OrdinaCalendarEvent = { availability: undefined, booking: undefined, employee: undefined, title: undefined, school: undefined, start: undefined }

    // act
    component.eventClick({ event: mockEvent, sourceEvent: undefined });

    // assert
    expect(component.possibleToBook).toBeTruthy();
    expect(component.selectedEvent).toBe(mockEvent);
  })
  it('should set the current event when clicked and block the booking option when already booked', () => {
    // arrange
    const mockEvent: OrdinaCalendarEvent = { availability: undefined, booking: { availabilityId: undefined, schoolId: undefined}, employee: undefined, title: undefined, school: undefined, start: undefined }

    // act
    component.eventClick({ event: mockEvent, sourceEvent: undefined });

    // assert
    expect(component.possibleToBook).toBeFalsy();
    expect(component.selectedEvent).toBe(mockEvent);
  })
});
