import {NativeDateAdapter} from "@angular/material/core";

export class OrdinaDateAdapter extends NativeDateAdapter {
  getWeekOfYear(date:Date) {
    // get the first day of the year:
    const dayInYear: number = this.getDayInYear(date);
    const dayOfWeek: number = this.getDayOfWeek(date);
    const dayOfWeekStartOfYear: number = this.getDayOfWeek(new Date(date.getFullYear(), 0, 1))
    const weekNum: number = (dayInYear + 6) / 7 + (dayOfWeekStartOfYear > dayOfWeek ? 1 : 0);
    return Math.max(Math.floor(weekNum), 1);
  }
  getDayInYear(date:Date) {
    const ref = new Date(date.getFullYear(), 0, 1);
    const diff = date.getTime() - ref.getTime();
    return Math.floor(diff / 24 / 60 / 60 / 1000); // return the day in year
  }
}
