import { TestBed, async } from '@angular/core/testing';
import {OrdinaDateAdapter} from "./ordina-date-adapter";
import {Platform, PlatformModule} from "@angular/cdk/platform";
import {MAT_DATE_LOCALE} from "@angular/material/core";

describe('Ordina DateAdapter', () => {
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [ PlatformModule ],
      declarations: [ ],
      providers: [
        { provide: OrdinaDateAdapter, useClass: OrdinaDateAdapter, deps: [ MAT_DATE_LOCALE, Platform ] }
      ]
    }).compileComponents()
  }));

  it('should return the day in year', () => {
    const dates = [
      { date: new Date(2020, 0, 1), expectedDayInYear: 0},
      { date: new Date(2020, 1, 1), expectedDayInYear: 31}, // 31 days in Jan
      { date: new Date(2020, 2, 1), expectedDayInYear: 60},  // 31 days in Jan + 29 days in Feb
      { date: new Date(2019, 11, 31), expectedDayInYear: 364}, // normal year
      { date: new Date(2020, 11, 31), expectedDayInYear: 365} // leap year
      ]

    const ordinaDateAdataper: OrdinaDateAdapter = TestBed.get(OrdinaDateAdapter);

    dates.forEach(date => {
      const dayInYear = ordinaDateAdataper.getDayInYear(date.date);
      expect(dayInYear).toEqual(date.expectedDayInYear);
    })
  })

  it('should return the week of year', () => {
    const dates = [
      { date: new Date(2020, 0, 1), expectedWeekInYear: 1},
      { date: new Date(2020, 1, 1), expectedWeekInYear: 5}, // 31 days in Jan
      { date: new Date(2020, 2, 1), expectedWeekInYear: 10},  // 31 days in Jan + 29 days in Feb
      { date: new Date(2019, 11, 31), expectedWeekInYear: 52}, // normal year
      { date: new Date(2020, 11, 31), expectedWeekInYear: 53} // leap year
    ]

    const ordinaDateAdataper: OrdinaDateAdapter = TestBed.get(OrdinaDateAdapter);

    dates.forEach(date => {
      const weekInYear = ordinaDateAdataper.getWeekOfYear(date.date);
      expect(weekInYear).toEqual(date.expectedWeekInYear);
    })
  })
})
