import {NgModule} from "@angular/core";
import {MatButtonModule,
  MatDividerModule,  MatCardModule,  MatListModule,  MatToolbarModule,  MatIconModule,  MatFormFieldModule,  MatSelectModule,  MatBadgeModule,
  MatDialogModule,  MatTableModule,  MatCheckboxModule,  MatSortModule,  MatPaginatorModule,  MatDatepickerModule, MatNativeDateModule,
  MatInputModule, MatTabsModule, MatAutocompleteModule, MatGridListModule, MatButtonToggleModule
} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({

  imports: [
    MatButtonModule, MatDividerModule, MatCardModule, MatListModule, MatToolbarModule, MatIconModule, MatFormFieldModule,
    MatSelectModule, BrowserAnimationsModule, MatDividerModule, MatBadgeModule, MatDialogModule, MatCheckboxModule, MatTableModule,
    MatSortModule, MatPaginatorModule, MatDatepickerModule, MatNativeDateModule, MatInputModule, MatTabsModule,
    MatAutocompleteModule, MatGridListModule, MatButtonToggleModule, ReactiveFormsModule
  ],
  exports: [
    MatButtonModule, MatDividerModule, MatCardModule, MatListModule, MatToolbarModule, MatIconModule, MatFormFieldModule,
    MatSelectModule, BrowserAnimationsModule, MatDividerModule, MatBadgeModule, MatDialogModule, MatCheckboxModule, MatTableModule,
    MatSortModule, MatPaginatorModule, MatDatepickerModule, MatNativeDateModule, MatInputModule, MatTabsModule,
    MatAutocompleteModule, MatGridListModule, MatButtonToggleModule, ReactiveFormsModule
  ]

})
export class AngularMaterialModule { }
