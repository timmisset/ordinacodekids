import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {SchoolComponent, SchoolFormData} from './school.component';
import {AngularMaterialModule} from "../resources/angular-material.module";
import {SchoolService} from "../services/school/school.service";
import {EducationPackageService} from "../services/education-package/education-package.service";
import {of, Subject} from "rxjs";
import {Test} from "tslint";

class MockSchoolService {
  subjectSchools: Subject<School> = new Subject<School>()
  deleteSchool() {}
  saveSchool() {}
}
class MockEducationPackageService {

}

describe('SchoolComponent', () => {
  let component: SchoolComponent;
  let fixture: ComponentFixture<SchoolComponent>;
  let mockSchool: School;
  let mockSchool2: School;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule],
      declarations: [ SchoolComponent ],
      providers: [
        {provide: SchoolService, useClass:MockSchoolService },
        {provide: EducationPackageService, useClass:MockSchoolService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockSchool = { contactName: "John Doe", contactEmail: "johndoe@email.com", name: "School Awesome", description: "My awesome school", password: "", id: 0 }
    mockSchool2 = { contactName: "John Doe", contactEmail: "johndoe@email.com", name: "School Not So Awesome", description: "My awesome school", password: "", id: 0 }
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the school form data when the selected value changes', () => {
    // arrange
    // act
    component.schoolForm.get("select_school").setValue(mockSchool);
    fixture.detectChanges();

    // assert
    expect(component.schoolForm.get("contactName").value).toEqual(mockSchool.contactName);
    expect(component.schoolForm.get("contactEmail").value).toEqual(mockSchool.contactEmail);
    expect(component.schoolForm.get("name").value).toEqual(mockSchool.name);
    expect(component.schoolForm.get("description").value).toEqual(mockSchool.description);
    //
    expect( component.schoolForm.get("contactEmail").disabled).toBeTruthy();
  })
  it('should filter the schools based on the text input', () => {

    component.schools = [mockSchool, mockSchool2];
    component.filteredOptions = [mockSchool, mockSchool2];

    // act
    component.schoolForm.get("select_school").setValue("Not So");
    fixture.detectChanges();

    // assert
    expect(component.filteredOptions).toEqual([mockSchool2])

  });
  it('should set the schools after an update in the subjectSchool', () => {
    // arrange
    const schoolService:SchoolService = TestBed.get(SchoolService);
    component.schools = [];
    component.filteredOptions = [];

    // act
    schoolService.subjectSchools.next([mockSchool, mockSchool2]);

    // assert
    expect(component.filteredOptions).toEqual([mockSchool, mockSchool2])
    expect(component.schools).toEqual([mockSchool, mockSchool2])

  })
  it('filter should be safe against empty input', () => {
    // arrange
    component.schools = [mockSchool, mockSchool2];
    component.filteredOptions = [mockSchool, mockSchool2];

    // act
    component.filterSchools("");

    // assert
    expect(component.schools).toEqual( [mockSchool, mockSchool2])
    expect(component.filteredOptions).toEqual( [])
  })
  it('should reset the form', () => {
    // arrange
    component.schoolData = <SchoolFormData> Object.assign({select_school: mockSchool}, mockSchool);

    // act
    component.new();

    // assert
    expect(component.schoolForm.get("contactEmail").enabled).toBeTruthy();
    for(let prop in component.schoolData) {
      expect(component.schoolData[prop]).toBeNull();
    }
  })
  it('should reset the form after confirmation for removal of the school is true', async () => {
    // arrange
    component.school = mockSchool;
    const schoolService:SchoolService = TestBed.get(SchoolService);
    const spyOnSchoolService = spyOn(schoolService, "deleteSchool").and.returnValue(Promise.resolve(true));
    const spyOnSchoolForm = spyOn(component.schoolForm, "reset");

    // act
    const confirmed = await component.remove();

    // assert
    expect(confirmed).toBeTruthy();
    expect(spyOnSchoolService).toHaveBeenCalledTimes(1);
    expect(spyOnSchoolForm).toHaveBeenCalledTimes(1);

  })

  it('should NOT reset the form after confirmation for removal of the school is false', async () => {
    // arrange
    component.school = mockSchool;
    const schoolService:SchoolService = TestBed.get(SchoolService);
    const spyOnSchoolService = spyOn(schoolService, "deleteSchool").and.returnValue(Promise.resolve(false));
    const spyOnSchoolForm = spyOn(component.schoolForm, "reset");

    // act
    const confirmed = await component.remove();

    // assert
    expect(confirmed).toBeFalsy();
    expect(spyOnSchoolService).toHaveBeenCalledTimes(1);
    expect(spyOnSchoolForm).toHaveBeenCalledTimes(0);
  })

  it('should NOT call deleteSchool when no school is selected', async () => {
    // arrange
    component.school = undefined;
    const schoolService:SchoolService = TestBed.get(SchoolService);
    const spyOnSchoolService = spyOn(schoolService, "deleteSchool").and.returnValue(Promise.resolve(false));
    const spyOnSchoolForm =  spyOn(component.schoolForm, "reset");

    // act
    const confirmed = await component.remove();

    // assert
    expect(confirmed).toBeFalsy();
    expect(spyOnSchoolService).toHaveBeenCalledTimes(0);
    expect(spyOnSchoolForm).toHaveBeenCalledTimes(0);
  })

  it('should call saveSchool when the data is valid', () => {
    // arrange
    component.schoolForm.get("select_school").setValue(mockSchool);
    const schoolService:SchoolService = TestBed.get(SchoolService);
    const spyOnSchoolService = spyOn(schoolService, "saveSchool");
    expect(component.school).toEqual(mockSchool);

    // act
    component.save();

    // assert
    expect(spyOnSchoolService).toHaveBeenCalledTimes(1);
    expect(spyOnSchoolService).toHaveBeenCalledWith(mockSchool);
  })

  it('should NOT call saveSchool when the data is invalid', () => {
    // arrange
    const tooShortPassword = "hi";
    component.schoolForm.get("select_school").setValue(mockSchool);
    component.schoolForm.get("password").setValue(tooShortPassword);
    const schoolService:SchoolService = TestBed.get(SchoolService);
    const spyOnSchoolService = spyOn(schoolService, "saveSchool");

    // act
    component.save();

    // assert
    expect(component.schoolForm.valid).toBeFalsy();
    expect(spyOnSchoolService).toHaveBeenCalledTimes(0);

  })

});
