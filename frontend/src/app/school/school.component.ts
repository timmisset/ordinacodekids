import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SchoolService} from "../services/school/school.service";
import {EducationPackageService} from "../services/education-package/education-package.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogConfirmComponent} from "../dialog-confirm/dialog-confirm.component";

export interface SchoolFormData extends School {
  select_school: School,
  password: string
}

@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styleUrls: ['./school.component.css']
})
export class SchoolComponent implements OnInit {
  filteredOptions: School[] = [];
  schools: School[];

  schoolForm: FormGroup = new FormGroup({
    select_school: new FormControl(),
    name: new FormControl(undefined, Validators.required),
    description: new FormControl(undefined),
    contactEmail: new FormControl(undefined, [Validators.required, Validators.email]),
    contactName: new FormControl(undefined, Validators.required),
    password: new FormControl(undefined, Validators.minLength(6)),
    id: new FormControl()
  });
  school: School;
  schoolData: SchoolFormData;

  displaySchool(school:School): string {
    return school ? school.name : ''
  };

  constructor(private schoolService: SchoolService,
              private educationPackageService: EducationPackageService,


  ) { }

  ngOnInit() {
    this.schoolForm.get("select_school").valueChanges.subscribe(value => {
      // value changed, either by typing text or selection of one of the schools from the list
      // when typing, filter the list
      // when school, select the school as the active school
      if(value && value !== this.school) {
        if(typeof value === 'object') {
          this.school = value;
          this.schoolData = Object.assign({ select_school: value, password: '' }, value);
          this.schoolForm.setValue(this.schoolData);

          this.schoolForm.get("password").clearValidators();
          this.schoolForm.get("password").setValidators(Validators.minLength(6))
          this.schoolForm.get("contactEmail").disable();
          this.schoolForm.markAllAsTouched();
        }
        else{ this.filterSchools(value); }
      }

    })
    this.schoolService.subjectSchools.subscribe(value => {this.schools = value; this.filteredOptions = this.schools;});
    this.schools = this.schoolService.schools;

  }

  filterSchools(filter:string) {
    filter = filter.toLocaleLowerCase();
    this.filteredOptions = this.schools.filter(school => {
      if(!school || !filter) { return false; }

      return school.name.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(school.name) > -1 ||
        school.contactEmail.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(school.contactEmail) > -1 ||
        school.contactName.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(school.contactName) > -1
    })
  }

  new() {
    this.schoolForm.reset();

    this.schoolForm.get("password").clearValidators();
    this.schoolForm.get("password").setValidators([Validators.required, Validators.minLength(6)]);

    this.schoolForm.get("contactEmail").enable();
    this.schoolData = this.schoolForm.getRawValue();
  }

  async remove() {
    if(this.school) {
      const confirmed = await this.schoolService.deleteSchool(this.school);
      if(confirmed) {
        this.schoolForm.reset();
      }
      return confirmed;
    }
    return false;
  }

  save() {
    if(this.schoolForm.valid) {
      this.schoolData = this.schoolForm.getRawValue();
      const school = Object.assign(this.schoolData, {});
      delete school.select_school;
      this.schoolService.saveSchool(school);
    }
    else{
      this.schoolForm.markAllAsTouched();
    }
  }
}
