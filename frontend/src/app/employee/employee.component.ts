import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {EmployeeService} from "../services/employee/employee.service";
import {EducationPackageService} from "../services/education-package/education-package.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogConfirmComponent} from "../dialog-confirm/dialog-confirm.component";

export interface EmployeeFormData extends Employee {
  select_employee: Employee,
  password: string
}

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  filteredOptions: Employee[] = [];
  employees: Employee[];

  employeeForm: FormGroup = new FormGroup({
    select_employee: new FormControl(),
    firstName: new FormControl(undefined, Validators.required),
    lastName: new FormControl(undefined, Validators.required),
    email: new FormControl(undefined, [Validators.required, Validators.email]),
    educationPackages: new FormControl(undefined, Validators.required),
    password: new FormControl(undefined, Validators.minLength(6)),
    id: new FormControl()
  });
  employee: Employee;
  employeeData: EmployeeFormData;
  educationPackages: string[] = [];


  displayEmployee(employee:Employee): string {
    return employee ? `${employee.firstName} ${employee.lastName}` : ''
  };

  constructor(private employeeService: EmployeeService,
              private educationPackageService: EducationPackageService,


  ) { }

  ngOnInit() {
    this.employeeForm.get("select_employee").valueChanges.subscribe(value => {
      // value changed, either by typing text or selection of one of the employees from the list
      // when typing, filter the list
      // when employee, select the employee as the active employee
      if(value && value !== this.employee) {
        if(typeof value === 'object') {
          this.employee = value;
          this.employeeData = Object.assign({ select_employee: value, password: '' }, value);
          this.employeeForm.setValue(this.employeeData);
          this.employeeForm.get("password").clearValidators();
          this.employeeForm.get("password").setValidators(Validators.minLength(6))
          this.employeeForm.get("email").disable();
          this.employeeForm.markAllAsTouched();
        }
        else{ this.filterEmployees(value); }
      }

    })
    this.employeeService.subjectEmployees.subscribe(value => {this.employees = value; this.filteredOptions = this.employees;});
    this.employees = this.employeeService.employees;

    this.educationPackageService.subjectEducationPackages.subscribe(value => { this.educationPackages = value.map(value => value.name);});
    this.educationPackages = this.educationPackageService.educationPackages ? this.educationPackageService.educationPackages.map(value => value.name) : [];
  }

  filterEmployees(filter:string) {
    filter = filter.toLocaleLowerCase();
    this.filteredOptions = this.employees.filter(employee => {
      if(!employee || !filter) { return false; }

      return employee.firstName.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(employee.firstName) > -1 ||
        employee.lastName.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(employee.lastName) > -1 ||
        employee.email.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(employee.email) > -1
    })
  }

  new() {
    this.employeeForm.reset();

    this.employeeForm.get("password").clearValidators();
    this.employeeForm.get("password").setValidators([Validators.required, Validators.minLength(6)]);

    this.employeeForm.get("email").enable();
    this.employeeData = this.employeeForm.getRawValue();
  }

  async remove() {
    if(this.employee) {
      const confirmed = await this.employeeService.deleteEmployee(this.employee);
      if(confirmed) {
        this.employeeForm.reset();
      }
      return confirmed;
    }
  }

  save() {
    if(this.employeeForm.valid) {
      this.employeeData = this.employeeForm.getRawValue();
      const employee = Object.assign(this.employeeData, {});
      delete employee.select_employee;
      this.employeeService.saveEmployee(employee);
    }
    else{
      this.employeeForm.markAllAsTouched();
    }
  }
}
