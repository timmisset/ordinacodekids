import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {EmployeeComponent, EmployeeFormData} from './employee.component';
import {AngularMaterialModule} from "../resources/angular-material.module";
import {EmployeeService} from "../services/employee/employee.service";
import {EducationPackageService} from "../services/education-package/education-package.service";
import {of, Subject} from "rxjs";
import {EducationPackage} from "../model/EducationPackage";

class MockEmployeeService {
  subjectEmployees: Subject<Employee> = new Subject<Employee>()
  deleteEmployee() {}
  saveEmployee() {}
}
class MockEducationPackageService {
  subjectEducationPackages: Subject<EducationPackage[]> = new Subject<EducationPackage[]>();
}

describe('EmployeeComponent', () => {
  let component: EmployeeComponent;
  let fixture: ComponentFixture<EmployeeComponent>;
  let mockEmployee: Employee;
  let mockEmployee2: Employee;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule],
      declarations: [ EmployeeComponent ],
      providers: [
          {provide: EmployeeService, useClass:MockEmployeeService },
        {provide: EducationPackageService, useClass: MockEducationPackageService }
        ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockEmployee = { firstName: "John", lastName: "Doe", email: "johndoe@email.com", educationPackages: ["Agile", "Coding"], password: "", id: 0 }
    mockEmployee2 = { firstName: "Jane", lastName: "Doe", email: "janedoe@email.com", educationPackages: ["Agile", "Security"], password: "", id: 0 }
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the employee form data when the selected value changes', () => {
    // arrange
    // act
    component.employeeForm.get("select_employee").setValue(mockEmployee);
    fixture.detectChanges();

    // assert
    expect(component.employeeForm.get("firstName").value).toEqual(mockEmployee.firstName);
    expect(component.employeeForm.get("lastName").value).toEqual(mockEmployee.lastName);
    expect(component.employeeForm.get("email").value).toEqual(mockEmployee.email);
    expect(component.employeeForm.get("educationPackages").value).toEqual(mockEmployee.educationPackages);
    //
    expect( component.employeeForm.get("email").disabled).toBeTruthy();
  })
  it('should filter the employees based on the text input', () => {

    component.employees = [mockEmployee, mockEmployee2];
    component.filteredOptions = [mockEmployee, mockEmployee2];

    // act
    component.employeeForm.get("select_employee").setValue("Jane");
    fixture.detectChanges();

    // assert
    expect(component.filteredOptions).toEqual([mockEmployee2])

  });
  it('should set the employees after an update in the subjectEmployee', () => {
    // arrange
    const employeeService:EmployeeService = TestBed.get(EmployeeService);
    component.employees = [];
    component.filteredOptions = [];

    // act
    employeeService.subjectEmployees.next([mockEmployee, mockEmployee2]);

    // assert
    expect(component.filteredOptions).toEqual([mockEmployee, mockEmployee2])
    expect(component.employees).toEqual([mockEmployee, mockEmployee2])

  })
  it('filter should be safe against empty input', () => {
    // arrange
    component.employees = [mockEmployee, mockEmployee2];
    component.filteredOptions = [mockEmployee, mockEmployee2];

    // act
    component.filterEmployees("");

    // assert
    expect(component.employees).toEqual( [mockEmployee, mockEmployee2])
    expect(component.filteredOptions).toEqual( [])
  })
  it('should reset the form', () => {
    // arrange
    component.employeeData = <EmployeeFormData> Object.assign({select_employee: mockEmployee}, mockEmployee);

    // act
    component.new();

    // assert
    expect(component.employeeForm.get("email").enabled).toBeTruthy();
    for(let prop in component.employeeData) {
      expect(component.employeeData[prop]).toBeNull();
    }
  })
  it('should reset the form after confirmation for removal of the employee is true', async () => {
    // arrange
    component.employee = mockEmployee;
    const employeeService:EmployeeService = TestBed.get(EmployeeService);
    const spyOnEmployeeService = spyOn(employeeService, "deleteEmployee").and.returnValue(Promise.resolve(true));
    const spyOnEmployeeForm = spyOn(component.employeeForm, "reset");

    // act
    const confirmed = await component.remove();

    // assert
    expect(confirmed).toBeTruthy();
    expect(spyOnEmployeeService).toHaveBeenCalledTimes(1);
    expect(spyOnEmployeeForm).toHaveBeenCalledTimes(1);

  })

  it('should NOT reset the form after confirmation for removal of the employee is false', async () => {
    // arrange
    component.employee = mockEmployee;
    const employeeService:EmployeeService = TestBed.get(EmployeeService);
    const spyOnEmployeeService = spyOn(employeeService, "deleteEmployee").and.returnValue(Promise.resolve(false));
    const spyOnEmployeeForm = spyOn(component.employeeForm, "reset");

    // act
    const confirmed = await component.remove();

    // assert
    expect(confirmed).toBeFalsy();
    expect(spyOnEmployeeService).toHaveBeenCalledTimes(1);
    expect(spyOnEmployeeForm).toHaveBeenCalledTimes(0);
  })

  it('should NOT call deleteEmployee when no employee is selected', async () => {
    // arrange
    component.employee = undefined;
    const employeeService:EmployeeService = TestBed.get(EmployeeService);
    const spyOnEmployeeService = spyOn(employeeService, "deleteEmployee").and.returnValue(Promise.resolve(false));
    const spyOnEmployeeForm =  spyOn(component.employeeForm, "reset");

    // act
    const confirmed = await component.remove();

    // assert
    expect(confirmed).toBeFalsy();
    expect(spyOnEmployeeService).toHaveBeenCalledTimes(0);
    expect(spyOnEmployeeForm).toHaveBeenCalledTimes(0);
  })

  it('should call saveEmployee when the data is valid', () => {
    // arrange
    component.employeeForm.get("select_employee").setValue(mockEmployee);
    const employeeService:EmployeeService = TestBed.get(EmployeeService);
    const spyOnEmployeeService = spyOn(employeeService, "saveEmployee");
    expect(component.employee).toEqual(mockEmployee);

    // act
    component.save();

    // assert
    expect(spyOnEmployeeService).toHaveBeenCalledTimes(1);
    expect(spyOnEmployeeService).toHaveBeenCalledWith(mockEmployee);
  })

  it('should NOT call saveEmployee when the data is invalid', () => {
    // arrange
    const tooShortPassword = "hi";
    component.employeeForm.get("select_employee").setValue(mockEmployee);
    component.employeeForm.get("password").setValue(tooShortPassword);
    const employeeService:EmployeeService = TestBed.get(EmployeeService);
    const spyOnEmployeeService = spyOn(employeeService, "saveEmployee");

    // act
    component.save();

    // assert
    expect(component.employeeForm.valid).toBeFalsy();
    expect(spyOnEmployeeService).toHaveBeenCalledTimes(0);

  })
  it('should respond to a change in the education packages', () => {
    // arrange
    const educationPackageService: EducationPackageService = TestBed.get(EducationPackageService);
    educationPackageService.educationPackages = [{ description: "Coding", id: 1, name: "Coding" }];
    component.ngOnInit();
    const educationPackagesAtStart = component.educationPackages;

    const eductionPackages: EducationPackage[] = [
      { description: "Coding", id: 1, name: "Coding" },
      { description: "Some fancy new package", id: 2, name: "Fancy" }
      ]

    // act
    educationPackageService.subjectEducationPackages.next(eductionPackages);

    // assert
    expect(educationPackagesAtStart).toEqual(["Coding"])
    expect(component.educationPackages).toEqual(["Coding", "Fancy"])
  })

});
