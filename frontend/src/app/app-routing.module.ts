import { NgModule } from '@angular/core';
import {Routes, RouterModule, Params} from '@angular/router';
import {CalendarComponent} from "./calendar/calendar.component";
import {AvailabilityComponent} from "./availability/availability.component";
import {EmployeeComponent} from "./employee/employee.component";
import {SchoolComponent} from "./school/school.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [

  // routes for roles
  { path: 'calendar', component: CalendarComponent, data: { roles: ["ROLE_ADMIN", "ROLE_EMPLOYEE","ROLE_SCHOOL"], index: 0, label: 'Calendar'} },
  { path: 'availability', component: AvailabilityComponent, data: { roles: ["ROLE_ADMIN", "ROLE_EMPLOYEE"], index: 1, label: 'Availability'} },
  { path: 'employee', component: EmployeeComponent, data: { roles: ["ROLE_ADMIN"], index: 2, label: 'Employees'} },
  { path: 'school', component: SchoolComponent, data: { roles: ["ROLE_ADMIN"], index: 3, label: 'Schools'} },
  { path: 'logout', component: LoginComponent, data: { queryParams: {logout: true}, roles: ["ROLE_ADMIN", "ROLE_EMPLOYEE","ROLE_SCHOOL"], index: 4, label: 'Logout'} },
  { path: '', redirectTo: '/calendar', pathMatch: 'full'},
  // login
  { path: 'login', component: LoginComponent, data: { roles: ["ROLE_NONE"], index: 0, label: 'Login'} },
  ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
