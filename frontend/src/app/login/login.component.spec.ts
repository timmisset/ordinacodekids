import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {AngularMaterialModule} from "../resources/angular-material.module";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {User} from "../model/User";
import {Observable, of, Subject, Subscriber} from "rxjs";
import {AvailabilityService} from "../services/availability/availability.service";
import {RouterTestingModule} from "@angular/router/testing";

class MockAuthenticationService {
  login() {}
  logout() {}
}
class MockActivatedRoute {
  subscriber: Subscriber<any>;
  queryParams: Observable<any> = new Observable<any>(subscriber => {
    this.subscriber = subscriber;
  })

  emit(value: any) {
    this.subscriber.next(value);
  }
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [AngularMaterialModule ],
      declarations: [ LoginComponent ],
      providers: [{ provide: AuthenticationService, useClass: MockAuthenticationService},
        {provide: ActivatedRoute, useClass: MockActivatedRoute }
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('SHOULD logout the user when a logout parameter IS present', () => {
    // arrange
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const spyOnAvailabilityService = spyOn(authenticationService, "logout").and.stub();
    const activatedRoute: MockActivatedRoute = TestBed.get(ActivatedRoute);

    // act
    activatedRoute.emit({ logout: true});

    // assert
    expect(spyOnAvailabilityService).toHaveBeenCalledTimes(1);
  })
  it('SHOULD NOT logout the user when a logout parameter IS NOT present', () => {
    // arrange
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const spyOnAvailabilityService = spyOn(authenticationService, "logout").and.stub();
    const activatedRoute: MockActivatedRoute = TestBed.get(ActivatedRoute);

    // act
    activatedRoute.emit({ });

    // assert
    expect(spyOnAvailabilityService).toHaveBeenCalledTimes(0);
  })
  it('SHOULD login when the data on the form IS valid', () => {
    // arrange
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const spyOnAvailabilityService = spyOn(authenticationService, "login").and.stub();
    component.loginForm.setValue({email: 'email@email.com', password: 'password'}); // no validation on user account itself

    // act
    component.login();

    // assert
    expect(spyOnAvailabilityService).toHaveBeenCalledTimes(1);
  })
  it('SHOULD NOT login when the data on the form IS NOT valid', () => {
    // arrange
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const spyOnAvailabilityService = spyOn(authenticationService, "login").and.stub();
    component.loginForm.setValue({email: '', password: 'password'}); // no validation on user account itself

    // act
    component.login();

    // assert
    expect(spyOnAvailabilityService).toHaveBeenCalledTimes(0);
  })
});
