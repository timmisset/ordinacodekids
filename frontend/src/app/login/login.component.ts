import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private authenticationService: AuthenticationService) { }

  loginForm: FormGroup = new FormGroup({
    email: new FormControl(undefined, Validators.required),
    password: new FormControl(undefined, Validators.required)
  })

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if(!!params.logout) {
        this.authenticationService.logout();
      }
    })
  }

  login() {
    if(this.loginForm.valid) {
      const {email, password } = this.loginForm.getRawValue();
      this.authenticationService.login(email, password);
    }
  }

}
