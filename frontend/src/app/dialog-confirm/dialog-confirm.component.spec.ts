import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogConfirmComponent } from './dialog-confirm.component';
import {AngularMaterialModule} from "../resources/angular-material.module";
import {MAT_DIALOG_DATA } from "@angular/material/dialog";
import {DialogConfirm} from "../model/DialogConfirm";
import {By} from "@angular/platform-browser";

describe('DialogConfirmComponent', () => {
  let component: DialogConfirmComponent;
  let fixture: ComponentFixture<DialogConfirmComponent>;
  let data:DialogConfirm = {
    cancelButtonText: "Cancel",
    description: "Description",
    showCancelButton: true,
    title: "My Title"
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule],
      declarations: [ DialogConfirmComponent ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: data }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should inject the information into the MatDialog', () => {
    // assert
    expect(component.data).toEqual(data);
  })

  it('should display the cancel button', () => {
    // arrange

    // act
    component.data.showCancelButton = true;
    fixture.detectChanges();

    // assert
    expect(fixture.debugElement.query(By.css("button[data-button-type=cancel]"))).toBeTruthy()
  })

  it('should hide the cancel button', () => {
    // arrange

    // act
    component.data.showCancelButton = false;
    fixture.detectChanges();

    // assert
    expect(fixture.debugElement.query(By.css("button[data-button-type=cancel]"))).toBeFalsy()
  })
});
