import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DialogConfirm} from "../model/DialogConfirm";

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.css']
})
export class DialogConfirmComponent {

  data:DialogConfirm = {
    cancelButtonText: "Cancel",
    description: "",
    showCancelButton: true,
    title: ""
  };

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public settings: DialogConfirm) {

    Object.assign(this.data, settings);
  }

}
