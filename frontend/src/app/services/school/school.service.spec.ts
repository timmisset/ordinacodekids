import { TestBed } from '@angular/core/testing';

import { SchoolService } from './school.service';
import {HttpService} from "../http/http.service";
import {ToastrService} from "ngx-toastr";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AuthenticationService} from "../authentication/authentication.service";
import {Observable, of} from "rxjs";



describe('SchoolService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: HttpService, useValue: { post() {}, put() {},  delete() {} } },
      {provide: ToastrService, useValue: { success() { } } },
      {provide: MatDialog, useValue: { open() { }}},
      {provide: AuthenticationService, useValue: { createUser() { }, setUserPassword() {}}}
    ]
  }));

  it('should be created', () => {
    const service: SchoolService = TestBed.get(SchoolService);
    expect(service).toBeTruthy();
  });

  it('should set the schools', () => {
    // arrange:
    const service: SchoolService = TestBed.get(SchoolService);
    const schools: School[] = [
      {id: 2, name: "School 2", description: "", password: "", contactName: "", contactEmail: ""},
      {id: 1, name: "School 1", description: "", password: "", contactName: "", contactEmail: ""}]
    const serviceSpy = spyOn(service.subjectSchools, "next");

    // act:
    service.setSchools(schools);

    // assert:
    expect(service.schools.length).toBe(2);
    expect(service.mappedSchools[1].name).toBe("School 1");
    expect(service.mappedSchools[2].name).toBe("School 2");
    expect(service.schools[0].name).toBe("School 1");
    expect(serviceSpy).toHaveBeenCalledTimes(1);
  })
  it('should create a new school and trigger creation of the corresponding user', () => {
    // arrange
    // get mocks
    const service: SchoolService = TestBed.get(SchoolService);
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const httpService: HttpService = TestBed.get(HttpService);

    const mockSchool: School = {name: "School 1", description: "", password: "myPassword", contactName: "", contactEmail: "email"}
    service.schools = [];
    service.mappedSchools = {};

    // spies
    const serviceSpy = spyOn(service, "setSchools").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "post").and.returnValue(of(Object.assign({id:1}, mockSchool)));
    const authSpy = spyOn(authenticationService, "createUser");

    // act
    service.saveSchool(mockSchool);

    // assert
    expect(httpSpy).toHaveBeenCalledWith("school", mockSchool);
    expect(service.schools.length).toBe(1);

    expect(service.mappedSchools[1]).toEqual(Object.assign({id:1}, mockSchool));
    expect(serviceSpy).toHaveBeenCalledTimes(1);
    expect(toastrSpy).toHaveBeenCalledTimes(1);
    expect(authSpy).toHaveBeenCalledWith("email", "myPassword", "ROLE_SCHOOL");

  })
  it('should save an existing school', () => {
    // arrange
    // get mocks
    const service: SchoolService = TestBed.get(SchoolService);
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const httpService: HttpService = TestBed.get(HttpService);

    const mockSchool: School = {id: 1, name: "School 1", description: "", password: "myPassword", contactName: "", contactEmail: "email"}
    service.schools = [mockSchool];
    service.mappedSchools = {1: mockSchool};

    // spies
    const serviceSpy = spyOn(service, "setSchools").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "put").and.returnValue(of(mockSchool));
    const authSpy = spyOn(authenticationService, "setUserPassword");

    // act
    service.saveSchool(mockSchool);

    // assert
    expect(httpSpy).toHaveBeenCalledWith("school", mockSchool.id, mockSchool);
    expect(service.schools.length).toBe(1);
    expect(service.mappedSchools[1]).toEqual(mockSchool);
    expect(serviceSpy).toHaveBeenCalledTimes(0);
    expect(toastrSpy).toHaveBeenCalledTimes(1);
    expect(authSpy).toHaveBeenCalledWith("email", "myPassword");

  })
  it('should delete the school when confirmed by the user', async () => {
    // arrange
    // get mocks
    const service: SchoolService = TestBed.get(SchoolService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const dialog: MatDialog = TestBed.get(MatDialog);
    const httpService: HttpService = TestBed.get(HttpService);

    const mockSchool: School = {id: 1, name: "School 1", description: "", password: "myPassword", contactName: "", contactEmail: ""}
    service.schools = [mockSchool];
    service.mappedSchools = {1: mockSchool};

    // spies
    const serviceSpy = spyOn(service, "setSchools").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "delete").and.returnValue(of(undefined));
    const dialogRef = {
      afterClosed() : Observable<boolean> {
        return of(true);
      }
    }
    const dialogSpyOpen = spyOn(dialog, "open").and.returnValue(<MatDialogRef<any, any>>dialogRef);

    // act
    const confirmed = await service.deleteSchool(mockSchool);

    // assert
    expect(dialogSpyOpen).toHaveBeenCalledTimes(1);
    expect(confirmed).toBeTruthy();
    expect(httpSpy).toHaveBeenCalledWith("school", mockSchool.id);
    expect(service.schools.length).toBe(0);
    expect(service.mappedSchools).toEqual({});
    expect(serviceSpy).toHaveBeenCalledTimes(1);
    expect(toastrSpy).toHaveBeenCalledTimes(1);
  })
  it('shouldn\'t delete the school when confirmed by the user', async () => {
    // arrange
    // get mocks
    const service: SchoolService = TestBed.get(SchoolService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const dialog: MatDialog = TestBed.get(MatDialog);
    const httpService: HttpService = TestBed.get(HttpService);

    const mockSchool: School = {id: 1, name: "School 1", description: "", password: "myPassword", contactName: "", contactEmail: ""}
    service.schools = [mockSchool];
    service.mappedSchools = {1: mockSchool};

    // spies
    const serviceSpy = spyOn(service, "setSchools").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "delete").and.returnValue(of(undefined));
    const dialogRef = {
      afterClosed() : Observable<boolean> {
        return of(false);
      }
    }
    const dialogSpyOpen = spyOn(dialog, "open").and.returnValue(<MatDialogRef<any, any>>dialogRef);

    // act
    const confirmed = await service.deleteSchool(mockSchool);

    // assert
    expect(dialogSpyOpen).toHaveBeenCalledTimes(1);
    expect(confirmed).toBeFalsy();
    expect(httpSpy).toHaveBeenCalledTimes(0);
    expect(service.schools.length).toBe(1);
    expect(service.mappedSchools).toEqual({1: mockSchool});
    expect(serviceSpy).toHaveBeenCalledTimes(0);
    expect(toastrSpy).toHaveBeenCalledTimes(0);
  })

});
