import { Injectable } from '@angular/core';
import {HttpService} from "../http/http.service";
import {Subject} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {DialogConfirmComponent} from "../../dialog-confirm/dialog-confirm.component";
import {MatDialog} from "@angular/material/dialog";
import {AuthenticationService} from "../authentication/authentication.service";

@Injectable({
  providedIn: 'root'
})
export class SchoolService {
  constructor(private httpService: HttpService,
              private toastr: ToastrService,
              private dialog: MatDialog,
              private authenticationService: AuthenticationService

  ) {
  }

  schools:School[];
  mappedSchools = {  };
  subjectSchools: Subject<School[]> =  new Subject<School[]>();
  schoolUpdated: Subject<School> = new Subject<School>();

  setSchools(schools:School[]) {
    this.schools = schools.sort((a:School, b:School) => { return a.name > b.name ? 1 : -1});;
    this.mappedSchools = {};

    this.schools.forEach(school => this.mappedSchools[school.id] = school);
    this.subjectSchools.next(this.schools);
  }

  async deleteSchool(school:School): Promise<boolean> {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '500px',
      data: {
        title: "Are you sure?",
        description: `This will remove ${school.name} and all associated bookings`
      }
    });
    const confirmed = await dialogRef.afterClosed().toPromise();
    if(confirmed) {
      this.httpService.delete("school", school.id);
      this.schools = this.schools.filter(schoolInCollection => schoolInCollection.id != school.id);
      this.setSchools(this.schools);
      this.toastr.success(`${school.name} has been removed`)
    }
    return confirmed;
  }

  saveSchool(school:School) {
    if(school.id > 0) {
      // save existing:
      this.httpService.put("school", school.id, school).subscribe(value => {
        Object.assign(this.mappedSchools[school.id], value);
        this.schoolUpdated.next(value);
        this.toastr.success("School information updated");

        if(school.password) {
          this.authenticationService.setUserPassword(school.contactEmail, school.password);
        }

      });
    }
    else{
      this.httpService.post("school", school).subscribe(value => {
        this.schools.push(value);
        this.setSchools(this.schools);
        this.toastr.success("School account has been created");
        this.authenticationService.createUser(school.contactEmail, school.password, "ROLE_SCHOOL");
      });
    }
  }

}
