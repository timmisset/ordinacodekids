import { Injectable } from '@angular/core';
import {HttpService} from "../http/http.service";
import {Subject} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {DialogConfirmComponent} from "../../dialog-confirm/dialog-confirm.component";
import {MatDialog} from "@angular/material/dialog";
import {AuthenticationService} from "../authentication/authentication.service";
import {AvailabilityService} from "../availability/availability.service";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  constructor(private httpService: HttpService,
              private toastr: ToastrService,
              private dialog: MatDialog,
              private authenticationService: AuthenticationService,
              private availabilityService: AvailabilityService

  ) {
  }

  employees:Employee[];
  mappedEmployees = {  };
  subjectEmployees: Subject<Employee[]> =  new Subject<Employee[]>();
  employeeUpdated: Subject<Employee> = new Subject<Employee>();

  setEmployees(employees:Employee[]) {
    this.employees = employees.sort((a: Employee, b: Employee) => {
      return a.firstName > b.firstName ? 1 : -1
    });
    this.mappedEmployees = {};
    this.employees.forEach(employee => this.mappedEmployees[employee.id] = employee);
    this.subjectEmployees.next(this.employees);
  }

  async deleteEmployee(employee:Employee): Promise<boolean> {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '500px',
      data: {
        title: "Are you sure?",
        description: `This will remove ${employee.firstName} ${employee.lastName} and all associated availability slots and potential bookings`
      }
    });
    const confirmed = await dialogRef.afterClosed().toPromise();
    if(confirmed) {
      this.httpService.delete("employee", employee.id);

      // filter the employee collection
      this.employees = this.employees.filter(employeeInCollection => employeeInCollection.id != employee.id);
      this.setEmployees(this.employees);

      // filter the availabilities
      const availabilities = this.availabilityService.availabilities.filter(availability => availability.employeeId != employee.id);
      this.availabilityService.setAvailabilities(availabilities);

      this.toastr.success(`${employee.firstName} ${employee.lastName} has been removed`)
    }
    return confirmed;
  }

  saveEmployee(employee:Employee) {
    if(employee.id > 0) {
      // save existing:
      this.httpService.put("employee", employee.id, employee).subscribe(value => {
          Object.assign(this.mappedEmployees[employee.id], value);
          this.employeeUpdated.next(value);
          this.toastr.success("Employee information updated");

          if(employee.password) {
            this.authenticationService.setUserPassword(employee.email, employee.password);
          }

        });
    }
    else{
      this.httpService.post("employee", employee).subscribe(value => {
        this.employees.push(value);
        this.setEmployees(this.employees);
        this.toastr.success("Employee account has been created");
        this.authenticationService.createUser(employee.email, employee.password, "ROLE_EMPLOYEE");
      });
    }
  }

}
