import { TestBed } from '@angular/core/testing';

import { EmployeeService } from './employee.service';
import {HttpService} from "../http/http.service";
import {ToastrService} from "ngx-toastr";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AuthenticationService} from "../authentication/authentication.service";
import {AvailabilityService} from "../availability/availability.service";
import {EducationPackageService} from "../education-package/education-package.service";
import {EducationPackage} from "../../model/EducationPackage";
import {Observable, of} from "rxjs";
import {SchoolService} from "../school/school.service";



describe('EmployeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: HttpService, useValue: { post() {}, put() {},  delete() {} } },
      {provide: ToastrService, useValue: { success() { } } },
      {provide: MatDialog, useValue: { open() { }}},
      {provide: AuthenticationService, useValue: { createUser() { }, setUserPassword() {}}},
      {provide: AvailabilityService, useValue: { setAvailabilities() {}}}

    ]
  }));

  it('should be created', () => {
    const service: EmployeeService = TestBed.get(EmployeeService);
    expect(service).toBeTruthy();
  });

  it('should set the employees', () => {
    // arrange:
    const service: EmployeeService = TestBed.get(EmployeeService);
    const employees: Employee[] = [{id: 2, email: "", firstName: "PersonB", lastName: "", educationPackages: []}, {id: 1, email: "", firstName: "PersonA", lastName: "", educationPackages: []}]
    const serviceSpy = spyOn(service.subjectEmployees, "next");

    // act:
    service.setEmployees(employees);

    // assert:
    expect(service.employees.length).toBe(2);
    expect(service.mappedEmployees[1].firstName).toBe("PersonA");
    expect(service.mappedEmployees[2].firstName).toBe("PersonB");
    expect(service.employees[0].firstName).toBe("PersonA");
    expect(serviceSpy).toHaveBeenCalledTimes(1);
  })
  it('should create a new employee and trigger creation of the corresponding user', () => {
    // arrange
    // get mocks
    const service: EmployeeService = TestBed.get(EmployeeService);
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const httpService: HttpService = TestBed.get(HttpService);

    const mockEmployee: Employee = { password: "myPassword", email: "email", firstName: "PersonB", lastName: "", educationPackages: []}
    service.employees = [];
    service.mappedEmployees = {};

    // spies
    const serviceSpy = spyOn(service, "setEmployees").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "post").and.returnValue(of(Object.assign({id:1}, mockEmployee)));
    const authSpy = spyOn(authenticationService, "createUser");

    // act
    service.saveEmployee(mockEmployee);

    // assert
    expect(httpSpy).toHaveBeenCalledWith("employee", mockEmployee);
    expect(service.employees.length).toBe(1);

    expect(service.mappedEmployees[1]).toEqual(Object.assign({id:1}, mockEmployee));
    expect(serviceSpy).toHaveBeenCalledTimes(1);
    expect(toastrSpy).toHaveBeenCalledTimes(1);
    expect(authSpy).toHaveBeenCalledWith("email", "myPassword", "ROLE_EMPLOYEE");

  })
  it('should save an existing employee', () => {
    // arrange
    // get mocks
    const service: EmployeeService = TestBed.get(EmployeeService);
    const authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const httpService: HttpService = TestBed.get(HttpService);

    const mockEmployee: Employee = { id: 1, password: "myPassword", email: "email", firstName: "PersonB", lastName: "", educationPackages: []}
    service.employees = [mockEmployee];
    service.mappedEmployees = {1: mockEmployee};

    // spies
    const serviceSpy = spyOn(service, "setEmployees").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "put").and.returnValue(of(mockEmployee));
    const authSpy = spyOn(authenticationService, "setUserPassword");

    // act
    service.saveEmployee(mockEmployee);

    // assert
    expect(httpSpy).toHaveBeenCalledWith("employee", mockEmployee.id, mockEmployee);
    expect(service.employees.length).toBe(1);
    expect(service.mappedEmployees[1]).toEqual(mockEmployee);
    expect(serviceSpy).toHaveBeenCalledTimes(0);
    expect(toastrSpy).toHaveBeenCalledTimes(1);
    expect(authSpy).toHaveBeenCalledWith("email", "myPassword");

  })
  it('should delete the employee when confirmed by the user', async () => {
    // arrange
    // get mocks
    const service: EmployeeService = TestBed.get(EmployeeService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const dialog: MatDialog = TestBed.get(MatDialog);
    const httpService: HttpService = TestBed.get(HttpService);
    const availabilityService: AvailabilityService = TestBed.get(AvailabilityService);


    const mockEmployee: Employee = { id: 1, password: "myPassword", email: "email", firstName: "PersonB", lastName: "", educationPackages: []}
    service.employees = [mockEmployee];
    service.mappedEmployees = {1: mockEmployee};
    availabilityService.availabilities = [{employeeId: mockEmployee.id, bookingId: 1, date: new Date()}];

    // spies
    const serviceSpy = spyOn(service, "setEmployees").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "delete").and.returnValue(of(undefined));
    const dialogRef = {
      afterClosed() : Observable<boolean> {
        return of(true);
      }
    }
    const dialogSpyOpen = spyOn(dialog, "open").and.returnValue(<MatDialogRef<any, any>>dialogRef);
    const availabilitySpy = spyOn(availabilityService, "setAvailabilities");

    // act
    const confirmed = await service.deleteEmployee(mockEmployee);

    // assert
    expect(dialogSpyOpen).toHaveBeenCalledTimes(1);
    expect(confirmed).toBeTruthy();
    expect(httpSpy).toHaveBeenCalledWith("employee", mockEmployee.id);
    expect(service.employees.length).toBe(0);
    expect(service.mappedEmployees).toEqual({});
    expect(serviceSpy).toHaveBeenCalledTimes(1);
    expect(toastrSpy).toHaveBeenCalledTimes(1);
    expect(availabilitySpy).toHaveBeenCalledTimes(1);
  })
  it('shouldn\'t delete the employee when confirmed by the user', async () => {
    // arrange
    // get mocks
    const service: EmployeeService = TestBed.get(EmployeeService);
    const toastrService: ToastrService = TestBed.get(ToastrService);
    const dialog: MatDialog = TestBed.get(MatDialog);
    const httpService: HttpService = TestBed.get(HttpService);

    const mockEmployee: Employee = { id: 1, password: "myPassword", email: "email", firstName: "PersonB", lastName: "", educationPackages: []}
    service.employees = [mockEmployee];
    service.mappedEmployees = {1: mockEmployee};

    // spies
    const serviceSpy = spyOn(service, "setEmployees").and.callThrough();
    const toastrSpy = spyOn(toastrService, "success");
    const httpSpy = spyOn(httpService, "delete").and.returnValue(of(undefined));
    const dialogRef = {
      afterClosed() : Observable<boolean> {
        return of(false);
      }
    }
    const dialogSpyOpen = spyOn(dialog, "open").and.returnValue(<MatDialogRef<any, any>>dialogRef);

    // act
    const confirmed = await service.deleteEmployee(mockEmployee);

    // assert
    expect(dialogSpyOpen).toHaveBeenCalledTimes(1);
    expect(confirmed).toBeFalsy();
    expect(httpSpy).toHaveBeenCalledTimes(0);
    expect(service.employees.length).toBe(1);
    expect(service.mappedEmployees).toEqual({1: mockEmployee});
    expect(serviceSpy).toHaveBeenCalledTimes(0);
    expect(toastrSpy).toHaveBeenCalledTimes(0);
  })
});
