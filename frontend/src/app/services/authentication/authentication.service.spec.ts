import { TestBed } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';
import {HttpService} from "../http/http.service";
import {ToastrService} from "ngx-toastr";
import {Observable, of} from "rxjs";
import {User} from "../../model/User";
import {RouterTestingModule} from "@angular/router/testing";
import {Router} from "@angular/router";
import {AngularMaterialModule} from "../../resources/angular-material.module";
import {Component} from "@angular/core";

@Component({
  template: ``,
  selector: 'mock'
})
class MockComponent {

}

class MockHttp {

  get(): Observable<User>{
    const admin = of({ role: "ROLE_ADMIN", id: 1, email: "admin"});
    const employee = of({ role: "ROLE_EMPLOYEE", id: 1, email: "employee"});
    const school  = of({ role: "ROLE_SCHOOL", id: 1, email: "school"});
    switch(email) {
      default:
      case "admin": return admin;
      case "employee": return employee;
      case "school": return school;
    }
  }
  put() { return of(undefined)}
  post() { return of(undefined)}
}

let email: string;;
let password: string;;


describe('AuthenticationService', () => {
  beforeEach(() =>
  {
      TestBed.configureTestingModule({
        declarations: [MockComponent],
        imports: [ AngularMaterialModule,
                    RouterTestingModule.withRoutes([{ path: 'login', component: MockComponent }, {path: 'calendar', component: MockComponent }] )],
      providers: [
        { provide: HttpService, useClass: MockHttp },
        { provide: ToastrService, useValue: { success: {} }}
      ]
    })

  });

  it('should be created', () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    expect(service).toBeTruthy();
  });

  it('should login to the application as admin', async () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    const httpService: HttpService = TestBed.get(HttpService)

    const httpServiceSpyGet = spyOn(httpService, "get").and.callThrough();

    email = "admin";
    password  = "admin";
    let authData = `Basic ${window.btoa(`${email}:${password}`)}`;
    await service.login(email, password);

    expect(httpServiceSpyGet).toHaveBeenCalledWith("user/login")
    expect(service.currentLoginUser.authData).toBe(authData);
    expect(service.currentUserSubject.getValue().role).toBe("ROLE_ADMIN")
    expect(service.currentUserIsAdmin).toBeTruthy()
  })

  it('should login to the application as employee', async () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    const httpService: HttpService = TestBed.get(HttpService)

    const httpServiceSpyGet = spyOn(httpService, "get").and.callThrough();

    email = "employee";
    password  = "employee";
    let authData = `Basic ${window.btoa(`${email}:${password}`)}`;
    await service.login(email, password);

    expect(httpServiceSpyGet).toHaveBeenCalledWith("user/login")
    expect(service.currentLoginUser.authData).toBe(authData);
    expect(service.currentUserSubject.getValue().role).toBe("ROLE_EMPLOYEE")
    expect(service.currentUserIsAdmin).toBeFalsy()
  })

  it('should login to the application as school', async () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    const httpService: HttpService = TestBed.get(HttpService)

    const httpServiceSpyGet = spyOn(httpService,  "get").and.callThrough();

    email = "school";
    password  = "school";
    let authData = `Basic ${window.btoa(`${email}:${password}`)}`;
    await service.login(email, password);

    expect(httpServiceSpyGet).toHaveBeenCalledWith("user/login")
    expect(service.currentLoginUser.authData).toBe(authData);
    expect(service.currentUserSubject.getValue().role).toBe("ROLE_SCHOOL")
    expect(service.currentUserIsAdmin).toBeFalsy()
  })

  it('should set the user password', () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    const httpService: HttpService = TestBed.get(HttpService)
    const toastrService: ToastrService = TestBed.get(ToastrService);

    const httpServiceSpyPut = spyOn(httpService, "put").and.callThrough();
    const toastrServiceSpy = spyOn(toastrService, "success").and.stub();

    service.setUserPassword(email, password);

    expect(httpServiceSpyPut).toHaveBeenCalledWith("user", "setPassword", {email: email, password: password});
    expect(toastrServiceSpy).toHaveBeenCalledWith("User password has been updated");
  })

  it('should have created the user', () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    const httpService: HttpService = TestBed.get(HttpService)
    const toastrService: ToastrService = TestBed.get(ToastrService);

    const httpServiceSpyPut = spyOn(httpService, "post").and.callThrough();
    const toastrServiceSpy = spyOn(toastrService, "success").and.stub();

    service.createUser(email, password, "role");

    expect(httpServiceSpyPut).toHaveBeenCalledWith("user", {email: email, password: password, role: "role"});
    expect(toastrServiceSpy).toHaveBeenCalledWith("User account has been created");
  })

  it('should logout the user', () => {
    // arrange
    const service: AuthenticationService = TestBed.get(AuthenticationService);

    let currentUser: User;
    service.currentUserSubject.subscribe(value => currentUser = value);


    // act
    service.logout();

    // assert
    expect(service.currentLoginUser).toBeUndefined();
    expect(service.currentUserId).toBeUndefined();
    expect(service.currentUserIsAdmin).toBeFalsy();
    expect(currentUser).toBeUndefined();

  })
  it('should navigate away from the login screen', async () => {
    // arrange
    const router: Router = TestBed.get(Router);
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    await router.navigate(['login']);

    // act
    await service.login('admin', 'admin');

    // assert
    expect(router.url.toLocaleLowerCase()).toEqual('/calendar');

  })
});
