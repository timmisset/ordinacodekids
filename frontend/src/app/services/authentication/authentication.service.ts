import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {User} from "../../model/User";
import {HttpService} from "../http/http.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  currentUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(undefined);
  currentLoginUser: User;
  currentUserId: number;
  currentUserIsAdmin: boolean = false;

  constructor(private httpService:HttpService, private toastr:ToastrService, private router: Router) {

  }

  async login(email, password) {
    this.currentUserSubject.next(undefined);

    this.currentLoginUser = {
      email: email,
      authData: `Basic ${window.btoa(`${email}:${password}`)}`
    };

    const currentUser: User = await this.httpService.get("user/login").toPromise();
    this.currentUserIsAdmin = currentUser.role === "ROLE_ADMIN";
    this.currentUserId = currentUser.id;
    this.currentUserSubject.next(currentUser);

    if (this.router.url === '/login') {
      await this.router.navigate(['/calendar'])
    }
    return currentUser;

  }

  setUserPassword(email, password) {
    this.httpService.put("user", 'setPassword',{ email: email, password:password})
      .subscribe(() => { this.toastr.success("User password has been updated"); });
  }
  createUser(email, password, role) {
    const user:User = {
      email: email,
      password: password,
      role: role
    }
    this.httpService.post("user", user)
      .subscribe(() => { this.toastr.success("User account has been created"); });
  }

  logout() {
    this.currentUserSubject.next(undefined);
    this.currentUserIsAdmin = false;
    this.currentLoginUser = undefined;
    this.currentUserId = undefined;
  }
}
