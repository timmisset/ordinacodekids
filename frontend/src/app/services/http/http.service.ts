import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  options = {
    //https://ordina-code-kids.herokuapp.com/
    SERVER_ADDRESS: "ordina-code-kids.herokuapp.com",
    SERVER_PORT: "",
    SERVER_PROTOCOL: "https"
  };


  constructor(private http:HttpClient) {

  }

  getServer() {
    return `${this.options.SERVER_PROTOCOL}://${this.options.SERVER_ADDRESS}${this.options.SERVER_PORT ? ':' : ''}${this.options.SERVER_PORT}`
  }

  getControllerEndpoint(controller:string) {
    return `${this.getServer()}/api/${controller}`;
  }

  get(controller:string, options?:any):Observable<any> {
    return this.http.get(this.getControllerEndpoint(controller), options)
  }

  post(controller:string, payload:any):Observable<any> {
    return this.http.post(this.getControllerEndpoint(controller), payload);
  }

  put(controller:string, id: any, payload:any):Observable<any> {
    return this.http.put(this.getControllerEndpoint(controller) + "/" + id, payload);
  }

  delete(controller:string, id: any):Observable<any> {
    if(Array.isArray(id)) {
      return this.http.request('delete', this.getControllerEndpoint(controller), { body: id})
    }
    else{
      return this.http.delete(this.getControllerEndpoint(controller + "/" + id));
    }

  }
}
