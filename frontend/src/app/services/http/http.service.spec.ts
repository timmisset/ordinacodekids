import { TestBed } from '@angular/core/testing';

import { HttpService } from './http.service';
import {HttpClient} from "@angular/common/http";

describe('HttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: HttpClient, useValue: { get(){}, post(){}, put(){}, delete(){}, request() {} }}
    ]
  }));

  it('should be created', () => {
    const service: HttpService = TestBed.get(HttpService);
    expect(service).toBeTruthy();
  });

  it('should generate the right controller endpoint', () => {
    // arrange
    const service: HttpService = TestBed.get(HttpService);
    service.options = {
      SERVER_ADDRESS: "localhost",
      SERVER_PORT: "8080",
      SERVER_PROTOCOL: "http"
    };

    // act
    const endpoint = service.getControllerEndpoint("myController");

    // assert
    expect(endpoint).toEqual("http://localhost:8080/api/myController");
  })

  it('should call the corresponding backend methods', () => {
    const service: HttpService = TestBed.get(HttpService);
    service.options = {
      SERVER_ADDRESS: "localhost",
      SERVER_PORT: "8080",
      SERVER_PROTOCOL: "http"
    };
    const httpClient: HttpClient = TestBed.get(HttpClient);
    // spies:
    const spyGet = spyOn(httpClient, "get");
    const spyPost = spyOn(httpClient, "post");
    const spyPut = spyOn(httpClient, "put");
    const spyDelete = spyOn(httpClient, "delete");


    const endpoint = "http://localhost:8080/api/myController";
    const payload = { some: "setting"}
    const id = "item1";
    // act
    service.get("myController", payload);
    service.post("myController", payload);
    service.put("myController", id, payload);
    service.delete("myController", id);

    // assert
    expect(spyGet).toHaveBeenCalledWith(endpoint, payload);
    expect(spyPost).toHaveBeenCalledWith(endpoint, payload);
    expect(spyPut).toHaveBeenCalledWith(`${endpoint}/${id}`, payload);
    expect(spyDelete).toHaveBeenCalledWith(`${endpoint}/${id}`);
  })

  it('should call the controller itself with a body of id\'s when an array of ids is used as payload for delete', () => {
    // arrange
    const service: HttpService = TestBed.get(HttpService);
    service.options = {
      SERVER_ADDRESS: "localhost",
      SERVER_PORT: "8080",
      SERVER_PROTOCOL: "http"
    };
    const httpClient: HttpClient = TestBed.get(HttpClient);
    const endpoint = "http://localhost:8080/api/myController";
    const ids = ["item1", "item2", "item3"];

    // spies
    const spyRequest = spyOn(httpClient, "request");

    // act
    service.delete("myController", ids);

    // assert
    expect(spyRequest).toHaveBeenCalledWith("delete", endpoint, { body: ids});
  })
});
