import { TestBed } from '@angular/core/testing';

import { CalendarService } from './calendar.service';
import {HttpService} from "../http/http.service";
import {AuthenticationService} from "../authentication/authentication.service";
import {ToastrService} from "ngx-toastr";
import {AvailabilityService} from "../availability/availability.service";
import {EmployeeService} from "../employee/employee.service";
import {EducationPackageService} from "../education-package/education-package.service";
import {SchoolService} from "../school/school.service";
import {BookingService} from "../booking/booking.service";
import {BehaviorSubject, of, Subject} from "rxjs";
import {User} from "../../model/User";
import {OrdinaCalendarEvent} from "../../model/OrdinaCalendarEvent";

class MockAvailabilityService {
  availabilities: Availability[] = [];
  subjectAvailabilities: Subject<Availability[]> = new Subject<Availability[]>();
  mappedAvailabilities: any = {};

  setAvailabilities() {}
}
class MockEmployeeService {
  employees: Employee[] = [];
  subjectEmployees: Subject<Employee[]> = new Subject<Employee[]>();
  mappedEmployees: any = {};

  setEmployees() {}
}
class MockSchoolService {
  schools: School[] = [];
  subjectSchools: Subject<School[]> = new Subject<School[]>();
  mappedSchools: any = {};

  setSchools() {}
}
class MockBookingService {
  bookings: Booking[] = [];
  subjectBookings: Subject<Booking[]> = new Subject<Booking[]>();
  mappedBookings: any = {};

  setBookings() {}
}
class MockAuthenticationService {
  mockUser: User = {email: "admin", password: "admin", role: "ROLE_ADMIN"}
  currentUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(this.mockUser);
}

describe('CalendarService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: HttpService, useValue: { get() { return of([])}}},
      {provide: ToastrService, useValue: { success() {}}},
      {provide: AvailabilityService, useClass: MockAvailabilityService},
      {provide: EmployeeService, useClass: MockEmployeeService},
      {provide: SchoolService, useClass: MockSchoolService},
      {provide: BookingService, useClass: MockBookingService},
      {provide: AuthenticationService, useClass: MockAuthenticationService}
    ]}));

  it('should be created', () => {
    const service: CalendarService = TestBed.get(CalendarService);
    expect(service).toBeTruthy();
  });

  it('should react to a change in the subscriptions', () => {
    // arrange
    // spies
    const service: CalendarService = TestBed.get(CalendarService);
    const availabilityService: AvailabilityService = TestBed.get(AvailabilityService);
    const schoolService: SchoolService = TestBed.get(SchoolService);
    const bookingService: BookingService = TestBed.get(BookingService);
    const employeeService: EmployeeService = TestBed.get(EmployeeService);

    const spyOnService = spyOn(service, "setCalendarEvents").and.stub();

    // act
    availabilityService.subjectAvailabilities.next([]);
    schoolService.subjectSchools.next([]);
    bookingService.subjectBookings.next([]);
    employeeService.subjectEmployees.next([]);


    // assert
    expect(spyOnService).toHaveBeenCalledTimes(4);
  })
  it('shouldn\'t create events when not allowed', () => {
    // arrange
    const service: CalendarService = TestBed.get(CalendarService);
    const availabilityService: AvailabilityService = TestBed.get(AvailabilityService);
    const employeeService: EmployeeService = TestBed.get(EmployeeService);

    availabilityService.availabilities = [{date: new Date(), employeeId: 1}]
    employeeService.employees = [{id: 1, email: "user@user.com", firstName: "John", lastName: "Doe", educationPackages: []}]

    service.canSetCalendarEvents = false;
    service.calendarEvents = [];

    // act
    service.setCalendarEvents();

    // assert
    expect(availabilityService.availabilities.length).toBeGreaterThan(0);
    expect(employeeService.employees.length).toBeGreaterThan(0);
    expect(service.calendarEvents.length).toEqual(0);
  })
  it('should create events when allowed', () => {
    // arrange
    const service: CalendarService = TestBed.get(CalendarService);
    const availabilityService: AvailabilityService = TestBed.get(AvailabilityService);
    const employeeService: EmployeeService = TestBed.get(EmployeeService);

    availabilityService.availabilities = [{date: new Date(), employeeId: 1}]
    const mockEmployee = {id: 1, email: "user@user.com", firstName: "John", lastName: "Doe", educationPackages: []};
    employeeService.mappedEmployees = {1: mockEmployee}

    service.canSetCalendarEvents = true;
    service.calendarEvents = [];

    // act
    service.setCalendarEvents();

    // assert
    expect(service.calendarEvents.length).toEqual(1);
    expect(service.calendarEvents[0].employee).toBe(mockEmployee);
  })

  it('should link the booking to the event', () => {
    // arrange
    const service: CalendarService = TestBed.get(CalendarService);
    const availabilityService: AvailabilityService = TestBed.get(AvailabilityService);
    const employeeService: EmployeeService = TestBed.get(EmployeeService);
    const bookingService: BookingService = TestBed.get(BookingService);
    const schoolService: SchoolService = TestBed.get(SchoolService);

    availabilityService.availabilities = [{id: 1, bookingId:1, date: new Date(), employeeId: 1}];
    const mockEmployee = {id: 1, email: "user@user.com", firstName: "John", lastName: "Doe", educationPackages: []};
    const mockBooking = {availabilityId: 1, schoolId: 1, id: 1};
    const mockSchool = {id:1 , contactEmail: "", contactName: "", description: "", name: ""};

    employeeService.mappedEmployees = {1: mockEmployee};
    bookingService.mappedBookings = {1: mockBooking};
    schoolService.mappedSchools = {1: mockSchool};

    service.canSetCalendarEvents = true;
    service.calendarEvents = [];

    // act
    service.setCalendarEvents();

    // assert
    expect(service.calendarEvents.length).toEqual(1);
    expect(service.calendarEvents[0].booking).toBe(mockBooking);
    expect(service.calendarEvents[0].school).toBe(mockSchool);
  })

  it('should link the user to the employee', () => {
    // arrange
    const schoolService:  SchoolService = TestBed.get(SchoolService);
    const employeeService: EmployeeService = TestBed.get(EmployeeService);
    const authService: AuthenticationService = TestBed.get(AuthenticationService);
    const service: CalendarService = TestBed.get(CalendarService);

    const mockEmployee = {id: 1, email: "admin", firstName: "John", lastName: "Doe", educationPackages: []};
    const mockSchool = {id:1 , contactEmail: "", contactName: "", description: "", name: ""};
    const mockUser = authService.currentUserSubject.getValue();

    employeeService.mappedEmployees = {1: mockEmployee};
    employeeService.employees = [mockEmployee];

    schoolService.mappedSchools = {1: mockSchool};
    schoolService.schools = [mockSchool];

    // act
    service.linkUserToSchoolOrEmployee();

    // assert
    expect(mockUser.employee).toBe(mockEmployee);
    expect(mockUser.school).toBeUndefined();
  })

  it('should filter booked events', () => {
    // arrange
    const service: CalendarService = TestBed.get(CalendarService);
    const mockBookedEvent: OrdinaCalendarEvent = {
      availability: { date: new Date(), bookingId: 1, employeeId: 1 },
      employee: undefined,
      booking: { id: undefined, schoolId: undefined, availabilityId: undefined },
      school: undefined,
      start: new Date(),
      end: new Date(),
      title: undefined
    }
    const mockAvailableEvent: OrdinaCalendarEvent = {
      availability: { date: new Date(), bookingId: 1, employeeId: 1 },
      employee: undefined,
      booking: undefined,
      school: undefined,
      start: new Date(),
      end: new Date(),
      title: undefined
    }

    const events: OrdinaCalendarEvent[] = [mockBookedEvent, mockAvailableEvent];
    service.SHOW_BOOKED_SLOTS = false;

    // act
    service.subjectCalendarEvents.subscribe(value => {
      // put the assertion inside the subscription
      expect(value).toEqual([mockAvailableEvent])
    })
    // then trigger the subject to be updated via the formatAndFilterEvents
    service.formatAndFilterEvents(events);
  })

  it('shouldn\'t filter booked events', () => {
    // arrange
    const service: CalendarService = TestBed.get(CalendarService);
    const mockBookedEvent: OrdinaCalendarEvent = {
      availability: { date: new Date(), bookingId: 1, employeeId: 1 },
      employee: undefined,
      booking: { id: undefined, schoolId: undefined, availabilityId: undefined },
      school: undefined,
      start: new Date(),
      end: new Date(),
      title: undefined
    }
    const mockAvailableEvent: OrdinaCalendarEvent = {
      availability: { date: new Date(), bookingId: 1, employeeId: 1 },
      employee: undefined,
      booking: undefined,
      school: undefined,
      start: new Date(),
      end: new Date(),
      title: undefined
    }

    const events: OrdinaCalendarEvent[] = [mockBookedEvent, mockAvailableEvent];
    service.SHOW_BOOKED_SLOTS = true;

    // act
    service.subjectCalendarEvents.subscribe(value => {
      // put the assertion inside the subscription
      expect(value).toEqual([mockBookedEvent, mockAvailableEvent])
    })
    // then trigger the subject to be updated via the formatAndFilterEvents
    service.formatAndFilterEvents(events);
  })

  it('should return the correct colors', () => {
    // arrange
    const service: CalendarService = TestBed.get(CalendarService);

    const mockBooking: Booking = { availabilityId: 1, schoolId: 1, id: 1 }
    const mockUser: User = { email: "some@email.com"}
    const mockSchool: School = {name: "", description: "", contactName: "", contactEmail: "some@email.com"}
    const mockEmployee: Employee = {educationPackages: [], lastName: "", firstName: "", email: ""}

    service.currentUser = mockUser;

    // act
    const colorAvailable = service.getColorForEvent(undefined, undefined, undefined);
    const colorOwner = service.getColorForEvent(mockBooking, mockSchool, mockEmployee);
    mockUser.email = "another@user.com";
    const colorOtherUser = service.getColorForEvent(mockBooking, mockSchool, mockEmployee);

    // assert
    expect(colorAvailable).toBe(service.COLOR_AVAILABLE);
    expect(colorOwner).toBe(service.COLOR_BOOKED_BY_ME);
    expect(colorOtherUser).toBe(service.COLOR_BOOKED);



  })

  it('should call formatAndFilter when showBookedEvents is toggled', () => {
    // arrange
    const service: CalendarService = TestBed.get(CalendarService);
    const showBookedSlots = service.SHOW_BOOKED_SLOTS;
    const spyOnFormatAndFilter = spyOn(service, "formatAndFilterEvents").and.stub();

    // act
    service.toggleBookedSlots();

    // assert
    expect(service.SHOW_BOOKED_SLOTS).toBe(!showBookedSlots);
    expect(spyOnFormatAndFilter).toHaveBeenCalledTimes(1);
  })

  it('should reset the demo data', () => {
    // this test can be removed if the demo data as a whole is removed from the project
    // assert
    const service: CalendarService = TestBed.get(CalendarService);
    const httpService: HttpService = TestBed.get(HttpService);

    // spies
    const spyOnHttp = spyOn(httpService, "get").and.returnValue(of(true));
    const spyOnLoadCalendarEvents = spyOn(service, "loadCalendarEvents").and.stub();

    // act
    service.resetDemoData();

    // assert
    expect(spyOnHttp).toHaveBeenCalledWith("demo/reset", { responseType: "text"})
    expect(spyOnLoadCalendarEvents).toHaveBeenCalledTimes(1);


  })
});
