import { Injectable } from '@angular/core';
import {forkJoin, Subject} from "rxjs";
import {OrdinaCalendarEvent} from "../../model/OrdinaCalendarEvent";
import {HttpService} from "../http/http.service";
import {AuthenticationService} from "../authentication/authentication.service";
import {User} from "../../model/User";
import {ToastrService} from "ngx-toastr";
import {AvailabilityService} from "../availability/availability.service";
import {EmployeeService} from "../employee/employee.service";
import {EducationPackageService} from "../education-package/education-package.service";
import {SchoolService} from "../school/school.service";
import {BookingService} from "../booking/booking.service";

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  subjectCalendarEvents:Subject<OrdinaCalendarEvent[]> = new Subject<OrdinaCalendarEvent[]>();
  subjectCalendarHoursInDay:Subject<number> = new Subject();

  HOURS_PER_BLOCK_OPTIMUM = 2;
  SHOW_BOOKED_SLOTS = true;

  COLOR_BOOKED = { primary: '#A9A9A9', secondary: '#A9A9A9' };
  COLOR_AVAILABLE = { primary: '#e98300', secondary: '#fff' };
  COLOR_BOOKED_BY_ME = { primary: '#e98300', secondary: '#e98300' };

  constructor(private httpService: HttpService,
              private authenticationService:AuthenticationService,
              private toastr: ToastrService,
              private availabilityService: AvailabilityService,
              private employeeService:EmployeeService,
              private educationPackageService: EducationPackageService,
              private schoolService: SchoolService,
              private bookingService: BookingService

  ) {
    // subscribe to user login / change
    authenticationService.currentUserSubject.subscribe(user => {
        if(user) {
          this.currentUser = user;
          this.loadCalendarEvents(); }
    });

    // subscribe to the data updates that require calendar items to be recreated:
    this.availabilityService.subjectAvailabilities.subscribe(value => this.setCalendarEvents());
    this.employeeService.subjectEmployees.subscribe(value => this.setCalendarEvents());
    this.schoolService.subjectSchools.subscribe(value => this.setCalendarEvents());
    this.bookingService.subjectBookings.subscribe(value => this.setCalendarEvents());

  }
  calendarEvents:OrdinaCalendarEvent[] = [];

  currentCalendarHoursInDay = 8;
  currentUser:User;


  // reload all the data from the server, usually triggered after user login
  public loadCalendarEvents() {
     // going to refresh all data, prevent calling setCalendarEvents 5 times
     this.canSetCalendarEvents = false;

     // forkJoin to request all data and run when all have been received
     forkJoin([
       this.httpService.get("availability"),
       this.httpService.get("school"),
       this.httpService.get("booking"),
       this.httpService.get("employee"),
       this.httpService.get("educationpackage")])

       .subscribe((value:any[]) => {

          this.availabilityService.setAvailabilities(value[0]);
          this.schoolService.setSchools(value[1]);
          this.bookingService.setBookings(value[2]);

          this.canSetCalendarEvents = true; // enable setting the calendar events
          this.employeeService.setEmployees(value[3]);

          this.educationPackageService.setEducationPackages(value[4]);

          // generate the calendar events
          this.setCalendarEvents();

          // link the current user to the school or employee:
          this.linkUserToSchoolOrEmployee();

         }
       );
  }

  canSetCalendarEvents = true;
  setCalendarEvents() {
    if(!this.canSetCalendarEvents) { return; }
    // generate the calendar events from the data
    const allEvents:OrdinaCalendarEvent[] = this.availabilityService.availabilities.map(availability => {
      availability.date = new Date(availability.date);

      const employee:Employee = this.employeeService.mappedEmployees[availability.employeeId];
      const booking:Booking = this.bookingService.mappedBookings[availability.bookingId];
      const school:School = booking ? this.schoolService.mappedSchools[booking.schoolId] : undefined;

      const calendarEvent:OrdinaCalendarEvent = {
        availability: availability,
        booking: booking,
        employee: employee,
        school: school,
        allDay: false,
        end: new Date(availability.date.setHours(15)),
        start: new Date(availability.date.setHours(9)),
        title: this.getTitle(employee, booking),
        color: this.getColorForEvent(booking, school, employee)
      };
      return calendarEvent;
    });
    // set the events
    this.calendarEvents = allEvents;

    // format and filter them based on the app settings
    // then emits the calendar events via subjectCalendarEvents
    this.formatAndFilterEvents(allEvents);
  }

  linkUserToSchoolOrEmployee() {
    // make the user school or user employee reference:
    const user:User = this.authenticationService.currentUserSubject.getValue();

    user.school = this.schoolService.schools.find(school => school.contactEmail === user.email);
    user.employee = this.employeeService.employees.find(employee => employee.email === user.email);

  }

  getColorForEvent(booking:Booking, school:School, employee:Employee) {
    if(!booking || !this.currentUser || !school || !employee) { return this.COLOR_AVAILABLE; }
    if(this.currentUser.email === school.contactEmail || this.currentUser.email === employee.email) {
      return this.COLOR_BOOKED_BY_ME;
    }
    return this.COLOR_BOOKED;
  }

  formatAndFilterEvents(allEvents:OrdinaCalendarEvent[]) {

    if(!this.SHOW_BOOKED_SLOTS) {
      allEvents = allEvents.filter(item => !item.booking)
    }

    const mappedEventsByDate = {};
    let maxSize = 0;
    allEvents.forEach(availability => {
        const availabilitiesForDate:OrdinaCalendarEvent[] = mappedEventsByDate[availability.availability.date + ""] || [];
        availabilitiesForDate.push(availability);
        maxSize = Math.max(maxSize, availabilitiesForDate.length);
        mappedEventsByDate[availability.availability.date + ""] = availabilitiesForDate;
      }
    );

    const hoursInDay = Math.min(24, this.HOURS_PER_BLOCK_OPTIMUM*maxSize);
    this.currentCalendarHoursInDay = hoursInDay;
    this.subjectCalendarHoursInDay.next(this.currentCalendarHoursInDay);

    // determine the blocksize for events
    // there is room for 24 hours of blocks on the calendar
    const blockSize = Math.max(1, Math.ceil(hoursInDay / maxSize));
    for (let mappedEventsByDateKey in mappedEventsByDate) {
      const availabilitiesForDate:OrdinaCalendarEvent[] = mappedEventsByDate[mappedEventsByDateKey];
      let offset = 0;
      availabilitiesForDate.forEach(availability => {
        availability.start = new Date(availability.start.setHours(offset * blockSize));
        availability.end = new Date(availability.end.setHours(offset * blockSize + blockSize));
        offset++;
      })
    }

    this.subjectCalendarEvents.next(allEvents);

  }

  public toggleBookedSlots() {
    this.SHOW_BOOKED_SLOTS = !this.SHOW_BOOKED_SLOTS;
    this.formatAndFilterEvents(this.calendarEvents);
  }

  public resetDemoData() {
    this.httpService.get("demo/reset", {
      responseType: "text"
    }).subscribe(() => {
      this.loadCalendarEvents();
    })
  }

  getTitle(employee:Employee, booking:Booking) {
    return `${employee.firstName} ${employee.lastName} <br>
                            ${employee.educationPackages} <br>
                            ${booking ? 'Geboekt door: ' + this.schoolService.mappedSchools[booking.schoolId].name : ''} 
                            `;
  }

}
