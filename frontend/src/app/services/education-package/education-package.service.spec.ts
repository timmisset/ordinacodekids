import { TestBed } from '@angular/core/testing';

import { EducationPackageService } from './education-package.service';
import {EducationPackage} from "../../model/EducationPackage";
import {HttpService} from "../http/http.service";

describe('EmployeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: HttpService, useValue: { }}
    ]
  }));

  it('should be created', () => {
    const service: EducationPackageService = TestBed.get(EducationPackageService);
    expect(service).toBeTruthy();
  });

  it('should set the educationPackages', () => {
    // arrange:
    const service: EducationPackageService = TestBed.get(EducationPackageService);
    const educationPackages: EducationPackage[] = [{id: 1, name: "coding", description: ""}, {id: 2, name: "coding", description: ""}]
    const serviceSpy = spyOn(service.subjectEducationPackages, "next");

    // act:
    service.setEducationPackages(educationPackages);

    // assert:
    expect(service.educationPackages.length).toBe(2);
    expect(service.mappedEducationPackages[1]).toBe(educationPackages[0]);
    expect(service.mappedEducationPackages[2]).toBe(educationPackages[1]);
    expect(serviceSpy).toHaveBeenCalledTimes(1);
  })

});
