import { Injectable } from '@angular/core';
import {HttpService} from "../http/http.service";
import {Subject} from "rxjs";
import {EducationPackage} from "../../model/EducationPackage";

@Injectable({
  providedIn: 'root'
})
export class EducationPackageService {
  constructor(private httpService: HttpService) {
  }

  educationPackages:EducationPackage[];
  mappedEducationPackages = {  };
  subjectEducationPackages: Subject<EducationPackage[]> =  new Subject<EducationPackage[]>();

  setEducationPackages(educationPackages:EducationPackage[]) {
    this.educationPackages = educationPackages;
    this.mappedEducationPackages = {}
    educationPackages.forEach(educationPackage => this.mappedEducationPackages[educationPackage.id] = educationPackage);
    this.subjectEducationPackages.next(educationPackages);

  }

}
