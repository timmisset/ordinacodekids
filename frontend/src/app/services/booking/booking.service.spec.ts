import { TestBed } from '@angular/core/testing';

import { BookingService } from './booking.service';
import {HttpService} from "../http/http.service";
import {AvailabilityService} from "../availability/availability.service";
import {BehaviorSubject, of} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {AuthenticationService} from "../authentication/authentication.service";
import {User} from "../../model/User";
import {MatDialog} from "@angular/material/dialog";

class MockHttp {
  post() {}
  delete() {}
}
class MockAuthenticationService {
  mockSchool: School = {
    contactEmail: "",
    contactName: "",
    description: "",
    id: 10,
    name: "",
    password: ""
  }
  mockUser: User = {
    school: this.mockSchool,
    password: "",
    email: "",
  }
  currentUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(this.mockUser)
}
class MockAvailabilityService {
  mappedAvailabilities = {};
  setAvailabilities() {}
}

describe('BookingService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: HttpService, useClass: MockHttp},
      {provide: AuthenticationService, useClass: MockAuthenticationService},
      {provide: AvailabilityService, useClass: MockAvailabilityService },
      {provide: ToastrService, useValue: { success() {} }},
      {provide: MatDialog, useValue: { open() {} }}
    ]
  }));

  it('should be created', () => {
    const service: BookingService = TestBed.get(BookingService);
    expect(service).toBeTruthy();
  });

  it('should create the booking', () => {
    // arrange
    // get mocked services
    const mockAuthenticationService: MockAuthenticationService = new MockAuthenticationService();
    const service: BookingService = TestBed.get(BookingService);
    const httpService: HttpService = TestBed.get(HttpService);
    const availabilityService: AvailabilityService = TestBed.get(AvailabilityService);
    const toastrService:ToastrService = TestBed.get(ToastrService);

    // mock data
    const date: Date = new Date(2020, 0, 1);
    const mockAvailability: Availability = { id: 5,  date: date, employeeId: 1, bookingId: undefined }
    const mockBooking: Booking = { availabilityId: mockAvailability.id, schoolId: mockAuthenticationService.mockSchool.id}
    const mockBookingId: number = 500; // will be assigned after post
    service.bookings = [];
    service.mappedBookings = {};
    availabilityService.mappedAvailabilities = { }
    availabilityService.mappedAvailabilities[mockAvailability.id] = mockAvailability;

    // spies
    const serviceSpySubjectNext = spyOn(service.subjectBookings, "next");
    const httpServicePostSpy = spyOn(httpService, "post").and.returnValue(of(Object.assign({id: mockBookingId}, mockBooking)));
    const toastrMock = spyOn(toastrService, "success").and.stub();

    // act
    service.createBooking(mockAvailability);

    // assert
    expect(httpServicePostSpy).toHaveBeenCalledWith("booking", mockBooking);
    expect(mockAvailability.bookingId).toEqual(mockBookingId);
    expect(toastrMock).toHaveBeenCalledTimes(1);
    expect(serviceSpySubjectNext).toHaveBeenCalledTimes(1);
  })

  it('should remove the booking', () => {
    // arrange
    // get mocked services
    const mockAuthenticationService: MockAuthenticationService = new MockAuthenticationService();
    const service: BookingService = TestBed.get(BookingService);
    const httpService: HttpService = TestBed.get(HttpService);
    const availabilityService: AvailabilityService = TestBed.get(AvailabilityService);

    // mock data
    const date: Date = new Date(2020, 0, 1);
    const mockAvailability: Availability = { id: 5,  date: date, employeeId: 1, bookingId: undefined }
    const mockBooking: Booking = { id: 500, availabilityId: mockAvailability.id, schoolId: mockAuthenticationService.mockSchool.id}
    service.bookings = [mockBooking];
    service.mappedBookings = { };
    service.mappedBookings[mockBooking.id] = mockBooking;
    availabilityService.mappedAvailabilities = { }
    availabilityService.mappedAvailabilities[mockAvailability.id] = mockAvailability;

    // spies
    const serviceSpySubjectNext = spyOn(service.subjectBookings, "next");
    const httpServiceDeleteSpy = spyOn(httpService, "delete").and.returnValue(of(undefined));

    // act
    service.removeBooking(mockBooking);

    // assert
    expect(httpServiceDeleteSpy).toHaveBeenCalledWith("booking", mockBooking.id);
    expect(mockAvailability.bookingId).toEqual(undefined);
    expect(serviceSpySubjectNext).toHaveBeenCalledTimes(1);
  })
  it('should set the bookings', () => {
      // arrange:
      const service: BookingService = TestBed.get(BookingService);
      const bookings: Booking[] = [{id: 1, schoolId: 1, availabilityId: 1}, {id: 2, schoolId: 2, availabilityId: 2}]
      const serviceSpy = spyOn(service.subjectBookings, "next");

      // act:
      service.setBookings(bookings);

      // assert:
      expect(service.bookings.length).toBe(2);
      expect(service.mappedBookings[1]).toBe(bookings[0]);
      expect(service.mappedBookings[2]).toBe(bookings[1]);
      expect(serviceSpy).toHaveBeenCalledTimes(1);
  })
});
