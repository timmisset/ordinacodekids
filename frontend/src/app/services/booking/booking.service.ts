import { Injectable } from '@angular/core';
import {HttpService} from "../http/http.service";
import {Subject} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {MatDialog} from "@angular/material/dialog";
import {AuthenticationService} from "../authentication/authentication.service";
import {AvailabilityService} from "../availability/availability.service";

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  constructor(private httpService: HttpService,
              private toastr: ToastrService,
              private dialog: MatDialog,
              private authenticationService: AuthenticationService,
              private availabilityService: AvailabilityService
  ) {
  }

  bookings:Booking[];
  mappedBookings = {  };
  subjectBookings: Subject<Booking[]> =  new Subject<Booking[]>();

  setBookings(bookings:Booking[]) {
    this.bookings = bookings;
    this.mappedBookings = {};
    this.bookings.forEach(booking => this.mappedBookings[booking.id] = booking);
    this.subjectBookings.next(this.bookings);
  }

  removeBooking(booking:Booking) {
    this.httpService.delete("booking", booking.id).subscribe(() => {
      // disconnect the availability and booking relation
      this.availabilityService.mappedAvailabilities[booking.availabilityId].bookingId = undefined;
      this.availabilityService.setAvailabilities(this.availabilityService.availabilities);

      // remove the booking from the collection
      this.bookings = this.bookings.filter(bookingInCollection => bookingInCollection.id != booking.id);
      this.setBookings(this.bookings);
    })
    return true;
  }

  createBooking(availability:Availability) {
    const booking:Booking = {
      availabilityId: availability.id,
      schoolId: this.authenticationService.currentUserSubject.getValue().school.id
    };

    this.httpService.post("booking", booking).subscribe(booking => {
      // connect the availability and booking relation
      this.availabilityService.mappedAvailabilities[booking.availabilityId].bookingId = booking.id;
      this.availabilityService.setAvailabilities(this.availabilityService.availabilities);

      this.bookings.push(booking);
      this.setBookings(this.bookings);
      this.toastr.success("Booking created")
    });

  }

}
