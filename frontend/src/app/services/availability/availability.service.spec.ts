import { TestBed } from '@angular/core/testing';

import { AvailabilityService } from './availability.service';
import {Observable, of} from "rxjs";
import {HttpService} from "../http/http.service";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";

class MockHttp {
  get() { return of({ }); }
  post() {return of({ }); }
  put() {return of({ }); }
  delete() {return of(true)}
}
class MockDialog {
  open() { }
}

describe('AvailabilityService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: HttpService, useClass: MockHttp},
      {provide: MatDialog, useClass: MockDialog},
      {provide: ToastrService, useValue: { success: {}}}
    ]
  }));

  it('should be created', () => {
    const service: AvailabilityService = TestBed.get(AvailabilityService);
    expect(service).toBeTruthy();
  });

  it('should create the availability', () => {
    // arrange
    // mock data
    const date: Date = new Date(2020, 0, 1);
    const mockAvailability: Availability = { date: date, employeeId: 1, bookingId: 1 }

    // service instance
    const service: AvailabilityService = TestBed.get(AvailabilityService);
    service.availabilities = [];
    const serviceSpySubjectNext = spyOn(service.subjectAvailabilities, "next");

    // mock the httpService
    const httpService: HttpService = TestBed.get(HttpService);
    const httpServicePostSpy = spyOn(httpService, "post").and.returnValue(of(mockAvailability));

    // mock the toastrService
    const toastrService:ToastrService = TestBed.get(ToastrService);
    const toastrMock = spyOn(toastrService, "success").and.stub();

    // act
    service.createAvailability(mockAvailability);

    // assert
    expect(httpService.post).toHaveBeenCalledWith("availability", mockAvailability);
    expect(service.availabilities.length).toBe(1);
    expect(serviceSpySubjectNext).toHaveBeenCalledTimes(1);
    expect(toastrMock).toHaveBeenCalledTimes(1);

  })
  it('should set the availabilities', () => {
    // arrange:
    const service: AvailabilityService = TestBed.get(AvailabilityService);
    const availabilities: Availability[] = [{id: 1, date: new Date(), employeeId: 1, bookingId: 1}, {id: 2, date: new Date(), employeeId: 2, bookingId: 2}]
    const serviceSpy = spyOn(service.subjectAvailabilities, "next");

    // act:
    service.setAvailabilities(availabilities);

    // assert:
    expect(service.availabilities.length).toBe(2);
    expect(service.mappedAvailabilities[1]).toBe(availabilities[0]);
    expect(service.mappedAvailabilities[2]).toBe(availabilities[1]);
    expect(serviceSpy).toHaveBeenCalledTimes(1);

  })
  it('should remove the availabilities when confirmed by the user', async () => {
    // arrange:
    const service: AvailabilityService = TestBed.get(AvailabilityService);
    const availabilities: Availability[] = [{id: 1, date: new Date(), employeeId: 1, bookingId: 1}, {id: 2, date: new Date(), employeeId: 2, bookingId: 2}]
    const httpService: HttpService = TestBed.get(HttpService);
    const httpServiceSpy = spyOn(httpService, "delete").and.callThrough();

    const dialog: MatDialog = TestBed.get(MatDialog);
    const dialogRef = {
      afterClosed() : Observable<boolean> {
        return of(true);
      }
    }
    const dialogSpyOpen = spyOn(dialog, "open").and.returnValue(<MatDialogRef<any, any>>dialogRef);

    service.availabilities = availabilities;
    service.mappedAvailabilities = { 1: availabilities[0], 2: availabilities[1]}

    // act:
    const response = await service.removeAvailabilities(availabilities);


    // assert:
    expect(response).toBeTruthy();
    // the dialog open should have been called
    expect(dialogSpyOpen).toHaveBeenCalledTimes(1);
    // the stub will return an object with the afterClosed() method that returns an observable which is resolved to true
    // this will trigger the call to the backend:
    expect(httpServiceSpy).toHaveBeenCalledTimes(1);

    // expect the collections to be empty:
    expect(service.availabilities).toEqual([]);
    expect(service.mappedAvailabilities).toEqual({});
  })

  it('should remove the availabilities without confirmation when no booking is present', async () => {
    // arrange:
    const service: AvailabilityService = TestBed.get(AvailabilityService);
    const availabilities: Availability[] = [{id: 1, date: new Date(), employeeId: 1, bookingId: undefined}, {id: 2, date: new Date(), employeeId: 2, bookingId: undefined}]
    const httpService: HttpService = TestBed.get(HttpService);
    const httpServiceSpy = spyOn(httpService, "delete").and.callThrough();

    const dialog: MatDialog = TestBed.get(MatDialog);
    const dialogRef = {
      afterClosed() : Observable<boolean> {
        return of(true);
      }
    }
    const dialogSpyOpen = spyOn(dialog, "open").and.returnValue(<MatDialogRef<any, any>>dialogRef);

    service.availabilities = availabilities;
    service.mappedAvailabilities = { 1: availabilities[0], 2: availabilities[1]}

    // act:
    const response = await service.removeAvailabilities(availabilities);


    // assert:
    expect(response).toBeTruthy();
    // the dialog open should have been called
    expect(dialogSpyOpen).toHaveBeenCalledTimes(0);
    // the stub will return an object with the afterClosed() method that returns an observable which is resolved to true
    // this will trigger the call to the backend:
    expect(httpServiceSpy).toHaveBeenCalledTimes(1);

    // expect the collections to be empty:
    expect(service.availabilities).toEqual([]);
    expect(service.mappedAvailabilities).toEqual({});
  })

  it('shouldn\'t remove the availabilities when cancelled by the user', async () => {
    // arrange:
    const service: AvailabilityService = TestBed.get(AvailabilityService);
    const availabilities: Availability[] = [{id: 1, date: new Date(), employeeId: 1, bookingId: 1}, {id: 2, date: new Date(), employeeId: 2, bookingId: 2}]
    const httpService: HttpService = TestBed.get(HttpService);
    const httpServiceSpy = spyOn(httpService, "delete").and.callThrough();

    const dialog: MatDialog = TestBed.get(MatDialog);
    const dialogRef = {
      afterClosed() : Observable<boolean> {
        return of(false);
      }
    }
    const dialogSpyOpen = spyOn(dialog, "open").and.returnValue(<MatDialogRef<any, any>>dialogRef);

    service.mappedAvailabilities = { 1: availabilities[0], 2: availabilities[1]}
    service.availabilities = availabilities;

    // act:
    const response = await service.removeAvailabilities(availabilities);

    // assert:
    expect(response).toBeFalsy();
       // the dialog open should have been called
    expect(dialogSpyOpen).toHaveBeenCalledTimes(1);
    // the delete should not have been called
    expect(httpServiceSpy).toHaveBeenCalledTimes(0);
    expect(service.availabilities.length).toBe(2);
    expect(service.mappedAvailabilities).toEqual({ 1: availabilities[0], 2: availabilities[1]});
  })
});
