import {Injectable} from '@angular/core';
import {HttpService} from "../http/http.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogConfirmComponent} from "../../dialog-confirm/dialog-confirm.component";
import {Subject} from "rxjs";
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class AvailabilityService {

  constructor(private httpService: HttpService,
              private dialog: MatDialog,
              private toastr: ToastrService) {
  }

  availabilities: Availability[];
  subjectAvailabilities: Subject<Availability[]> = new Subject<Availability[]>();
  mappedAvailabilities: any;

  /**
   * Creates the new availability slot, no information on associated bookings will be stored.
   * Flow = Create availability, then book the slot
   * @param availability
   */
  createAvailability(availability: Availability) {
    const hours = availability.date.getHours() - availability.date.getTimezoneOffset() / 60;
    availability.date.setHours(hours);
    this.httpService.post("availability", availability).subscribe(value => {
      value.date = new Date(value.date);
      this.availabilities.push(value);
      this.subjectAvailabilities.next(this.availabilities);
      this.toastr.success("Availability added!");
    })
  }

  setAvailabilities(availabilities: Availability[]) {
    this.availabilities = availabilities;
    this.mappedAvailabilities = {};
    availabilities.forEach(availability => this.mappedAvailabilities[availability.id] = availability);
    this.subjectAvailabilities.next(this.availabilities);
  }

  async removeAvailabilities(availabilities: Availability[]): Promise<boolean> {
    if (availabilities.find(availability => (availability.bookingId))) {
      // ask for confirmation:
      const dialogRef = this.dialog.open(DialogConfirmComponent, {
        width: '500px',
        data: {
          title: "Are you sure?",
          description: "One or more items in your selection have an active booking, are you sure you wish to remove them?"
        }
      });
      const confirmed = await dialogRef.afterClosed().toPromise();
      if (!confirmed) { return false; }

      this.removeAvailabilitiesCall(availabilities);
      return true;

    } else {
      this.removeAvailabilitiesCall(availabilities);
      return true;
    }
  }

  private removeAvailabilitiesCall(availabilities: Availability[]) {
    this.httpService.delete("availability", availabilities.map(availability => availability.id)).subscribe((value) => {
      if(value) { this.removeAvailabilitiesFromCurrent(availabilities); }
    })
  }

  private removeAvailabilitiesFromCurrent(availabilities: Availability[]) {
    this.availabilities = this.availabilities.filter(availability => availabilities.indexOf(availability) === -1);
    this.setAvailabilities(this.availabilities);
  }
}
