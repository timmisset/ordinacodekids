import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DateAdapter } from 'angular-calendar';
import { CalendarModule } from 'angular-calendar';
import { CalendarComponent } from './calendar/calendar.component';
import {adapterFactory} from "angular-calendar/date-adapters/date-fns";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {CalendarService} from "./services/calendar/calendar.service";
import {AngularMaterialModule} from "./resources/angular-material.module";
import { CalendarEventComponent } from './calendar-event/calendar-event.component';
import {BasicAuthInterceptor} from "./interceptors/basic-auth.interceptor";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {ToastrModule} from "ngx-toastr";
import { DemobarComponent } from './demobar/demobar.component';
import { AvailabilityComponent } from './availability/availability.component';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';
import { EmployeeComponent } from './employee/employee.component';
import {ControlContainer} from "@angular/forms";
import {OrdinaDateAdapter} from "./util/ordina-date-adapter";
import { AnimatedIconComponent } from './animated-icon/animated-icon.component';
import { SchoolComponent } from './school/school.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    CalendarEventComponent,
    DemobarComponent,
    AvailabilityComponent,
    DialogConfirmComponent,
    AnimatedIconComponent,
    EmployeeComponent,
    SchoolComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    MatSnackBarModule,
    ToastrModule.forRoot(),
    AngularMaterialModule,
    HttpClientModule
  ],
  entryComponents: [
    DialogConfirmComponent
  ],
  providers: [
    CalendarService,
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    OrdinaDateAdapter
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
