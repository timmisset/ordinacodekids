import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { DemobarComponent } from './demobar.component';
import {HttpService} from "../services/http/http.service";
import {CalendarService} from "../services/calendar/calendar.service";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {ToastrService} from "ngx-toastr";
import {SchoolService} from "../services/school/school.service";
import {EmployeeService} from "../services/employee/employee.service";
import {AngularMaterialModule} from "../resources/angular-material.module";
import {User} from "../model/User";
import {Subject} from "rxjs";
import {Test} from "tslint";
import {By} from "@angular/platform-browser";
import {log} from "util";

class MockCalendarService {
  resetDemoData() {}
  toggleBookedSlots() {}
}
class MockAuthenticationService {
  currentUserSubject: Subject<User> = new Subject<User>();
  login(email, password) {
    const user:User = {
      email: email, password: password, role: ("ROLE_" + email).toUpperCase()
    }
    this.currentUserSubject.next(user);
  }
}
class MockEmployeeService {
  subjectEmployees: Subject<Employee[]> = new Subject<Employee[]>();
  employees: []
}
class MockSchoolService {
  subjectSchools: Subject<School[]> = new Subject<School[]>();
  schools: []
}

describe('DemobarComponent', () => {
  let component: DemobarComponent;
  let fixture: ComponentFixture<DemobarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AngularMaterialModule],
      declarations: [ DemobarComponent ],
      providers: [
        {provide: CalendarService, useClass: MockCalendarService},
        {provide: AuthenticationService, useClass: MockAuthenticationService},
        {provide: ToastrService, useValue: { success(){}, error(){} }},
        {provide: EmployeeService, useClass: MockEmployeeService },
        {provide: SchoolService, useClass: MockSchoolService },
        DemobarComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemobarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select the right school or employee after login', () => {
    // arrange
    const authService:AuthenticationService = TestBed.get(AuthenticationService);
    const mockSchool:School = {contactEmail: "school", contactName: undefined, description: undefined, name: undefined}
    const mockEmployee:Employee = { email: "employee", firstName: undefined, lastName: undefined, educationPackages: []}

    TestBed.get(SchoolService).schools = [mockSchool];
    TestBed.get(EmployeeService).employees = [mockEmployee];

    // act
    component.ngOnInit(); // subscribe to the subjects


    authService.login("school", "school"); // call the login
    const selectedSchool = component.selectedSchool;

    authService.login("employee", "employee"); // call the login

    const selectedEmployee = component.selectedEmployee;

    // assert
    expect(selectedSchool).toBe(mockSchool)
    expect(selectedEmployee).toBe(mockEmployee)

  })

  it('should reset the demo data if logged in as admin', () => {
    // arrange
    const calendarService:CalendarService = TestBed.get(CalendarService);
    fixture.debugElement.componentInstance.currentUser = { email: "admin", role: "ROLE_ADMIN"}
    const spyOnCalendarService = spyOn(calendarService, "resetDemoData");

    // act
    fixture.debugElement.query(By.css('button[data-button-id=btnResetDemoData]')).triggerEventHandler('click', undefined);

    // assert
    expect(spyOnCalendarService).toHaveBeenCalled();
  })
  it('shouldn\'t reset the demo data if logged in as NOT admin', fakeAsync(() => {
    // arrange
    const calendarService:CalendarService = TestBed.get(CalendarService);
    fixture.debugElement.componentInstance.currentUser = { email: "employee", role: "ROLE_EMPLOYEE"}

    const toastr:ToastrService = TestBed.get(ToastrService);
    const spyOnCalendarService = spyOn(calendarService, "resetDemoData");
    const spyOnToastr = spyOn(toastr, "error");

    // act
    fixture.debugElement.query(By.css('button[data-button-id=btnResetDemoData]')).triggerEventHandler('click', undefined);

    // assert

    expect(spyOnCalendarService).toHaveBeenCalledTimes(0);
    expect(spyOnToastr).toHaveBeenCalled();
    expect(component.adminButtonBadge).toEqual("!");

    tick(2500);

    expect(component.adminButtonBadge).toEqual("");

  }))
  it('should toggle the booked slots', () => {
    // arrange
    const calendarService:CalendarService = TestBed.get(CalendarService);
    const spyOnCalendarService = spyOn(calendarService, "toggleBookedSlots");

    // act
    fixture.debugElement.query(By.css('button[data-button-id=btnToggleBookedSlots]')).triggerEventHandler('click', undefined);

    // assert
    expect(spyOnCalendarService).toHaveBeenCalled();

  })
  it('should login as a role', () => {
    // arrange
    component.selectedEmployee = { email: "employee@email.com", educationPackages: undefined, lastName: undefined, firstName: undefined}
    component.selectedSchool = { contactEmail: "school@email.com", name: undefined, description: undefined, contactName: undefined }
    const auth: AuthenticationService = TestBed.get(AuthenticationService);
    const spyOnAuth = spyOn(auth, "login");

    // act
    component.login("employee")
    component.login("school")
    component.login("admin")

    // assert
    expect(spyOnAuth).toHaveBeenCalledTimes(3);
    expect(spyOnAuth).toHaveBeenCalledWith("employee@email.com", "employee");
    expect(spyOnAuth).toHaveBeenCalledWith("school@email.com", "school");
    expect(spyOnAuth).toHaveBeenCalledWith("admin", "admin");
  })
  it('should respond to subscriptions', () => {
    // arrange
    const employeeService:EmployeeService = TestBed.get(EmployeeService);
    const schoolService:SchoolService = TestBed.get(SchoolService);

    component.schools = [];
    component.employees = [];
    const mockSchool:School = {contactEmail: "school", contactName: undefined, description: undefined, name: undefined}
    const mockEmployee:Employee = { email: "employee", firstName: undefined, lastName: undefined, educationPackages: []}

    component.ngOnInit(); // subscribe

    // act
    employeeService.subjectEmployees.next([mockEmployee]);
    schoolService.subjectSchools.next([mockSchool]);

    // assert
    expect(component.schools).toEqual([mockSchool]);
    expect(component.employees).toEqual([mockEmployee]);
  })
});
