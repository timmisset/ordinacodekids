import { Component, OnInit } from '@angular/core';
import {User} from "../model/User";
import {CalendarService} from "../services/calendar/calendar.service";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {ToastrService} from "ngx-toastr";
import {EmployeeService} from "../services/employee/employee.service";
import {SchoolService} from "../services/school/school.service";

@Component({
  selector: 'app-demobar',
  templateUrl: './demobar.component.html',
  styleUrls: ['./demobar.component.css']
})
export class DemobarComponent implements OnInit {

  schools:School[] = [];
  employees:Employee[] = [];
  selectedSchool:School;
  selectedEmployee:Employee;
  currentUser: User;
  adminButtonBadge: any;

  constructor(private calendarService:CalendarService,
              private authenticationService:AuthenticationService,
              private toastr:ToastrService,
              private employeeService: EmployeeService,
              private schoolService:SchoolService

  ) {

  }

  ngOnInit() {
    this.employees = this.employeeService.employees;
    this.schools = this.schoolService.schools;

    this.employeeService.subjectEmployees.subscribe(employees => this.employees = employees);
    this.schoolService.subjectSchools.subscribe(schools => this.schools = schools);
    this.authenticationService.currentUserSubject.subscribe(user => {
      console.log('user', user);
      this.setCurrentUserforDemoToolbar(user)
    });


  }

  private setCurrentUserforDemoToolbar(user:User) {
    if(user) {
      this.currentUser = user;
      if(this.currentUser.role === "ROLE_SCHOOL") {
        this.selectedSchool = this.schools.filter(school => school.contactEmail === user.email)[0];
      }
      if(this.currentUser.role === "ROLE_EMPLOYEE") {
        this.selectedEmployee = this.employees.filter(employee => employee.email === user.email)[0];
      }
    }

  }

  resetDemo() {
    if(this.currentUser.role === "ROLE_ADMIN") {
      this.calendarService.resetDemoData();
    }
    else {
      this.toastr.error("You must first become the admin user by clicking the 'Admin'", "", { positionClass : "toast-top-center"});
      this.adminButtonBadge = "!";
      setTimeout(() => {this.adminButtonBadge = ""}, 2500);
    }
  }
  toggleBookedSlots() {
    this.calendarService.toggleBookedSlots();
  }
  login(type) {
    if(type === 'employee') { this.authenticationService.login(this.selectedEmployee.email, 'employee')}
    if(type === 'school') { this.authenticationService.login(this.selectedSchool.contactEmail, 'school')}
    if(type === 'admin') { this.authenticationService.login('admin', 'admin')}
  }
}
