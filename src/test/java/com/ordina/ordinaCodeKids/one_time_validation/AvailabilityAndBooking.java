package com.ordina.ordinaCodeKids.one_time_validation;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityRepository;
import com.ordina.ordinaCodeKids.booking.Booking;
import com.ordina.ordinaCodeKids.booking.BookingRepository;
import com.ordina.ordinaCodeKids.education_package.EducationPackageRepository;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.employee.EmployeeRepository;
import com.ordina.ordinaCodeKids.school.School;
import com.ordina.ordinaCodeKids.school.SchoolRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * The goal if this test is to see if the database relationship and cascading works as expected
 * Can be disabled once successfully tested since it's only validating expected JpaRepository behavior
 * based on the column mappings and cascading settings
 */
@SpringBootTest
public class AvailabilityAndBooking {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    AvailabilityRepository availabilityRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    EducationPackageRepository educationPackageRepository;

    private final LocalDate date = LocalDate.of(2020, 1, 10);

    @Tag("OneTimeValidationTest")
    @Tag("IntegrationTest")
    @Test()
    @Transactional()
    void availabilityAndBooking() {
        Employee mockEmployee = MockData.getMockEmployee();
        mockEmployee.getEducationPackages().clear(); // not needed for this test
        mockEmployee.setId(0); // should be assigned by the repository
        School mockSchool = MockData.getMockSchool();

        employeeRepository.save(mockEmployee);
        schoolRepository.save(mockSchool);

        // create the availability without a booking
        Availability availability = Availability.AvailabilityBuilder.anAvailability()
                .employee(mockEmployee)
                .date(date)
                .build();
        availabilityRepository.save(availability);
        Optional<Availability> optionalAvailability = availabilityRepository.findById(availability.getId());
        assertTrue(optionalAvailability.isPresent());

        // create the booking for the availability
        Booking booking = Booking.BookingBuilder.aBooking()
                .availability(availability)
                .school(mockSchool)
                .build();
        bookingRepository.save(booking);

        availability.setBooking(booking); // this is done by the booking service in the normal flow

        assertEquals(booking.getAvailability(), availability);
        Optional<Booking> optionalBooking = bookingRepository.findById(booking.getId());
        assertTrue(optionalBooking.isPresent());

        // check if removing the availability now clears the booking:
        availabilityRepository.delete(availability);

        optionalAvailability = availabilityRepository.findById(availability.getId());
        assertTrue(optionalAvailability.isEmpty());
        optionalBooking = bookingRepository.findById(booking.getId());
        assertTrue(optionalBooking.isEmpty());
    }

    @Tag("OneTimeValidationTest")
    @Tag("IntegrationTest")
    @Test()
    @Transactional()
    void employeeAndAvailability() {
        Employee mockEmployee = MockData.getMockEmployee();
        mockEmployee.getEducationPackages().clear(); // not needed for this test
        mockEmployee.setId(0); // should be assigned by the repository
        employeeRepository.save(mockEmployee);

        // create the availability without a booking
        Availability availability = Availability.AvailabilityBuilder.anAvailability()
                .employee(mockEmployee)
                .date(date)
                .build();

        Optional<Employee> optionalEmployee = employeeRepository.findById(mockEmployee.getId());
        assertTrue(optionalEmployee.isPresent());
        Optional<Availability> optionalAvailability = availabilityRepository.findById(availability.getId());
        assertTrue(optionalAvailability.isEmpty());

        availabilityRepository.save(availability);
        optionalAvailability = availabilityRepository.findById(availability.getId());
        assertTrue(optionalAvailability.isPresent());
        assertTrue(mockEmployee.getAvailabilities().contains(availability));

        // now remove the employee and check if the associated availability is also removed:
        employeeRepository.delete(mockEmployee);

        optionalEmployee = employeeRepository.findById(mockEmployee.getId());
        assertTrue(optionalEmployee.isEmpty());

        optionalAvailability = availabilityRepository.findById(availability.getId());
        assertTrue(optionalAvailability.isEmpty());


    }


}
