package com.ordina.ordinaCodeKids.user;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.UserAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserControllerTest {

    UserController userController;
    UserService userService;
    List<User> mockUsers;
    User mockUser;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(mock(UserRepository.class), mock(EmailService.class));
        userService = spy(userService);
        userController = new UserController(userService);

        mockUsers = MockData.getMockUsers();
        mockUser = MockData.getMockUser();
    }

    @Test
    void getUsers() {
        // arrange
        doReturn(mockUsers).when(userService).getUsers();

        // act
        ResponseEntity<List<UserDTO>> response = userController.getUsers();

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockUsers.size(), response.getBody().size());
        verify(userService, times(1)).getUsers();
    }

    @Test
    void getUser() throws UserNotFoundException {
        // arrange
        doReturn(mockUser).when(userService).getUserById(anyLong());

        // act
        ResponseEntity<UserDTO> response = userController.getUser(0);

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockUser.getEmail(), response.getBody().getEmail());
        verify(userService, times(1)).getUserById(anyLong());
    }

    @Test
    void getUserThrowsUserNotFoundException() throws UserNotFoundException {
        // arrange
        UserNotFoundException exception = new UserNotFoundException(mockUser.getId());
        doThrow(exception).when(userService).getUserById(anyLong());

        // act assert
        try {
            userController.getUser(mockUser.getId());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void createUser() throws UserAlreadyExistsException {
        // arrange
        long newUserId = 11;
        doAnswer(invocationOnMock -> {
            User user = invocationOnMock.getArgument(0);
            user.setId(newUserId);
            return user;
        }).when(userService).createUser(any());

        // act
        ResponseEntity<UserDTO> response = userController.createUser(new UserDTO());
        UserDTO createdUser = response.getBody();

        // assert
        assertNotNull(createdUser);
        assertEquals(newUserId, createdUser.getId());
        verify(userService, times(1)).createUser(any());
    }

    @Test
    void createUserThrowsUserAlreadyExistsException() throws UserAlreadyExistsException {
        // arrange
        UserAlreadyExistsException exception = new UserAlreadyExistsException(mockUser);
        doThrow(exception).when(userService).createUser(any());

        // act assert
        try {
            userController.createUser(new UserDTO());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void updateUser() throws UserNotFoundException {
        // arrange
        long newUserId = 11;
        doAnswer(invocationOnMock -> {
            User user = invocationOnMock.getArgument(0);
            user.setId(newUserId);
            return user;
        }).when(userService).updateUser(any());

        // act
        ResponseEntity<UserDTO> response = userController.updateUser(0, new UserDTO());
        UserDTO updatedUser = response.getBody();

        // assert
        assertNotNull(updatedUser);
        assertEquals(newUserId, updatedUser.getId());
        verify(userService, times(1)).updateUser(any());
    }

    @Test
    void updateUserThrowsUserNotFoundException() throws UserNotFoundException {
        // arrange
        UserNotFoundException exception = new UserNotFoundException(mockUser.getId());
        doThrow(exception).when(userService).getUserById(anyLong());

        long mockUserId = mockUser.getId();
        UserDTO mockUserDTO = UserDTO.UserDTOBuilder.anUserDTO().id(mockUserId).build();

        // act assert
        try {
            userController.updateUser(mockUserId, mockUserDTO);
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void deleteUser() throws UserNotFoundException {
        // arrange
        long mockUserId = mockUser.getId();
        doNothing().when(userService).deleteUserById(mockUserId);

        // act
        userController.deleteUser(mockUserId);

        // assert
        verify(userService, times(1)).deleteUserById(mockUserId);
    }

    @Test
    void deleteUserThrowsUserNotFoundException() throws UserNotFoundException {
        // arrange
        UserNotFoundException exception = new UserNotFoundException(mockUser.getId());
        doThrow(exception).when(userService).getUserById(anyLong());

        long mockUserId = mockUser.getId();

        // act assert
       try {
           userController.deleteUser(mockUserId);
       }catch( ResponseStatusException e) {
           assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
           assertEquals(exception.getMessage(), e.getReason());
       }
    }

    @Test
    void updateUserPassword() throws UserNotFoundException {
        // arrange
        UserDTO userDTO = MockData.getMockUserDTO();
        doNothing().when(userService).setUserPassword(anyString(), anyString());

        // act
        ResponseEntity<UserDTO> response = userController.updateUserPassword(userDTO);

        // assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody().getPassword());

    }

    @Test
    void updateUserPasswordThrowsUserNotFoundException() throws UserNotFoundException {
        // arrange
        UserDTO userDTO = MockData.getMockUserDTO();
        doThrow(UserNotFoundException.class).when(userService).setUserPassword(anyString(), anyString());

        try {
            // act
            ResponseEntity<UserDTO> response = userController.updateUserPassword(userDTO);
        }
        catch(ResponseStatusException e) {
            // assert
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
        }

    }

    @Test
    void getUserFromLogin() throws UserNotFoundException {
        // arrange
        doReturn(mockUser).when(userService).getCurrentUser();
        doReturn(mockUser).when(userService).getUserById(mockUser.getId());

        // act
        ResponseEntity<UserDTO> response = userController.getUser();

        // assert
        assertEquals(mockUser.getId(), response.getBody().getId());
        assertEquals(mockUser.getEmail(), response.getBody().getEmail());
        assertEquals(mockUser.getRole(), response.getBody().getRole());
        assertEquals(null, response.getBody().getPassword());
    }
}
