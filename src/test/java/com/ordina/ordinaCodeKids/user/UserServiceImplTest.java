package com.ordina.ordinaCodeKids.user;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.UserAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    private UserRepository userRepository;
    private UserService userService;
    private EmailService emailService;
    private List<User> mockUsers;
    private User mockUser;

    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        emailService = mock(EmailService.class);
        userService = new UserServiceImpl(userRepository, emailService);

        mockUser = MockData.getMockUser();
        mockUsers = MockData.getMockUsers();
    }

    @Test
    // check that the saved user is returned and no exceptions are thrown
    void createUser() throws UserAlreadyExistsException {
        // arrange
        doReturn(false).when(userRepository).existsById(anyLong());
        doReturn(mockUser).when(userRepository).save(any());


        // act
        User addedUser = userService.createUser(mockUser);

        // assert
        assertEquals(mockUser, addedUser);
        verify(userRepository, times(1)).existsById(anyLong());
        verify(emailService, times(2)).sendSimpleMessage(eq(mockUser.getEmail()), anyString(), anyString());
    }

    @Test
    // check that the exception is correctly thrown when the user already exists
    void createUserThrowsUserAlreadyExistsException() {
        // arrange
        doReturn(true).when(userRepository).existsById(anyLong());

        // act assert
        assertThrows(UserAlreadyExistsException.class, () -> userService.createUser(mockUser));
        verify(emailService, times(0)).sendSimpleMessage(eq(mockUser.getEmail()), anyString(), anyString());
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteUser() throws UserNotFoundException {
        // arrange
        doReturn(true).when(userRepository).existsById(anyLong());

        // act
        userService.deleteUser(mockUser);

        // assert
        verify(userRepository, times(1)).deleteById(mockUser.getId());
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteUserById() throws UserNotFoundException {
        // arrange
        doReturn(true).when(userRepository).existsById(anyLong());

        // act
        userService.deleteUserById(mockUser.getId());

        // assert
        verify(userRepository, times(1)).deleteById(mockUser.getId());
    }

    @Test
        // check that the repository is called with the correct id for removal
    void deleteUserByIdThrowsUserNotFoundException() {
        // arrange
        doReturn(false).when(userRepository).existsById(anyLong());

        // act assert
        assertThrows(UserNotFoundException.class, () -> userService.deleteUser(mockUser));
    }

    @Test
        // check that the repository is called with the correct id for removal
    void deleteUserByEmail() throws UserNotFoundException {
        // arrange
        doReturn(true).when(userRepository).existsById(anyLong());
        doReturn(Optional.of(mockUser)).when(userRepository).findOneByEmail(anyString());

        // act
        userService.deleteUserByEmail(mockUser.getEmail());

        // assert
        verify(userRepository, times(1)).findOneByEmail(mockUser.getEmail());
        verify(userRepository, times(1)).deleteById(mockUser.getId());
    }

    @Test
        // check that the repository is called with the correct id for removal
    void deleteUserByEmailThrowsUserNotFoundException() {
        // arrange
        doReturn(false).when(userRepository).existsById(anyLong());

        // act assert
        assertThrows(UserNotFoundException.class, () -> userService.deleteUser(mockUser));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteUserThrowsUserNotFoundException() {
        // arrange
        doReturn(false).when(userRepository).existsById(anyLong());

        // act assert
        assertThrows(UserNotFoundException.class, () -> userService.deleteUser(mockUser));
    }

    @Test
    // check that the repository is called with item to save
    void updateUser() throws UserNotFoundException {
        // arrange
        doReturn(mockUser).when(userRepository).save(any());
        doReturn(true).when(userRepository).existsById(anyLong());

        // act
        User savedUser = userService.updateUser(mockUsers.get(0));

        // assert
        assertEquals(mockUser, savedUser);
        verify(emailService, times(1)).sendSimpleMessage(eq(mockUser.getEmail()), anyString(), anyString());
    }

    @Test
        // check that the repository is called with item to save
    void updateUserThrowsUserNotFoundException() {
        // arrange
        doReturn(false).when(userRepository).existsById(anyLong());

        // act assert
        assertThrows(UserNotFoundException.class, () -> userService.updateUser(mockUser));
    }

    @Test
    // check that the user is returned when the repository finds it by the provided id
    void getUserById() throws UserNotFoundException {
        // arrange
        doReturn(Optional.of(mockUser)).when(userRepository).findById(any());

        // act
        User returnedUser = userService.getUserById(0);

        // assert
        assertEquals(returnedUser, mockUser);
        verify(userRepository, times(1)).findById(any());
    }

    @Test
    // check that the correct exception is thrown when the repository returns an empty optional
    void getUserByIdThrowsUserNotFoundException() {
        // arrange
        doReturn(Optional.empty()).when(userRepository).findById(any());

        // act, assert
        assertThrows(UserNotFoundException.class, () -> userService.getUserById(0));
    }

    @Test
    // check that all the users are returned
    void getUsers() {
        // arrange
        doReturn(mockUsers).when(userRepository).findAll();

        // act
        List<User> returnedUsers = userService.getUsers();

        // assert
        assertEquals(mockUsers, returnedUsers);
        verify(userRepository, times(1)).findAll();

    }

    @Test
    // check if the mapping is performed correctly
    void toUserMapping() {
        // arrange
        UserDTO userDTO = UserDTO.UserDTOBuilder.anUserDTO()
                .email(mockUser.getEmail())
                .password(mockUser.getPassword())
                .id(mockUser.getId())
                .build();

        // act
        User user = userService.toUserMapping(userDTO);

        // assert
        assertEquals(userDTO.getEmail(), user.getEmail());
        assertEquals(userDTO.getRole(), user.getRole());
        assertEquals(userDTO.getId(), user.getId());
    }

    @Test
    void fromUserMapping() {
        // arrange

        // act
        UserDTO userDTO = userService.fromUserMapping(mockUser);

        // assert
        assertEquals(mockUser.getEmail(), userDTO.getEmail());
        assertEquals(mockUser.getRole(), userDTO.getRole());
        assertNull(userDTO.getPassword());
        assertEquals(mockUser.getId(), userDTO.getId());

    }

    @Test
    void setUserPassword() throws UserNotFoundException {
        // arrange
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String rawPassword = "password";

        doReturn(Optional.of(mockUser)).when(userRepository).findOneByEmail(anyString());

        // act
        userService.setUserPassword("", rawPassword);

        // assert
        assertTrue(encoder.matches(rawPassword, mockUser.getPassword()));

    }

    @Test
    void setAdminUser() {
        // arrange
        final List<User> addedUsers = new ArrayList<>();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        doReturn(Optional.empty()).when(userRepository).findByRole(anyString());
        doAnswer(invocationOnMock -> {
            User user = invocationOnMock.getArgument(0);
            addedUsers.add(user);
            return user;
        }).when(userRepository).save(any(User.class));

        // set the field values that are obtained using spring config
        ReflectionTestUtils.setField(userService, "adminEmail", "admin");
        ReflectionTestUtils.setField(userService, "adminPassword", "admin");

        // act
        userService.setAdminUser();
        User addedUser = addedUsers.get(0);

        // assert
        verify(userRepository, times(1)).save(any(User.class));
        assertEquals("admin", addedUser.getEmail());
        assertTrue(encoder.matches("admin", addedUser.getPassword()));
        assertEquals("ROLE_ADMIN", addedUser.getRole());

    }

 }
