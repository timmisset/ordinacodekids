package com.ordina.ordinaCodeKids.school;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.exception.SchoolAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.SchoolNotFoundException;
import com.ordina.ordinaCodeKids.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class SchoolControllerTest {

    SchoolController schoolController;
    SchoolService schoolService;
    List<School> mockSchools;
    School mockSchool;

    @BeforeEach
    void setUp() {
        schoolService = new SchoolServiceImpl(mock(SchoolRepository.class), mock(UserService.class));
        schoolService = spy(schoolService);
        schoolController = new SchoolController(schoolService);

        mockSchools = MockData.getMockSchools();
        mockSchool = MockData.getMockSchool();
    }

    @Test
    void getSchools() {
        // arrange
        doReturn(mockSchools).when(schoolService).getSchools();

        // act
        ResponseEntity<List<SchoolDTO>> response = schoolController.getSchools();

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockSchools.size(), response.getBody().size());
        verify(schoolService, times(1)).getSchools();
    }

    @Test
    void getSchool() throws SchoolNotFoundException {
        // arrange
        doReturn(mockSchool).when(schoolService).getSchoolById(anyLong());

        // act
        ResponseEntity<SchoolDTO> response = schoolController.getSchool(0);

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockSchool.getName(), response.getBody().getName());
        verify(schoolService, times(1)).getSchoolById(anyLong());
    }

    @Test
    void getSchoolThrowsSchoolNotFoundException() throws SchoolNotFoundException {
        // arrange
        SchoolNotFoundException exception = new SchoolNotFoundException(mockSchool.getId());
        doThrow(exception).when(schoolService).getSchoolById(anyLong());

        // act assert
        try {
            schoolController.getSchool(mockSchool.getId());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void createSchool() throws SchoolAlreadyExistsException {
        // arrange
        long newSchoolId = 11;
        doAnswer(invocationOnMock -> {
            School school = invocationOnMock.getArgument(0);
            school.setId(newSchoolId);
            return school;
        }).when(schoolService).createSchool(any());

        // act
        ResponseEntity<SchoolDTO> response = schoolController.createSchool(new SchoolDTO());
        SchoolDTO createdSchool = response.getBody();

        // assert
        assertNotNull(createdSchool);
        assertEquals(newSchoolId, createdSchool.getId());
        verify(schoolService, times(1)).createSchool(any());
    }

    @Test
    void createSchoolThrowsSchoolAlreadyExistsException() throws SchoolAlreadyExistsException {
        // arrange
        SchoolAlreadyExistsException exception = new SchoolAlreadyExistsException(mockSchool);
        doThrow(exception).when(schoolService).createSchool(any());

        // act assert
        try {
            schoolController.createSchool(new SchoolDTO());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void updateSchool() throws SchoolNotFoundException {
        // arrange
        long newSchoolId = 11;
        doAnswer(invocationOnMock -> {
            School school = invocationOnMock.getArgument(0);
            school.setId(newSchoolId);
            return school;
        }).when(schoolService).updateSchool(any());

        // act
        ResponseEntity<SchoolDTO> response = schoolController.updateSchool(0, new SchoolDTO());
        SchoolDTO updatedSchool = response.getBody();

        // assert
        assertNotNull(updatedSchool);
        assertEquals(newSchoolId, updatedSchool.getId());
        verify(schoolService, times(1)).updateSchool(any());
    }

    @Test
    void updateSchoolThrowsSchoolNotFoundException() throws SchoolNotFoundException {
        // arrange
        SchoolNotFoundException exception = new SchoolNotFoundException(mockSchool.getId());
        doThrow(exception).when(schoolService).getSchoolById(anyLong());

        long mockSchoolId = mockSchool.getId();
        SchoolDTO mockSchoolDTO = SchoolDTO.SchoolDTOBuilder.aSchoolDTO().id(mockSchoolId).build();

        // act assert
        try {
            schoolController.updateSchool(mockSchoolId, mockSchoolDTO);
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void deleteSchool() throws SchoolNotFoundException {
        // arrange
        long mockSchoolId = mockSchool.getId();
        doNothing().when(schoolService).deleteSchoolById(mockSchoolId);

        // act
        schoolController.deleteSchool(mockSchoolId);

        // assert
        verify(schoolService, times(1)).deleteSchoolById(mockSchoolId);
    }

    @Test
    void deleteSchoolThrowsSchoolNotFoundException() throws SchoolNotFoundException {
        // arrange
        SchoolNotFoundException exception = new SchoolNotFoundException(mockSchool.getId());
        doThrow(exception).when(schoolService).getSchoolById(anyLong());

        long mockSchoolId = mockSchool.getId();

        // act assert
       try {
           schoolController.deleteSchool(mockSchoolId);
       }catch( ResponseStatusException e) {
           assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
           assertEquals(exception.getMessage(), e.getReason());
       }
    }
}
