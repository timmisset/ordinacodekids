package com.ordina.ordinaCodeKids.school;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.exception.SchoolAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.SchoolNotFoundException;
import com.ordina.ordinaCodeKids.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class SchoolServiceImplTest {

    private SchoolRepository schoolRepository;
    private SchoolService schoolService;
    private UserService userService;
    private List<School> mockSchools;
    private School mockSchool;

    @BeforeEach
    void setUp() {
        schoolRepository = mock(SchoolRepository.class);
        userService = mock(UserService.class);
        schoolService = new SchoolServiceImpl(schoolRepository, userService);

        mockSchool = MockData.getMockSchool();
        mockSchools = MockData.getMockSchools();
    }

    @Test
    // check that the saved school is returned and no exceptions are thrown
    void createSchool() throws SchoolAlreadyExistsException {
        // arrange
        doReturn(false).when(schoolRepository).existsById(anyLong());
        doReturn(mockSchool).when(schoolRepository).save(any());


        // act
        School addedSchool = schoolService.createSchool(mockSchool);

        // assert
        assertEquals(mockSchool, addedSchool);
        verify(schoolRepository, times(1)).existsById(anyLong());
    }

    @Test
    // check that the exception is correctly thrown when the school already exists
    void createSchoolThrowsSchoolAlreadyExistsException() {
        // arrange
        doReturn(true).when(schoolRepository).existsById(anyLong());

        // act assert
        assertThrows(SchoolAlreadyExistsException.class, () -> schoolService.createSchool(mockSchool));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteSchool() throws SchoolNotFoundException {
        // arrange
        doReturn(true).when(schoolRepository).existsById(anyLong());
        doReturn(mockSchool).when(schoolRepository).getOne(mockSchool.getId());

        // act
        schoolService.deleteSchool(mockSchool);

        // assert
        verify(schoolRepository, times(1)).deleteById(mockSchool.getId());
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteSchoolById() throws SchoolNotFoundException {
        // arrange
        doReturn(true).when(schoolRepository).existsById(anyLong());
        doReturn(mockSchool).when(schoolRepository).getOne(mockSchool.getId());

        // act
        schoolService.deleteSchoolById(mockSchool.getId());

        // assert
        verify(schoolRepository, times(1)).deleteById(mockSchool.getId());
    }

    @Test
        // check that the repository is called with the correct id for removal
    void deleteSchoolByIdThrowsSchoolNotFoundException() {
        // arrange
        doReturn(false).when(schoolRepository).existsById(anyLong());

        // act assert
        assertThrows(SchoolNotFoundException.class, () -> schoolService.deleteSchool(mockSchool));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteSchoolThrowsSchoolNotFoundException() {
        // arrange
        doReturn(false).when(schoolRepository).existsById(anyLong());

        // act assert
        assertThrows(SchoolNotFoundException.class, () -> schoolService.deleteSchool(mockSchool));
    }

    @Test
    // check that the repository is called with item to save
    void updateSchool() throws SchoolNotFoundException {
        // arrange
        doReturn(mockSchool).when(schoolRepository).save(any());
        doReturn(true).when(schoolRepository).existsById(anyLong());

        // act
        School savedSchool = schoolService.updateSchool(mockSchools.get(0));

        // assert
        assertEquals(mockSchool, savedSchool);
    }

    @Test
        // check that the repository is called with item to save
    void updateSchoolThrowsSchoolNotFoundException() {
        // arrange
        doReturn(false).when(schoolRepository).existsById(anyLong());

        // act assert
        assertThrows(SchoolNotFoundException.class, () -> schoolService.updateSchool(mockSchool));
    }

    @Test
    // check that the school is returned when the repository finds it by the provided id
    void getSchoolById() throws SchoolNotFoundException {
        // arrange
        doReturn(Optional.of(mockSchool)).when(schoolRepository).findById(any());

        // act
        School returnedSchool = schoolService.getSchoolById(0);

        // assert
        assertEquals(returnedSchool, mockSchool);
        verify(schoolRepository, times(1)).findById(any());
    }

    @Test
    // check that the correct exception is thrown when the repository returns an empty optional
    void getSchoolByIdThrowsSchoolNotFoundException() {
        // arrange
        doReturn(Optional.empty()).when(schoolRepository).findById(any());

        // act, assert
        assertThrows(SchoolNotFoundException.class, () -> schoolService.getSchoolById(0));
    }

    @Test
    // check that all the schools are returned
    void getSchools() {
        // arrange
        doReturn(mockSchools).when(schoolRepository).findAll();

        // act
        List<School> returnedSchools = schoolService.getSchools();

        // assert
        assertEquals(mockSchools, returnedSchools);
        verify(schoolRepository, times(1)).findAll();

    }

    @Test
    // check if the mapping is performed correctly
    void toSchoolMapping() {
        // arrange
        SchoolDTO schoolDTO = SchoolDTO.SchoolDTOBuilder.aSchoolDTO()
                .contactEmail(mockSchool.getContactEmail())
                .description(mockSchool.getDescription())
                .id(mockSchool.getId())
                .contactName(mockSchool.getContactName())
                .name(mockSchool.getName())
                .build();

        // act
        School school = schoolService.toSchoolMapping(schoolDTO);

        // assert
        assertEquals(schoolDTO.getContactEmail(), school.getContactEmail());
        assertEquals(schoolDTO.getContactName(), school.getContactName());
        assertEquals(schoolDTO.getDescription(), school.getDescription());
        assertEquals(schoolDTO.getName(), school.getName());
        assertEquals(schoolDTO.getId(), school.getId());
    }

    @Test
    void fromSchoolMapping() {
        // arrange

        // act
        SchoolDTO schoolDTO = schoolService.fromSchoolMapping(mockSchool);

        // assert
        assertEquals(mockSchool.getContactEmail(), schoolDTO.getContactEmail());
        assertEquals(mockSchool.getContactName(), schoolDTO.getContactName());
        assertEquals(mockSchool.getDescription(), schoolDTO.getDescription());
        assertEquals(mockSchool.getName(), schoolDTO.getName());
        assertEquals(mockSchool.getId(), schoolDTO.getId());

    }
}
