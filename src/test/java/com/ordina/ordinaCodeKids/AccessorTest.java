package com.ordina.ordinaCodeKids;


import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityDTO;
import com.ordina.ordinaCodeKids.booking.Booking;
import com.ordina.ordinaCodeKids.booking.BookingDTO;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.education_package.EducationPackageDTO;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.employee.EmployeeDTO;
import com.ordina.ordinaCodeKids.school.School;
import com.ordina.ordinaCodeKids.school.SchoolDTO;
import org.junit.jupiter.api.Test;

/**
 * Accessor test to get to 100% code coverage
 */
public class AccessorTest {
    private static final Validator ACCESSOR_VALIDATOR = ValidatorBuilder.create()
            .with(new GetterTester())
            .with(new SetterTester())
            .build();

    public static void validateAccessors(final Class<?> clazz) {
        ACCESSOR_VALIDATOR.validate(PojoClassFactory.getPojoClass(clazz));
    }

    @Test
    public void testAvailability() {
        validateAccessors(Availability.class);
        validateAccessors(AvailabilityDTO.class);
    }

    @Test
    public void testBooking() {
        validateAccessors(Booking.class);
        validateAccessors(BookingDTO.class);
    }

    @Test
    public void testEducationPackage() {
        validateAccessors(EducationPackage.class);
        validateAccessors(EducationPackageDTO.class);
    }

    @Test
    public void testEmployee() {
        validateAccessors(Employee.class);
        validateAccessors(EmployeeDTO.class);
    }

    @Test
    public void testSchool() {
        validateAccessors(School.class);
        validateAccessors(SchoolDTO.class);
    }
}

