package com.ordina.ordinaCodeKids;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class OrdinaCodeKidsApplicationTests {

	@Test
	@Tag("IntegrationTest")
	void contextLoads() {
	}

}
