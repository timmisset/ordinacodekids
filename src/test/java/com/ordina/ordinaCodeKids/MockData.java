package com.ordina.ordinaCodeKids;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityDTO;
import com.ordina.ordinaCodeKids.booking.Booking;
import com.ordina.ordinaCodeKids.booking.BookingDTO;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.education_package.EducationPackageDTO;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.employee.EmployeeDTO;
import com.ordina.ordinaCodeKids.school.School;
import com.ordina.ordinaCodeKids.user.User;
import com.ordina.ordinaCodeKids.user.UserDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MockData {

    private static long id = 1;


    // /////////////////////////////////////////////////////////////////////////////////////////
    // Education Packages
    // /////////////////////////////////////////////////////////////////////////////////////////
    public static List<EducationPackage> getMockEducationPackages() {
        List<EducationPackage> mockEducationPackages = new ArrayList<>();
        mockEducationPackages.add(getMockEducationPackage());

        return mockEducationPackages;
    }

    public static EducationPackage getMockEducationPackage() {
        return EducationPackage.EducationPackageBuilder.anEducationPackage()
                .id(id++)
                .name("Coding")
                .description("A full coding experience")
                .build();
    }

    public static EducationPackageDTO getMockEducationPackageDTO() {
        return EducationPackageDTO.EducationPackageDTOBuilder.anEducationPackageDTO()
                .id(id++)
                .name("Coding")
                .description("A full coding experience")
                .build();
    }

    // /////////////////////////////////////////////////////////////////////////////////////////
    // School
    // /////////////////////////////////////////////////////////////////////////////////////////
    public static School getMockSchool() {
        return School.SchoolBuilder.aSchool()
                .contactName("Contact")
                .contactEmail("email@email.com")
                .name("School name")
                .description("A super fun school")
                .id(id++)
                .build();
    }
    public static List<School> getMockSchools() {
        List<School> mockSchools = new ArrayList<>();
        mockSchools.add(getMockSchool());
        return mockSchools;
    }


    // /////////////////////////////////////////////////////////////////////////////////////////
    // Availability
    // /////////////////////////////////////////////////////////////////////////////////////////
    private static final LocalDate localDate = LocalDate.of(2020, 1, 10);
    public static Availability getMockAvailability() {
        return Availability.AvailabilityBuilder.anAvailability()
                .date(localDate)
                .employee(getMockEmployee())
                .id(id++)
                .build();
    }
    public static List<Availability> getMockAvailabilities() {
        List<Availability> mockAvailabilities = new ArrayList<>();
        mockAvailabilities.add(getMockAvailability());
        return mockAvailabilities;
    }
    public static AvailabilityDTO getMockAvailabilityDTO() {
        return AvailabilityDTO.AvailabilityDTOBuilder.anAvailabilityDTO()
                .date(localDate)
                .employeeId(getMockEmployee().getId())
                .id(id++)
                .build();
    }

    // /////////////////////////////////////////////////////////////////////////////////////////
    // User
    // /////////////////////////////////////////////////////////////////////////////////////////
    public static User getMockUser() { return getMockUser("admin"); }
    public static User getMockUser(String role) {
        return User.UserBuilder.anUser()
                .email(role + "@email.com")
                .role(role)
                .password("myPassword")
                .id(id++)
                .build();
    }
    public static List<User> getMockUsers() {
        List<User> mockUsers = new ArrayList<>();
        mockUsers.add(getMockUser("admin"));
        mockUsers.add(getMockUser("school"));
        mockUsers.add(getMockUser("employee"));
        return mockUsers;
    }
    public static UserDTO getMockUserDTO() { return getMockUserDTO("admin"); }
    public static UserDTO getMockUserDTO(String role) {
        return UserDTO.UserDTOBuilder.anUserDTO()
                .email(role + "@email.com")
                .role(role)
                .password("myPassword")
                .id(id++)
                .build();
    }

    // /////////////////////////////////////////////////////////////////////////////////////////
    // Employee
    // /////////////////////////////////////////////////////////////////////////////////////////
    public static Employee getMockEmployee() {
        return Employee.EmployeeBuilder.anEmployee()
                .email("my@email.com")
                .educationPackages(getMockEducationPackages())
                .firstName("firstname")
                .lastName("lastname")
                .id(id++)
                .build();
    }
    public static List<Employee> getMockEmployees() {
        List<Employee> mockEmployees = new ArrayList<>();
        mockEmployees.add(getMockEmployee());
        return mockEmployees;
    }
    public static EmployeeDTO getMockEmployeeDTO() {
        return EmployeeDTO.EmployeeDTOBuilder.anEmployeeDTO()
                .email("my@email.com")
                .educationPackages(List.of(getMockEducationPackage().getName()))
                .firstName("firstname")
                .lastName("lastname")
                .id(id++)
                .build();
    }
    // /////////////////////////////////////////////////////////////////////////////////////////
    // Booking
    // /////////////////////////////////////////////////////////////////////////////////////////
    public static Booking getMockBooking() {
        return Booking.BookingBuilder.aBooking()
                .id(id++)
                .availability(getMockAvailability())
                .school(getMockSchool())
                .build();
    }
    
    public static List<Booking> getMockBookings() {
        List<Booking> mockBookings = new ArrayList<>();
        mockBookings.add(getMockBooking());
        return mockBookings;
    }
    public static BookingDTO getMockBookingDTO() {
        return BookingDTO.BookingDTOBuilder.aBookingDTO()
                .id(id++)
                .availabilityId(getMockAvailability().getId())
                .schoolId(getMockSchool().getId())
                .build();
    }

}
