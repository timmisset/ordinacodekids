package com.ordina.ordinaCodeKids.employee;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.EmployeeAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EmployeeNotFoundException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;
import com.ordina.ordinaCodeKids.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EmployeeServiceImplTest {

    private EmployeeRepository employeeRepository;
    private EmployeeService employeeService;
    private List<Employee> mockEmployees;
    private Employee mockEmployee;
    private UserService userService;

    @BeforeEach
    void setUp() {
        employeeRepository = mock(EmployeeRepository.class);
        userService = spy(UserService.class);
        employeeService = new EmployeeServiceImpl(employeeRepository, userService);

        mockEmployee = MockData.getMockEmployee();
        mockEmployees = MockData.getMockEmployees();


    }

    @Test
    // check that the saved employee is returned and no exceptions are thrown
    void createEmployee() throws EmployeeAlreadyExistsException {
        // arrange
        doReturn(false).when(employeeRepository).existsById(anyLong());
        doReturn(mockEmployee).when(employeeRepository).save(any());


        // act
        Employee addedEmployee = employeeService.createEmployee(mockEmployee);

        // assert
        assertEquals(mockEmployee, addedEmployee);
        verify(employeeRepository, times(1)).existsById(anyLong());
    }

    @Test
    // check that the exception is correctly thrown when the employee already exists
    void createEmployeeThrowsEmployeeAlreadyExistsException() {
        // arrange
        doReturn(true).when(employeeRepository).existsById(anyLong());

        // act assert
        assertThrows(EmployeeAlreadyExistsException.class, () -> employeeService.createEmployee(mockEmployee));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteEmployee() throws EmployeeNotFoundException, UserNotFoundException {
        // arrange
        doReturn(true).when(employeeRepository).existsById(anyLong());
        doReturn(mockEmployee).when(employeeRepository).getOne(anyLong());

        // act
        employeeService.deleteEmployee(mockEmployee);

        // assert
        verify(employeeRepository, times(1)).delete(mockEmployee);
        verify(userService, times(1)).deleteUserByEmail(mockEmployee.getEmail());
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteEmployeeById() throws EmployeeNotFoundException, UserNotFoundException {
        // arrange
        doReturn(true).when(employeeRepository).existsById(anyLong());
        doReturn(mockEmployee).when(employeeRepository).getOne(anyLong());

        // act
        employeeService.deleteEmployeeById(mockEmployee.getId());

        // assert
        verify(employeeRepository, times(1)).delete(mockEmployee);
        verify(userService, times(1)).deleteUserByEmail(mockEmployee.getEmail());
    }

    @Test
        // check that the repository is called with the correct id for removal
    void deleteEmployeeByIdThrowsEmployeeNotFoundException() {
        // arrange
        doReturn(false).when(employeeRepository).existsById(anyLong());

        // act assert
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.deleteEmployee(mockEmployee));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteEmployeeThrowsEmployeeNotFoundException() {
        // arrange
        doReturn(false).when(employeeRepository).existsById(anyLong());

        // act assert
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.deleteEmployee(mockEmployee));
    }

    @Test
    // check that the repository is called with item to save
    void updateEmployee() throws EmployeeNotFoundException, EmployeeAlreadyExistsException {
        // arrange
        doReturn(mockEmployee).when(employeeRepository).save(any());
        doReturn(true).when(employeeRepository).existsById(anyLong());

        // act
        Employee savedEmployee = employeeService.updateEmployee(mockEmployees.get(0));

        // assert
        assertEquals(mockEmployee, savedEmployee);
    }

    @Test
        // check that the repository is called with item to save
    void updateEmployeeThrowsEmployeeNotFoundException() {
        // arrange
        doReturn(false).when(employeeRepository).existsById(anyLong());

        // act assert
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.updateEmployee(mockEmployee));
    }

    @Test
    // check that the employee is returned when the repository finds it by the provided id
    void getEmployeeById() throws EmployeeNotFoundException {
        // arrange
        doReturn(Optional.of(mockEmployee)).when(employeeRepository).findById(any());

        // act
        Employee returnedEmployee = employeeService.getEmployeeById(0);

        // assert
        assertEquals(returnedEmployee, mockEmployee);
        verify(employeeRepository, times(1)).findById(any());
    }

    @Test
    // check that the correct exception is thrown when the repository returns an empty optional
    void getEmployeeByIdThrowsEmployeeNotFoundException() {
        // arrange
        doReturn(Optional.empty()).when(employeeRepository).findById(any());

        // act, assert
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.getEmployeeById(0));
    }

    @Test
    // check that all the employees are returned
    void getEmployees() {
        // arrange
        doReturn(mockEmployees).when(employeeRepository).findAll();

        // act
        List<Employee> returnedEmployees = employeeService.getEmployees();

        // assert
        assertEquals(mockEmployees, returnedEmployees);
        verify(employeeRepository, times(1)).findAll();

    }

    @Test
    // check if the mapping is performed correctly
    void toEmployeeMapping() {
        // arrange
        EmployeeDTO employeeDTO = MockData.getMockEmployeeDTO();

        // act
        Employee employee = employeeService.toEmployeeMapping(employeeDTO);

        // assert
        assertEquals(employeeDTO.getEmail(), employee.getEmail());
        assertEquals(employeeDTO.getId(), employee.getId());
        employeeDTO.getEducationPackages().forEach(educationPackage -> assertEquals(employeeDTO.getEducationPackages().get(0), employee.getEducationPackages().get(0).getName()));
    }

    @Test
    void fromEmployeeMapping() {
        // arrange

        // act
        EmployeeDTO employeeDTO = employeeService.fromEmployeeMapping(mockEmployee);

        // assert
        assertEquals(mockEmployee.getEmail(), employeeDTO.getEmail());
        assertEquals(mockEmployee.getId(), employeeDTO.getId());
        mockEmployee.getEducationPackages().forEach(
                educationPackage ->
                        assertTrue(employeeDTO.getEducationPackages().contains(educationPackage.getName()))
        );

    }

    @Test
    void addEmployeeWithEmailDuplicateThrowsEmployeeAlreadyExistsException() {
        // arrange
        doReturn(true).when(employeeRepository).existsByEmail(anyString());

        // act assert
        assertThrows(EmployeeAlreadyExistsException.class, () -> employeeService.createEmployee(mockEmployee));
    }

    @Test
    void updateEmployeeWithEmailDuplicateThrowsEmployeeAlreadyExistsException() {
        // arrange
        doReturn(true).when(employeeRepository).existsById(anyLong());
        doReturn(true).when(employeeRepository).existsByEmailAndIdNot(anyString(), anyLong());

        // act assert
        assertThrows(EmployeeAlreadyExistsException.class, () -> employeeService.updateEmployee(mockEmployee));
    }

    @Test
    void getEmployeesByEducationPackage() {
        // arrange
        EducationPackage mockEducationPackage = MockData.getMockEducationPackage();
        doReturn(mockEmployees).when(employeeRepository).findAllByEducationPackagesContains(mockEducationPackage);

        // act
        List<Employee> returnedEmployees = employeeService.getEmployeesByEducationPackage(mockEducationPackage);

        // assert
        assertEquals(mockEmployees, returnedEmployees);
    }
}
