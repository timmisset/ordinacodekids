package com.ordina.ordinaCodeKids.employee;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.education_package.EducationPackageService;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.EducationPackageNotFoundException;
import com.ordina.ordinaCodeKids.exception.EmployeeAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EmployeeNotFoundException;
import com.ordina.ordinaCodeKids.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EmployeeControllerTest {

    private EmployeeController employeeController;
    private EmployeeService employeeService;
    private List<Employee> mockEmployees;
    private Employee mockEmployee;
    private EducationPackageService educationPackageService;


    @BeforeEach
    void setUp() {
        employeeService = new EmployeeServiceImpl(mock(EmployeeRepository.class), mock(UserService.class));
        employeeService = spy(employeeService);

        educationPackageService = mock(EducationPackageService.class);
        employeeController = new EmployeeController(employeeService, educationPackageService);

        mockEmployees = MockData.getMockEmployees();
        mockEmployee = MockData.getMockEmployee();
    }

    @Test
    void getEmployees() {
        // arrange
        doReturn(mockEmployees).when(employeeService).getEmployees();

        // act
        ResponseEntity<List<EmployeeDTO>> response = employeeController.getEmployees();

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockEmployees.size(), response.getBody().size());
        verify(employeeService, times(1)).getEmployees();
    }

    @Test
    void getEmployee() throws EmployeeNotFoundException {
        // arrange
        doReturn(mockEmployee).when(employeeService).getEmployeeById(anyLong());

        // act
        ResponseEntity<EmployeeDTO> response = employeeController.getEmployee(0);

        // assert
        assertNotNull(response.getBody());
        EmployeeDTO employeeDTO = response.getBody();
        assertEquals(mockEmployee.getEmail(), employeeDTO.getEmail());
        mockEmployee.getEducationPackages().forEach(educationPackage -> assertTrue(employeeDTO.getEducationPackages().contains(educationPackage.getName())));
        verify(employeeService, times(1)).getEmployeeById(anyLong());
    }

    @Test
    void getEmployeeThrowsEmployeeNotFoundException() throws EmployeeNotFoundException {
        // arrange
        EmployeeNotFoundException exception = new EmployeeNotFoundException(mockEmployee.getId());
        doThrow(exception).when(employeeService).getEmployeeById(anyLong());

        // act assert
        try {
            employeeController.getEmployee(mockEmployee.getId());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void createEmployee() throws EmployeeAlreadyExistsException {
        // arrange
        long newEmployeeId = 11;
        doAnswer(invocationOnMock -> {
            Employee employee = invocationOnMock.getArgument(0);
            employee.setId(newEmployeeId);
            return employee;
        }).when(employeeService).createEmployee(any());

        // act
        ResponseEntity<EmployeeDTO> response = employeeController.createEmployee(new EmployeeDTO());
        EmployeeDTO createdEmployee = response.getBody();

        // assert
        assertNotNull(createdEmployee);
        assertEquals(newEmployeeId, createdEmployee.getId());
        verify(employeeService, times(1)).createEmployee(any());
    }

    @Test
    void createEmployeeThrowsEmployeeAlreadyExistsException() throws EmployeeAlreadyExistsException {
        // arrange
        EmployeeAlreadyExistsException exception = new EmployeeAlreadyExistsException(mockEmployee);
        doThrow(exception).when(employeeService).createEmployee(any());

        // act assert
        try {
            employeeController.createEmployee(new EmployeeDTO());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void addEmployeeWithEmailDuplicateThrowsEmployeeAlreadyExistsException() throws EmployeeAlreadyExistsException {
        EmployeeAlreadyExistsException exception = new EmployeeAlreadyExistsException(mockEmployee.getEmail());
        doThrow(exception).when(employeeService).createEmployee(any());

        // act assert
        try {
            employeeController.createEmployee(new EmployeeDTO());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void updateEmployee() throws EmployeeNotFoundException, EmployeeAlreadyExistsException {
        // arrange
        long newEmployeeId = 11;
        doAnswer(invocationOnMock -> {
            Employee employee = invocationOnMock.getArgument(0);
            employee.setId(newEmployeeId);
            return employee;
        }).when(employeeService).updateEmployee(any());

        // act
        ResponseEntity<EmployeeDTO> response = employeeController.updateEmployee(0, new EmployeeDTO());
        EmployeeDTO updatedEmployee = response.getBody();

        // assert
        assertNotNull(updatedEmployee);
        assertEquals(newEmployeeId, updatedEmployee.getId());
        verify(employeeService, times(1)).updateEmployee(any());
    }

    @Test
    void updateEmployeeThrowsEmployeeNotFoundException() throws EmployeeNotFoundException {
        // arrange
        EmployeeDTO mockEmployeeDTO = MockData.getMockEmployeeDTO();
        EmployeeNotFoundException exception = new EmployeeNotFoundException(mockEmployeeDTO.getId());
        doThrow(exception).when(employeeService).getEmployeeById(anyLong());



        // act assert
        try {
            employeeController.updateEmployee(mockEmployeeDTO.getId(), mockEmployeeDTO);
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void updateEmployeeThrowsEmployeeAlreadyExistsException() throws EmployeeAlreadyExistsException, EmployeeNotFoundException {
        // arrange
        EmployeeAlreadyExistsException exception = new EmployeeAlreadyExistsException(mockEmployee.getEmail());
        doThrow(exception).when(employeeService).updateEmployee(any());

        long mockEmployeeId = mockEmployee.getId();
        EmployeeDTO mockEmployeeDTO = MockData.getMockEmployeeDTO();

        // act assert
        try {
            employeeController.updateEmployee(mockEmployeeId, mockEmployeeDTO);
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void deleteEmployee() throws EmployeeNotFoundException {
        // arrange
        long mockEmployeeId = mockEmployee.getId();
        doNothing().when(employeeService).deleteEmployeeById(mockEmployeeId);

        // act
        employeeController.deleteEmployee(mockEmployeeId);

        // assert
        verify(employeeService, times(1)).deleteEmployeeById(mockEmployeeId);
    }

    @Test
    void deleteEmployeeThrowsEmployeeNotFoundException() throws EmployeeNotFoundException {
        // arrange
        EmployeeNotFoundException exception = new EmployeeNotFoundException(mockEmployee.getId());
        doThrow(exception).when(employeeService).getEmployeeById(anyLong());

        long mockEmployeeId = mockEmployee.getId();

        // act assert
       try {
           employeeController.deleteEmployee(mockEmployeeId);
       }catch( ResponseStatusException e) {
           assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
           assertEquals(exception.getMessage(), e.getReason());
       }
    }

    @Test
    void getEmployeesByEducationPackage() throws EducationPackageNotFoundException {
        // arrange
        List<EducationPackage> educationPackages = MockData.getMockEducationPackages();
        EducationPackage educationPackage = educationPackages.get(0);
        doReturn(educationPackage).when(educationPackageService).getEducationPackageByName(anyString());
        doReturn(mockEmployees).when(employeeService).getEmployeesByEducationPackage(educationPackage);

        // act
        ResponseEntity<List<EmployeeDTO>> response = employeeController.getEmployeesByEducationPackage(educationPackage.getName());

        // assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(mockEmployees.size(), response.getBody().size());

    }

}
