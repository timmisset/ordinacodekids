package com.ordina.ordinaCodeKids.demo;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityRepository;
import com.ordina.ordinaCodeKids.booking.Booking;
import com.ordina.ordinaCodeKids.booking.BookingRepository;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.employee.EmployeeRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DemoServiceImplTest_IntegrationTest {

    @Autowired
    DemoService demoService;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    AvailabilityRepository availabilityRepository;

    @Test
    @Transactional
    @Tag("IntegrationTest")
    void setDemoData() {
        demoService.setDemoData();

        List<Employee> employees = employeeRepository.findAll();
        assertEquals(demoService.getEmployees().size(), employees.size());

        List<Booking> bookings = bookingRepository.findAll();
        assertTrue(bookings.size() > 0);

        List<Availability> availabilities = availabilityRepository.findAll();
        assertTrue(availabilities.size() > bookings.size());
    }

    @Test
    @Transactional
    @Tag("IntegrationTest")
    void resetTest() {
        demoService.reset();
    }
}
