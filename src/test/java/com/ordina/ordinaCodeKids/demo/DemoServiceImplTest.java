package com.ordina.ordinaCodeKids.demo;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.education_package.EducationPackageRepository;
import com.ordina.ordinaCodeKids.employee.EmployeeRepository;
import com.ordina.ordinaCodeKids.school.SchoolRepository;
import com.ordina.ordinaCodeKids.user.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class DemoServiceImplTest {

    private DemoService demoService;

    @BeforeEach()
    void setUp() {
        demoService = new DemoServiceImpl(
                mock(EducationPackageRepository.class),
                mock(EmployeeRepository.class),
                mock(SchoolRepository.class),
                mock(UserRepository.class)
                );
    }

    @Test
    void getRandomDemoEducationPackageSelection() {

        for(int i = 0; i < 10; i++) {
            List<EducationPackage> packages = demoService.pickEducationPackage();
            List<String> packageNames = packages.stream().map(EducationPackage::getName).collect(Collectors.toList());

            packageNames.forEach(packageName -> assertEquals(1, Collections.frequency(packageNames, packageName)));
        }
    }

    @Test
    void pickName() {
        List<String> names = new ArrayList<>();
        for(int i = 0; i < 100; i++) {
            Map<String, String> pickedName = demoService.pickName();
            names.add(pickedName.get("firstname") + "." + pickedName.get("lastname"));
        }
        names.forEach(name -> assertEquals(1, Collections.frequency(names, name)));
    }

    @Test
    void getAvailabilities() {
        List<Availability> availabilities = demoService.getAvailabilities();
        assertFalse(availabilities.isEmpty());
    }


}
