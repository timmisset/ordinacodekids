package com.ordina.ordinaCodeKids.booking;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.availability.AvailabilityService;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.AvailabilityIsAlreadyBookedException;
import com.ordina.ordinaCodeKids.exception.BookingAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.BookingNotFoundException;
import com.ordina.ordinaCodeKids.exception.NotTheOwnerException;
import com.ordina.ordinaCodeKids.school.SchoolService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class BookingControllerTest {

    private BookingController bookingController;
    private BookingService bookingService;
    private List<Booking> mockBookings;
    private Booking mockBooking;

    @BeforeEach
    void setUp() {
        bookingService = new BookingServiceImpl(mock(BookingRepository.class),
                mock(AvailabilityService.class),
                mock(SchoolService.class), mock(EmailService.class));
        bookingService = spy(bookingService);
        bookingController = new BookingController(bookingService);

        mockBookings = MockData.getMockBookings();
        mockBooking = MockData.getMockBooking();
    }

    @Test
    void getBookings() {
        // arrange
        doReturn(mockBookings).when(bookingService).getBookings();

        // act
        ResponseEntity<List<BookingDTO>> response = bookingController.getBookings();

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockBookings.size(), response.getBody().size());
        verify(bookingService, times(1)).getBookings();
    }

    @Test
    void getBooking() throws BookingNotFoundException {
        // arrange
        doReturn(mockBooking).when(bookingService).getBookingById(anyLong());

        // act
        ResponseEntity<BookingDTO> response = bookingController.getBooking(0);

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockBooking.getAvailability().getId(), response.getBody().getAvailabilityId());
        verify(bookingService, times(1)).getBookingById(anyLong());
    }

    @Test
    void getBookingThrowsBookingNotFoundException() throws BookingNotFoundException {
        // arrange
        BookingNotFoundException exception = new BookingNotFoundException(mockBooking.getId());
        doThrow(exception).when(bookingService).getBookingById(anyLong());

        // act assert
        try {
            bookingController.getBooking(mockBooking.getId());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void createBooking() throws BookingAlreadyExistsException, AvailabilityIsAlreadyBookedException, NotTheOwnerException {
        // arrange
        long newBookingId = 11;
        doAnswer(invocationOnMock -> {
            Booking booking = invocationOnMock.getArgument(0);
            booking.setId(newBookingId);
            return booking;
        }).when(bookingService).createBooking(any());

        // act
        ResponseEntity<BookingDTO> response = bookingController.createBooking(new BookingDTO());
        BookingDTO createdBooking = response.getBody();

        // assert
        assertNotNull(createdBooking);
        assertEquals(newBookingId, createdBooking.getId());
        verify(bookingService, times(1)).createBooking(any());
    }

    @Test
    void createBookingThrowsBookingAlreadyExistsException() throws BookingAlreadyExistsException, AvailabilityIsAlreadyBookedException, NotTheOwnerException {
        // arrange
        BookingAlreadyExistsException exception = new BookingAlreadyExistsException(mockBooking);
        doThrow(exception).when(bookingService).createBooking(any());

        // act assert
        try {
            bookingController.createBooking(new BookingDTO());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void updateBooking() throws BookingNotFoundException, NotTheOwnerException {
        // arrange
        long newBookingId = 11;
        doAnswer(invocationOnMock -> {
            Booking booking = invocationOnMock.getArgument(0);
            booking.setId(newBookingId);
            return booking;
        }).when(bookingService).updateBooking(any());

        // act
        ResponseEntity<BookingDTO> response = bookingController.updateBooking(0, new BookingDTO());
        BookingDTO updatedBooking = response.getBody();

        // assert
        assertNotNull(updatedBooking);
        assertEquals(newBookingId, updatedBooking.getId());
        verify(bookingService, times(1)).updateBooking(any());
    }

    @Test
    void updateBookingThrowsBookingNotFoundException() throws BookingNotFoundException {
        // arrange
        BookingNotFoundException exception = new BookingNotFoundException(mockBooking.getId());
        doThrow(exception).when(bookingService).getBookingById(anyLong());

        long mockBookingId = mockBooking.getId();
        BookingDTO mockBookingDTO = BookingDTO.BookingDTOBuilder.aBookingDTO().id(mockBookingId).build();

        // act assert
        try {
            bookingController.updateBooking(mockBookingId, mockBookingDTO);
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void deleteBooking() throws BookingNotFoundException, NotTheOwnerException {
        // arrange
        long mockBookingId = mockBooking.getId();
        doNothing().when(bookingService).deleteBookingById(mockBookingId);

        // act
        bookingController.deleteBooking(mockBookingId);

        // assert
        verify(bookingService, times(1)).deleteBookingById(mockBookingId);
    }

    @Test
    void deleteBookingThrowsBookingNotFoundException() throws BookingNotFoundException {
        // arrange
        BookingNotFoundException exception = new BookingNotFoundException(mockBooking.getId());
        doThrow(exception).when(bookingService).getBookingById(anyLong());

        long mockBookingId = mockBooking.getId();

        // act assert
       try {
           bookingController.deleteBooking(mockBookingId);
       }catch( ResponseStatusException e) {
           assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
           assertEquals(exception.getMessage(), e.getReason());
       }
    }
}
