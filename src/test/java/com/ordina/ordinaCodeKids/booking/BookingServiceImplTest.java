package com.ordina.ordinaCodeKids.booking;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityService;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.*;
import com.ordina.ordinaCodeKids.school.School;
import com.ordina.ordinaCodeKids.school.SchoolService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BookingServiceImplTest {

    private BookingRepository bookingRepository;
    private BookingService bookingService;
    private List<Booking> mockBookings;
    private Booking mockBooking;
    private EmailService emailService;
    Availability mockAvailability;
    School mockSchool;

    @BeforeEach
    void setUp() throws AvailabilityNotFoundException, SchoolNotFoundException {
        SchoolService schoolService = mock(SchoolService.class);
        AvailabilityService availabilityService = mock(AvailabilityService.class);
        emailService = mock(EmailService.class);

        bookingRepository = mock(BookingRepository.class);
        bookingService = spy(new BookingServiceImpl(bookingRepository, availabilityService, schoolService, emailService));

        mockBooking = MockData.getMockBooking();
        mockBookings = MockData.getMockBookings();

        mockAvailability = Availability.AvailabilityBuilder.anAvailability().id(mockBooking.getAvailability().getId()).build();
        mockSchool = MockData.getMockSchool();

        doReturn(mockAvailability)
                .when(availabilityService).getAvailabilityById(anyLong());
        doReturn(true).when(bookingService).canClaim(any(Booking.class));
        doReturn(mockSchool)
                .when(schoolService).getSchoolById(anyLong());
    }

    @Test
    // check that the saved booking is returned and no exceptions are thrown
    void createBooking() throws BookingAlreadyExistsException, AvailabilityIsAlreadyBookedException, NotTheOwnerException {
        // arrange
        doReturn(false).when(bookingRepository).existsById(anyLong());
        doReturn(false).when(bookingRepository).existsByAvailabilityId(anyLong());
        doReturn(mockBooking).when(bookingRepository).save(any());

        // act
        Booking addedBooking = bookingService.createBooking(mockBooking);

        // assert
        assertEquals(mockBooking, addedBooking);
        verify(bookingRepository, times(1)).existsById(anyLong());
        verify(emailService, times(1)).sendSimpleMessage(eq(addedBooking.getAvailability().getEmployee().getEmail()), anyString(), anyString());
        verify(emailService, times(1)).sendSimpleMessage(eq(addedBooking.getSchool().getContactEmail()), anyString(), anyString());
    }

    @Test
    // check that the exception is correctly thrown when the booking already exists
    void createBookingThrowsBookingAlreadyExistsException() {
        // arrange
        doReturn(true).when(bookingRepository).existsById(anyLong());
        doReturn(false).when(bookingRepository).existsByAvailabilityId(anyLong());

        // act assert
        assertThrows(BookingAlreadyExistsException.class, () -> bookingService.createBooking(mockBooking));
    }

    @Test
        // check that the exception is correctly thrown when the booking already exists
    void createBookingThrowsAvailabilityIsAlreadyBookedException() {
        // arrange
        doReturn(false).when(bookingRepository).existsById(anyLong());
        doReturn(true).when(bookingRepository).existsByAvailabilityId(anyLong());

        // act assert
        assertThrows(AvailabilityIsAlreadyBookedException.class, () -> bookingService.createBooking(mockBooking));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteBooking() throws NotTheOwnerException {
        // arrange
        doReturn(true).when(bookingRepository).existsById(anyLong());

        // act
        bookingService.deleteBooking(mockBooking);

        // assert
        verify(bookingRepository, times(1)).delete(mockBooking);
        verify(emailService, times(1)).sendSimpleMessage(eq(mockBooking.getAvailability().getEmployee().getEmail()), anyString(), anyString());
        verify(emailService, times(1)).sendSimpleMessage(eq(mockBooking.getSchool().getContactEmail()), anyString(), anyString());
    }

    @Test
    void deleteBookingThrowsNotTheOwnerException() {
        // arrange
        bookingService = spy(bookingService);
        doReturn(false).when(bookingService).canClaim(any(Booking.class));

        // act assert
        assertThrows(NotTheOwnerException.class, () -> { bookingService.deleteBooking(mockBooking); });
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteBookingById() throws BookingNotFoundException, NotTheOwnerException {
        // arrange
        doReturn(true).when(bookingRepository).existsById(anyLong());
        doReturn(Optional.of(mockBooking)).when(bookingRepository).findById(mockBooking.getId());

        // act
        bookingService.deleteBookingById(mockBooking.getId());

        // assert
        verify(bookingRepository, times(1)).delete(mockBooking);
        verify(emailService, times(1)).sendSimpleMessage(eq(mockBooking.getAvailability().getEmployee().getEmail()), anyString(), anyString());
        verify(emailService, times(1)).sendSimpleMessage(eq(mockBooking.getSchool().getContactEmail()), anyString(), anyString());
    }

    @Test
        // check that the repository is called with the correct id for removal
    void deleteBookingByIdThrowsBookingNotFoundException() {
        // arrange
        doReturn(false).when(bookingRepository).existsById(anyLong());

        // act assert
        assertThrows(BookingNotFoundException.class, () -> bookingService.deleteBookingById(mockBooking.getId()));
    }


    @Test
    // check that the repository is called with item to save
    void updateBooking() throws BookingNotFoundException, NotTheOwnerException {
        // arrange
        doReturn(mockBooking).when(bookingRepository).save(any());
        doReturn(true).when(bookingRepository).existsById(anyLong());

        // act
        Booking savedBooking = bookingService.updateBooking(mockBookings.get(0));

        // assert
        assertEquals(mockBooking, savedBooking);
    }

    @Test
        // check that the repository is called with item to save
    void updateBookingThrowsBookingNotFoundException() {
        // arrange
        doReturn(false).when(bookingRepository).existsById(anyLong());

        // act assert
        assertThrows(BookingNotFoundException.class, () -> bookingService.updateBooking(mockBooking));
    }

    @Test
    // check that the booking is returned when the repository finds it by the provided id
    void getBookingById() throws BookingNotFoundException {
        // arrange
        doReturn(Optional.of(mockBooking)).when(bookingRepository).findById(any());

        // act
        Booking returnedBooking = bookingService.getBookingById(0);

        // assert
        assertEquals(returnedBooking, mockBooking);
        verify(bookingRepository, times(1)).findById(any());
    }

    @Test
    // check that the correct exception is thrown when the repository returns an empty optional
    void getBookingByIdThrowsBookingNotFoundException() {
        // arrange
        doReturn(Optional.empty()).when(bookingRepository).findById(any());

        // act, assert
        assertThrows(BookingNotFoundException.class, () -> bookingService.getBookingById(0));
    }

    @Test
    // check that all the bookings are returned
    void getBookings() {
        // arrange
        doReturn(mockBookings).when(bookingRepository).findAll();

        // act
        List<Booking> returnedBookings = bookingService.getBookings();

        // assert
        assertEquals(mockBookings, returnedBookings);
        verify(bookingRepository, times(1)).findAll();

    }

    @Test
    // check if the mapping is performed correctly
    void toBookingMapping() {
        // arrange
        BookingDTO bookingDTO = MockData.getMockBookingDTO();
        bookingDTO.setAvailabilityId(mockAvailability.getId());

        // act
        Booking booking = bookingService.toBookingMapping(bookingDTO);

        // assert
        assertEquals(bookingDTO.getAvailabilityId(), booking.getAvailability().getId());
        assertEquals(bookingDTO.getId(), booking.getId());
    }

    @Test
    void fromBookingMapping() {
        // arrange

        // act
        BookingDTO bookingDTO = bookingService.fromBookingMapping(mockBooking);

        // assert
        assertEquals(mockBooking.getAvailability().getId(), bookingDTO.getAvailabilityId());
        assertEquals(mockBooking.getId(), bookingDTO.getId());

    }

    @Test
    void getOwner() {
        // arrange
        Booking booking = MockData.getMockBooking();
        School school = MockData.getMockSchool();
        booking.setSchool(school);

        // act
        String owner = bookingService.getOwner(booking);
        String owner_nullInput = bookingService.getOwner(null);
        String owner_nullInputForSchool = bookingService.getOwner(new Booking());

        // assert
        assertNull(owner_nullInput);
        assertNull(owner_nullInputForSchool);
        assertEquals(school.getContactEmail(), owner);
    }

}
