package com.ordina.ordinaCodeKids.scenarios;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityRepository;
import com.ordina.ordinaCodeKids.booking.*;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.employee.EmployeeRepository;
import com.ordina.ordinaCodeKids.exception.AvailabilityIsAlreadyBookedException;
import com.ordina.ordinaCodeKids.school.School;
import com.ordina.ordinaCodeKids.school.SchoolRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Scenario 2 and 3 as described on the GitLab issue board are combined in this class
 */
@SpringBootTest
public class Scenario2 {

    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    AvailabilityRepository availabilityRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    BookingController bookingController;

    @Autowired
    BookingRepository bookingRepository;

    @Test
    @Tag("IntegrationTest")
    @Transactional
    @WithMockUser(roles = "ADMIN")
    void deleteBookingViaAvailability() {
        // STEP 1: CREATE THE SCHOOL AND AVAILABILITY AND EMPLOYEE
        School school = School.SchoolBuilder.aSchool().name("My school").contactEmail("contact@school.com").contactName("contactname").description("My awesome school").build();
        schoolRepository.save(school);

        Employee employee = Employee.EmployeeBuilder.anEmployee().email("user1@email.com").firstName("firstname").lastName("lastname").build();
        employeeRepository.save(employee);

        LocalDate date1 = LocalDate.of(2020, 1, 10);
        Availability availability = Availability.AvailabilityBuilder.anAvailability().date(date1).employee(employee).build();
        availabilityRepository.save(availability);
        long availabilityId = availability.getId();

        // STEP 2: BOOK THE AVAILABILITY
        BookingDTO booking = BookingDTO.BookingDTOBuilder.aBookingDTO().schoolId(school.getId()).availabilityId(availability.getId()).build();
        ResponseEntity<BookingDTO> response = bookingController.createBooking(booking);

        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        long createdBookingId = response.getBody().getId();

        assertTrue(createdBookingId > 0);
        Optional<Booking> optionalBooking = bookingRepository.findById(createdBookingId);
        assertTrue(optionalBooking.isPresent());

        availabilityRepository.flush();

        Optional<Availability> optionalAvailability = availabilityRepository.findById(availabilityId);
        assertTrue(optionalAvailability.isPresent());


        assertNotNull(optionalAvailability.get().getBooking());
        assertEquals(optionalBooking.get().getId(), optionalAvailability.get().getBooking().getId());

        boolean foundByAvailabilityId = bookingRepository.existsByAvailabilityId(availabilityId);
        assertTrue(foundByAvailabilityId);

        // STEP 3: THROW EXCEPTION WHEN THE AVAILABILITY IS BOOKED AGAIN
        booking = BookingDTO.BookingDTOBuilder.aBookingDTO().schoolId(school.getId()).availabilityId(availabilityId).build();
        try{
            bookingController.createBooking(booking);
            fail(); // should throw the exception
        }catch(ResponseStatusException e) {
            AvailabilityIsAlreadyBookedException availabilityIsAlreadyBookedException = new AvailabilityIsAlreadyBookedException(availability);

            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(availabilityIsAlreadyBookedException.getMessage(), e.getReason());
        }

        // STEP 4: REMOVE THE BOOKING USING THE CONTROLLER ENDPOINT
        bookingController.deleteBooking(createdBookingId);

        // validate that the booking has been removed
        assertFalse(bookingRepository.existsById(createdBookingId));

        // STEP 5: ADD A BOOKING TO THE AVAILABILITY SLOT AND THEN REMOVE THE AVAILABILITY, THIS SHOULD REMOVE THE BOOKING ALSO
        booking = BookingDTO.BookingDTOBuilder.aBookingDTO().schoolId(school.getId()).availabilityId(availability.getId()).build();
        response = bookingController.createBooking(booking);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        booking = response.getBody();
        assertNotNull(booking);
        assertTrue(booking.getId() > 0);

        availabilityRepository.delete(availability);
        Optional<Availability> availabilityOptional = availabilityRepository.findById(availability.getId());
        assertTrue(availabilityOptional.isEmpty());
        optionalBooking = bookingRepository.findById(booking.getId());
        assertTrue(optionalBooking.isEmpty());

    }


    @Test
    @Tag("IntegrationTest")
    @Transactional
    @WithMockUser(roles = "ADMIN")
    void deleteBookingAndAvailabilityViaEmployee() {

        // STEP 1: CREATE THE SCHOOL AND AVAILABILITY AND EMPLOYEE
        School school = School.SchoolBuilder.aSchool().name("My school").contactEmail("contact@school.com").contactName("contactname").description("My awesome school").build();
        schoolRepository.save(school);

        Employee employee = Employee.EmployeeBuilder.anEmployee().email("user1@email.com").firstName("firstname").lastName("lastname").build();
        employeeRepository.save(employee);

        LocalDate date1 = LocalDate.of(2020, 1, 10);
        Availability availability = Availability.AvailabilityBuilder.anAvailability().date(date1).employee(employee).build();
        availabilityRepository.save(availability);
        long availabilityId = availability.getId();

        // STEP 2: BOOK THE AVAILABILITY
        BookingDTO booking = BookingDTO.BookingDTOBuilder.aBookingDTO().schoolId(school.getId()).availabilityId(availability.getId()).build();
        ResponseEntity<BookingDTO> response = bookingController.createBooking(booking);

        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        long createdBookingId = response.getBody().getId();

        assertTrue(createdBookingId > 0);
        Optional<Booking> optionalBooking = bookingRepository.findById(createdBookingId);
        assertTrue(optionalBooking.isPresent());

        availabilityRepository.flush();

        Optional<Availability> optionalAvailability = availabilityRepository.findById(availabilityId);
        assertTrue(optionalAvailability.isPresent());


        assertNotNull(optionalAvailability.get().getBooking());
        assertEquals(optionalBooking.get().getId(), optionalAvailability.get().getBooking().getId());

        boolean foundByAvailabilityId = bookingRepository.existsByAvailabilityId(availabilityId);
        assertTrue(foundByAvailabilityId);

        // STEP 3: THROW EXCEPTION WHEN THE AVAILABILITY IS BOOKED AGAIN
        booking = BookingDTO.BookingDTOBuilder.aBookingDTO().schoolId(school.getId()).availabilityId(availabilityId).build();
        try{
            bookingController.createBooking(booking);
            fail(); // should throw the exception
        }catch(ResponseStatusException e) {
            AvailabilityIsAlreadyBookedException availabilityIsAlreadyBookedException = new AvailabilityIsAlreadyBookedException(availability);

            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(availabilityIsAlreadyBookedException.getMessage(), e.getReason());
        }


        // STEP 4: REMOVE THE BOOKING USING THE CONTROLLER ENDPOINT
        bookingController.deleteBooking(createdBookingId);

        // validate that the booking has been removed
        assertFalse(bookingRepository.existsById(createdBookingId));

        // STEP 5: ADD A BOOKING TO THE AVAILABILITY SLOT AND THEN REMOVE THE AVAILABILITY, THIS SHOULD REMOVE THE BOOKING ALSO
        booking = BookingDTO.BookingDTOBuilder.aBookingDTO().schoolId(school.getId()).availabilityId(availability.getId()).build();
        response = bookingController.createBooking(booking);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        booking = response.getBody();
        assertNotNull(booking);
        assertTrue(booking.getId() > 0);

        employeeRepository.delete(employee);

        // now assert everything is removed:
        assertTrue(employeeRepository.findById(employee.getId()).isEmpty());
        assertTrue(availabilityRepository.findById(availability.getId()).isEmpty());
        assertTrue(bookingRepository.findById(booking.getId()).isEmpty());
    }

}
