package com.ordina.ordinaCodeKids.scenarios;

import com.ordina.ordinaCodeKids.availability.AvailabilityController;
import com.ordina.ordinaCodeKids.availability.AvailabilityDTO;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.employee.EmployeeRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class Scenario1 {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    AvailabilityController availabilityController;

    @Test
    @Tag("IntegrationTest")
    @Transactional
    void scenario1() {
        // STEP 1: CREATE EMPLOYEES
        List<Employee> employees = new ArrayList<>();
        employees.add(Employee.EmployeeBuilder.anEmployee().email("user1@email.com").firstName("firstname").lastName("lastname").build());
        employees.add(Employee.EmployeeBuilder.anEmployee().email("user2@email.com").firstName("firstname").lastName("lastname").build());
        employeeRepository.saveAll(employees);

        // STEP 2: CREATE AVAILABILITIES AND STORE USING THE CONTROLLER
        long employeeId1 = employees.get(0).getId();
        long employeeId2 = employees.get(1).getId();
        LocalDate date1 = LocalDate.of(2020, 1, 10);
        LocalDate date2 = LocalDate.of(2020, 1, 12);
        LocalDate date3 = LocalDate.of(2020, 2, 2);

        List<AvailabilityDTO> availabilities = new ArrayList<>();
        // employee 1
        availabilities.add(AvailabilityDTO.AvailabilityDTOBuilder.anAvailabilityDTO().employeeId(employeeId1).date(date1).build());
        availabilities.add(AvailabilityDTO.AvailabilityDTOBuilder.anAvailabilityDTO().employeeId(employeeId1).date(date2).build());
        availabilities.add(AvailabilityDTO.AvailabilityDTOBuilder.anAvailabilityDTO().employeeId(employeeId1).date(date3).build());
        // employee 2
        availabilities.add(AvailabilityDTO.AvailabilityDTOBuilder.anAvailabilityDTO().employeeId(employeeId2).date(date2).build());

        availabilities.forEach(availabilityController::createAvailability);

        // STEP 3: PREVENT DUPLICATE AVAILABILITIES IF ALREADY EXISTS BASED ON USER AND DATE
        try{
            availabilityController.createAvailability(availabilities.get(0));
            fail(); // duplication exception should be thrown
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
        }

        // Step 4: LIST CREATED AVAILABILITIES
        ResponseEntity<List<AvailabilityDTO>> response = availabilityController.getAvailabilities();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(availabilities.size(), response.getBody().size());

        // Step 5: LIST CREATED AVAILABILITIES BY USER
        response = availabilityController.getAvailabilitiesByEmployee(employeeId1);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        response.getBody().forEach(availabilityDTO -> assertEquals(employeeId1, availabilityDTO.getEmployeeId()));

        response = availabilityController.getAvailabilitiesByEmployee(employeeId2);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        response.getBody().forEach(availabilityDTO -> assertEquals(employeeId2, availabilityDTO.getEmployeeId()));

    }


}
