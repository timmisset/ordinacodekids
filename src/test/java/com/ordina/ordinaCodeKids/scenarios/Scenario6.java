package com.ordina.ordinaCodeKids.scenarios;

import com.ordina.ordinaCodeKids.school.SchoolController;
import com.ordina.ordinaCodeKids.school.SchoolDTO;
import com.ordina.ordinaCodeKids.school.SchoolRepository;
import com.ordina.ordinaCodeKids.user.User;
import com.ordina.ordinaCodeKids.user.UserController;
import com.ordina.ordinaCodeKids.user.UserDTO;
import com.ordina.ordinaCodeKids.user.UserRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Create a new school, verify that a corresponding user has been generated also
 * then remove the school and verify that the user has been removed also
 */
@SpringBootTest
public class Scenario6 {

    @Autowired
    SchoolController schoolController;

    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    UserController userController;

    @Autowired
    UserRepository userRepository;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Test
    @Tag("IntegrationTest")
    @Transactional
    void scenario5() {
        String email = "johndoe@email.com";
        String password = "myPassword";
        SchoolDTO schoolDTO = SchoolDTO.SchoolDTOBuilder.aSchoolDTO()
                .contactEmail(email)
                .name("My school")
                .contactName("John Doe")
                .description("Some text")
                .build();

        UserDTO userDTO = UserDTO.UserDTOBuilder.anUserDTO()
                .email(email)
                .password(password)
                .role("ROLE_SCHOOL")
                .build();

        ResponseEntity<SchoolDTO> response = schoolController.createSchool(schoolDTO);
        SchoolDTO createdSchoolDTO = response.getBody();

        userController.createUser(userDTO);

        // verify added school:
        assertTrue(schoolRepository.existsByContactEmail(email));

        // verify added user:
        assertTrue(userRepository.existsByEmail(email));

        // verify password is added correctly
        Optional<User> optionalUser = userRepository.findOneByEmail(email);
        assertTrue(optionalUser.isPresent());
        assertTrue(encoder.matches(password, optionalUser.get().getPassword()));

        // remove school:
        schoolController.deleteSchool(createdSchoolDTO.getId());

        // verify removing school:
        assertFalse(schoolRepository.existsByContactEmail(email));

        // verify removing user:
        assertFalse(userRepository.existsByEmail(email));

    }

}
