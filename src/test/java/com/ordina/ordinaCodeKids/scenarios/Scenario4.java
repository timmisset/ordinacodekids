package com.ordina.ordinaCodeKids.scenarios;

import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.education_package.EducationPackageRepository;
import com.ordina.ordinaCodeKids.employee.EmployeeController;
import com.ordina.ordinaCodeKids.employee.EmployeeDTO;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class Scenario4 {

    @Autowired
    EducationPackageRepository educationPackageRepository;

    @Autowired
    EmployeeController employeeController;

    @Test
    @Tag("IntegrationTest")
    @Transactional
    void scenario4() {
        // STEP 1: CREATE EDUCATION PACKAGES
        List<EducationPackage> educationPackages = new ArrayList<>();
        educationPackages.add(EducationPackage.EducationPackageBuilder.anEducationPackage().name("Coding").build());
        educationPackages.add(EducationPackage.EducationPackageBuilder.anEducationPackage().name("Security").build());
        educationPackages.add(EducationPackage.EducationPackageBuilder.anEducationPackage().name("Agile").build());
        educationPackageRepository.saveAll(educationPackages);

        List<String> educationPackagesAsString = educationPackages.stream().map(EducationPackage::getName).collect(Collectors.toList());

        // STEP 2: CREATE EMPLOYEES AND STORE USING THE CONTROLLER
        List<EmployeeDTO> employees = new ArrayList<>();
        employees.add(EmployeeDTO.EmployeeDTOBuilder.anEmployeeDTO().email("user1@email.com").firstName("firstname").lastName("lastname").educationPackages(List.of("Coding", "Security")).build());
        employees.add(EmployeeDTO.EmployeeDTOBuilder.anEmployeeDTO().email("user2@email.com").firstName("firstname").lastName("lastname").educationPackages(List.of("Coding")).build());
        employees.add(EmployeeDTO.EmployeeDTOBuilder.anEmployeeDTO().email("user3@email.com").firstName("firstname").lastName("lastname").educationPackages(List.of("Coding", "Agile")).build());
        employees.forEach(employee -> employeeController.createEmployee(employee));

        // STEP 3: RETRIEVE THE EMPLOYEE LIST AND VERIFY THE EMPLOYEES HAVE BEEN ADDED:
        ResponseEntity<List<EmployeeDTO>> response = employeeController.getEmployees();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(employees.size(), response.getBody().size());

        employees = response.getBody();
        List<String> returnedEmployeeEmails = employees.stream().map(EmployeeDTO::getEmail).collect(Collectors.toList());
        List.of("user1@email.com", "user2@email.com", "user3@email.com").forEach(
                email -> assertTrue(returnedEmployeeEmails.contains(email))
        );

        // STEP 4: LIST EMPLOYEES BY EDUCATION PACKAGE
        response = employeeController.getEmployeesByEducationPackage("Coding");
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(employees.size(), response.getBody().size());

        employees = response.getBody();
        List<String> returnedEmployeeEmailsForEducationPackage = employees.stream().map(EmployeeDTO::getEmail).collect(Collectors.toList());
        List.of("user1@email.com", "user2@email.com", "user3@email.com").forEach(
                email -> assertTrue(returnedEmployeeEmailsForEducationPackage.contains(email))
        );

        response = employeeController.getEmployeesByEducationPackage("Agile");
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().size());

        employees = response.getBody();
        List<String> returnedEmployeeEmailsForEducationPackage2 = employees.stream().map(EmployeeDTO::getEmail).collect(Collectors.toList());
        List.of("user3@email.com").forEach(
                email -> assertTrue(returnedEmployeeEmailsForEducationPackage2.contains(email))
        );

    }



}
