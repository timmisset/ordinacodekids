package com.ordina.ordinaCodeKids.scenarios;

import com.ordina.ordinaCodeKids.employee.EmployeeController;
import com.ordina.ordinaCodeKids.employee.EmployeeDTO;
import com.ordina.ordinaCodeKids.employee.EmployeeRepository;
import com.ordina.ordinaCodeKids.user.User;
import com.ordina.ordinaCodeKids.user.UserController;
import com.ordina.ordinaCodeKids.user.UserDTO;
import com.ordina.ordinaCodeKids.user.UserRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Create a new employee, verify that a corresponding user has been generated also
 * then remove the employee and verify that the user has been removed also
 */
@SpringBootTest
public class Scenario5 {

    @Autowired
    EmployeeController employeeController;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    UserController userController;

    @Autowired
    UserRepository userRepository;

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Test
    @Tag("IntegrationTest")
    @Transactional
    void scenario5() {
        String email = "johndoe@email.com";
        String password = "myPassword";
        EmployeeDTO employeeDTO = EmployeeDTO.EmployeeDTOBuilder.anEmployeeDTO()
                .email(email)
                .firstName("John")
                .lastName("Doe")
                .educationPackages(new ArrayList<>())
                .build();

        UserDTO userDTO = UserDTO.UserDTOBuilder.anUserDTO()
                .email(email)
                .password(password)
                .role("ROLE_EMPLOYEE")
                .build();

        ResponseEntity<EmployeeDTO> response = employeeController.createEmployee(employeeDTO);
        EmployeeDTO createdEmployeeDTO = response.getBody();

        userController.createUser(userDTO);

        // verify added employee:
        assertTrue(employeeRepository.existsByEmail(email));

        // verify added user:
        assertTrue(userRepository.existsByEmail(email));

        // verify password is added correctly
        Optional<User> optionalUser = userRepository.findOneByEmail(email);
        assertTrue(optionalUser.isPresent());
        assertTrue(encoder.matches(password, optionalUser.get().getPassword()));

        // remove employee:
        employeeController.deleteEmployee(createdEmployeeDTO.getId());

        // verify removing employee:
        assertFalse(employeeRepository.existsByEmail(email));

        // verify removing user:
        assertFalse(userRepository.existsByEmail(email));

    }

}
