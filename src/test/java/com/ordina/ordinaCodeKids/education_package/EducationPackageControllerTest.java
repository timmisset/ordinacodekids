package com.ordina.ordinaCodeKids.education_package;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.exception.EducationPackageAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EducationPackageNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EducationPackageControllerTest {

    private EducationPackageController educationPackageController;
    private EducationPackageService educationPackageService;
    private EducationPackage mockEducationPackage;
    private List<EducationPackage> mockEducationPackages;

    @BeforeEach
    void setUp() {
        mockEducationPackage = MockData.getMockEducationPackage();
        mockEducationPackages = MockData.getMockEducationPackages();

        educationPackageService = new EducationPackageServiceImpl(mock(EducationPackageRepository.class));
        educationPackageService = spy(educationPackageService);
        educationPackageController = new EducationPackageController(educationPackageService);

    }

    @Test
    void getEducationPackages() {
        // arrange
        doReturn(mockEducationPackages).when(educationPackageService).getEducationPackages();

        // act
        ResponseEntity<List<EducationPackageDTO>> response = educationPackageController.getEducationPackages();

        // assert
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockEducationPackages.get(0).getId(), response.getBody().get(0).getId());
    }

    @Test
    void getEducationPackage() throws EducationPackageNotFoundException {
        // arrange
        long educationPackageId = mockEducationPackage.getId();
        doReturn(mockEducationPackage).when(educationPackageService).getEducationPackageById(educationPackageId);

        // act
        ResponseEntity<EducationPackageDTO> response = educationPackageController.getEducationPackage(educationPackageId);

        // assert
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockEducationPackage.getId(), response.getBody().getId());
    }

    @Test
    void getEducationPackageThrowsEducationPackageNotFoundException() throws EducationPackageNotFoundException {
        // arrange
        long educationPackageId = mockEducationPackage.getId();
        EducationPackageNotFoundException exception = new EducationPackageNotFoundException(educationPackageId);
        doThrow(exception).when(educationPackageService).getEducationPackageById(educationPackageId);

        // act
        try {
            educationPackageController.getEducationPackage(educationPackageId);
        }catch( ResponseStatusException e) {
            // assert
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void createEducationPackage() throws EducationPackageAlreadyExistsException {
        // arrange
        long newEducationPackageId = 11;
        doAnswer(invocationOnMock -> {
            EducationPackage educationPackage = invocationOnMock.getArgument(0);
            educationPackage.setId(newEducationPackageId);
            return educationPackage;
        }).when(educationPackageService).createEducationPackage(any());

        // act
        ResponseEntity<EducationPackageDTO> response = educationPackageController.createEducationPackage(new EducationPackageDTO());

        // assert
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(newEducationPackageId, response.getBody().getId());
    }

    @Test
    void createEducationPackageThrowsEducationPackageAlreadyExistsException() throws EducationPackageAlreadyExistsException {
        // arrange
        EducationPackageAlreadyExistsException exception = new EducationPackageAlreadyExistsException(mockEducationPackage);
        doThrow(exception).when(educationPackageService).createEducationPackage(any());

        // act
        try{
            educationPackageController.createEducationPackage(new EducationPackageDTO());
        }catch(ResponseStatusException e ) {
            // assert
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void updateEducationPackage() throws EducationPackageNotFoundException {
        // arrange
        long newEducationPackageId = 11;
        doAnswer(invocationOnMock -> {
            EducationPackage educationPackage = invocationOnMock.getArgument(0);
            educationPackage.setId(newEducationPackageId);
            return educationPackage;
        }).when(educationPackageService).updateEducationPackage(any());

        // act
        ResponseEntity<EducationPackageDTO> response = educationPackageController.updateEducationPackage(0, new EducationPackageDTO());
        EducationPackageDTO updatedEducationPackage = response.getBody();

        // assert
        assertNotNull(updatedEducationPackage);
        assertEquals(newEducationPackageId, updatedEducationPackage.getId());
        verify(educationPackageService, times(1)).updateEducationPackage(any());
    }

    @Test
    void updateEducationPackageThrowsEducationPackageNotFoundException() throws EducationPackageNotFoundException {
        // arrange
        EducationPackageNotFoundException exception = new EducationPackageNotFoundException(mockEducationPackage.getId());
        doThrow(exception).when(educationPackageService).getEducationPackageById(anyLong());

        long mockEducationPackageId = mockEducationPackage.getId();
        EducationPackageDTO mockEducationPackageDTO = EducationPackageDTO.EducationPackageDTOBuilder.anEducationPackageDTO().id(mockEducationPackageId).build();

        // act assert
        try {
            educationPackageController.updateEducationPackage(mockEducationPackageId, mockEducationPackageDTO);
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void deleteEducationPackage() throws EducationPackageNotFoundException {
        // arrange
        long mockEducationPackageId = mockEducationPackage.getId();
        doNothing().when(educationPackageService).deleteEducationPackageById(mockEducationPackageId);

        // act
        educationPackageController.deleteEducationPackage(mockEducationPackageId);

        // assert
        verify(educationPackageService, times(1)).deleteEducationPackageById(mockEducationPackageId);
    }

    @Test
    void deleteEducationPackageThrowsEducationPackageNotFoundException() throws EducationPackageNotFoundException {
        // arrange
        EducationPackageNotFoundException exception = new EducationPackageNotFoundException(mockEducationPackage.getId());
        doThrow(exception).when(educationPackageService).getEducationPackageById(anyLong());

        long mockEducationPackageId = mockEducationPackage.getId();

        // act assert
        try {
            educationPackageController.deleteEducationPackage(mockEducationPackageId);
        }catch( ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }
}
