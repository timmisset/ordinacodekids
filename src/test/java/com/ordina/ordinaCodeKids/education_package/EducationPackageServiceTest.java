package com.ordina.ordinaCodeKids.education_package;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.exception.EducationPackageAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EducationPackageNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EducationPackageServiceTest {

    private EducationPackageService educationPackageService;
    private EducationPackageRepository educationPackageRepository;
    private EducationPackage mockEducationPackage;
    private List<EducationPackage> mockEducationPackages;

    @BeforeEach
    void setUp() {
        mockEducationPackage = MockData.getMockEducationPackage();
        mockEducationPackages = MockData.getMockEducationPackages();

        educationPackageRepository = mock(EducationPackageRepository.class);
        educationPackageService = new EducationPackageServiceImpl(educationPackageRepository);
    }

    @Test
    void createEducationPackage() throws EducationPackageAlreadyExistsException {
        // arrange
        doReturn(false).when(educationPackageRepository).existsById(anyLong());

        // act
        educationPackageService.createEducationPackage(mockEducationPackage);

        // assert
        verify(educationPackageRepository, times(1)).existsById(anyLong());
        verify(educationPackageRepository, times(1)).save(mockEducationPackage);
    }

    @Test
    void createEducationPackageThrowsEducationPackageAlreadyExistsException() {
        // arrange
        doReturn(true).when(educationPackageRepository).existsById(anyLong());

        // act assert
        assertThrows(EducationPackageAlreadyExistsException.class, () -> educationPackageService.createEducationPackage(mockEducationPackage));
    }

    @Test
    void deleteEducationPackageById() throws EducationPackageNotFoundException {
        // arrange
        long educationPackageId = mockEducationPackage.getId();
        doReturn(true).when(educationPackageRepository).existsById(anyLong());

        // act
        educationPackageService.deleteEducationPackageById(educationPackageId);

        // assert
        verify(educationPackageRepository, times(1)).existsById(anyLong());
        verify(educationPackageRepository, times(1)).existsById(educationPackageId);
    }

    @Test
    void deleteEducationPackageByIdThrowsEducationPackageNotFoundException() {
        // arrange
        doReturn(false).when(educationPackageRepository).existsById(anyLong());
        long educationPackageId = mockEducationPackage.getId();

        // act assert
        assertThrows(EducationPackageNotFoundException.class, () -> educationPackageService.deleteEducationPackageById(educationPackageId));
    }

    @Test
    void updateEducationPackage() throws EducationPackageNotFoundException {
        // arrange
        doReturn(true).when(educationPackageRepository).existsById(anyLong());

        // act
        educationPackageService.updateEducationPackage(mockEducationPackage);

        // assert
        verify(educationPackageRepository, times(1)).existsById(anyLong());
        verify(educationPackageRepository, times(1)).save(mockEducationPackage);

    }

    @Test
    void updateEducationPackageByIdThrowsEducationPackageNotFoundException() {
        // arrange
        doReturn(false).when(educationPackageRepository).existsById(anyLong());

        // act assert
        assertThrows(EducationPackageNotFoundException.class, () -> educationPackageService.updateEducationPackage(mockEducationPackage));
    }

    @Test
    void getEducationPackageById() throws EducationPackageNotFoundException {
        // arrange
        long mockEducationPackageId = mockEducationPackage.getId();
        doReturn(Optional.of(mockEducationPackage)).when(educationPackageRepository).findById(mockEducationPackageId);

        // act
        EducationPackage returnedEducationPackage = educationPackageService.getEducationPackageById(mockEducationPackageId);

        // assert
        assertEquals(mockEducationPackage, returnedEducationPackage);
        verify(educationPackageRepository, times(1)).findById(mockEducationPackageId);

    }
    @Test
    void getEducationPackageByIdThrowsEducationPackageNotFoundException () {
        // arrange
        long mockEducationPackageId = mockEducationPackage.getId();
        doReturn(Optional.empty()).when(educationPackageRepository).findById(anyLong());

        // act assert
        assertThrows(EducationPackageNotFoundException.class, () -> educationPackageService.getEducationPackageById(mockEducationPackageId));
    }

    @Test
    void getEducationPackageByName() throws EducationPackageNotFoundException {
        // arrange
        String mockEducationPackageName = mockEducationPackage.getName();
        doReturn(Optional.of(mockEducationPackage)).when(educationPackageRepository).findByName(mockEducationPackageName);

        // act
        EducationPackage returnedEducationPackage = educationPackageService.getEducationPackageByName(mockEducationPackageName);

        // assert
        assertEquals(mockEducationPackage, returnedEducationPackage);
        verify(educationPackageRepository, times(1)).findByName(mockEducationPackageName);
    }

    @Test
    void getEducationPackageByNameThrowsEducationPackageNotFoundException () {
        // arrange
        String mockEducationPackageName = mockEducationPackage.getName();
        doReturn(Optional.empty()).when(educationPackageRepository).findByName(anyString());

        // act assert
        assertThrows(EducationPackageNotFoundException.class, () -> educationPackageService.getEducationPackageByName(mockEducationPackageName));
    }

    @Test
    void getEducationPackages() {
        // arrange
        doReturn(mockEducationPackages).when(educationPackageRepository).findAll();

        // act
        List<EducationPackage> returnedEducationPackages = educationPackageService.getEducationPackages();

        // assert
        assertEquals(mockEducationPackages, returnedEducationPackages);
        verify(educationPackageRepository, times(1)).findAll();
    }

    @Test
    void toEducationPackageMapping() {
        // arrange
        EducationPackageDTO educationPackageDTO = MockData.getMockEducationPackageDTO();

        // act
        EducationPackage educationPackage = educationPackageService.toEducationPackageMapping(educationPackageDTO);

        // assert
        assertEquals(educationPackageDTO.getId(), educationPackage.getId());
        assertEquals(educationPackageDTO.getName(), educationPackage.getName());
        assertEquals(educationPackageDTO.getDescription(), educationPackage.getDescription());
    }

    @Test
    void fromEducationPackageMapping() {
        // arrange

        // act
        EducationPackageDTO educationPackageDTO = educationPackageService.fromEducationPackageMapping(mockEducationPackage);

        // assert
        assertEquals(mockEducationPackage.getId(), educationPackageDTO.getId());
        assertEquals(mockEducationPackage.getName(), educationPackageDTO.getName());
        assertEquals(mockEducationPackage.getDescription(), educationPackageDTO.getDescription());
    }
}
