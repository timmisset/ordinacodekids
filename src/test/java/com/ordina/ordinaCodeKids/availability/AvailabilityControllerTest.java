package com.ordina.ordinaCodeKids.availability;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.exception.AvailabilityAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.AvailabilityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AvailabilityControllerTest {

    private AvailabilityController availabilityController;
    private AvailabilityService availabilityService;
    private List<Availability> mockAvailabilities;
    private Availability mockAvailability;

    @BeforeEach
    void setUp() {
        availabilityService = new AvailabilityServiceImpl(mock(AvailabilityRepository.class));
        availabilityService = spy(availabilityService);
        availabilityController = new AvailabilityController(availabilityService);

        mockAvailabilities = MockData.getMockAvailabilities();
        mockAvailability = MockData.getMockAvailability();
    }

    @Test
    void getAvailabilities() {
        // arrange
        doReturn(mockAvailabilities).when(availabilityService).getAvailabilities();

        // act
        ResponseEntity<List<AvailabilityDTO>> response = availabilityController.getAvailabilities();

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockAvailabilities.size(), response.getBody().size());
        verify(availabilityService, times(1)).getAvailabilities();
    }

    @Test
    void getAvailability() throws AvailabilityNotFoundException {
        // arrange
        doReturn(mockAvailability).when(availabilityService).getAvailabilityById(anyLong());

        // act
        ResponseEntity<AvailabilityDTO> response = availabilityController.getAvailability(0);

        // assert
        assertNotNull(response.getBody());
        assertEquals(mockAvailability.getEmployee().getId(), response.getBody().getEmployeeId());
        verify(availabilityService, times(1)).getAvailabilityById(anyLong());
    }

    @Test
    void getAvailabilityThrowsAvailabilityNotFoundException() throws AvailabilityNotFoundException {
        // arrange
        AvailabilityNotFoundException exception = new AvailabilityNotFoundException(mockAvailability.getId());
        doThrow(exception).when(availabilityService).getAvailabilityById(anyLong());

        // act assert
        try {
            availabilityController.getAvailability(mockAvailability.getId());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void createAvailability() throws AvailabilityAlreadyExistsException {
        // arrange
        long newAvailabilityId = 11;
        doAnswer(invocationOnMock -> {
            Availability availability = invocationOnMock.getArgument(0);
            availability.setId(newAvailabilityId);
            return availability;
        }).when(availabilityService).createAvailability(any());

        // act
        ResponseEntity<AvailabilityDTO> response = availabilityController.createAvailability(new AvailabilityDTO());
        AvailabilityDTO createdAvailability = response.getBody();

        // assert
        assertNotNull(createdAvailability);
        assertEquals(newAvailabilityId, createdAvailability.getId());
        verify(availabilityService, times(1)).createAvailability(any());
    }

    @Test
    void createAvailabilityThrowsAvailabilityAlreadyExistsException() throws AvailabilityAlreadyExistsException {
        // arrange
        AvailabilityAlreadyExistsException exception = new AvailabilityAlreadyExistsException(mockAvailability);
        doThrow(exception).when(availabilityService).createAvailability(any());

        // act assert
        try {
            availabilityController.createAvailability(new AvailabilityDTO());
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void updateAvailability() throws AvailabilityNotFoundException {
        // arrange
        long newAvailabilityId = 11;
        doAnswer(invocationOnMock -> {
            Availability availability = invocationOnMock.getArgument(0);
            availability.setId(newAvailabilityId);
            return availability;
        }).when(availabilityService).updateAvailability(any());

        // act
        ResponseEntity<AvailabilityDTO> response = availabilityController.updateAvailability(0, new AvailabilityDTO());
        AvailabilityDTO updatedAvailability = response.getBody();

        // assert
        assertNotNull(updatedAvailability);
        assertEquals(newAvailabilityId, updatedAvailability.getId());
        verify(availabilityService, times(1)).updateAvailability(any());
    }

    @Test
    void updateAvailabilityThrowsAvailabilityNotFoundException() throws AvailabilityNotFoundException {
        // arrange
        AvailabilityNotFoundException exception = new AvailabilityNotFoundException(mockAvailability.getId());
        doThrow(exception).when(availabilityService).getAvailabilityById(anyLong());

        long mockAvailabilityId = mockAvailability.getId();
        AvailabilityDTO mockAvailabilityDTO = AvailabilityDTO.AvailabilityDTOBuilder.anAvailabilityDTO().id(mockAvailabilityId).build();

        // act assert
        try {
            availabilityController.updateAvailability(mockAvailabilityId, mockAvailabilityDTO);
        }
        catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void deleteAvailability() throws AvailabilityNotFoundException {
        // arrange
        long mockAvailabilityId = mockAvailability.getId();
        doNothing().when(availabilityService).deleteAvailabilityById(mockAvailabilityId);

        // act
        availabilityController.deleteAvailability(mockAvailabilityId);

        // assert
        verify(availabilityService, times(1)).deleteAvailabilityById(mockAvailabilityId);
    }

    @Test
    void deleteAvailabilityThrowsAvailabilityNotFoundException() throws AvailabilityNotFoundException {
        // arrange
        AvailabilityNotFoundException exception = new AvailabilityNotFoundException(mockAvailability.getId());
        doThrow(exception).when(availabilityService).getAvailabilityById(anyLong());

        long mockAvailabilityId = mockAvailability.getId();

        // act assert
       try {
           availabilityController.deleteAvailability(mockAvailabilityId);
       }catch( ResponseStatusException e) {
           assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
           assertEquals(exception.getMessage(), e.getReason());
       }
    }

    @Test
    void deleteAvailabilities() throws AvailabilityNotFoundException {
        // arrange
        long mockAvailabilityId = mockAvailability.getId();
        doNothing().when(availabilityService).deleteAvailabilityById(mockAvailabilityId);

        // act
        availabilityController.deleteAvailabilities(List.of(mockAvailabilityId, mockAvailabilityId));

        // assert
        verify(availabilityService, times(2)).deleteAvailabilityById(mockAvailabilityId);
    }

    @Test
    void deleteAvailabilitiesThrowsAvailabilityNotFoundException() throws AvailabilityNotFoundException {
        // arrange
        AvailabilityNotFoundException exception = new AvailabilityNotFoundException(mockAvailability.getId());
        doThrow(exception).when(availabilityService).getAvailabilityById(anyLong());

        long mockAvailabilityId = mockAvailability.getId();

        // act assert
        try {
            availabilityController.deleteAvailabilities(List.of(mockAvailabilityId));
        }catch( ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            assertEquals(exception.getMessage(), e.getReason());
        }
    }

    @Test
    void getAvailabilitiesByEmployee() {
        // arrange
        doReturn(mockAvailabilities).when(availabilityService).getAvailabilitiesByEmployeeId(anyLong());

        // act
        ResponseEntity<List<AvailabilityDTO>> response = availabilityController.getAvailabilitiesByEmployee(0);

        // assert
        assertTrue(response.hasBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(availabilityService, times(1)).getAvailabilitiesByEmployeeId(anyLong());

    }
}
