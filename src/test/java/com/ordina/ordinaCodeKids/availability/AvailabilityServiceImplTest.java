package com.ordina.ordinaCodeKids.availability;

import com.ordina.ordinaCodeKids.MockData;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.exception.AvailabilityAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.AvailabilityNotFoundException;
import com.ordina.ordinaCodeKids.exception.EmployeeNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class AvailabilityServiceImplTest {

    private AvailabilityRepository availabilityRepository;
    private AvailabilityService availabilityService;
    private List<Availability> mockAvailabilities;
    private Availability mockAvailability;

    @BeforeEach
    void setUp() {
        availabilityRepository = mock(AvailabilityRepository.class);
        availabilityService = new AvailabilityServiceImpl(availabilityRepository);

        mockAvailability = MockData.getMockAvailability();
        mockAvailabilities = MockData.getMockAvailabilities();
    }

    @Test
    // check that the saved availability is returned and no exceptions are thrown
    void createAvailability() throws AvailabilityAlreadyExistsException {
        // arrange
        doReturn(false).when(availabilityRepository).existsById(anyLong());
        doReturn(mockAvailability).when(availabilityRepository).save(any());


        // act
        Availability addedAvailability = availabilityService.createAvailability(mockAvailability);

        // assert
        assertEquals(mockAvailability, addedAvailability);
        verify(availabilityRepository, times(1)).existsById(anyLong());
    }

    @Test
    // check that the exception is correctly thrown when the availability already exists
    void createAvailabilityThrowsAvailabilityAlreadyExistsException() {
        // arrange
        doReturn(true).when(availabilityRepository).existsById(anyLong());

        // act assert
        assertThrows(AvailabilityAlreadyExistsException.class, () -> availabilityService.createAvailability(mockAvailability));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteAvailability() throws AvailabilityNotFoundException {
        // arrange
        doReturn(true).when(availabilityRepository).existsById(anyLong());

        // act
        availabilityService.deleteAvailability(mockAvailability);

        // assert
        verify(availabilityRepository, times(1)).deleteById(mockAvailability.getId());
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteAvailabilityById() throws AvailabilityNotFoundException {
        // arrange
        doReturn(true).when(availabilityRepository).existsById(anyLong());

        // act
        availabilityService.deleteAvailabilityById(mockAvailability.getId());

        // assert
        verify(availabilityRepository, times(1)).deleteById(mockAvailability.getId());
    }

    @Test
        // check that the repository is called with the correct id for removal
    void deleteAvailabilityByIdThrowsAvailabilityNotFoundException() {
        // arrange
        doReturn(false).when(availabilityRepository).existsById(anyLong());

        // act assert
        assertThrows(AvailabilityNotFoundException.class, () -> availabilityService.deleteAvailability(mockAvailability));
    }

    @Test
    // check that the repository is called with the correct id for removal
    void deleteAvailabilityThrowsAvailabilityNotFoundException() {
        // arrange
        doReturn(false).when(availabilityRepository).existsById(anyLong());

        // act assert
        assertThrows(AvailabilityNotFoundException.class, () -> availabilityService.deleteAvailability(mockAvailability));
    }

    @Test
    // check that the repository is called with item to save
    void updateAvailability() throws AvailabilityNotFoundException {
        // arrange
        doReturn(mockAvailability).when(availabilityRepository).save(any());
        doReturn(true).when(availabilityRepository).existsById(anyLong());

        // act
        Availability savedAvailability = availabilityService.updateAvailability(mockAvailabilities.get(0));

        // assert
        assertEquals(mockAvailability, savedAvailability);
    }

    @Test
        // check that the repository is called with item to save
    void updateAvailabilityThrowsAvailabilityNotFoundException() {
        // arrange
        doReturn(false).when(availabilityRepository).existsById(anyLong());

        // act assert
        assertThrows(AvailabilityNotFoundException.class, () -> availabilityService.updateAvailability(mockAvailability));
    }

    @Test
    // check that the availability is returned when the repository finds it by the provided id
    void getAvailabilityById() throws AvailabilityNotFoundException {
        // arrange
        doReturn(Optional.of(mockAvailability)).when(availabilityRepository).findById(any());

        // act
        Availability returnedAvailability = availabilityService.getAvailabilityById(0);

        // assert
        assertEquals(returnedAvailability, mockAvailability);
        verify(availabilityRepository, times(1)).findById(any());
    }

    @Test
    // check that the correct exception is thrown when the repository returns an empty optional
    void getAvailabilityByIdThrowsAvailabilityNotFoundException() {
        // arrange
        doReturn(Optional.empty()).when(availabilityRepository).findById(any());

        // act, assert
        assertThrows(AvailabilityNotFoundException.class, () -> availabilityService.getAvailabilityById(0));
    }

    @Test
    // check that all the availabilities are returned
    void getAvailabilities() {
        // arrange
        doReturn(mockAvailabilities).when(availabilityRepository).findAll();

        // act
        List<Availability> returnedAvailabilities = availabilityService.getAvailabilities();

        // assert
        assertEquals(mockAvailabilities, returnedAvailabilities);
        verify(availabilityRepository, times(1)).findAll();

    }

    @Test
    // check if the mapping is performed correctly
    void toAvailabilityMapping() {
        // arrange
        AvailabilityDTO availabilityDTO = MockData.getMockAvailabilityDTO();

        // act
        Availability availability = availabilityService.toAvailabilityMapping(availabilityDTO);

        // assert
        assertEquals(availabilityDTO.getEmployeeId(), availability.getEmployee().getId());
        assertEquals(availabilityDTO.getId(), availability.getId());
    }

    @Test
    void fromAvailabilityMapping() {
        // arrange

        // act
        AvailabilityDTO availabilityDTO = availabilityService.fromAvailabilityMapping(mockAvailability);

        // assert
        assertEquals(mockAvailability.getEmployee().getId(), availabilityDTO.getEmployeeId());
        assertEquals(mockAvailability.getId(), availabilityDTO.getId());

    }

    @Test
    void getAvailabilitiesByEmployeeId() {
        // arrange
        Employee mockEmployee = MockData.getMockEmployee();
        doAnswer(invocationOnMock -> {
            if(((Employee)invocationOnMock.getArgument(0)).getId() != mockEmployee.getId()) {
                throw new EmployeeNotFoundException(invocationOnMock.getArgument(0));
            }
            return mockAvailabilities;
        }).when(availabilityRepository).findAllByEmployee(any(Employee.class));

        // act
        List<Availability> availabilitiesByEmployee = availabilityService.getAvailabilitiesByEmployeeId(mockEmployee.getId());

        // assert
        assertEquals(mockAvailabilities, availabilitiesByEmployee);
    }

}
