(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/animated-icon/animated-icon.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/animated-icon/animated-icon.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button mat-icon-button (click)=\"buttonClick()\">\r\n  <mat-icon [ngClass]=\"animatedPosition ? 'animatedIcon' : 'defaultIcon'\">{{animatedPosition ? defaultIcon : animatedIcon}}</mat-icon>\r\n</button>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"mat-title\">{{title}}</div>\r\n<app-demobar *ngIf=\"showToolbar\"></app-demobar>\r\n<div class=\"tabs\">\r\n  <mat-tab-group (selectedTabChange)=\"tabChanged($event)\" [(selectedIndex)]=\"selectedRouteIndex\">\r\n    <mat-tab *ngFor=\"let route of routes\"  [label]=\"route.data.label\"></mat-tab>\r\n  </mat-tab-group>\r\n  <span class=\"spacer\"></span>\r\n  <app-animated-icon defaultIcon=\"keyboard_arrow_down\" (click)=\"showToolbar=!showToolbar\" animatedIcon=\"keyboard_arrow_up\"></app-animated-icon>\r\n</div>\r\n<router-outlet></router-outlet>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/availability/availability.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/availability/availability.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-toolbar *ngIf=\"isAdminOrEmployee\">\r\n  <mat-toolbar-row>\r\n    <span class=\"spacer\"></span>\r\n    <mat-form-field [matBadge]=\"selectEmployeeBadge\" matBadgePosition=\"after\" matBadgeColor=\"accent\"  *ngIf=\"isAdmin\">\r\n      <mat-label>for employee: </mat-label>\r\n      <mat-select [(value)]=\"employee\">\r\n        <mat-option *ngFor=\"let employee of employees\" [value]=\"employee\">{{employee.firstName}} {{employee.lastName}}</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field>\r\n      <input matInput (dateChange)=\"dateSelected($event)\" [matDatepickerFilter]=\"dateFilter\" [matDatepicker]=\"picker\" placeholder=\"\">\r\n      <mat-datepicker #picker></mat-datepicker>\r\n    </mat-form-field>\r\n\r\n    <button mat-icon-button color=\"primary\" title=\"Create\" (click)=\"openPicker(picker)\"><mat-icon>add_circle</mat-icon></button>\r\n    <button mat-icon-button color=\"primary\" title=\"Delete\" *ngIf=\"isAdminOrEmployee && selection.hasValue() > 0\" (click)=\"remove()\"><mat-icon>delete</mat-icon></button>\r\n  </mat-toolbar-row>\r\n</mat-toolbar>\r\n<mat-form-field>\r\n  <input matInput data-input-id=\"filter\" (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\r\n</mat-form-field>\r\n<table mat-table [dataSource]=\"dataSource\" matSort  class=\"mat-elevation-z8\">\r\n\r\n  <!-- Checkbox Column -->\r\n  <ng-container matColumnDef=\"select\">\r\n    <th mat-header-cell *matHeaderCellDef>\r\n      <mat-checkbox (change)=\"$event ? masterToggle($event) : null\"\r\n                    [checked]=\"selection.hasValue() && isAllSelected()\"\r\n                    [indeterminate]=\"selection.hasValue() && !isAllSelected()\" >\r\n      </mat-checkbox>\r\n    </th>\r\n    <td mat-cell *matCellDef=\"let row\">\r\n      <mat-checkbox (click)=\"$event.stopPropagation()\"\r\n                    (change)=\"$event ? selection.toggle(row) : null\"\r\n                    [checked]=\"selection.isSelected(row)\"\r\n                    >\r\n      </mat-checkbox>\r\n    </td>\r\n  </ng-container>\r\n\r\n  <!-- Position Column -->\r\n  <ng-container matColumnDef=\"date\">\r\n    <th mat-header-cell mat-sort-header *matHeaderCellDef> Date </th>\r\n    <td mat-cell *matCellDef=\"let availability\"> {{ getAvailabilityDate(availability) }} </td>\r\n  </ng-container>\r\n\r\n  <!-- Name Column -->\r\n  <ng-container matColumnDef=\"employee\">\r\n    <th mat-header-cell mat-sort-header *matHeaderCellDef> Name </th>\r\n    <td mat-cell *matCellDef=\"let availability\"> {{ getEmployeeAttribute(availability.employeeId, \"firstName\")}} {{ getEmployeeAttribute(availability.employeeId, \"lastName\")}} </td>\r\n  </ng-container>\r\n\r\n  <!-- School Name Column -->\r\n  <ng-container matColumnDef=\"booking-school-name\">\r\n    <th mat-header-cell mat-sort-header *matHeaderCellDef> School Name </th>\r\n    <td mat-cell *matCellDef=\"let availability\"> {{ getSchoolAttribute(availability.bookingId, \"name\") }} </td>\r\n  </ng-container>\r\n\r\n  <!-- School Contact Column -->\r\n  <ng-container matColumnDef=\"booking-school-contact\">\r\n    <th mat-header-cell mat-sort-header *matHeaderCellDef> School Contact </th>\r\n    <td mat-cell *matCellDef=\"let availability\"> {{ getSchoolAttribute(availability.bookingId, \"contactName\") }} </td>\r\n  </ng-container>\r\n\r\n  <!-- School Contact Email Column -->\r\n  <ng-container matColumnDef=\"booking-school-contact-email\">\r\n    <th mat-header-cell mat-sort-header *matHeaderCellDef> School Contact Email </th>\r\n    <td mat-cell *matCellDef=\"let availability\"> {{ getSchoolAttribute(availability.bookingId, \"contactEmail\") }} </td>\r\n  </ng-container>\r\n\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n  <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n      (click)=\"selection.toggle(row)\">\r\n  </tr>\r\n\r\n</table>\r\n<mat-paginator [pageSizeOptions]=\"[10, 20, dataSource.data.length]\" showFirstLastButtons></mat-paginator>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar-event/calendar-event.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/calendar-event/calendar-event.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-card>\r\n  <mat-card-content>\r\n    <mat-list>\r\n      <mat-list-item>\r\n        <span>\r\n          {{ ordinaCalendarEvent.availability.date.toLocaleDateString() }}\r\n          -\r\n          {{ ordinaCalendarEvent.employee.firstName }} {{ ordinaCalendarEvent.employee.lastName }}\r\n        </span>\r\n        <span class=\"spacer\"></span>\r\n        <button *ngIf=\"canBook()\" mat-stroked-button color=\"primary\" (click)=\"book()\">Book</button>\r\n        <button *ngIf=\"canCancel()\" mat-stroked-button color=\"accent\" (click)=\"cancel()\">Cancel</button>\r\n        <mat-form-field [matBadge]=\"selectSchoolBadge\" matBadgePosition=\"after\" matBadgeColor=\"accent\"  *ngIf=\"isAdmin && !ordinaCalendarEvent.booking\">\r\n          <mat-label>for school: </mat-label>\r\n          <mat-select [(value)]=\"targetSchool\">\r\n            <mat-option *ngFor=\"let school of schools\" [value]=\"school\">{{school.name}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <button mat-icon-button color=\"primary\" (click)=\"close()\"><mat-icon>cancel</mat-icon></button>\r\n\r\n\r\n      </mat-list-item>\r\n      <mat-list-item>Skills: {{ ordinaCalendarEvent.employee.educationPackages.join(\", \") }}</mat-list-item>\r\n\r\n    </mat-list>\r\n\r\n\r\n  </mat-card-content>\r\n\r\n</mat-card>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar/calendar.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/calendar/calendar.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-toolbar>\r\n  <mat-toolbar-row>\r\n    <h3>\r\n      {{ title }}\r\n    </h3>\r\n    <span class=\"spacer\"></span>\r\n    <mat-button-toggle-group>\r\n      <mat-button-toggle mat-icon-button (click)=\"offset(-1)\"  title=\"Previous {{ this.view }}\"><mat-icon>arrow_back</mat-icon></mat-button-toggle>\r\n      <mat-button-toggle mat-icon-button (click)=\"current()\"  title=\"Current {{ this.view }}\"><mat-icon>center_focus_strong</mat-icon></mat-button-toggle>\r\n      <mat-button-toggle mat-icon-button (click)=\"offset(1)\" title=\"Next {{ this.view }}\"><mat-icon>arrow_forward</mat-icon></mat-button-toggle>\r\n      <mat-button-toggle mat-icon-button (click)=\"toggleView()\"><mat-icon title=\"Show month view\" *ngIf=\"view === 'week'\">view_comfy</mat-icon><mat-icon title=\"Show week view\" *ngIf=\"view === 'month'\">view_column</mat-icon></mat-button-toggle>\r\n    </mat-button-toggle-group>\r\n\r\n  </mat-toolbar-row>\r\n</mat-toolbar>\r\n\r\n<div *ngIf=\"showEvent()\">\r\n  <app-calendar-event\r\n    [ordinaCalendarEvent]=\"selectedEvent\"\r\n    (closeClicked)=\"this.selectedEvent = undefined\"></app-calendar-event>\r\n</div>\r\n<div [ngSwitch]=\"view\">\r\n  <mwl-calendar-month-view\r\n    *ngSwitchCase=\"CalendarView.Month\"\r\n    [viewDate]=\"viewDate\"\r\n    [events]=\"events\"\r\n    [excludeDays]=\"excludeDays\"\r\n    (dayClicked)=\"dayClicked($event.day)\"\r\n  >\r\n  </mwl-calendar-month-view>\r\n  <div *ngSwitchCase=\"CalendarView.Week\">\r\n      <mwl-calendar-week-view\r\n      (eventClicked)=\"eventClick($event)\"\r\n      [viewDate]=\"viewDate\"\r\n      [events]=\"events\"\r\n      dayStartHour=\"0\"\r\n      [dayEndHour]=\"calendarHoursInDay\"\r\n      [excludeDays]=\"excludeDays\">\r\n    </mwl-calendar-week-view>\r\n  </div>\r\n\r\n</div>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/demobar/demobar.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/demobar/demobar.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-toolbar>\r\n  <mat-toolbar-row>\r\n    <span>\r\n      Ordina CodeKids :: DemoBar\r\n    </span>\r\n    <span class=\"spacer\"></span>\r\n    <button mat-icon-button color=\"primary\" data-button-id=\"btnResetDemoData\" title=\"Reset demo data\" (click)=\"resetDemo()\"><mat-icon>refresh</mat-icon></button>\r\n    <button mat-icon-button color=\"primary\" data-button-id=\"btnToggleBookedSlots\" title=\"Show / hide booked slots\" (click)=\"toggleBookedSlots()\"><mat-icon>remove_red_eye</mat-icon></button>\r\n\r\n  </mat-toolbar-row>\r\n  <mat-toolbar-row class=\"second-toolbar-row\">\r\n    <span>Login as</span>\r\n    <span class=\"spacer\"></span>\r\n    <mat-form-field>\r\n      <mat-label>Employee</mat-label>\r\n      <mat-select [(value)]=\"selectedEmployee\" (valueChange)=\"login('employee')\">\r\n        <mat-option *ngFor=\"let employee of employees\" [value]=\"employee\">{{employee.firstName}} {{employee.lastName}}</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field>\r\n      <mat-label>School</mat-label>\r\n      <mat-select [(value)]=\"selectedSchool\" (valueChange)=\"login('school')\">\r\n        <mat-option *ngFor=\"let school of schools\" [value]=\"school\">{{school.name}}</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <button [matBadge]=\"adminButtonBadge\" matBadgePosition=\"before\" matBadgeColor=\"accent\" mat-stroked-button color=\"primary\" title=\"Login as admin\" (click)=\"login('admin')\">Admin</button>\r\n  </mat-toolbar-row>\r\n  <mat-toolbar-row class=\"second-toolbar-row\">\r\n    <span>\r\n      Logged in as:\r\n      {{ currentUser ? currentUser.email + ' - ' + currentUser.role : '' }}\r\n    </span>\r\n  </mat-toolbar-row>\r\n\r\n</mat-toolbar>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dialog-confirm/dialog-confirm.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dialog-confirm/dialog-confirm.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 *ngIf=\"data.title\" mat-dialog-title>{{data.title}}</h1>\r\n<div mat-dialog-content>\r\n  <p>{{ data.description }}</p>\r\n</div>\r\n<div mat-dialog-actions>\r\n  <button *ngIf=\"data.showCancelButton\" data-button-type=\"cancel\" cdkFocusInitial mat-button [mat-dialog-close]=\"false\">{{ data.cancelButtonText }}</button>\r\n  <button mat-button [mat-dialog-close]=\"true\" data-button-type=\"confirm\" >Ok</button>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/employee/employee.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/employee/employee.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"employeeForm\">\r\n\r\n    <mat-form-field>\r\n      <mat-label>Select employee</mat-label>\r\n      <input type=\"text\" placeholder=\"Start typing ... \" matInput formControlName=\"select_employee\" [matAutocomplete]=\"auto\">\r\n      <mat-autocomplete #auto=\"matAutocomplete\" [displayWith]=\"displayEmployee\">\r\n        <mat-option *ngFor=\"let filteredEmployee of filteredOptions\" [value]=\"filteredEmployee\">\r\n          {{filteredEmployee.firstName}} {{filteredEmployee.lastName}}\r\n        </mat-option>\r\n      </mat-autocomplete>\r\n    </mat-form-field>\r\n    <div *ngIf=\"employeeData\">\r\n      <mat-form-field class=\"shared-row\">\r\n        <mat-label>First name</mat-label>\r\n        <input type=\"text\" placeholder=\"First name\" matInput formControlName=\"firstName\">\r\n      </mat-form-field>\r\n      <mat-form-field class=\"shared-row\">\r\n        <mat-label>Last name</mat-label>\r\n        <input type=\"text\" placeholder=\"Last name\" matInput formControlName=\"lastName\">\r\n      </mat-form-field>\r\n      <mat-form-field>\r\n        <mat-label>Email</mat-label>\r\n        <input type=\"text\" placeholder=\"Email\" matInput formControlName=\"email\">\r\n      </mat-form-field>\r\n      <mat-form-field>\r\n        <mat-label>Password</mat-label>\r\n        <input type=\"password\" placeholder=\"Password\" matInput formControlName=\"password\">\r\n      </mat-form-field>\r\n      <mat-form-field>\r\n        <mat-label>Education packages</mat-label>\r\n        <mat-select formControlName=\"educationPackages\" multiple>\r\n          <mat-option *ngFor=\"let educationPackage of educationPackages\" [value]=\"educationPackage\">{{educationPackage}}</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n</form>\r\n<mat-button-toggle-group>\r\n  <mat-button-toggle mat-stroked-button (click)=\"new()\">Create new</mat-button-toggle>\r\n  <mat-button-toggle *ngIf=\"employee\" mat-stroked-button (click)=\"remove()\">Remove</mat-button-toggle>\r\n  <mat-button-toggle *ngIf=\"employeeData\" mat-stroked-button (click)=\"save()\">Save</mat-button-toggle>\r\n</mat-button-toggle-group>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/school/school.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/school/school.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"schoolForm\">\r\n\r\n  <mat-form-field>\r\n    <mat-label>Select school</mat-label>\r\n    <input type=\"text\" placeholder=\"Start typing ... \" matInput formControlName=\"select_school\" [matAutocomplete]=\"auto\">\r\n    <mat-autocomplete #auto=\"matAutocomplete\" [displayWith]=\"displaySchool\">\r\n      <mat-option *ngFor=\"let filteredSchool of filteredOptions\" [value]=\"filteredSchool\">\r\n        {{filteredSchool.name}}\r\n      </mat-option>\r\n    </mat-autocomplete>\r\n  </mat-form-field>\r\n  <div *ngIf=\"schoolData\">\r\n    <mat-form-field class=\"shared-row\">\r\n      <mat-label>Name</mat-label>\r\n      <input type=\"text\" placeholder=\"Name\" matInput formControlName=\"name\">\r\n    </mat-form-field>\r\n    <mat-form-field class=\"shared-row\">\r\n      <mat-label>Contact name</mat-label>\r\n      <input type=\"text\" placeholder=\"Contact name\" matInput formControlName=\"contactName\">\r\n    </mat-form-field>\r\n    <mat-form-field>\r\n      <mat-label>Contact email</mat-label>\r\n      <input type=\"text\" placeholder=\"Contact email\" matInput formControlName=\"contactEmail\">\r\n    </mat-form-field>\r\n    <mat-form-field>\r\n      <mat-label>Password</mat-label>\r\n      <input type=\"password\" placeholder=\"Password\" matInput formControlName=\"password\">\r\n    </mat-form-field>\r\n  </div>\r\n\r\n</form>\r\n<mat-button-toggle-group>\r\n  <mat-button-toggle mat-stroked-button (click)=\"new()\">Create new</mat-button-toggle>\r\n  <mat-button-toggle *ngIf=\"school\" mat-stroked-button (click)=\"remove()\">Remove</mat-button-toggle>\r\n  <mat-button-toggle *ngIf=\"schoolData\" mat-stroked-button (click)=\"save()\">Save</mat-button-toggle>\r\n</mat-button-toggle-group>\r\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/animated-icon/animated-icon.component.css":
/*!***********************************************************!*\
  !*** ./src/app/animated-icon/animated-icon.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".animatedIcon {\r\n  transition: transform .5s;\r\n  transform: rotate(360deg);\r\n  color: rgb(103, 58, 183);\r\n}\r\n.defaultIcon {\r\n  transition: transform .5s;\r\n  transform: rotate(0deg);\r\n  color: #000000;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYW5pbWF0ZWQtaWNvbi9hbmltYXRlZC1pY29uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIseUJBQXlCO0VBQ3pCLHdCQUF3QjtBQUMxQjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLHVCQUF1QjtFQUN2QixjQUFjO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvYW5pbWF0ZWQtaWNvbi9hbmltYXRlZC1pY29uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYW5pbWF0ZWRJY29uIHtcclxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzO1xyXG4gIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgY29sb3I6IHJnYigxMDMsIDU4LCAxODMpO1xyXG59XHJcbi5kZWZhdWx0SWNvbiB7XHJcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC41cztcclxuICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG5cclxuIl19 */");

/***/ }),

/***/ "./src/app/animated-icon/animated-icon.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/animated-icon/animated-icon.component.ts ***!
  \**********************************************************/
/*! exports provided: AnimatedIconComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnimatedIconComponent", function() { return AnimatedIconComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AnimatedIconComponent = class AnimatedIconComponent {
    constructor() {
        this.animatedPosition = false;
    }
    ngOnInit() {
    }
    buttonClick() {
        this.animatedPosition = !this.animatedPosition;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], AnimatedIconComponent.prototype, "defaultIcon", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], AnimatedIconComponent.prototype, "animatedIcon", void 0);
AnimatedIconComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-animated-icon',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./animated-icon.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/animated-icon/animated-icon.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./animated-icon.component.css */ "./src/app/animated-icon/animated-icon.component.css")).default]
    })
], AnimatedIconComponent);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./calendar/calendar.component */ "./src/app/calendar/calendar.component.ts");
/* harmony import */ var _availability_availability_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./availability/availability.component */ "./src/app/availability/availability.component.ts");
/* harmony import */ var _employee_employee_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employee/employee.component */ "./src/app/employee/employee.component.ts");
/* harmony import */ var _school_school_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./school/school.component */ "./src/app/school/school.component.ts");







const routes = [
    // routes for roles
    { path: 'calendar', component: _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_3__["CalendarComponent"], data: { roles: ["ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_SCHOOL"], index: 0, label: 'Calendar' } },
    { path: 'availability', component: _availability_availability_component__WEBPACK_IMPORTED_MODULE_4__["AvailabilityComponent"], data: { roles: ["ROLE_ADMIN", "ROLE_EMPLOYEE"], index: 1, label: 'Availability' } },
    { path: 'employee', component: _employee_employee_component__WEBPACK_IMPORTED_MODULE_5__["EmployeeComponent"], data: { roles: ["ROLE_ADMIN"], index: 2, label: 'Employees' } },
    { path: 'school', component: _school_school_component__WEBPACK_IMPORTED_MODULE_6__["SchoolComponent"], data: { roles: ["ROLE_ADMIN"], index: 3, label: 'Schools' } },
    { path: '', redirectTo: '/calendar', pathMatch: 'full' },
    // login
    { path: 'login', component: _availability_availability_component__WEBPACK_IMPORTED_MODULE_4__["AvailabilityComponent"], data: { roles: ["ROLE_NONE"], index: 0, label: 'Login' } },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".animatedIcon {\r\n  transition: transform .5s;\r\n  transform: rotate(360deg);\r\n}\r\n\r\n.tabs {\r\n  display: flex;\r\n  align-items: center;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFuaW1hdGVkSWNvbiB7XHJcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC41cztcclxuICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xyXG59XHJcblxyXG4udGFicyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");




let AppComponent = class AppComponent {
    constructor(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
        this.routes = [];
        this.selectedRouteIndex = 0;
        this.showToolbar = true;
        this.title = "Ordina CodeKids";
    }
    ngOnInit() {
        this.router.events.subscribe(value => {
            value instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"] ? this.setTab(value) : '';
        });
        this.authenticationService.currentUserSubject.subscribe(user => {
            const role = user ? user.role : "ROLE_NONE";
            this.routes = this.router.config.filter(value => value.data && value.data.roles.indexOf(role) > -1).sort((a, b) => a.data.index > b.data.index ? 1 : -1);
            this.setTabForUrl(this.router.url);
        });
    }
    tabChanged($event) {
        return this.router.navigate([this.routes.find(value => value.data.index === $event.index).path]);
    }
    // make sure that the initial route is reflected in the selected tab
    setTab(value) {
        this.setTabForUrl(value.urlAfterRedirects);
    }
    setTabForUrl(url) {
        url = url.startsWith("/") ? url.substr(1) : url;
        const selectedRoute = this.routes.find(route => route.path === url);
        if (selectedRoute) {
            this.selectedRouteIndex = selectedRoute.data.index;
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm2015/angular-calendar.js");
/* harmony import */ var _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./calendar/calendar.component */ "./src/app/calendar/calendar.component.ts");
/* harmony import */ var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-calendar/date-adapters/date-fns */ "./node_modules/angular-calendar/date-adapters/date-fns/index.js");
/* harmony import */ var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/calendar/calendar.service */ "./src/app/services/calendar/calendar.service.ts");
/* harmony import */ var _resources_angular_material_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./resources/angular-material.module */ "./src/app/resources/angular-material.module.ts");
/* harmony import */ var _calendar_event_calendar_event_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./calendar-event/calendar-event.component */ "./src/app/calendar-event/calendar-event.component.ts");
/* harmony import */ var _interceptors_basic_auth_interceptor__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./interceptors/basic-auth.interceptor */ "./src/app/interceptors/basic-auth.interceptor.ts");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm2015/snack-bar.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _demobar_demobar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./demobar/demobar.component */ "./src/app/demobar/demobar.component.ts");
/* harmony import */ var _availability_availability_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./availability/availability.component */ "./src/app/availability/availability.component.ts");
/* harmony import */ var _dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./dialog-confirm/dialog-confirm.component */ "./src/app/dialog-confirm/dialog-confirm.component.ts");
/* harmony import */ var _employee_employee_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./employee/employee.component */ "./src/app/employee/employee.component.ts");
/* harmony import */ var _util_ordina_date_adapter__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./util/ordina-date-adapter */ "./src/app/util/ordina-date-adapter.ts");
/* harmony import */ var _animated_icon_animated_icon_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./animated-icon/animated-icon.component */ "./src/app/animated-icon/animated-icon.component.ts");
/* harmony import */ var _school_school_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./school/school.component */ "./src/app/school/school.component.ts");























let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_6__["CalendarComponent"],
            _calendar_event_calendar_event_component__WEBPACK_IMPORTED_MODULE_11__["CalendarEventComponent"],
            _demobar_demobar_component__WEBPACK_IMPORTED_MODULE_15__["DemobarComponent"],
            _availability_availability_component__WEBPACK_IMPORTED_MODULE_16__["AvailabilityComponent"],
            _dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_17__["DialogConfirmComponent"],
            _animated_icon_animated_icon_component__WEBPACK_IMPORTED_MODULE_20__["AnimatedIconComponent"],
            _employee_employee_component__WEBPACK_IMPORTED_MODULE_18__["EmployeeComponent"],
            _school_school_component__WEBPACK_IMPORTED_MODULE_21__["SchoolComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            angular_calendar__WEBPACK_IMPORTED_MODULE_5__["CalendarModule"].forRoot({
                provide: angular_calendar__WEBPACK_IMPORTED_MODULE_5__["DateAdapter"],
                useFactory: angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_7__["adapterFactory"]
            }),
            _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_13__["MatSnackBarModule"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_14__["ToastrModule"].forRoot(),
            _resources_angular_material_module__WEBPACK_IMPORTED_MODULE_10__["AngularMaterialModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"]
        ],
        entryComponents: [
            _dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_17__["DialogConfirmComponent"]
        ],
        providers: [
            _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_9__["CalendarService"],
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HTTP_INTERCEPTORS"], useClass: _interceptors_basic_auth_interceptor__WEBPACK_IMPORTED_MODULE_12__["BasicAuthInterceptor"], multi: true },
            _util_ordina_date_adapter__WEBPACK_IMPORTED_MODULE_19__["OrdinaDateAdapter"]
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/availability/availability.component.css":
/*!*********************************************************!*\
  !*** ./src/app/availability/availability.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table {\r\n  width: 100%;\r\n}\r\nth.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXZhaWxhYmlsaXR5L2F2YWlsYWJpbGl0eS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiO0FBQ0E7RUFDRSxZQUFZO0FBQ2QiLCJmaWxlIjoic3JjL2FwcC9hdmFpbGFiaWxpdHkvYXZhaWxhYmlsaXR5LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxudGgubWF0LXNvcnQtaGVhZGVyLXNvcnRlZCB7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/availability/availability.component.ts":
/*!********************************************************!*\
  !*** ./src/app/availability/availability.component.ts ***!
  \********************************************************/
/*! exports provided: AvailabilityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvailabilityComponent", function() { return AvailabilityComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");
/* harmony import */ var _services_availability_availability_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/availability/availability.service */ "./src/app/services/availability/availability.service.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm2015/table.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm2015/collections.js");
/* harmony import */ var _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/calendar/calendar.service */ "./src/app/services/calendar/calendar.service.ts");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm2015/sort.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm2015/paginator.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _services_employee_employee_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/employee/employee.service */ "./src/app/services/employee/employee.service.ts");
/* harmony import */ var _services_school_school_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/school/school.service */ "./src/app/services/school/school.service.ts");
/* harmony import */ var _services_booking_booking_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/booking/booking.service */ "./src/app/services/booking/booking.service.ts");













let AvailabilityComponent = class AvailabilityComponent {
    constructor(authenticationService, calendarService, availabilityService, employeeService, schoolService, toastr, bookingService) {
        this.authenticationService = authenticationService;
        this.calendarService = calendarService;
        this.availabilityService = availabilityService;
        this.employeeService = employeeService;
        this.schoolService = schoolService;
        this.toastr = toastr;
        this.bookingService = bookingService;
        this.displayedColumns = ['select', 'date', 'employee', 'booking-school-name', 'booking-school-contact', 'booking-school-contact-email'];
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"]();
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.ignoreFilter = false;
        this.selectEmployeeBadge = "";
        this.isAdmin = false;
        this.isAdminOrEmployee = false;
        /**
         * The date filter used by the date picker to disable certain dates
         * @param date
         */
        this.dateFilter = (date) => {
            const day = date.getDay();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6 && date > new Date() &&
                this.dataSource.data.find(value => value.date.toLocaleDateString() === date.toLocaleDateString() && value.employeeId === this.employee.id) === undefined;
        };
    }
    ngOnInit() {
        this.availabilityService.subjectAvailabilities.subscribe(availabilities => {
            this.load(availabilities);
        });
        this.load(this.availabilityService.availabilities);
        this.employeeService.subjectEmployees.subscribe(value => { if (value) {
            this.employees = value;
        } });
        this.employees = this.employeeService.employees;
        this.authenticationService.currentUserSubject.subscribe(user => {
            if (user) {
                if (user.role === "ROLE_ADMIN") {
                    this.isAdmin = true;
                    this.employee = undefined;
                }
                else {
                    this.isAdmin = false;
                    this.employee = this.employees.filter(employee => {
                        return employee.email === user.email;
                    })[0];
                }
                this.isAdminOrEmployee = ["ROLE_ADMIN", "ROLE_EMPLOYEE"].indexOf(user.role) > -1;
            }
        });
    }
    load(availabilities) {
        availabilities = this.filterToOwner(availabilities);
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](availabilities);
        this.dataSource.sort = this.sort;
        this.dataSource.sortingDataAccessor = (availability, columnDef) => {
            switch (columnDef) {
                case "date": return availability.date;
                case "employee": return this.padSortingValue(`${this.getEmployeeAttribute(availability.employeeId, "firstName")} ${this.getEmployeeAttribute(availability.employeeId, "lastName")}`, this.sort.direction);
                case "booking-school-name": return this.padSortingValue(this.getSchoolAttribute(availability.bookingId, "name"), this.sort.direction);
                case "booking-school-contact": return this.padSortingValue(this.getSchoolAttribute(availability.bookingId, "contactName"), this.sort.direction);
                case "booking-school-contact-email": return this.padSortingValue(this.getSchoolAttribute(availability.bookingId, "contactEmail"), this.sort.direction);
                default:
                    return "";
            }
        };
        this.dataSource.filterPredicate = ((availability, filter) => {
            filter = filter.trim().toLowerCase();
            let match = false;
            const employeeAttributes = [this.getEmployeeAttribute(availability.employeeId, "firstName"), this.getEmployeeAttribute(availability.employeeId, "lastName"), this.getEmployeeAttribute(availability.employeeId, "email")];
            employeeAttributes.forEach((value) => {
                if (value) {
                    value = value.toLowerCase().trim();
                    if (value.indexOf(filter) > -1 || filter.indexOf(value) > -1) {
                        match = true;
                        return;
                    }
                }
            });
            const schoolAttributes = [this.getSchoolAttribute(availability.bookingId, "name"), this.getSchoolAttribute(availability.bookingId, "contactName"), this.getSchoolAttribute(availability.bookingId, "contactEmail")];
            schoolAttributes.forEach((value) => {
                if (value) {
                    value = value.toLowerCase().trim();
                    if (value.indexOf(filter) > -1 || filter.indexOf(value) > -1) {
                        match = true;
                        return;
                    }
                }
            });
            return match;
        });
        this.dataSource.paginator = this.paginator;
    }
    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }
    filterToOwner(availabilities) {
        if (this.ignoreFilter || this.authenticationService.currentUserIsAdmin || !availabilities) {
            return availabilities;
        }
        else {
            return availabilities.filter(value => this.employeeService.mappedEmployees[value.employeeId].email === this.authenticationService.currentUserSubject.getValue().email
                ||
                    (this.bookingService.mappedBookings[value.bookingId] &&
                        this.schoolService.mappedSchools[this.bookingService.mappedBookings[value.bookingId].schoolId].contactEmail ===
                            this.authenticationService.currentUserSubject.getValue().email));
        }
    }
    applyFilter(filterValue) {
        this.dataSource.filter = filterValue;
    }
    /**
     * This method will pad the value with prefix 'ZZZ' or '000' in case the value is '' or undefined
     * to make sure that sorting on column values will never list the empty ones on top
     * @param value
     * @param direction
     */
    padSortingValue(value, direction) {
        const padWith = direction === 'asc' ? 'zzz' : '000';
        return !(value) ? padWith : value.toLowerCase();
    }
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle($event) {
        $event.checked ?
            this.dataSource.connect().getValue().forEach(row => this.selection.select(row)) :
            this.selection.clear();
    }
    getBooking(bookingId) {
        return this.bookingService.mappedBookings[bookingId];
    }
    getSchoolAttribute(bookingId, attribute) {
        const booking = this.getBooking(bookingId);
        if (booking) {
            const school = this.schoolService.mappedSchools[booking.schoolId];
            return school[attribute];
        }
    }
    getEmployeeAttribute(employeeId, attribute) {
        const employee = this.employeeService.mappedEmployees[employeeId];
        if (employee) {
            return employee[attribute];
        }
    }
    getAvailabilityDate(availability) {
        return availability.date.toLocaleDateString();
    }
    createAvailability(date) {
        const availability = {
            date: date,
            employeeId: this.employee.id
        };
        this.availabilityService.createAvailability(availability);
    }
    dateSelected($event) {
        this.calendarDate = $event.value;
        this.createAvailability($event.value);
    }
    openPicker(picker) {
        if (this.isAdmin && !this.employee) {
            // throw error:
            this.selectEmployeeBadge = "!";
            this.toastr.warning("You must first select an employee to add availability for");
            this.calendarDate = undefined;
            setTimeout(() => { this.selectEmployeeBadge = ""; }, 2500);
            picker.close();
        }
        else {
            picker.open();
        }
    }
    remove() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const confirmed = this.availabilityService.removeAvailabilities(this.selection.selected);
            if (confirmed) {
                this.selection.clear();
            }
        });
    }
};
AvailabilityComponent.ctorParameters = () => [
    { type: _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
    { type: _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_6__["CalendarService"] },
    { type: _services_availability_availability_service__WEBPACK_IMPORTED_MODULE_3__["AvailabilityService"] },
    { type: _services_employee_employee_service__WEBPACK_IMPORTED_MODULE_10__["EmployeeService"] },
    { type: _services_school_school_service__WEBPACK_IMPORTED_MODULE_11__["SchoolService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"] },
    { type: _services_booking_booking_service__WEBPACK_IMPORTED_MODULE_12__["BookingService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_sort__WEBPACK_IMPORTED_MODULE_7__["MatSort"], { static: true })
], AvailabilityComponent.prototype, "sort", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"], { static: true })
], AvailabilityComponent.prototype, "paginator", void 0);
AvailabilityComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-availability',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./availability.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/availability/availability.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./availability.component.css */ "./src/app/availability/availability.component.css")).default]
    })
], AvailabilityComponent);



/***/ }),

/***/ "./src/app/calendar-event/calendar-event.component.css":
/*!*************************************************************!*\
  !*** ./src/app/calendar-event/calendar-event.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mat-card-title {\r\n  display:flex\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FsZW5kYXItZXZlbnQvY2FsZW5kYXItZXZlbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9jYWxlbmRhci1ldmVudC9jYWxlbmRhci1ldmVudC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC1jYXJkLXRpdGxlIHtcclxuICBkaXNwbGF5OmZsZXhcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/calendar-event/calendar-event.component.ts":
/*!************************************************************!*\
  !*** ./src/app/calendar-event/calendar-event.component.ts ***!
  \************************************************************/
/*! exports provided: CalendarEventComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarEventComponent", function() { return CalendarEventComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/calendar/calendar.service */ "./src/app/services/calendar/calendar.service.ts");
/* harmony import */ var _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _services_school_school_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/school/school.service */ "./src/app/services/school/school.service.ts");
/* harmony import */ var _services_booking_booking_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/booking/booking.service */ "./src/app/services/booking/booking.service.ts");







let CalendarEventComponent = class CalendarEventComponent {
    constructor(calendarService, authenticationService, toastr, schoolService, bookingService) {
        this.calendarService = calendarService;
        this.authenticationService = authenticationService;
        this.toastr = toastr;
        this.schoolService = schoolService;
        this.bookingService = bookingService;
        this.closeClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.isAdmin = false;
    }
    ngOnInit() {
        this.schoolService.subjectSchools.subscribe(value => {
            this.schools = value;
        });
        this.schools = this.schoolService.schools;
        this.authenticationService.currentUserSubject.subscribe(value => {
            this.currentUser = value;
            this.isAdmin = value && value.role === "ROLE_ADMIN";
        });
    }
    book() {
        if (this.isAdmin) {
            if (!this.targetSchool) {
                this.toastr.error("As administrator, you must select a school to link to this booking", "", { positionClass: "toast-top-center" });
                this.selectSchoolBadge = "!";
                setTimeout(() => { this.selectSchoolBadge = ""; }, 2500);
                return;
            }
            this.currentUser.school = this.targetSchool;
        }
        this.bookingService.createBooking(this.ordinaCalendarEvent.availability);
    }
    cancel() {
        this.bookingService.removeBooking(this.ordinaCalendarEvent.booking);
    }
    close() {
        this.closeClicked.emit();
    }
    canBook() {
        return !this.hasOwner() && (this.isAdmin || (this.currentUser && this.currentUser.role === "ROLE_SCHOOL"));
    }
    canCancel() {
        return this.hasOwner() && (this.isAdmin || this.isOwner());
    }
    isOwner() {
        return this.currentUser &&
            this.hasOwner() &&
            this.currentUser.email === this.ordinaCalendarEvent.school.contactEmail;
    }
    hasOwner() {
        return !!this.ordinaCalendarEvent.school;
    }
};
CalendarEventComponent.ctorParameters = () => [
    { type: _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_2__["CalendarService"] },
    { type: _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
    { type: _services_school_school_service__WEBPACK_IMPORTED_MODULE_5__["SchoolService"] },
    { type: _services_booking_booking_service__WEBPACK_IMPORTED_MODULE_6__["BookingService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], CalendarEventComponent.prototype, "ordinaCalendarEvent", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], CalendarEventComponent.prototype, "closeClicked", void 0);
CalendarEventComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-calendar-event',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./calendar-event.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar-event/calendar-event.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./calendar-event.component.css */ "./src/app/calendar-event/calendar-event.component.css")).default]
    })
], CalendarEventComponent);



/***/ }),

/***/ "./src/app/calendar/calendar.component.css":
/*!*************************************************!*\
  !*** ./src/app/calendar/calendar.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/calendar/calendar.component.ts":
/*!************************************************!*\
  !*** ./src/app/calendar/calendar.component.ts ***!
  \************************************************/
/*! exports provided: CalendarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarComponent", function() { return CalendarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm2015/angular-calendar.js");
/* harmony import */ var _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/calendar/calendar.service */ "./src/app/services/calendar/calendar.service.ts");
/* harmony import */ var _util_ordina_date_adapter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../util/ordina-date-adapter */ "./src/app/util/ordina-date-adapter.ts");





let CalendarComponent = class CalendarComponent {
    constructor(calendarService, dateAdapter) {
        this.calendarService = calendarService;
        this.dateAdapter = dateAdapter;
        this.view = angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Month;
        this.calendarHoursInDay = 8;
        this.viewDate = new Date();
        this.excludeDays = [0, 6];
        this.CalendarView = angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"];
        this.events = this.calendarService.calendarEvents;
    }
    offset(stepSize = 1) {
        this.viewDate = this.view == angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Month ?
            this.dateAdapter.addCalendarMonths(this.viewDate, stepSize) :
            this.dateAdapter.addCalendarDays(this.viewDate, stepSize * 7);
        this.updateTitle();
    }
    showEvent() {
        return this.view == angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Week && this.selectedEvent;
    }
    ngOnInit() {
        this.calendarService.subjectCalendarEvents.subscribe(value => {
            this.events = value;
        });
        this.calendarService.subjectCalendarHoursInDay.subscribe(value => {
            this.calendarHoursInDay = value;
        });
        this.updateTitle();
    }
    dayClicked(day) {
        if (day.isFuture) {
            // jump to the week view for the specified day
            this.viewDate = day.date;
            this.view = angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Week;
            this.updateTitle();
        }
    }
    eventClick($event) {
        const event = $event.event;
        this.possibleToBook = !event.booking;
        this.selectedEvent = event;
    }
    current() {
        this.viewDate = new Date();
    }
    toggleView() {
        this.view = this.view === angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Month ? angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Week : angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Month;
        this.updateTitle();
    }
    updateTitle() {
        this.title = this.view === angular_calendar__WEBPACK_IMPORTED_MODULE_2__["CalendarView"].Month ? this.getMonthTitle() : this.getWeekTitle();
    }
    getMonthTitle() {
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return this.viewDate.getFullYear() + " - " + months[this.viewDate.getMonth()];
    }
    getWeekTitle() {
        return "Week# " + this.dateAdapter.getWeekOfYear(this.viewDate);
    }
};
CalendarComponent.ctorParameters = () => [
    { type: _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_3__["CalendarService"] },
    { type: _util_ordina_date_adapter__WEBPACK_IMPORTED_MODULE_4__["OrdinaDateAdapter"] }
];
CalendarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-calendar',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./calendar.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar/calendar.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./calendar.component.css */ "./src/app/calendar/calendar.component.css")).default]
    })
], CalendarComponent);



/***/ }),

/***/ "./src/app/demobar/demobar.component.css":
/*!***********************************************!*\
  !*** ./src/app/demobar/demobar.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".second-toolbar-row {\r\n  font-size: 14px;\r\n  height: 32px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGVtb2Jhci9kZW1vYmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFlO0VBQ2YsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvZGVtb2Jhci9kZW1vYmFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2Vjb25kLXRvb2xiYXItcm93IHtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgaGVpZ2h0OiAzMnB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/demobar/demobar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/demobar/demobar.component.ts ***!
  \**********************************************/
/*! exports provided: DemobarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DemobarComponent", function() { return DemobarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/calendar/calendar.service */ "./src/app/services/calendar/calendar.service.ts");
/* harmony import */ var _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _services_employee_employee_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/employee/employee.service */ "./src/app/services/employee/employee.service.ts");
/* harmony import */ var _services_school_school_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/school/school.service */ "./src/app/services/school/school.service.ts");







let DemobarComponent = class DemobarComponent {
    constructor(calendarService, authenticationService, toastr, employeeService, schoolService) {
        this.calendarService = calendarService;
        this.authenticationService = authenticationService;
        this.toastr = toastr;
        this.employeeService = employeeService;
        this.schoolService = schoolService;
        this.schools = [];
        this.employees = [];
    }
    ngOnInit() {
        this.employeeService.subjectEmployees.subscribe(employees => this.employees = employees);
        this.schoolService.subjectSchools.subscribe(schools => this.schools = schools);
        this.authenticationService.currentUserSubject.subscribe(user => {
            this.setCurrentUserforDemoToolbar(user);
        });
        this.authenticationService.login("admin", "admin");
    }
    setCurrentUserforDemoToolbar(user) {
        if (user) {
            this.currentUser = user;
            if (this.currentUser.role === "ROLE_SCHOOL") {
                this.selectedSchool = this.schools.filter(school => school.contactEmail === user.email)[0];
            }
            if (this.currentUser.role === "ROLE_EMPLOYEE") {
                this.selectedEmployee = this.employees.filter(employee => employee.email === user.email)[0];
            }
        }
    }
    resetDemo() {
        if (this.currentUser.role === "ROLE_ADMIN") {
            this.calendarService.resetDemoData();
        }
        else {
            this.toastr.error("You must first become the admin user by clicking the 'Admin'", "", { positionClass: "toast-top-center" });
            this.adminButtonBadge = "!";
            setTimeout(() => { this.adminButtonBadge = ""; }, 2500);
        }
    }
    toggleBookedSlots() {
        this.calendarService.toggleBookedSlots();
    }
    login(type) {
        if (type === 'employee') {
            this.authenticationService.login(this.selectedEmployee.email, 'employee');
        }
        if (type === 'school') {
            this.authenticationService.login(this.selectedSchool.contactEmail, 'school');
        }
        if (type === 'admin') {
            this.authenticationService.login('admin', 'admin');
        }
    }
};
DemobarComponent.ctorParameters = () => [
    { type: _services_calendar_calendar_service__WEBPACK_IMPORTED_MODULE_2__["CalendarService"] },
    { type: _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
    { type: _services_employee_employee_service__WEBPACK_IMPORTED_MODULE_5__["EmployeeService"] },
    { type: _services_school_school_service__WEBPACK_IMPORTED_MODULE_6__["SchoolService"] }
];
DemobarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-demobar',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./demobar.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/demobar/demobar.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./demobar.component.css */ "./src/app/demobar/demobar.component.css")).default]
    })
], DemobarComponent);



/***/ }),

/***/ "./src/app/dialog-confirm/dialog-confirm.component.css":
/*!*************************************************************!*\
  !*** ./src/app/dialog-confirm/dialog-confirm.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpYWxvZy1jb25maXJtL2RpYWxvZy1jb25maXJtLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/dialog-confirm/dialog-confirm.component.ts":
/*!************************************************************!*\
  !*** ./src/app/dialog-confirm/dialog-confirm.component.ts ***!
  \************************************************************/
/*! exports provided: DialogConfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogConfirmComponent", function() { return DialogConfirmComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");



let DialogConfirmComponent = class DialogConfirmComponent {
    constructor(settings) {
        this.settings = settings;
        this.data = {
            cancelButtonText: "Cancel",
            description: "",
            showCancelButton: true,
            title: ""
        };
        Object.assign(this.data, settings);
    }
};
DialogConfirmComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
];
DialogConfirmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dialog-confirm',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dialog-confirm.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dialog-confirm/dialog-confirm.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dialog-confirm.component.css */ "./src/app/dialog-confirm/dialog-confirm.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
], DialogConfirmComponent);



/***/ }),

/***/ "./src/app/employee/employee.component.css":
/*!*************************************************!*\
  !*** ./src/app/employee/employee.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("form {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n.shared-row {\r\n  width: 50%;\r\n}\r\nmat-form-field {\r\n  width: 100%;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZW1wbG95ZWUvZW1wbG95ZWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLFVBQVU7QUFDWjtBQUNBO0VBQ0UsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvZW1wbG95ZWUvZW1wbG95ZWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImZvcm0ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG4uc2hhcmVkLXJvdyB7XHJcbiAgd2lkdGg6IDUwJTtcclxufVxyXG5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/employee/employee.component.ts":
/*!************************************************!*\
  !*** ./src/app/employee/employee.component.ts ***!
  \************************************************/
/*! exports provided: EmployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeComponent", function() { return EmployeeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_employee_employee_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/employee/employee.service */ "./src/app/services/employee/employee.service.ts");
/* harmony import */ var _services_education_package_education_package_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/education-package/education-package.service */ "./src/app/services/education-package/education-package.service.ts");





let EmployeeComponent = class EmployeeComponent {
    constructor(employeeService, educationPackageService) {
        this.employeeService = employeeService;
        this.educationPackageService = educationPackageService;
        this.filteredOptions = [];
        this.employeeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            select_employee: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            educationPackages: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)),
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
        });
        this.educationPackages = [];
    }
    displayEmployee(employee) {
        return employee ? `${employee.firstName} ${employee.lastName}` : '';
    }
    ;
    ngOnInit() {
        this.employeeForm.get("select_employee").valueChanges.subscribe(value => {
            // value changed, either by typing text or selection of one of the employees from the list
            // when typing, filter the list
            // when employee, select the employee as the active employee
            if (value && value !== this.employee) {
                if (typeof value === 'object') {
                    this.employee = value;
                    this.employeeData = Object.assign({ select_employee: value, password: '' }, value);
                    this.employeeForm.setValue(this.employeeData);
                    this.employeeForm.get("password").clearValidators();
                    this.employeeForm.get("password").setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6));
                    this.employeeForm.get("email").disable();
                    this.employeeForm.markAllAsTouched();
                }
                else {
                    this.filterEmployees(value);
                }
            }
        });
        this.employeeService.subjectEmployees.subscribe(value => { this.employees = value; this.filteredOptions = this.employees; });
        this.employees = this.employeeService.employees;
        this.educationPackageService.subjectEducationPackages.subscribe(value => { this.educationPackages = value.map(value => value.name); });
        this.educationPackages = this.educationPackageService.educationPackages ? this.educationPackageService.educationPackages.map(value => value.name) : [];
    }
    filterEmployees(filter) {
        filter = filter.toLocaleLowerCase();
        this.filteredOptions = this.employees.filter(employee => {
            if (!employee || !filter) {
                return false;
            }
            return employee.firstName.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(employee.firstName) > -1 ||
                employee.lastName.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(employee.lastName) > -1 ||
                employee.email.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(employee.email) > -1;
        });
    }
    new() {
        this.employeeForm.reset();
        this.employeeForm.get("password").clearValidators();
        this.employeeForm.get("password").setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]);
        this.employeeForm.get("email").enable();
        this.employeeData = this.employeeForm.getRawValue();
    }
    remove() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.employee) {
                const confirmed = yield this.employeeService.deleteEmployee(this.employee);
                if (confirmed) {
                    this.employeeForm.reset();
                }
                return confirmed;
            }
        });
    }
    save() {
        if (this.employeeForm.valid) {
            this.employeeData = this.employeeForm.getRawValue();
            const employee = Object.assign(this.employeeData, {});
            delete employee.select_employee;
            this.employeeService.saveEmployee(employee);
        }
        else {
            this.employeeForm.markAllAsTouched();
        }
    }
};
EmployeeComponent.ctorParameters = () => [
    { type: _services_employee_employee_service__WEBPACK_IMPORTED_MODULE_3__["EmployeeService"] },
    { type: _services_education_package_education_package_service__WEBPACK_IMPORTED_MODULE_4__["EducationPackageService"] }
];
EmployeeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-employee',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./employee.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/employee/employee.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./employee.component.css */ "./src/app/employee/employee.component.css")).default]
    })
], EmployeeComponent);



/***/ }),

/***/ "./src/app/interceptors/basic-auth.interceptor.ts":
/*!********************************************************!*\
  !*** ./src/app/interceptors/basic-auth.interceptor.ts ***!
  \********************************************************/
/*! exports provided: BasicAuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicAuthInterceptor", function() { return BasicAuthInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");






let BasicAuthInterceptor = class BasicAuthInterceptor {
    constructor(authenticationService, toastr) {
        this.authenticationService = authenticationService;
        this.toastr = toastr;
    }
    intercept(request, next) {
        // add authorization header with basic auth credentials if available
        const user = this.authenticationService.currentLoginUser;
        if (user) {
            console.log("making request with user: " + user.email);
            request = request.clone({
                setHeaders: {
                    Authorization: user.authData
                }
            });
        }
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((error) => {
            this.toastr.error(error.error.message);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
        }));
    }
};
BasicAuthInterceptor.ctorParameters = () => [
    { type: _services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"] }
];
BasicAuthInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], BasicAuthInterceptor);



/***/ }),

/***/ "./src/app/resources/angular-material.module.ts":
/*!******************************************************!*\
  !*** ./src/app/resources/angular-material.module.ts ***!
  \******************************************************/
/*! exports provided: AngularMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularMaterialModule", function() { return AngularMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");





let AngularMaterialModule = class AngularMaterialModule {
};
AngularMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ]
    })
], AngularMaterialModule);



/***/ }),

/***/ "./src/app/school/school.component.css":
/*!*********************************************!*\
  !*** ./src/app/school/school.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("form {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n.shared-row {\r\n  width: 50%;\r\n}\r\nmat-form-field {\r\n  width: 100%;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2Nob29sL3NjaG9vbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UsVUFBVTtBQUNaO0FBQ0E7RUFDRSxXQUFXO0FBQ2IiLCJmaWxlIjoic3JjL2FwcC9zY2hvb2wvc2Nob29sLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJmb3JtIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbn1cclxuLnNoYXJlZC1yb3cge1xyXG4gIHdpZHRoOiA1MCU7XHJcbn1cclxubWF0LWZvcm0tZmllbGQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/school/school.component.ts":
/*!********************************************!*\
  !*** ./src/app/school/school.component.ts ***!
  \********************************************/
/*! exports provided: SchoolComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolComponent", function() { return SchoolComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_school_school_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/school/school.service */ "./src/app/services/school/school.service.ts");
/* harmony import */ var _services_education_package_education_package_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/education-package/education-package.service */ "./src/app/services/education-package/education-package.service.ts");





let SchoolComponent = class SchoolComponent {
    constructor(schoolService, educationPackageService) {
        this.schoolService = schoolService;
        this.educationPackageService = educationPackageService;
        this.filteredOptions = [];
        this.schoolForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            select_school: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined),
            contactEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            contactName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](undefined, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)),
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
        });
    }
    displaySchool(school) {
        return school ? school.name : '';
    }
    ;
    ngOnInit() {
        this.schoolForm.get("select_school").valueChanges.subscribe(value => {
            // value changed, either by typing text or selection of one of the schools from the list
            // when typing, filter the list
            // when school, select the school as the active school
            if (value && value !== this.school) {
                if (typeof value === 'object') {
                    this.school = value;
                    this.schoolData = Object.assign({ select_school: value, password: '' }, value);
                    this.schoolForm.setValue(this.schoolData);
                    this.schoolForm.get("password").clearValidators();
                    this.schoolForm.get("password").setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6));
                    this.schoolForm.get("contactEmail").disable();
                    this.schoolForm.markAllAsTouched();
                }
                else {
                    this.filterSchools(value);
                }
            }
        });
        this.schoolService.subjectSchools.subscribe(value => { this.schools = value; this.filteredOptions = this.schools; });
        this.schools = this.schoolService.schools;
    }
    filterSchools(filter) {
        filter = filter.toLocaleLowerCase();
        this.filteredOptions = this.schools.filter(school => {
            if (!school || !filter) {
                return false;
            }
            return school.name.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(school.name) > -1 ||
                school.contactEmail.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(school.contactEmail) > -1 ||
                school.contactName.toLocaleLowerCase().indexOf(filter) > -1 || filter.toLocaleLowerCase().indexOf(school.contactName) > -1;
        });
    }
    new() {
        this.schoolForm.reset();
        this.schoolForm.get("password").clearValidators();
        this.schoolForm.get("password").setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]);
        this.schoolForm.get("contactEmail").enable();
        this.schoolData = this.schoolForm.getRawValue();
    }
    remove() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.school) {
                const confirmed = yield this.schoolService.deleteSchool(this.school);
                if (confirmed) {
                    this.schoolForm.reset();
                }
                return confirmed;
            }
            return false;
        });
    }
    save() {
        if (this.schoolForm.valid) {
            this.schoolData = this.schoolForm.getRawValue();
            const school = Object.assign(this.schoolData, {});
            delete school.select_school;
            this.schoolService.saveSchool(school);
        }
        else {
            this.schoolForm.markAllAsTouched();
        }
    }
};
SchoolComponent.ctorParameters = () => [
    { type: _services_school_school_service__WEBPACK_IMPORTED_MODULE_3__["SchoolService"] },
    { type: _services_education_package_education_package_service__WEBPACK_IMPORTED_MODULE_4__["EducationPackageService"] }
];
SchoolComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-school',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./school.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/school/school.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./school.component.css */ "./src/app/school/school.component.css")).default]
    })
], SchoolComponent);



/***/ }),

/***/ "./src/app/services/authentication/authentication.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/authentication/authentication.service.ts ***!
  \*******************************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");





let AuthenticationService = class AuthenticationService {
    constructor(httpService, toastr) {
        this.httpService = httpService;
        this.toastr = toastr;
        this.currentUserSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](undefined);
        this.currentUserIsAdmin = false;
    }
    login(email, password) {
        this.currentUserSubject.next(undefined);
        this.currentLoginUser = {
            email: email,
            authData: `Basic ${window.btoa(`${email}:${password}`)}`
        };
        this.httpService.get("user/login").subscribe((currentUser) => {
            this.currentUserIsAdmin = currentUser.role === "ROLE_ADMIN";
            this.currentUserId = currentUser.id;
            this.currentUserSubject.next(currentUser);
        });
    }
    setUserPassword(email, password) {
        this.httpService.put("user", 'setPassword', { email: email, password: password })
            .subscribe(() => { this.toastr.success("User password has been updated"); });
    }
    createUser(email, password, role) {
        const user = {
            email: email,
            password: password,
            role: role
        };
        this.httpService.post("user", user)
            .subscribe(() => { this.toastr.success("User account has been created"); });
    }
};
AuthenticationService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }
];
AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthenticationService);



/***/ }),

/***/ "./src/app/services/availability/availability.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/availability/availability.service.ts ***!
  \***************************************************************/
/*! exports provided: AvailabilityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvailabilityService", function() { return AvailabilityService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../dialog-confirm/dialog-confirm.component */ "./src/app/dialog-confirm/dialog-confirm.component.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");







let AvailabilityService = class AvailabilityService {
    constructor(httpService, dialog, toastr) {
        this.httpService = httpService;
        this.dialog = dialog;
        this.toastr = toastr;
        this.subjectAvailabilities = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
    }
    /**
     * Creates the new availability slot, no information on associated bookings will be stored.
     * Flow = Create availability, then book the slot
     * @param availability
     */
    createAvailability(availability) {
        const hours = availability.date.getHours() - availability.date.getTimezoneOffset() / 60;
        availability.date.setHours(hours);
        this.httpService.post("availability", availability).subscribe(value => {
            value.date = new Date(value.date);
            this.availabilities.push(value);
            this.subjectAvailabilities.next(this.availabilities);
            this.toastr.success("Availability added!");
        });
    }
    setAvailabilities(availabilities) {
        this.availabilities = availabilities;
        this.mappedAvailabilities = {};
        availabilities.forEach(availability => this.mappedAvailabilities[availability.id] = availability);
        this.subjectAvailabilities.next(this.availabilities);
    }
    removeAvailabilities(availabilities) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (availabilities.find(availability => (availability.bookingId))) {
                // ask for confirmation:
                const dialogRef = this.dialog.open(_dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_4__["DialogConfirmComponent"], {
                    width: '500px',
                    data: {
                        title: "Are you sure?",
                        description: "One or more items in your selection have an active booking, are you sure you wish to remove them?"
                    }
                });
                const confirmed = yield dialogRef.afterClosed().toPromise();
                if (!confirmed) {
                    return false;
                }
                this.removeAvailabilitiesCall(availabilities);
                return true;
            }
            else {
                this.removeAvailabilitiesCall(availabilities);
                return true;
            }
        });
    }
    removeAvailabilitiesCall(availabilities) {
        this.httpService.delete("availability", availabilities.map(availability => availability.id)).subscribe((value) => {
            if (value) {
                this.removeAvailabilitiesFromCurrent(availabilities);
            }
        });
    }
    removeAvailabilitiesFromCurrent(availabilities) {
        this.availabilities = this.availabilities.filter(availability => availabilities.indexOf(availability) === -1);
        this.setAvailabilities(this.availabilities);
    }
};
AvailabilityService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] }
];
AvailabilityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AvailabilityService);



/***/ }),

/***/ "./src/app/services/booking/booking.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/booking/booking.service.ts ***!
  \*****************************************************/
/*! exports provided: BookingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingService", function() { return BookingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");
/* harmony import */ var _availability_availability_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../availability/availability.service */ "./src/app/services/availability/availability.service.ts");








let BookingService = class BookingService {
    constructor(httpService, toastr, dialog, authenticationService, availabilityService) {
        this.httpService = httpService;
        this.toastr = toastr;
        this.dialog = dialog;
        this.authenticationService = authenticationService;
        this.availabilityService = availabilityService;
        this.mappedBookings = {};
        this.subjectBookings = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    setBookings(bookings) {
        this.bookings = bookings;
        this.mappedBookings = {};
        this.bookings.forEach(booking => this.mappedBookings[booking.id] = booking);
        this.subjectBookings.next(this.bookings);
    }
    removeBooking(booking) {
        this.httpService.delete("booking", booking.id).subscribe(() => {
            // disconnect the availability and booking relation
            this.availabilityService.mappedAvailabilities[booking.availabilityId].bookingId = undefined;
            this.availabilityService.setAvailabilities(this.availabilityService.availabilities);
            // remove the booking from the collection
            this.bookings = this.bookings.filter(bookingInCollection => bookingInCollection.id != booking.id);
            this.setBookings(this.bookings);
        });
        return true;
    }
    createBooking(availability) {
        const booking = {
            availabilityId: availability.id,
            schoolId: this.authenticationService.currentUserSubject.getValue().school.id
        };
        this.httpService.post("booking", booking).subscribe(booking => {
            // connect the availability and booking relation
            this.availabilityService.mappedAvailabilities[booking.availabilityId].bookingId = booking.id;
            this.availabilityService.setAvailabilities(this.availabilityService.availabilities);
            this.bookings.push(booking);
            this.setBookings(this.bookings);
            this.toastr.success("Booking created");
        });
    }
};
BookingService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] },
    { type: _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"] },
    { type: _availability_availability_service__WEBPACK_IMPORTED_MODULE_7__["AvailabilityService"] }
];
BookingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], BookingService);



/***/ }),

/***/ "./src/app/services/calendar/calendar.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/calendar/calendar.service.ts ***!
  \*******************************************************/
/*! exports provided: CalendarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarService", function() { return CalendarService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _availability_availability_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../availability/availability.service */ "./src/app/services/availability/availability.service.ts");
/* harmony import */ var _employee_employee_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../employee/employee.service */ "./src/app/services/employee/employee.service.ts");
/* harmony import */ var _education_package_education_package_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../education-package/education-package.service */ "./src/app/services/education-package/education-package.service.ts");
/* harmony import */ var _school_school_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../school/school.service */ "./src/app/services/school/school.service.ts");
/* harmony import */ var _booking_booking_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../booking/booking.service */ "./src/app/services/booking/booking.service.ts");











let CalendarService = class CalendarService {
    constructor(httpService, authenticationService, toastr, availabilityService, employeeService, educationPackageService, schoolService, bookingService) {
        this.httpService = httpService;
        this.authenticationService = authenticationService;
        this.toastr = toastr;
        this.availabilityService = availabilityService;
        this.employeeService = employeeService;
        this.educationPackageService = educationPackageService;
        this.schoolService = schoolService;
        this.bookingService = bookingService;
        this.subjectCalendarEvents = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.subjectCalendarHoursInDay = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.HOURS_PER_BLOCK_OPTIMUM = 2;
        this.SHOW_BOOKED_SLOTS = true;
        this.COLOR_BOOKED = { primary: '#A9A9A9', secondary: '#A9A9A9' };
        this.COLOR_AVAILABLE = { primary: '#e98300', secondary: '#fff' };
        this.COLOR_BOOKED_BY_ME = { primary: '#e98300', secondary: '#e98300' };
        this.calendarEvents = [];
        this.currentCalendarHoursInDay = 8;
        this.canSetCalendarEvents = true;
        // subscribe to user login / change
        authenticationService.currentUserSubject.subscribe(user => {
            if (user) {
                this.currentUser = user;
                this.loadCalendarEvents();
            }
        });
        // subscribe to the data updates that require calendar items to be recreated:
        this.availabilityService.subjectAvailabilities.subscribe(value => this.setCalendarEvents());
        this.employeeService.subjectEmployees.subscribe(value => this.setCalendarEvents());
        this.schoolService.subjectSchools.subscribe(value => this.setCalendarEvents());
        this.bookingService.subjectBookings.subscribe(value => this.setCalendarEvents());
    }
    // reload all the data from the server, usually triggered after user login
    loadCalendarEvents() {
        // going to refresh all data, prevent calling setCalendarEvents 5 times
        this.canSetCalendarEvents = false;
        // forkJoin to request all data and run when all have been received
        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])([
            this.httpService.get("availability"),
            this.httpService.get("school"),
            this.httpService.get("booking"),
            this.httpService.get("employee"),
            this.httpService.get("educationpackage")
        ])
            .subscribe((value) => {
            this.availabilityService.setAvailabilities(value[0]);
            this.schoolService.setSchools(value[1]);
            this.bookingService.setBookings(value[2]);
            this.canSetCalendarEvents = true; // enable setting the calendar events
            this.employeeService.setEmployees(value[3]);
            this.educationPackageService.setEducationPackages(value[4]);
            // generate the calendar events
            this.setCalendarEvents();
            // link the current user to the school or employee:
            this.linkUserToSchoolOrEmployee();
        });
    }
    setCalendarEvents() {
        if (!this.canSetCalendarEvents) {
            return;
        }
        // generate the calendar events from the data
        const allEvents = this.availabilityService.availabilities.map(availability => {
            availability.date = new Date(availability.date);
            const employee = this.employeeService.mappedEmployees[availability.employeeId];
            const booking = this.bookingService.mappedBookings[availability.bookingId];
            const school = booking ? this.schoolService.mappedSchools[booking.schoolId] : undefined;
            const calendarEvent = {
                availability: availability,
                booking: booking,
                employee: employee,
                school: school,
                allDay: false,
                end: new Date(availability.date.setHours(15)),
                start: new Date(availability.date.setHours(9)),
                title: this.getTitle(employee, booking),
                color: this.getColorForEvent(booking, school, employee)
            };
            return calendarEvent;
        });
        // set the events
        this.calendarEvents = allEvents;
        // format and filter them based on the app settings
        // then emits the calendar events via subjectCalendarEvents
        this.formatAndFilterEvents(allEvents);
    }
    linkUserToSchoolOrEmployee() {
        // make the user school or user employee reference:
        const user = this.authenticationService.currentUserSubject.getValue();
        user.school = this.schoolService.schools.find(school => school.contactEmail === user.email);
        user.employee = this.employeeService.employees.find(employee => employee.email === user.email);
    }
    getColorForEvent(booking, school, employee) {
        if (!booking || !this.currentUser || !school || !employee) {
            return this.COLOR_AVAILABLE;
        }
        if (this.currentUser.email === school.contactEmail || this.currentUser.email === employee.email) {
            return this.COLOR_BOOKED_BY_ME;
        }
        return this.COLOR_BOOKED;
    }
    formatAndFilterEvents(allEvents) {
        if (!this.SHOW_BOOKED_SLOTS) {
            allEvents = allEvents.filter(item => !item.booking);
        }
        const mappedEventsByDate = {};
        let maxSize = 0;
        allEvents.forEach(availability => {
            const availabilitiesForDate = mappedEventsByDate[availability.availability.date + ""] || [];
            availabilitiesForDate.push(availability);
            maxSize = Math.max(maxSize, availabilitiesForDate.length);
            mappedEventsByDate[availability.availability.date + ""] = availabilitiesForDate;
        });
        const hoursInDay = Math.min(24, this.HOURS_PER_BLOCK_OPTIMUM * maxSize);
        this.currentCalendarHoursInDay = hoursInDay;
        this.subjectCalendarHoursInDay.next(this.currentCalendarHoursInDay);
        // determine the blocksize for events
        // there is room for 24 hours of blocks on the calendar
        const blockSize = Math.max(1, Math.ceil(hoursInDay / maxSize));
        for (let mappedEventsByDateKey in mappedEventsByDate) {
            const availabilitiesForDate = mappedEventsByDate[mappedEventsByDateKey];
            let offset = 0;
            availabilitiesForDate.forEach(availability => {
                availability.start = new Date(availability.start.setHours(offset * blockSize));
                availability.end = new Date(availability.end.setHours(offset * blockSize + blockSize));
                offset++;
            });
        }
        this.subjectCalendarEvents.next(allEvents);
    }
    toggleBookedSlots() {
        this.SHOW_BOOKED_SLOTS = !this.SHOW_BOOKED_SLOTS;
        this.formatAndFilterEvents(this.calendarEvents);
    }
    resetDemoData() {
        this.httpService.get("demo/reset", {
            responseType: "text"
        }).subscribe(() => {
            this.loadCalendarEvents();
        });
    }
    getTitle(employee, booking) {
        return `${employee.firstName} ${employee.lastName} <br>
                            ${employee.educationPackages} <br>
                            ${booking ? 'Geboekt door: ' + this.schoolService.mappedSchools[booking.schoolId].name : ''} 
                            `;
    }
};
CalendarService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
    { type: _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"] },
    { type: _availability_availability_service__WEBPACK_IMPORTED_MODULE_6__["AvailabilityService"] },
    { type: _employee_employee_service__WEBPACK_IMPORTED_MODULE_7__["EmployeeService"] },
    { type: _education_package_education_package_service__WEBPACK_IMPORTED_MODULE_8__["EducationPackageService"] },
    { type: _school_school_service__WEBPACK_IMPORTED_MODULE_9__["SchoolService"] },
    { type: _booking_booking_service__WEBPACK_IMPORTED_MODULE_10__["BookingService"] }
];
CalendarService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], CalendarService);



/***/ }),

/***/ "./src/app/services/education-package/education-package.service.ts":
/*!*************************************************************************!*\
  !*** ./src/app/services/education-package/education-package.service.ts ***!
  \*************************************************************************/
/*! exports provided: EducationPackageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EducationPackageService", function() { return EducationPackageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");




let EducationPackageService = class EducationPackageService {
    constructor(httpService) {
        this.httpService = httpService;
        this.mappedEducationPackages = {};
        this.subjectEducationPackages = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    setEducationPackages(educationPackages) {
        this.educationPackages = educationPackages;
        this.mappedEducationPackages = {};
        educationPackages.forEach(educationPackage => this.mappedEducationPackages[educationPackage.id] = educationPackage);
        this.subjectEducationPackages.next(educationPackages);
    }
};
EducationPackageService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"] }
];
EducationPackageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], EducationPackageService);



/***/ }),

/***/ "./src/app/services/employee/employee.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/employee/employee.service.ts ***!
  \*******************************************************/
/*! exports provided: EmployeeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeService", function() { return EmployeeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../dialog-confirm/dialog-confirm.component */ "./src/app/dialog-confirm/dialog-confirm.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");
/* harmony import */ var _availability_availability_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../availability/availability.service */ "./src/app/services/availability/availability.service.ts");









let EmployeeService = class EmployeeService {
    constructor(httpService, toastr, dialog, authenticationService, availabilityService) {
        this.httpService = httpService;
        this.toastr = toastr;
        this.dialog = dialog;
        this.authenticationService = authenticationService;
        this.availabilityService = availabilityService;
        this.mappedEmployees = {};
        this.subjectEmployees = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.employeeUpdated = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    setEmployees(employees) {
        this.employees = employees.sort((a, b) => {
            return a.firstName > b.firstName ? 1 : -1;
        });
        this.mappedEmployees = {};
        this.employees.forEach(employee => this.mappedEmployees[employee.id] = employee);
        this.subjectEmployees.next(this.employees);
    }
    deleteEmployee(employee) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const dialogRef = this.dialog.open(_dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_5__["DialogConfirmComponent"], {
                width: '500px',
                data: {
                    title: "Are you sure?",
                    description: `This will remove ${employee.firstName} ${employee.lastName} and all associated availability slots and potential bookings`
                }
            });
            const confirmed = yield dialogRef.afterClosed().toPromise();
            if (confirmed) {
                this.httpService.delete("employee", employee.id);
                // filter the employee collection
                this.employees = this.employees.filter(employeeInCollection => employeeInCollection.id != employee.id);
                this.setEmployees(this.employees);
                // filter the availabilities
                const availabilities = this.availabilityService.availabilities.filter(availability => availability.employeeId != employee.id);
                this.availabilityService.setAvailabilities(availabilities);
                this.toastr.success(`${employee.firstName} ${employee.lastName} has been removed`);
            }
            return confirmed;
        });
    }
    saveEmployee(employee) {
        if (employee.id > 0) {
            // save existing:
            this.httpService.put("employee", employee.id, employee).subscribe(value => {
                Object.assign(this.mappedEmployees[employee.id], value);
                this.employeeUpdated.next(value);
                this.toastr.success("Employee information updated");
                if (employee.password) {
                    this.authenticationService.setUserPassword(employee.email, employee.password);
                }
            });
        }
        else {
            this.httpService.post("employee", employee).subscribe(value => {
                this.employees.push(value);
                this.setEmployees(this.employees);
                this.toastr.success("Employee account has been created");
                this.authenticationService.createUser(employee.email, employee.password, "ROLE_EMPLOYEE");
            });
        }
    }
};
EmployeeService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"] },
    { type: _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"] },
    { type: _availability_availability_service__WEBPACK_IMPORTED_MODULE_8__["AvailabilityService"] }
];
EmployeeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], EmployeeService);



/***/ }),

/***/ "./src/app/services/http/http.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/http/http.service.ts ***!
  \***********************************************/
/*! exports provided: HttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let HttpService = class HttpService {
    constructor(http) {
        this.http = http;
        this.options = {
            //https://ordina-code-kids.herokuapp.com/
            SERVER_ADDRESS: "ordina-code-kids.herokuapp.com",
            SERVER_PORT: "",
            SERVER_PROTOCOL: "https"
        };
    }
    getServer() {
        return `${this.options.SERVER_PROTOCOL}://${this.options.SERVER_ADDRESS}${this.options.SERVER_PORT ? ':' : ''}${this.options.SERVER_PORT}`;
    }
    getControllerEndpoint(controller) {
        return `${this.getServer()}/api/${controller}`;
    }
    get(controller, options) {
        return this.http.get(this.getControllerEndpoint(controller), options);
    }
    post(controller, payload) {
        return this.http.post(this.getControllerEndpoint(controller), payload);
    }
    put(controller, id, payload) {
        return this.http.put(this.getControllerEndpoint(controller) + "/" + id, payload);
    }
    delete(controller, id) {
        if (Array.isArray(id)) {
            return this.http.request('delete', this.getControllerEndpoint(controller), { body: id });
        }
        else {
            return this.http.delete(this.getControllerEndpoint(controller + "/" + id));
        }
    }
};
HttpService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
HttpService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], HttpService);



/***/ }),

/***/ "./src/app/services/school/school.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/school/school.service.ts ***!
  \***************************************************/
/*! exports provided: SchoolService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolService", function() { return SchoolService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/* harmony import */ var _dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../dialog-confirm/dialog-confirm.component */ "./src/app/dialog-confirm/dialog-confirm.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../authentication/authentication.service */ "./src/app/services/authentication/authentication.service.ts");








let SchoolService = class SchoolService {
    constructor(httpService, toastr, dialog, authenticationService) {
        this.httpService = httpService;
        this.toastr = toastr;
        this.dialog = dialog;
        this.authenticationService = authenticationService;
        this.mappedSchools = {};
        this.subjectSchools = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.schoolUpdated = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    setSchools(schools) {
        this.schools = schools.sort((a, b) => { return a.name > b.name ? 1 : -1; });
        ;
        this.mappedSchools = {};
        this.schools.forEach(school => this.mappedSchools[school.id] = school);
        this.subjectSchools.next(this.schools);
    }
    deleteSchool(school) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const dialogRef = this.dialog.open(_dialog_confirm_dialog_confirm_component__WEBPACK_IMPORTED_MODULE_5__["DialogConfirmComponent"], {
                width: '500px',
                data: {
                    title: "Are you sure?",
                    description: `This will remove ${school.name} and all associated bookings`
                }
            });
            const confirmed = yield dialogRef.afterClosed().toPromise();
            if (confirmed) {
                this.httpService.delete("school", school.id);
                this.schools = this.schools.filter(schoolInCollection => schoolInCollection.id != school.id);
                this.setSchools(this.schools);
                this.toastr.success(`${school.name} has been removed`);
            }
            return confirmed;
        });
    }
    saveSchool(school) {
        if (school.id > 0) {
            // save existing:
            this.httpService.put("school", school.id, school).subscribe(value => {
                Object.assign(this.mappedSchools[school.id], value);
                this.schoolUpdated.next(value);
                this.toastr.success("School information updated");
                if (school.password) {
                    this.authenticationService.setUserPassword(school.contactEmail, school.password);
                }
            });
        }
        else {
            this.httpService.post("school", school).subscribe(value => {
                this.schools.push(value);
                this.setSchools(this.schools);
                this.toastr.success("School account has been created");
                this.authenticationService.createUser(school.contactEmail, school.password, "ROLE_SCHOOL");
            });
        }
    }
};
SchoolService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"] },
    { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"] },
    { type: _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"] }
];
SchoolService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], SchoolService);



/***/ }),

/***/ "./src/app/util/ordina-date-adapter.ts":
/*!*********************************************!*\
  !*** ./src/app/util/ordina-date-adapter.ts ***!
  \*********************************************/
/*! exports provided: OrdinaDateAdapter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdinaDateAdapter", function() { return OrdinaDateAdapter; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm2015/core.js");


class OrdinaDateAdapter extends _angular_material_core__WEBPACK_IMPORTED_MODULE_1__["NativeDateAdapter"] {
    getWeekOfYear(date) {
        // get the first day of the year:
        const dayInYear = this.getDayInYear(date);
        const dayOfWeek = this.getDayOfWeek(date);
        const dayOfWeekStartOfYear = this.getDayOfWeek(new Date(date.getFullYear(), 0, 1));
        const weekNum = (dayInYear + 6) / 7 + (dayOfWeekStartOfYear > dayOfWeek ? 1 : 0);
        return Math.max(Math.floor(weekNum), 1);
    }
    getDayInYear(date) {
        const ref = new Date(date.getFullYear(), 0, 1);
        const diff = date.getTime() - ref.getTime();
        return Math.floor(diff / 24 / 60 / 60 / 1000); // return the day in year
    }
}


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Tim\IdeaProjects\ordinacodekids\frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map