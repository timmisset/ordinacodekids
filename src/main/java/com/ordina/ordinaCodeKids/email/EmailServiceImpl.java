package com.ordina.ordinaCodeKids.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class EmailServiceImpl implements EmailService {

    @Value("${ordinaCodeKids.sendAllEmailsTo: #{null}}")
    String sendAllEmailsTo;

    @Value("${ordinaCodeKids.sendEmails: #{false}}")
    Boolean sendEmails;

    @Value("${ordinaCodeKids.emailsFrom: codekids@ordina.nl}")
    String emailsFrom;

    private final JavaMailSender emailSender;
    private static final Logger log = LoggerFactory.getLogger(EmailService.class);


    private ScheduledExecutorService quickService = Executors.newScheduledThreadPool(20);

    public EmailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void sendSimpleMessage(
            String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();

        if(sendEmails) {
            if(sendAllEmailsTo != null && !sendAllEmailsTo.isEmpty()) {
                log.info(String.format("Should send email to '%s', however, the ordinaCodeKids.sendAllEmailsTo '%s' so all emails will be send there", to, sendAllEmailsTo));
                to = sendAllEmailsTo;
            }
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            message.setFrom(emailsFrom);

            // send email async, we don't need confirmation / error handling on this process (yet)
            quickService.submit(() -> emailSender.send(message));

        }
        else{
            log.info(String.format("Should send email to '%s', however, the ordinaCodeKids.sendEmail = false so no emails will be send", to));
        }

    }
}
