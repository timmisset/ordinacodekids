package com.ordina.ordinaCodeKids.configuration;

import com.ordina.ordinaCodeKids.user.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public SecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    BCryptPasswordEncoder passwordEncoder() {
        return  new BCryptPasswordEncoder();
    }

    private final DataSource dataSource;


    private String getUserQuery() {
        return "SELECT email as username, password, TRUE FROM user WHERE email = ?";
    }

    private String getAuthoritiesQuery() {
        return "SELECT email as username, role from user WHERE email = ?";
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder())
                .usersByUsernameQuery(getUserQuery())
                .authoritiesByUsernameQuery(getAuthoritiesQuery())
        ;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http    .cors().and()
                    .headers().frameOptions().sameOrigin().and()
                    .httpBasic().and()
                    .csrf().disable()
                    .formLogin().disable()


                    .authorizeRequests()
               
                        // /api/user
                        .antMatchers(HttpMethod.POST, "/api/user/**").hasRole(User.USER_ROLE_ADMIN)
                        .antMatchers(HttpMethod.PUT, "/api/user/**").hasRole(User.USER_ROLE_ADMIN)
                        .antMatchers(HttpMethod.GET, "/api/user/**").authenticated()
                        .antMatchers(HttpMethod.DELETE, "/api/user/**").hasRole(User.USER_ROLE_ADMIN)

                        .antMatchers(HttpMethod.POST, "/api/availability/**").authenticated()
                        .antMatchers(HttpMethod.PUT, "/api/availability/**").authenticated()
                        .antMatchers(HttpMethod.GET, "/api/availability/**").authenticated()
                        .antMatchers(HttpMethod.DELETE, "/api/availability/**").authenticated()

                        .antMatchers(HttpMethod.POST, "/api/employee/**").hasRole(User.USER_ROLE_ADMIN)
                        .antMatchers(HttpMethod.PUT, "/api/employee/**").hasRole(User.USER_ROLE_ADMIN)
                        .antMatchers(HttpMethod.GET, "/api/employee/**").authenticated()
                        .antMatchers(HttpMethod.DELETE, "/api/employee/**").hasRole(User.USER_ROLE_ADMIN)


                        .antMatchers(HttpMethod.POST, "/api/booking/**").authenticated()
                        .antMatchers(HttpMethod.PUT, "/api/booking/**").authenticated()
                        .antMatchers(HttpMethod.GET, "/api/booking/**").authenticated()
                        .antMatchers(HttpMethod.DELETE, "/api/booking/**").authenticated()

                        .antMatchers(HttpMethod.POST, "/api/school/**").hasRole(User.USER_ROLE_ADMIN)
                        .antMatchers(HttpMethod.PUT, "/api/school/**").hasRole(User.USER_ROLE_ADMIN)
                        .antMatchers(HttpMethod.GET, "/api/school/**").authenticated()
                        .antMatchers(HttpMethod.DELETE, "/api/school/**").hasRole(User.USER_ROLE_ADMIN)


            ;

    }

    @Bean
    CorsConfigurationSource corsConfigurationSource()
    {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        source.getCorsConfigurations().get("/**").addAllowedMethod(HttpMethod.DELETE);
        source.getCorsConfigurations().get("/**").addAllowedMethod(HttpMethod.PUT);
        return source;
    }
}
