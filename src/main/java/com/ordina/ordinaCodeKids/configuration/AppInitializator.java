package com.ordina.ordinaCodeKids.configuration;

import com.ordina.ordinaCodeKids.demo.DemoService;
import com.ordina.ordinaCodeKids.user.User;
import com.ordina.ordinaCodeKids.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
class AppInitializator {

    @Value("${demo.setDemoData: #{false}}")
    boolean setDemoData;

    @Value("${ordinaCodeKids.sendAllEmailsTo: #{null}}")
    String sendAllEmailsTo;

    private static final Logger log = LoggerFactory.getLogger(AppInitializator.class);
    private final DemoService demoService;
    private final UserService userService;

    AppInitializator(DemoService demoService, UserService userService) {
        this.demoService = demoService;
        this.userService = userService;
    }

    @PostConstruct
    private void init() {
        if(setDemoData) {
            log.info("Creating demo set for application");
            demoService.reset();
        }
        else{
            log.info("Not creating demo set because value of setDemoData = " + setDemoData);
        }
        Optional<User> user = userService.getUsers().stream().filter(user1 -> user1.getEmail().equals("admin")).findFirst();
        if(user.isEmpty()) {
            // set the admin account:
            userService.setAdminUser();
        }



    }

}
