package com.ordina.ordinaCodeKids.school;

import com.ordina.ordinaCodeKids.exception.SchoolAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.SchoolNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/school")
public class SchoolController {

    private final SchoolService schoolService;

    public SchoolController(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    @GetMapping()
    public ResponseEntity<List<SchoolDTO>> getSchools() {
        List<School> schools = schoolService.getSchools();
        List<SchoolDTO> schoolDTOs = schools.stream().map(schoolService::fromSchoolMapping).collect(Collectors.toList());
        return ResponseEntity.ok(schoolDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SchoolDTO> getSchool(@PathVariable long id) {
        try {
            School school = schoolService.getSchoolById(id);
            SchoolDTO schoolDTO = schoolService.fromSchoolMapping(school);
            return ResponseEntity.ok(schoolDTO);
        } catch (SchoolNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public ResponseEntity<SchoolDTO> createSchool(@RequestBody SchoolDTO schoolDTO) {
        School school = schoolService.toSchoolMapping(schoolDTO);
        try {
            schoolService.createSchool(school);
        } catch (SchoolAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        schoolDTO = schoolService.fromSchoolMapping(school);

        return ResponseEntity.ok(schoolDTO);

    }

    @PutMapping("/{id}")
    public ResponseEntity<SchoolDTO> updateSchool(@PathVariable long id, @RequestBody SchoolDTO schoolDTO) {
        try {
            School school = schoolService.toSchoolMapping(schoolDTO);
            schoolService.updateSchool(school);
            schoolDTO = schoolService.fromSchoolMapping(school);
            return ResponseEntity.ok().body(schoolDTO);
        } catch (SchoolNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteSchool(@PathVariable long id) {
        try {
            schoolService.deleteSchoolById(id);
            return ResponseEntity.ok(true);
        } catch (SchoolNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }



}
