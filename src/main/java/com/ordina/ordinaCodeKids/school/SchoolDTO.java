package com.ordina.ordinaCodeKids.school;

public class SchoolDTO {

    private long id;
    private String name;
    private String description;
    private String contactName;
    private String contactEmail;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public static final class SchoolDTOBuilder {
        private long id;
        private String name;
        private String description;
        private String contactName;
        private String contactEmail;

        private SchoolDTOBuilder() {
        }

        public static SchoolDTOBuilder aSchoolDTO() {
            return new SchoolDTOBuilder();
        }

        public SchoolDTOBuilder id(long id) {
            this.id = id;
            return this;
        }

        public SchoolDTOBuilder name(String name) {
            this.name = name;
            return this;
        }

        public SchoolDTOBuilder description(String description) {
            this.description = description;
            return this;
        }

        public SchoolDTOBuilder contactName(String contactName) {
            this.contactName = contactName;
            return this;
        }

        public SchoolDTOBuilder contactEmail(String contactEmail) {
            this.contactEmail = contactEmail;
            return this;
        }

        public SchoolDTO build() {
            SchoolDTO schoolDTO = new SchoolDTO();
            schoolDTO.setId(id);
            schoolDTO.setName(name);
            schoolDTO.setDescription(description);
            schoolDTO.setContactName(contactName);
            schoolDTO.setContactEmail(contactEmail);
            return schoolDTO;
        }
    }
}
