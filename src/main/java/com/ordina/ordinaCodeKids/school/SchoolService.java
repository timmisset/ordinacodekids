package com.ordina.ordinaCodeKids.school;

import com.ordina.ordinaCodeKids.exception.SchoolAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.SchoolNotFoundException;

import java.util.List;

public interface SchoolService {

    School createSchool(School school) throws SchoolAlreadyExistsException;

    void deleteSchool(School school) throws SchoolNotFoundException;

    void deleteSchoolById(long id) throws SchoolNotFoundException;

    School updateSchool(School school) throws SchoolNotFoundException;

    School getSchoolById(long id) throws SchoolNotFoundException;

    List<School> getSchools();

    School toSchoolMapping(SchoolDTO schoolDTO);

    SchoolDTO fromSchoolMapping(School school);
}
