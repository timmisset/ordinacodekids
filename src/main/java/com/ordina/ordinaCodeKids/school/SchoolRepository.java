package com.ordina.ordinaCodeKids.school;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SchoolRepository extends JpaRepository<School, Long> {


    boolean existsByContactEmail(String email);
}
