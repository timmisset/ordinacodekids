package com.ordina.ordinaCodeKids.school;

import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.SchoolAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.SchoolNotFoundException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;
import com.ordina.ordinaCodeKids.user.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SchoolServiceImpl implements SchoolService {

    private final SchoolRepository schoolRepository;
    private final UserService userService;

    public SchoolServiceImpl(SchoolRepository schoolRepository, UserService userService) {
        this.schoolRepository = schoolRepository;
        this.userService = userService;
    }

    @Override
    public School createSchool(School school) throws SchoolAlreadyExistsException {
        if(schoolRepository.existsById(school.getId())) {
            throw new SchoolAlreadyExistsException(school);
        }
        return schoolRepository.save(school);
    }

    @Override
    public void deleteSchool(School school) throws SchoolNotFoundException {
        if(!schoolRepository.existsById(school.getId())) {
            throw new SchoolNotFoundException(school.getId());
        }
        school = schoolRepository.getOne(school.getId());
        schoolRepository.deleteById(school.getId());

        // also remove the corresponding user account
        try {
            userService.deleteUserByEmail(school.getContactEmail());
        } catch (UserNotFoundException e) {
            // do nothing
        }
    }

    @Override
    public void deleteSchoolById(long id) throws SchoolNotFoundException {
        deleteSchool(School.SchoolBuilder.aSchool().id(id).build());
    }

    @Override
    public School updateSchool(School school) throws SchoolNotFoundException {
        if(!schoolRepository.existsById(school.getId())) {
            throw new SchoolNotFoundException(school.getId());
        }
        return schoolRepository.save(school);
    }

    @Override
    public School getSchoolById(long id) throws SchoolNotFoundException {
        Optional<School> school = schoolRepository.findById(id);
        if(school.isEmpty()) {
            throw new SchoolNotFoundException(id);
        }
        return school.get();
    }

    @Override
    public List<School> getSchools() { return schoolRepository.findAll(); }

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public School toSchoolMapping(SchoolDTO schoolDTO) {
        return modelMapper.map(schoolDTO, School.class);
    }

    @Override
    public SchoolDTO fromSchoolMapping(School school) {
        return modelMapper.map(school, SchoolDTO.class);
    }


}
