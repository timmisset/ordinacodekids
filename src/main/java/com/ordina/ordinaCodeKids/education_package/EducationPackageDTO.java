package com.ordina.ordinaCodeKids.education_package;

public class EducationPackageDTO {

    private long id;

    private String name;

    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final class EducationPackageDTOBuilder {
        long id;
        String name;
        String description;

        private EducationPackageDTOBuilder() {
        }

        public static EducationPackageDTOBuilder anEducationPackageDTO() {
            return new EducationPackageDTOBuilder();
        }

        public EducationPackageDTOBuilder id(long id) {
            this.id = id;
            return this;
        }

        public EducationPackageDTOBuilder name(String name) {
            this.name = name;
            return this;
        }

        public EducationPackageDTOBuilder description(String description) {
            this.description = description;
            return this;
        }

        public EducationPackageDTO build() {
            EducationPackageDTO educationPackageDTO = new EducationPackageDTO();
            educationPackageDTO.setId(id);
            educationPackageDTO.setName(name);
            educationPackageDTO.setDescription(description);
            return educationPackageDTO;
        }
    }
}
