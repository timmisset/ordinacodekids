package com.ordina.ordinaCodeKids.education_package;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;


@Entity
@EntityListeners(AuditingEntityListener.class)
public class EducationPackage {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String name;

    @Column()
    private String description;


    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;

    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "modified_by")
    @LastModifiedBy
    private String modifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    public static final class EducationPackageBuilder {
        private long id;
        private String name;
        private String description;

        private EducationPackageBuilder() {
        }

        public static EducationPackageBuilder anEducationPackage() {
            return new EducationPackageBuilder();
        }

        public EducationPackageBuilder id(long id) {
            this.id = id;
            return this;
        }

        public EducationPackageBuilder name(String name) {
            this.name = name;
            return this;
        }

        public EducationPackageBuilder description(String description) {
            this.description = description;
            return this;
        }



        public EducationPackage build() {
            EducationPackage educationPackage = new EducationPackage();
            educationPackage.setId(id);
            educationPackage.setName(name);
            educationPackage.setDescription(description);
            return educationPackage;
        }
    }
}
