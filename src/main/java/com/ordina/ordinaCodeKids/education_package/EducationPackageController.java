package com.ordina.ordinaCodeKids.education_package;

import com.ordina.ordinaCodeKids.exception.EducationPackageAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EducationPackageNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/educationpackage")
public class EducationPackageController {

    private final EducationPackageService educationPackageService;

    public EducationPackageController(EducationPackageService educationPackageService) {
        this.educationPackageService = educationPackageService;
    }

    @GetMapping()
    public ResponseEntity<List<EducationPackageDTO>> getEducationPackages() {
        List<EducationPackage> educationPackages = educationPackageService.getEducationPackages();
        List<EducationPackageDTO> educationPackageDTOs = educationPackages.stream().map(educationPackageService::fromEducationPackageMapping).collect(Collectors.toList());
        return ResponseEntity.ok(educationPackageDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EducationPackageDTO> getEducationPackage(@PathVariable long id) {
        try {
            EducationPackage educationPackage = educationPackageService.getEducationPackageById(id);
            EducationPackageDTO educationPackageDTO = educationPackageService.fromEducationPackageMapping(educationPackage);
            return ResponseEntity.ok(educationPackageDTO);
        } catch (EducationPackageNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public ResponseEntity<EducationPackageDTO> createEducationPackage(@RequestBody EducationPackageDTO educationPackageDTO) {
        EducationPackage educationPackage = educationPackageService.toEducationPackageMapping(educationPackageDTO);
        try {
            educationPackageService.createEducationPackage(educationPackage);
        } catch (EducationPackageAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        educationPackageDTO = educationPackageService.fromEducationPackageMapping(educationPackage);

        return ResponseEntity.ok(educationPackageDTO);

    }

    @PutMapping("/{id}")
    public ResponseEntity<EducationPackageDTO> updateEducationPackage(@PathVariable long id, @RequestBody EducationPackageDTO educationPackageDTO) {
        try {
            educationPackageDTO.setId(id);
            EducationPackage educationPackage = educationPackageService.toEducationPackageMapping(educationPackageDTO);
            educationPackageService.updateEducationPackage(educationPackage);
            educationPackageDTO = educationPackageService.fromEducationPackageMapping(educationPackage);
            return ResponseEntity.ok().body(educationPackageDTO);
        } catch (EducationPackageNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteEducationPackage(@PathVariable long id) {
        try {
            educationPackageService.deleteEducationPackageById(id);
            return ResponseEntity.ok(true);
        } catch (EducationPackageNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }



}
