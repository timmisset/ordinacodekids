package com.ordina.ordinaCodeKids.education_package;

import com.ordina.ordinaCodeKids.exception.EducationPackageAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EducationPackageNotFoundException;

import java.util.List;

public interface EducationPackageService {

    EducationPackage createEducationPackage(EducationPackage educationPackage) throws EducationPackageAlreadyExistsException;

    void deleteEducationPackage(EducationPackage educationPackage) throws EducationPackageNotFoundException;

    void deleteEducationPackageById(long id) throws EducationPackageNotFoundException;

    EducationPackage updateEducationPackage(EducationPackage educationPackage) throws EducationPackageNotFoundException;

    EducationPackage getEducationPackageById(long id) throws EducationPackageNotFoundException;

    EducationPackage getEducationPackageByName(String name) throws EducationPackageNotFoundException;

    List<EducationPackage> getEducationPackages();

    EducationPackage toEducationPackageMapping(EducationPackageDTO educationPackageDTO);

    EducationPackageDTO fromEducationPackageMapping(EducationPackage educationPackage);
}
