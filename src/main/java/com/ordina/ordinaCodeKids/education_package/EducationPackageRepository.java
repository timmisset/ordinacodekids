package com.ordina.ordinaCodeKids.education_package;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EducationPackageRepository extends JpaRepository<EducationPackage, Long> {

    Optional<EducationPackage> findByName(String name);

}
