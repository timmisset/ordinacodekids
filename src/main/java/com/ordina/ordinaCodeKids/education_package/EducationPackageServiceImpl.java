package com.ordina.ordinaCodeKids.education_package;

import com.ordina.ordinaCodeKids.exception.EducationPackageAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EducationPackageNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EducationPackageServiceImpl implements EducationPackageService {

    private final EducationPackageRepository educationPackageRepository;

    public EducationPackageServiceImpl(EducationPackageRepository educationPackageRepository) {
        this.educationPackageRepository = educationPackageRepository;
    }

    @Override
    public EducationPackage createEducationPackage(EducationPackage educationPackage) throws EducationPackageAlreadyExistsException {
        if(educationPackageRepository.existsById(educationPackage.getId())) {
            throw new EducationPackageAlreadyExistsException(educationPackage);
        }
        return educationPackageRepository.save(educationPackage);
    }

    @Override
    public void deleteEducationPackage(EducationPackage educationPackage) throws EducationPackageNotFoundException {
        if(!educationPackageRepository.existsById(educationPackage.getId())) {
            throw new EducationPackageNotFoundException(educationPackage.getId());
        }
        educationPackageRepository.deleteById(educationPackage.getId());
    }

    @Override
    public void deleteEducationPackageById(long id) throws EducationPackageNotFoundException {
        deleteEducationPackage(EducationPackage.EducationPackageBuilder.anEducationPackage().id(id).build());
    }

    @Override
    public EducationPackage updateEducationPackage(EducationPackage educationPackage) throws EducationPackageNotFoundException {
        if(!educationPackageRepository.existsById(educationPackage.getId())) {
            throw new EducationPackageNotFoundException(educationPackage.getId());
        }
        return educationPackageRepository.save(educationPackage);
    }

    @Override
    public EducationPackage getEducationPackageById(long id) throws EducationPackageNotFoundException {
        Optional<EducationPackage> educationPackage = educationPackageRepository.findById(id);
        if(educationPackage.isEmpty()) {
            throw new EducationPackageNotFoundException(id);
        }
        return educationPackage.get();
    }

    @Override
    public EducationPackage getEducationPackageByName(String name) throws EducationPackageNotFoundException {
        Optional<EducationPackage> educationPackage = educationPackageRepository.findByName(name);
        if(educationPackage.isEmpty()) {
            throw new EducationPackageNotFoundException(name);
        }
        return educationPackage.get();
    }

    @Override
    public List<EducationPackage> getEducationPackages() { return educationPackageRepository.findAll(); }

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public EducationPackage toEducationPackageMapping(EducationPackageDTO educationPackageDTO) {
        return modelMapper.map(educationPackageDTO, EducationPackage.class);
    }

    @Override
    public EducationPackageDTO fromEducationPackageMapping(EducationPackage educationPackage) {
        return modelMapper.map(educationPackage, EducationPackageDTO.class);
    }


}
