package com.ordina.ordinaCodeKids.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/demo")
public class DemoController {

    private final DemoService demoService;

    public DemoController(DemoService demoService) {
        this.demoService = demoService;
    }

    @GetMapping("/reset")
    public ResponseEntity<String> resetData() {
        demoService.reset();
        return ResponseEntity.ok("Finished resetting the demo data");
    }

}
