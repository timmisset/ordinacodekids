package com.ordina.ordinaCodeKids.demo;


import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.school.School;

import java.util.List;
import java.util.Map;

public interface DemoService {
    void reset();

    // employee removes -> availability removes -> booking
    void purge();

    void setDemoData();

    List<Employee> getEmployees();

    List<School> getSchools();

    List<Availability> getAvailabilities();

    Map<String, String> pickName();

    List<EducationPackage> getEducationPackages();

    List<EducationPackage> pickEducationPackage();
}
