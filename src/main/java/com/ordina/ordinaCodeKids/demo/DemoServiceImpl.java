package com.ordina.ordinaCodeKids.demo;

import com.ordina.ordinaCodeKids.OrdinaCodeKidsApplication;
import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.booking.Booking;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.education_package.EducationPackageRepository;
import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.employee.EmployeeRepository;
import com.ordina.ordinaCodeKids.school.School;
import com.ordina.ordinaCodeKids.school.SchoolRepository;
import com.ordina.ordinaCodeKids.user.User;
import com.ordina.ordinaCodeKids.user.UserRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Logger;

@Service
public class DemoServiceImpl implements DemoService {

    private final EducationPackageRepository educationPackageRepository;
    private final EmployeeRepository employeeRepository;
    private final SchoolRepository schoolRepository;
    private final UserRepository userRepository;

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    private final Map<String, String> encryped = new HashMap<>();

    private Logger logger = Logger.getLogger("DemoService");

    public DemoServiceImpl(EducationPackageRepository educationPackageRepository, EmployeeRepository employeeRepository, SchoolRepository schoolRepository, UserRepository userRepository) {
        this.educationPackageRepository = educationPackageRepository;
        this.employeeRepository = employeeRepository;
        this.schoolRepository = schoolRepository;
        this.userRepository = userRepository;
    }

    public static int NUMBER_OF_EMPLOYEES = 10;
    public static int NUMBER_OF_AVAILABILITIES = 100;

    /**
     * Number of available slots that are booked by schools.
     * Example: 3 -> 1 of 3 available slots are booked
     */
    public static int BOOKING_RATIO_1_IN = 3;
    public static int NUMBER_OF_SCHOOLS = 10;

    /**
     * Purges the database and resets the data
     */
    @Override
    public void reset() {
        purge();
        setDemoData();
    }

    // employee removes -> availability removes -> booking
    @Override
    public void purge() {
        employeeRepository.deleteAll();
        educationPackageRepository.deleteAll();
        schoolRepository.deleteAll();
        userRepository.deleteAll();

        schools = new ArrayList<>();
        employees = new ArrayList<>();
        educationPackages = new ArrayList<>();
        users = new ArrayList<>();
    }

    @Override
    public void setDemoData() {
        // load all data in memory
        getAvailabilities();

        // write to the database
        educationPackageRepository.saveAll(educationPackages);
        schoolRepository.saveAll(schools);
        employeeRepository.saveAll(employees);

        addUser("admin", User.USER_ROLE_ADMIN);

        userRepository.saveAll(users);

    }

    private Random random = new Random();

    List<User> users = new ArrayList<>();
    public void addUser(String email, String role) {
        String password;
        String rawPassword = role.toLowerCase();

        if(encryped.containsKey(role)) {
            password = encryped.get(role);
        }
        else{
            password = encoder.encode(rawPassword);
            encryped.put(role, password);
        }
        User user = User.UserBuilder.anUser()
                .email(email)
                .password(password)
                .role("ROLE_" + role)
                .build();
        users.add(user);
    }

    List<Employee> employees = new ArrayList<>();
    @Override
    public List<Employee> getEmployees() {

        if(employees.isEmpty()) {
            int numberOfEmployees = NUMBER_OF_EMPLOYEES;
            for(int i = 0; i < numberOfEmployees; i++) {
                Map<String, String> employeePickedName = pickName();
                String employeeDomainName = "ordina.nl";
                String employeeNameConcat = employeePickedName.get("firstname") + " " + employeePickedName.get("lastname");
                String employeeEmail = (employeeNameConcat.replaceAll(" ", ".") + "@" + employeeDomainName).toLowerCase();

                Employee employee = Employee.EmployeeBuilder.anEmployee()
                        .firstName(employeePickedName.get("firstname"))
                        .lastName(employeePickedName.get("lastname"))
                        .email(employeeEmail)
                        .educationPackages(pickEducationPackage())
                        .build();

                employees.add(employee);

                addUser(employeeEmail, User.USER_ROLE_EMPLOYEE);
            }

        }

        return employees;
    }

    public Employee pickEmployee() {
        return getEmployees().get(getRandomNumber(0, getEmployees().size() - 1));
    }



    // //////////////////////////////////////////////////////////////////////////////////////////////////////
    // SCHOOLS
    // //////////////////////////////////////////////////////////////////////////////////////////////////////
    List<School> schools = new ArrayList<>();
    @Override
    public List<School> getSchools() {
        if(schools.isEmpty()) {
            List<String> schoolNames = List.of("De Regenboog", "De Wegwijzer",
                    "Juliana", "Het Kompas", "De Rank", "Willem-Alexander", "Beatrix",
                    "De Ark", "De Bron", "Wilhelmina", "De Hoeksteen", "Simon Carmiggelt");
            if(NUMBER_OF_SCHOOLS > schoolNames.size()) {
                logger.warning("Maximum number of schools is = " + schoolNames.size() + ", use value has been adjusted");
                NUMBER_OF_SCHOOLS = schoolNames.size();
            }
            schoolNames = schoolNames.subList(0, NUMBER_OF_SCHOOLS);

            schoolNames.forEach(schoolName -> {
                Map<String, String> contact = pickName();
                String schoolDomainName = schoolName.toLowerCase().replaceAll(" ", "-") + ".nl";
                String schoolContactNameConcat = contact.get("firstname") + " " + contact.get("lastname");
                String schoolContactEmail = (schoolContactNameConcat.replaceAll(" ", ".") + "@" + schoolDomainName).toLowerCase();

                School school = School.SchoolBuilder.aSchool()
                        .contactEmail(schoolContactEmail)
                        .contactName(schoolContactNameConcat)
                        .description("Basisschool " + schoolName)
                        .name(schoolName)
                        .build();

                schools.add(school);
                addUser(schoolContactEmail, User.USER_ROLE_SCHOOL);
            });
        }
        return schools;
    }
    public School pickSchool() {
        return getSchools().get(getRandomNumber(0, getSchools().size() - 1));
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////
    // AVAILABILITIES
    // //////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public List<Availability> getAvailabilities() {
        logger.info("Building collection of availability slots");
        List<Availability> availabilities = new ArrayList<>();

        int numberOfAvailableSlots = NUMBER_OF_AVAILABILITIES;
        logger.info("Number of slots that will be created: " + numberOfAvailableSlots);

        List<DayOfWeek> weekendDays = List.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
        List<Employee> pickedEmployees = new ArrayList<>();
        for(int i = 0; i < numberOfAvailableSlots; i++) {
            // select a date for each available slot:
            LocalDate date = LocalDate.now();

            // number of days ahead, max = 100
            int numberOfDaysAhead = getRandomNumber(1, 100);
            date = date.plusDays(numberOfDaysAhead);
            while(weekendDays.contains(date.getDayOfWeek())) { // skip the weekends
                date = date.plusDays(getRandomNumber(1, 5)); // adjust to a different day. Choose random to prevent mondays to be overpopulated
            }

            Employee pickedEmployee = pickEmployee();
            List<Employee> duplicatesPicked = new ArrayList<>();
            while(pickedEmployees.contains(pickedEmployee)) {
                duplicatesPicked.add(pickedEmployee);
                employees.remove(pickedEmployee);
                pickedEmployee = pickEmployee();
            }
            pickedEmployees.add(pickedEmployee);
            employees.addAll(duplicatesPicked);

            Availability availability = Availability.AvailabilityBuilder.anAvailability()
                    .date(date)
                    .employee(pickedEmployee)
                    .build();
            availability.getEmployee().addAvailability(availability);
            availabilities.add(availability);

            // determine if the day is already booked:
            boolean isBooked = getRandomNumber(1, BOOKING_RATIO_1_IN) == 1; // 25% of the slots are booked
            if(isBooked) {
                Booking booking = Booking.BookingBuilder.aBooking()
                        .school(pickSchool())
                        .availability(availability)
                        .build();

                availability.setBooking(booking);
            }
        }
        logger.info("Finished creation of availabilities");
        return availabilities;

    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////
    // NAME PICKER
    // //////////////////////////////////////////////////////////////////////////////////////////////////////
    private List<String> firstnames = new ArrayList<>();
    private List<String> lastnames = new ArrayList<>();
    private List<String> pickedNames = new ArrayList<>();
    @Override
    public Map<String, String> pickName() {
        if(firstnames.isEmpty() || lastnames.isEmpty()) {
            Logger.getLogger("Names").info("Reading first and last names from the demo set");
            readNames();
        }
        HashMap<String, String> pickedName = new HashMap<>();
        pickedName.put("firstname", firstnames.get(getRandomNumber(0, firstnames.size() - 1)));
        pickedName.put("lastname", lastnames.get(getRandomNumber(0, lastnames.size() - 1)));

        String concatenatedName = pickedName.get("firstname") + "." + pickedName.get("lastname");

        if(pickedNames.contains(concatenatedName)) {
            return pickName();
        }
        pickedNames.add(concatenatedName);
        return pickedName;
    }
    private void readNames() {
        pickedNames = new ArrayList<>();
        final InputStream achternamenStream = this.getClass().getResourceAsStream("/achternamen.txt");
        final InputStream voornamenStream = this.getClass().getResourceAsStream("/voornamen.txt");
        try {
            String achternamen = readFromInputStream(achternamenStream); //FileUtils.readFileToString(new File(achternamenURL.getPath()), Charset.defaultCharset());
            String voornamen = readFromInputStream(voornamenStream);
            lastnames = Arrays.asList(achternamen.split(";"));
            firstnames = Arrays.asList(voornamen.split(";"));
        } catch (IOException e) {
            // TODO: catch exception
        }
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////
    // EDUCATION PACKAGES
    // //////////////////////////////////////////////////////////////////////////////////////////////////////
    private List<EducationPackage> educationPackages = new ArrayList<>();

    @Override
    public List<EducationPackage> getEducationPackages() {
            return List.of(
                    EducationPackage.EducationPackageBuilder.anEducationPackage().name("Coding").description("Learn all the basics needed to become a real software engineer").build(),
                    EducationPackage.EducationPackageBuilder.anEducationPackage().name("Security").description("Don't worry, after this course no one will be able to hack you anymore").build(),
                    EducationPackage.EducationPackageBuilder.anEducationPackage().name("Agile").description("Agile? Is that really necessary? Yes it is! This is why...").build()
            );
    }

    @Override
    public List<EducationPackage> pickEducationPackage() {
        if(educationPackages.isEmpty()) {
            educationPackages = getEducationPackages();
        }
        int numberOfPackages = getRandomNumber(2, educationPackages.size());
        List<EducationPackage> selection = new ArrayList<>();
        for(int i = 0; i < numberOfPackages; i++) {
            int selected = random.nextInt(educationPackages.size());

            EducationPackage educationPackage = educationPackages.get(selected);
            if(!selection.contains(educationPackage)) {
                selection.add(educationPackage);
            }else {
                i--;
            }
        }
        return selection;
    }

    private int getRandomNumber(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}
