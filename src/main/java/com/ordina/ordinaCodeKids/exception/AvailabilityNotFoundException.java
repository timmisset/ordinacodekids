package com.ordina.ordinaCodeKids.exception;

public class AvailabilityNotFoundException extends Throwable {
    public AvailabilityNotFoundException(long id) {
        super("Cannot find availability by id '" + id + "'");
    }
}
