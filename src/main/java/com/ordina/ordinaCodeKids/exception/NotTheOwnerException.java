package com.ordina.ordinaCodeKids.exception;


public class NotTheOwnerException extends Throwable {

    public NotTheOwnerException() {
        super("You are not the owner of this item or have sufficient rights to perform this operation");
    }

}
