package com.ordina.ordinaCodeKids.exception;

public class SchoolNotFoundException extends Throwable {
    public SchoolNotFoundException(long id) {
        super("Cannot find school by id '" + id + "'");
    }
}
