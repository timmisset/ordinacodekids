package com.ordina.ordinaCodeKids.exception;

import com.ordina.ordinaCodeKids.school.School;

public class SchoolAlreadyExistsException extends Throwable {
    public SchoolAlreadyExistsException(School school) {
        super("School with id '" + school.getId() + "' already exists");
    }
}
