package com.ordina.ordinaCodeKids.exception;

public class BookingNotFoundException extends Throwable {
    public BookingNotFoundException(long id) {
        super("Cannot find booking by id '" + id + "'");
    }
}
