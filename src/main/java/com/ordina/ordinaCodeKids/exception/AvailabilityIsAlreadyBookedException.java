package com.ordina.ordinaCodeKids.exception;

import com.ordina.ordinaCodeKids.availability.Availability;

public class AvailabilityIsAlreadyBookedException extends Throwable {
    public AvailabilityIsAlreadyBookedException(Availability availability) {
        super("The availability slot with id '" + availability.getId() + "' is already booked");
    }
}
