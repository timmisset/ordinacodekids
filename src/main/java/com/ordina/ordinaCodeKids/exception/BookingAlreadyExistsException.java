package com.ordina.ordinaCodeKids.exception;

import com.ordina.ordinaCodeKids.booking.Booking;

public class BookingAlreadyExistsException extends Throwable {
    public BookingAlreadyExistsException(Booking booking) {
        super("Booking with id '" + booking.getId() + "' already exists");
    }
}
