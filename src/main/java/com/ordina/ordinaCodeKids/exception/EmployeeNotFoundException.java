package com.ordina.ordinaCodeKids.exception;

public class EmployeeNotFoundException extends Throwable {
    public EmployeeNotFoundException(long id) {
        super("Cannot find employee by id '" + id + "'");
    }
}
