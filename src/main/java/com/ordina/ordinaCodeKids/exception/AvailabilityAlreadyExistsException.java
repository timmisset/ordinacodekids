package com.ordina.ordinaCodeKids.exception;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.employee.Employee;

import java.time.LocalDate;

public class AvailabilityAlreadyExistsException extends Throwable {
    public AvailabilityAlreadyExistsException(Availability availability) {
        super("Availability with id '" + availability.getId() + "' already exists");
    }
    public AvailabilityAlreadyExistsException(Employee employee, LocalDate date) {
        super("Availability already exists for employee id = " + employee.getId() + " on date " + date.toString());
    }
}
