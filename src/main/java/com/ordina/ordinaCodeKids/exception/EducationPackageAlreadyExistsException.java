package com.ordina.ordinaCodeKids.exception;

import com.ordina.ordinaCodeKids.education_package.EducationPackage;

public class EducationPackageAlreadyExistsException extends Exception {

    public EducationPackageAlreadyExistsException(EducationPackage educationPackage) {
        super("Education package with id '" + educationPackage.getId() + "' already exists");
    }

}
