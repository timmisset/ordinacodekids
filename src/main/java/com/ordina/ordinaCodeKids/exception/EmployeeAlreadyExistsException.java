package com.ordina.ordinaCodeKids.exception;

import com.ordina.ordinaCodeKids.employee.Employee;

public class EmployeeAlreadyExistsException extends Exception {

    public EmployeeAlreadyExistsException(Employee employee) {
        super("Employee with id '" + employee.getId() + "' already exists");
    }

    public EmployeeAlreadyExistsException(String email) {
        super("Employee with email '" + email + "' already exists");
    }

}
