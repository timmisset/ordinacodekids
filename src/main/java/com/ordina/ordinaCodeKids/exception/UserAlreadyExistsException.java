package com.ordina.ordinaCodeKids.exception;

import com.ordina.ordinaCodeKids.user.User;

public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(User user) {
        super("User with email " + user.getEmail() + " already exists");
    }

}
