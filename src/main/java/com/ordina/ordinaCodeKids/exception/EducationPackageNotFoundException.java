package com.ordina.ordinaCodeKids.exception;

public class EducationPackageNotFoundException extends Throwable {
    public EducationPackageNotFoundException(long id) {
        super("Cannot find education package by id '" + id + "'");
    }
    public EducationPackageNotFoundException(String name) {
        super("Cannot find education package by name '" + name + "'");
    }
}
