package com.ordina.ordinaCodeKids.exception;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String email) {
        super("Cannot find user with email " + email);
    }
    public UserNotFoundException(long id) { super("Cannot find user with id "  + id); }

}
