package com.ordina.ordinaCodeKids;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class OrdinaCodeKidsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdinaCodeKidsApplication.class, args);
	}

}
