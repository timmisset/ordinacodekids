package com.ordina.ordinaCodeKids.user;

import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.UserAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    private final EmailService emailService;

    public UserServiceImpl(UserRepository userRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.emailService = emailService;
    }

    @Override
    public User createUser(User user) throws UserAlreadyExistsException {
        if(userRepository.existsById(user.getId())) {
            throw new UserAlreadyExistsException(user);
        }
        // send confirmation email:
        emailService.sendSimpleMessage(user.getEmail(), "Account created", "An account has been created for you. You can login using your email. A password will be send in a separate email");

        setUserPassword(user, user.getPassword());
        userRepository.save(user);

        return user;
    }

    @Override
    public void deleteUser(User user) throws UserNotFoundException {
        if(!userRepository.existsById(user.getId())) {
            throw new UserNotFoundException(user.getId());
        }
        userRepository.deleteById(user.getId());
    }

    @Override
    public void deleteUserById(long id) throws UserNotFoundException {
        deleteUser(User.UserBuilder.anUser().id(id).build());
    }

    @Override
    public void deleteUserByEmail(String email) throws UserNotFoundException {
        deleteUser(getUserByEmail(email));
    }

    @Override
    public User updateUser(User user) throws UserNotFoundException {
        if(!userRepository.existsById(user.getId())) {
            throw new UserNotFoundException(user.getId());
        }
        if(user.getPassword() != null && !user.getPassword().isEmpty()) {
            setUserPassword(user, user.getPassword());
        }
        return userRepository.save(user);
    }

    @Override
    public User getUserById(long id) throws UserNotFoundException {
        Optional<User> user = userRepository.findById(id);
        if(user.isEmpty()) {
            throw new UserNotFoundException(id);
        }
        return user.get();
    }

    private void setUserPassword(User user, String password) {
        user.setPassword(encoder.encode(password));
        emailService.sendSimpleMessage(user.getEmail(), "Ordina CodeKids Password", "Your account password is " + password);
        userRepository.save(user);
    }

    @Override
    public void setUserPassword(String email, String password) throws UserNotFoundException {
        User user = getUserByEmail(email);
        setUserPassword(user, password);
    }

    @Override
    public User getUserByEmail(String email) throws UserNotFoundException {
        Optional<User> user = userRepository.findOneByEmail(email);
        if(user.isEmpty()) {
            throw new UserNotFoundException(email);
        }
        return user.get();
    }

    @Override
    public List<User> getUsers() { return userRepository.findAll(); }

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public User toUserMapping(UserDTO userDTO) {
        return modelMapper.map(userDTO, User.class);
    }

    @Override
    public UserDTO fromUserMapping(User user) {

        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        userDTO.setPassword(null);
        return userDTO;
    }

    @Value("${ordinaCodeKids.admin.email}")
    private String adminEmail;

    @Value("${ordinaCodeKids.admin.password}")
    private String adminPassword;

    @Override
    public void setAdminUser() {
        if(userRepository.findByRole(User.USER_ROLE_ADMIN).isEmpty()) {

            User user = User.UserBuilder.anUser()
                    .role("ROLE_" + User.USER_ROLE_ADMIN)
                    .email(adminEmail)
                    .password(encoder.encode(adminPassword))
                    .build();
            userRepository.save(user);
        }
    }

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> currentUser = userRepository.findOneByEmail(authentication.getName());
        return currentUser.orElse(null);
    }


}
