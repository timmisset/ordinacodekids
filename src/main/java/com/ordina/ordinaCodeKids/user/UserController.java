package com.ordina.ordinaCodeKids.user;

import com.ordina.ordinaCodeKids.exception.UserAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;
import com.ordina.ordinaCodeKids.user.User;
import com.ordina.ordinaCodeKids.user.UserDTO;
import com.ordina.ordinaCodeKids.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<User> users = userService.getUsers();
        List<UserDTO> userDTOs = users.stream().map(userService::fromUserMapping).collect(Collectors.toList());
        return ResponseEntity.ok(userDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable long id) {
        try {
            User user = userService.getUserById(id);
            UserDTO userDTO = userService.fromUserMapping(user);
            return ResponseEntity.ok(userDTO);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/login")
    public ResponseEntity<UserDTO> getUser() {
        return getUser(userService.getCurrentUser().getId());
    }

    @PostMapping()
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
        User user = userService.toUserMapping(userDTO);
        try {
            userService.createUser(user);
        } catch (UserAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        userDTO = userService.fromUserMapping(user);

        return ResponseEntity.ok(userDTO);

    }

    @PutMapping("/setPassword")
    public ResponseEntity<UserDTO> updateUserPassword(@RequestBody UserDTO userDTO){
        try {
            userService.setUserPassword(userDTO.getEmail(), userDTO.getPassword());
            userDTO.setPassword(null);
            return ResponseEntity.ok(userDTO);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> updateUser(@PathVariable long id, @RequestBody UserDTO userDTO) {
        try {
            User user = userService.toUserMapping(userDTO);
            userService.updateUser(user);
            userDTO = userService.fromUserMapping(user);
            return ResponseEntity.ok().body(userDTO);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable long id) {
        try {
            userService.deleteUserById(id);
            return ResponseEntity.ok(true);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }



}
