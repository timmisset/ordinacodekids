package com.ordina.ordinaCodeKids.user;

import com.ordina.ordinaCodeKids.exception.UserAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;

import java.util.List;

public interface UserService {

    User createUser(User user) throws UserAlreadyExistsException;

    void deleteUser(User user) throws UserNotFoundException;

    void deleteUserById(long id) throws UserNotFoundException;

    void deleteUserByEmail(String email) throws UserNotFoundException;

    User updateUser(User user) throws UserNotFoundException;

    User getUserById(long id) throws UserNotFoundException;

    void setUserPassword(String email, String password) throws UserNotFoundException;

    User getUserByEmail(String email) throws UserNotFoundException;

    List<User> getUsers();

    User toUserMapping(UserDTO userDTO);

    UserDTO fromUserMapping(User user);

    void setAdminUser();

    User getCurrentUser();
}
