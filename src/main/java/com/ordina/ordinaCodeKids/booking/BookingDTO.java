package com.ordina.ordinaCodeKids.booking;

public class BookingDTO {

    private long id;

    private long availabilityId;

    private long schoolId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAvailabilityId() {
        return availabilityId;
    }

    public void setAvailabilityId(long availabilityId) {
        this.availabilityId = availabilityId;
    }

    public long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(long schoolId) {
        this.schoolId = schoolId;
    }

    public static final class BookingDTOBuilder {
        private long id;
        private long availabilityId;
        private long schoolId;

        private BookingDTOBuilder() {
        }

        public static BookingDTOBuilder aBookingDTO() {
            return new BookingDTOBuilder();
        }

        public BookingDTOBuilder id(long id) {
            this.id = id;
            return this;
        }

        public BookingDTOBuilder availabilityId(long availabilityId) {
            this.availabilityId = availabilityId;
            return this;
        }

        public BookingDTOBuilder schoolId(long schoolId) {
            this.schoolId = schoolId;
            return this;
        }

        public BookingDTO build() {
            BookingDTO bookingDTO = new BookingDTO();
            bookingDTO.setId(id);
            bookingDTO.setAvailabilityId(availabilityId);
            bookingDTO.setSchoolId(schoolId);
            return bookingDTO;
        }
    }
}
