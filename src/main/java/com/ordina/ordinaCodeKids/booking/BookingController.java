package com.ordina.ordinaCodeKids.booking;

import com.ordina.ordinaCodeKids.exception.AvailabilityIsAlreadyBookedException;
import com.ordina.ordinaCodeKids.exception.BookingAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.BookingNotFoundException;
import com.ordina.ordinaCodeKids.exception.NotTheOwnerException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/booking")
public class BookingController {

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping()
    public ResponseEntity<List<BookingDTO>> getBookings() {
        List<Booking> bookings = bookingService.getBookings();
        List<BookingDTO> bookingDTOs = bookings.stream().map(bookingService::fromBookingMapping).collect(Collectors.toList());
        return ResponseEntity.ok(bookingDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookingDTO> getBooking(@PathVariable long id) {
        try {
            Booking booking = bookingService.getBookingById(id);
            BookingDTO bookingDTO = bookingService.fromBookingMapping(booking);
            return ResponseEntity.ok(bookingDTO);
        } catch (BookingNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public ResponseEntity<BookingDTO> createBooking(@RequestBody BookingDTO bookingDTO) {
        Booking booking = bookingService.toBookingMapping(bookingDTO);

        try {
            bookingService.createBooking(booking);
        } catch (BookingAlreadyExistsException | AvailabilityIsAlreadyBookedException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (NotTheOwnerException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

        bookingDTO = bookingService.fromBookingMapping(booking);

        return ResponseEntity.ok(bookingDTO);

    }

    @PutMapping("/{id}")
    public ResponseEntity<BookingDTO> updateBooking(@PathVariable long id, @RequestBody BookingDTO bookingDTO) {
        try {
            Booking booking = bookingService.toBookingMapping(bookingDTO);
            bookingService.updateBooking(booking);
            bookingDTO = bookingService.fromBookingMapping(booking);
            return ResponseEntity.ok().body(bookingDTO);
        } catch (BookingNotFoundException | NotTheOwnerException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteBooking(@PathVariable long id) {
        try {
            bookingService.deleteBookingById(id);
            return ResponseEntity.ok(true);
        } catch (BookingNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotTheOwnerException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }



}
