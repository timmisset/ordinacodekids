package com.ordina.ordinaCodeKids.booking;

import com.ordina.ordinaCodeKids.exception.AvailabilityIsAlreadyBookedException;
import com.ordina.ordinaCodeKids.exception.BookingAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.BookingNotFoundException;
import com.ordina.ordinaCodeKids.exception.NotTheOwnerException;

import java.util.List;

public interface BookingService {

    boolean canClaim(Booking booking);

    Booking createBooking(Booking bookings) throws BookingAlreadyExistsException, AvailabilityIsAlreadyBookedException, NotTheOwnerException;

    void deleteBooking(Booking bookings) throws NotTheOwnerException;

    String getOwner(Booking booking);

    void deleteBookingById(long id) throws BookingNotFoundException, NotTheOwnerException;

    Booking updateBooking(Booking bookings) throws BookingNotFoundException, NotTheOwnerException;

    Booking getBookingById(long id) throws BookingNotFoundException;

    List<Booking> getBookings();

    Booking toBookingMapping(BookingDTO bookingsDTO);

    BookingDTO fromBookingMapping(Booking bookings);
}
