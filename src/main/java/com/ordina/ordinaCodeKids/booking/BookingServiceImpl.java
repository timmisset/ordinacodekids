package com.ordina.ordinaCodeKids.booking;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityService;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.*;
import com.ordina.ordinaCodeKids.school.SchoolService;
import com.ordina.ordinaCodeKids.user.User;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final AvailabilityService availabilityService;
    private final SchoolService schoolService;
    private final EmailService emailService;

    public BookingServiceImpl(BookingRepository bookingRepository, AvailabilityService availabilityService, SchoolService schoolService, EmailService emailService) {
        this.bookingRepository = bookingRepository;
        this.availabilityService = availabilityService;
        this.schoolService = schoolService;
        this.emailService = emailService;
    }

    private void validateCreate(Booking booking) throws BookingAlreadyExistsException, AvailabilityIsAlreadyBookedException, NotTheOwnerException {
        if(!canClaim(booking)) {
            throw new NotTheOwnerException();
        }
        if(bookingRepository.existsById(booking.getId())) {
            throw new BookingAlreadyExistsException(booking);
        }
        if(booking.getAvailability() != null && bookingRepository.existsByAvailabilityId(booking.getAvailability().getId())) {
            throw new AvailabilityIsAlreadyBookedException(booking.getAvailability());
        }
    }

    @Override
    public boolean canClaim(Booking booking) {
        return User.isAdminOr(getOwner(booking));
    }

    @Override
    public Booking createBooking(Booking booking) throws BookingAlreadyExistsException, AvailabilityIsAlreadyBookedException, NotTheOwnerException {
        validateCreate(booking);
        // assign the booking to the validation:
        booking.getAvailability().setBooking(booking);

        // send booking confirmation:
        emailService.sendSimpleMessage(booking.getSchool().getContactEmail(),
                "Ordina CodeKids", "Gefeliciteerd, de booking voor '" + booking.getAvailability().getDate() + "' is geslaagd. Jullie kunnen " + booking.getAvailability().getEmployee().getFirstName() + " t.z.t. verwachten op jullie school");
        emailService.sendSimpleMessage(booking.getAvailability().getEmployee().getEmail(),
                "Ordina CodeKids", "De school " + booking.getSchool().getName() + " heeft u aangevraagd voor " + booking.getAvailability().getDate() + ". Veel plezier!");

        return bookingRepository.save(booking);
    }

    @Override
    public void deleteBooking(Booking booking) throws NotTheOwnerException {
        if(canClaim(booking)) {

            Availability availability = booking.getAvailability();
            booking.getAvailability().setBooking(null);

            // send booking confirmation:
            emailService.sendSimpleMessage(booking.getSchool().getContactEmail(),
                    "Ordina CodeKids", "De booking voor '" + availability.getDate() + "' is geannuleerd.");
            emailService.sendSimpleMessage(availability.getEmployee().getEmail(),
                    "Ordina CodeKids", "De booking voor school " + booking.getSchool().getName() + " op " + availability.getDate() + " is geannuleerd.");

            bookingRepository.delete(booking);
        }
        else{
            throw new NotTheOwnerException();
        }
    }

    @Override
    public String getOwner(Booking booking) {
        if(booking == null || booking.getSchool() == null) {
            return null;
        }
        return booking.getSchool().getContactEmail();
    }

    @Override
    public void deleteBookingById(long id) throws BookingNotFoundException, NotTheOwnerException {
        deleteBooking(getBookingById(id));
    }

    @Override
    public Booking updateBooking(Booking booking) throws BookingNotFoundException, NotTheOwnerException {
        if(!bookingRepository.existsById(booking.getId())) {
            throw new BookingNotFoundException(booking.getId());
        }
        if(!canClaim(booking)) {
            throw new NotTheOwnerException();
        }
        return bookingRepository.save(booking);
    }

    @Override
    public Booking getBookingById(long id) throws BookingNotFoundException {
        Optional<Booking> booking = bookingRepository.findById(id);
        if(booking.isEmpty()) {
            throw new BookingNotFoundException(id);
        }
        return booking.get();
    }

    @Override
    public List<Booking> getBookings() { return bookingRepository.findAll(); }

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public Booking toBookingMapping(BookingDTO bookingDTO) {
        TypeMap<BookingDTO, Booking> toBookingMap = modelMapper.getTypeMap(BookingDTO.class, Booking.class);
        if(toBookingMap == null) {
            toBookingMap = modelMapper.createTypeMap(BookingDTO.class, Booking.class);

            toBookingMap.addMappings(new PropertyMap<>() {
                @Override
                protected void configure() {
                using(mappingContext ->
                    {
                        try {
                            return availabilityService.getAvailabilityById(((BookingDTO) mappingContext.getSource()).getAvailabilityId());
                        } catch (AvailabilityNotFoundException e) {
                            return null;
                        }
                    }
                ).map(source, destination.getAvailability());
                }
            });
            toBookingMap.addMappings(new PropertyMap<>() {
                @Override
                protected void configure() {
                using(mappingContext -> {
                    try{
                        return schoolService.getSchoolById(((BookingDTO) mappingContext.getSource()).getSchoolId());
                    } catch (SchoolNotFoundException e) {
                        return null;
                    }
                }).map(source, destination.getSchool());
                }
            });
        }

        return toBookingMap.map(bookingDTO);
    }

    @Override
    public BookingDTO fromBookingMapping(Booking booking) {
        return modelMapper.map(booking, BookingDTO.class);
    }


}
