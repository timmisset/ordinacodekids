package com.ordina.ordinaCodeKids.booking;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.school.School;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Booking {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne(optional = false, mappedBy = "booking")
    private Availability availability;

    @ManyToOne(optional = false)
    private School school;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;

    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "modified_by")
    @LastModifiedBy
    private String modifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public static final class BookingBuilder {
        private long id;
        private Availability availability;
        private School school;

        private BookingBuilder() {
        }

        public static BookingBuilder aBooking() {
            return new BookingBuilder();
        }

        public BookingBuilder id(long id) {
            this.id = id;
            return this;
        }

        public BookingBuilder availability(Availability availability) {
            this.availability = availability;
            return this;
        }

        public BookingBuilder school(School school) {
            this.school = school;
            return this;
        }


        public Booking build() {
            Booking booking = new Booking();
            booking.setId(id);
            booking.setAvailability(availability);
            booking.setSchool(school);
            return booking;
        }
    }
}
