package com.ordina.ordinaCodeKids.booking;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    boolean existsByAvailabilityId(long availability_id);

}
