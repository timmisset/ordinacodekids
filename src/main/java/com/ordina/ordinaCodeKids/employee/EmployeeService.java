package com.ordina.ordinaCodeKids.employee;

import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.exception.EmployeeAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EmployeeNotFoundException;

import java.util.List;

public interface EmployeeService {

    Employee createEmployee(Employee employees) throws EmployeeAlreadyExistsException;

    void deleteEmployee(Employee employees) throws EmployeeNotFoundException;

    void deleteEmployeeById(long id) throws EmployeeNotFoundException;

    Employee updateEmployee(Employee employees) throws EmployeeNotFoundException, EmployeeAlreadyExistsException;

    Employee getEmployeeById(long id) throws EmployeeNotFoundException;

    List<Employee> getEmployees();


    List<Employee> getEmployeesByEducationPackage(EducationPackage educationPackage);

    Employee toEmployeeMapping(EmployeeDTO employeesDTO);

    EmployeeDTO fromEmployeeMapping(Employee employees);
}
