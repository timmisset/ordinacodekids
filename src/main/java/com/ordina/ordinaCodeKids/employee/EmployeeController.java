package com.ordina.ordinaCodeKids.employee;

import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.education_package.EducationPackageService;
import com.ordina.ordinaCodeKids.exception.EducationPackageNotFoundException;
import com.ordina.ordinaCodeKids.exception.EmployeeAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EmployeeNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final EducationPackageService educationPackageService;

    public EmployeeController(EmployeeService employeeService, EducationPackageService educationPackageService) {
        this.employeeService = employeeService;
        this.educationPackageService = educationPackageService;
    }

    @GetMapping()
    public ResponseEntity<List<EmployeeDTO>> getEmployees() {
        List<Employee> employees = employeeService.getEmployees();
        List<EmployeeDTO> employeeDTOs = employees.stream().map(employeeService::fromEmployeeMapping).collect(Collectors.toList());
        return ResponseEntity.ok(employeeDTOs);
    }

    @GetMapping("/educationpackage/{educationpackage}")
    public ResponseEntity<List<EmployeeDTO>> getEmployeesByEducationPackage(@PathVariable String educationpackage) {
        List<Employee> employees = employeeService.getEmployeesByEducationPackage(
                getEducationPackage(educationpackage)
        );
        List<EmployeeDTO> employeeDTOs = employees.stream().map(employeeService::fromEmployeeMapping).collect(Collectors.toList());
        return ResponseEntity.ok(employeeDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeDTO> getEmployee(@PathVariable long id) {
        try {
            Employee employee = employeeService.getEmployeeById(id);
            EmployeeDTO employeeDTO = employeeService.fromEmployeeMapping(employee);
            return ResponseEntity.ok(employeeDTO);
        } catch (EmployeeNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        Employee employee = employeeService.toEmployeeMapping(employeeDTO);
        try {
            setEducationPackages(employee);
            employeeService.createEmployee(employee);
        } catch (EmployeeAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        employeeDTO = employeeService.fromEmployeeMapping(employee);

        return ResponseEntity.ok(employeeDTO);

    }

    @PutMapping("/{id}")
    public ResponseEntity<EmployeeDTO> updateEmployee(@PathVariable long id, @RequestBody EmployeeDTO employeeDTO) {
        try {
            Employee employee = employeeService.toEmployeeMapping(employeeDTO);
            setEducationPackages(employee);
            employeeService.updateEmployee(employee);
            employeeDTO = employeeService.fromEmployeeMapping(employee);
            return ResponseEntity.ok().body(employeeDTO);
        } catch (EmployeeNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EmployeeAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteEmployee(@PathVariable long id) {
        try {
            employeeService.deleteEmployeeById(id);
            return ResponseEntity.ok(true);
        } catch (EmployeeNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private void setEducationPackages(Employee employee) {

        employee.setEducationPackages(
                employee.getEducationPackages().stream().map(educationPackage -> getEducationPackage(educationPackage.getName())).collect(Collectors.toList())
        );
    }
    private EducationPackage getEducationPackage(String name) {
        try {
            return educationPackageService.getEducationPackageByName(name);
        } catch (EducationPackageNotFoundException e) {
            return new EducationPackage();
        }
    }

}
