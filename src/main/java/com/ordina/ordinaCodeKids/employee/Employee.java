package com.ordina.ordinaCodeKids.employee;

import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Employee {

    public Employee() {
        this.availabilities = new ArrayList<>();
        this.educationPackages = new ArrayList<>();
    }

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String email;

    @ManyToMany()
    private List<EducationPackage> educationPackages;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employee")
    private List<Availability> availabilities;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;

    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "modified_by")
    @LastModifiedBy
    private String modifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<EducationPackage> getEducationPackages() {
        return educationPackages;
    }

    public void setEducationPackages(List<EducationPackage> educationPackages) {
        this.educationPackages = educationPackages;
    }

    public List<Availability> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(List<Availability> availabilities) {
        this.availabilities = availabilities;
        if(availabilities != null) {
            availabilities.forEach(availability -> availability.setEmployee(this));
        }
    }
    public void addAvailability(Availability availability) {
        availabilities.add(availability);
        availability.setEmployee(this);
    }
    public void removeAvailability(Availability availability) {
        availabilities.remove(availability);
        availability.setEmployee(null);
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public static final class EmployeeBuilder {
        private long id;
        private String firstName;
        private String lastName;
        private String email;
        private List<EducationPackage> educationPackages = new ArrayList<>();
        private List<Availability> availabilities = new ArrayList<>();

        private EmployeeBuilder() {
        }

        public static EmployeeBuilder anEmployee() {
            return new EmployeeBuilder();
        }

        public EmployeeBuilder id(long id) {
            this.id = id;
            return this;
        }

        public EmployeeBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public EmployeeBuilder email(String email) {
            this.email = email;
            return this;
        }

        public EmployeeBuilder educationPackages(List<EducationPackage> educationPackages) {
            this.educationPackages = educationPackages;
            return this;
        }

        public EmployeeBuilder availabilities(List<Availability> availabilities) {
            this.availabilities = availabilities;
            return this;
        }

        public Employee build() {
            Employee employee = new Employee();
            employee.setId(id);
            employee.setFirstName(firstName);
            employee.setLastName(lastName);
            employee.setEmail(email);
            employee.setEducationPackages(educationPackages);
            employee.setAvailabilities(availabilities);

            return employee;
        }
    }
}
