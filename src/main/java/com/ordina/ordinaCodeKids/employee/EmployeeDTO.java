package com.ordina.ordinaCodeKids.employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDTO {

    private long id;

    private String firstName;

    private String lastName;

    private String email;

    private List<String> educationPackages = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getEducationPackages() {
        return educationPackages;
    }

    public void setEducationPackages(List<String> educationPackages) {
        this.educationPackages = educationPackages;
    }

    public static final class EmployeeDTOBuilder {
        private long id;
        private String firstName;
        private String lastName;
        private String email;
        private List<String> educationPackages;

        private EmployeeDTOBuilder() {
        }

        public static EmployeeDTOBuilder anEmployeeDTO() {
            return new EmployeeDTOBuilder();
        }

        public EmployeeDTOBuilder id(long id) {
            this.id = id;
            return this;
        }

        public EmployeeDTOBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeDTOBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public EmployeeDTOBuilder email(String email) {
            this.email = email;
            return this;
        }

        public EmployeeDTOBuilder educationPackages(List<String> educationPackages) {
            this.educationPackages = educationPackages;
            return this;
        }

        public EmployeeDTO build() {
            EmployeeDTO employeeDTO = new EmployeeDTO();
            employeeDTO.setId(id);
            employeeDTO.setFirstName(firstName);
            employeeDTO.setLastName(lastName);
            employeeDTO.setEmail(email);
            employeeDTO.setEducationPackages(educationPackages);
            return employeeDTO;
        }
    }
}
