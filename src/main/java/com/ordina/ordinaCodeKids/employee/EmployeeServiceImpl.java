package com.ordina.ordinaCodeKids.employee;

import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import com.ordina.ordinaCodeKids.email.EmailService;
import com.ordina.ordinaCodeKids.exception.EmployeeAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.EmployeeNotFoundException;
import com.ordina.ordinaCodeKids.exception.UserNotFoundException;
import com.ordina.ordinaCodeKids.user.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final UserService userService;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, UserService userService) {
        this.employeeRepository = employeeRepository;
        this.userService = userService;
    }

    private void validateCreate(Employee employee) throws EmployeeAlreadyExistsException {
        if(employeeRepository.existsById(employee.getId())) {
            throw new EmployeeAlreadyExistsException(employee);
        }
        if(employeeRepository.existsByEmail(employee.getEmail())) {
            throw new EmployeeAlreadyExistsException(employee.getEmail());
        }
    }
    private void validateUpdate(Employee employee) throws EmployeeAlreadyExistsException, EmployeeNotFoundException {
        if(!employeeRepository.existsById(employee.getId())) {
            throw new EmployeeNotFoundException(employee.getId());
        }
        if(employeeRepository.existsByEmailAndIdNot(employee.getEmail(), employee.getId())) {
            throw new EmployeeAlreadyExistsException(employee.getEmail());
        }
    }

    @Override
    public Employee createEmployee(Employee employee) throws EmployeeAlreadyExistsException {
        validateCreate(employee);

        return employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee(Employee employee) throws EmployeeNotFoundException {
        if(!employeeRepository.existsById(employee.getId())) {
            throw new EmployeeNotFoundException(employee.getId());
        }
        employee = employeeRepository.getOne(employee.getId());
        employeeRepository.delete(employee);

        // also remove the corresponding user:
        try{
            userService.deleteUserByEmail(employee.getEmail());
        }catch(UserNotFoundException e) {
            // do nothing
        }

    }

    @Override
    public void deleteEmployeeById(long id) throws EmployeeNotFoundException {
        deleteEmployee(Employee.EmployeeBuilder.anEmployee().id(id).build());
    }

    @Override
    public Employee updateEmployee(Employee employee) throws EmployeeNotFoundException, EmployeeAlreadyExistsException {
        validateUpdate(employee);
        return employeeRepository.save(employee);
    }

    @Override
    public Employee getEmployeeById(long id) throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isEmpty()) {
            throw new EmployeeNotFoundException(id);
        }
        return employee.get();
    }

    @Override
    public List<Employee> getEmployees() { return employeeRepository.findAll(); }

    @Override
    public List<Employee> getEmployeesByEducationPackage(EducationPackage educationPackage) {
        return employeeRepository.findAllByEducationPackagesContains(educationPackage);
    }

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public Employee toEmployeeMapping(EmployeeDTO employeeDTO) {
        TypeMap<EmployeeDTO, Employee> toEmployeeMap = modelMapper.getTypeMap(EmployeeDTO.class, Employee.class);

        if(toEmployeeMap == null) {
            toEmployeeMap = modelMapper.createTypeMap(employeeDTO, Employee.class);
            toEmployeeMap.addMappings(new PropertyMap<>() {
                @Override
                protected void configure() {
                    using(mappingContext ->
                            ((EmployeeDTO) mappingContext.getSource()).getEducationPackages().stream()
                                    .map(name ->  EducationPackage.EducationPackageBuilder.anEducationPackage().name(name).build()).collect(Collectors.toList())
                    ).map(source, destination.getEducationPackages());
                }
            });
        }

        return toEmployeeMap.map(employeeDTO);
    }

    @Override
    public EmployeeDTO fromEmployeeMapping(Employee employee) {
        TypeMap<Employee, EmployeeDTO> fromEmployeeMap = modelMapper.getTypeMap(Employee.class, EmployeeDTO.class);

        if(fromEmployeeMap == null) {
            fromEmployeeMap = modelMapper.createTypeMap(employee, EmployeeDTO.class);
            fromEmployeeMap.addMappings(new PropertyMap<>() {
                @Override
                protected void configure() {
                    using(mappingContext ->
                            ((Employee) mappingContext.getSource()).getEducationPackages().stream()
                                    .map(EducationPackage::getName).collect(Collectors.toList())
                    ).map(source, destination.getEducationPackages());
                }
            });
        }


        return fromEmployeeMap.map(employee);
    }




}
