package com.ordina.ordinaCodeKids.employee;

import com.ordina.ordinaCodeKids.education_package.EducationPackage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    boolean existsByEmail(String email);
    boolean existsByEmailAndIdNot(String email, long id);

    List<Employee> findAllByEducationPackagesContains(EducationPackage educationPackage);
}
