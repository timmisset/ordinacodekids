package com.ordina.ordinaCodeKids.availability;

import com.ordina.ordinaCodeKids.employee.Employee;
import com.ordina.ordinaCodeKids.exception.AvailabilityAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.AvailabilityNotFoundException;
import com.ordina.ordinaCodeKids.availability.Availability;
import com.ordina.ordinaCodeKids.availability.AvailabilityDTO;
import com.ordina.ordinaCodeKids.availability.AvailabilityRepository;
import com.ordina.ordinaCodeKids.availability.AvailabilityService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AvailabilityServiceImpl implements AvailabilityService {

    private final AvailabilityRepository availabilityRepository;

    public AvailabilityServiceImpl(AvailabilityRepository availabilityRepository) {
        this.availabilityRepository = availabilityRepository;
    }

    private void validateCreate(Availability availability) throws AvailabilityAlreadyExistsException {
        if(availabilityRepository.existsById(availability.getId())) {
            throw new AvailabilityAlreadyExistsException(availability);
        }
        if(availabilityRepository.existsByEmployeeAndDate(availability.getEmployee(), availability.getDate())) {
            throw new AvailabilityAlreadyExistsException(availability.getEmployee(), availability.getDate());
        }
    }
    private void validateUpdate(Availability availability) throws AvailabilityNotFoundException {
        if(!availabilityRepository.existsById(availability.getId())) {
            throw new AvailabilityNotFoundException(availability.getId());
        }
    }
    
    @Override
    public Availability createAvailability(Availability availability) throws AvailabilityAlreadyExistsException {
        validateCreate(availability);
        return availabilityRepository.save(availability);
    }

    @Override
    public void deleteAvailability(Availability availability) throws AvailabilityNotFoundException {
        if(!availabilityRepository.existsById(availability.getId())) {
            throw new AvailabilityNotFoundException(availability.getId());
        }
        availabilityRepository.deleteById(availability.getId());
    }

    @Override
    public void deleteAvailabilityById(long id) throws AvailabilityNotFoundException {
        deleteAvailability(Availability.AvailabilityBuilder.anAvailability().id(id).build());
    }

    @Override
    public Availability updateAvailability(Availability availability) throws AvailabilityNotFoundException {
        validateUpdate(availability);
        return availabilityRepository.save(availability);
    }

    @Override
    public Availability getAvailabilityById(long id) throws AvailabilityNotFoundException {
        Optional<Availability> availability = availabilityRepository.findById(id);
        if(availability.isEmpty()) {
            throw new AvailabilityNotFoundException(id);
        }
        return availability.get();
    }

    @Override
    public List<Availability> getAvailabilities() { return availabilityRepository.findAll(); }

    @Override
    public List<Availability> getAvailabilitiesByEmployeeId(long employeeId) {
        return availabilityRepository.findAllByEmployee(Employee.EmployeeBuilder.anEmployee().id(employeeId).build());
    }

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public Availability toAvailabilityMapping(AvailabilityDTO availabilityDTO) {
        Availability availability = modelMapper.map(availabilityDTO, Availability.class);
        if(availabilityDTO.getBookingId() == 0) {
            // prevent creation of an empty booking for id = 0, we want to keep this null
            availability.setBooking(null);
        }
        return availability;
    }

    @Override
    public AvailabilityDTO fromAvailabilityMapping(Availability availability) {
        return modelMapper.map(availability, AvailabilityDTO.class);
    }


}
