package com.ordina.ordinaCodeKids.availability;

import java.time.LocalDate;

public class AvailabilityDTO {

    private long id;

    private LocalDate date;

    private long employeeId;

    private long bookingId;

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }


    public static final class AvailabilityDTOBuilder {
        private long id;
        private LocalDate date;
        private long employeeId;
        private long bookingId;

        private AvailabilityDTOBuilder() {
        }

        public static AvailabilityDTOBuilder anAvailabilityDTO() {
            return new AvailabilityDTOBuilder();
        }

        public AvailabilityDTOBuilder id(long id) {
            this.id = id;
            return this;
        }

        public AvailabilityDTOBuilder date(LocalDate date) {
            this.date = date;
            return this;
        }

        public AvailabilityDTOBuilder employeeId(long employeeId) {
            this.employeeId = employeeId;
            return this;
        }

        public AvailabilityDTOBuilder bookingId(long bookingId) {
            this.bookingId = bookingId;
            return this;
        }

        public AvailabilityDTO build() {
            AvailabilityDTO availabilityDTO = new AvailabilityDTO();
            availabilityDTO.setId(id);
            availabilityDTO.setDate(date);
            availabilityDTO.setEmployeeId(employeeId);
            availabilityDTO.setBookingId(bookingId);
            return availabilityDTO;
        }
    }
}
