package com.ordina.ordinaCodeKids.availability;

import com.ordina.ordinaCodeKids.booking.Booking;
import com.ordina.ordinaCodeKids.employee.Employee;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Availability {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private LocalDate date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="booking_id")
    private Booking booking;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;

    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "modified_by")
    @LastModifiedBy
    private String modifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;

        // bi-directional binding
        if(booking != null && booking.getAvailability() != this) {
            booking.setAvailability(this);
        }
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;

        // bi-directional binding
        if(employee != null && !employee.getAvailabilities().contains(this)) {
            employee.getAvailabilities().add(this);
        }
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    public static final class AvailabilityBuilder {
        private long id;
        private LocalDate date;
        private Employee employee;
        private Booking booking;

        private AvailabilityBuilder() {
        }

        public static AvailabilityBuilder anAvailability() {
            return new AvailabilityBuilder();
        }

        public AvailabilityBuilder id(long id) {
            this.id = id;
            return this;
        }

        public AvailabilityBuilder date(LocalDate date) {
            this.date = date;
            return this;
        }

        public AvailabilityBuilder employee(Employee employee) {
            this.employee = employee;
            return this;
        }

        public AvailabilityBuilder booking(Booking booking) {
            this.booking = booking;
            return this;
        }



        public Availability build() {
            Availability availability = new Availability();
            availability.setId(id);
            availability.setDate(date);
            availability.setEmployee(employee);
            availability.setBooking(booking);
            return availability;
        }
    }
}
