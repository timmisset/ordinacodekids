package com.ordina.ordinaCodeKids.availability;

import com.ordina.ordinaCodeKids.employee.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface AvailabilityRepository extends JpaRepository<Availability, Long> {
    boolean existsByEmployeeAndDate(Employee employee, LocalDate date);

    List<Availability> findAllByEmployee(Employee employee);
}
