package com.ordina.ordinaCodeKids.availability;

import com.ordina.ordinaCodeKids.availability.AvailabilityDTO;
import com.ordina.ordinaCodeKids.exception.AvailabilityAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.AvailabilityNotFoundException;

import java.util.List;

public interface AvailabilityService {

    Availability createAvailability(Availability availability) throws AvailabilityAlreadyExistsException;

    void deleteAvailability(Availability availability) throws AvailabilityNotFoundException;

    void deleteAvailabilityById(long id) throws AvailabilityNotFoundException;

    Availability updateAvailability(Availability availability) throws AvailabilityNotFoundException;

    Availability getAvailabilityById(long id) throws AvailabilityNotFoundException;

    List<Availability> getAvailabilities();

    List<Availability> getAvailabilitiesByEmployeeId(long employeeId);

    Availability toAvailabilityMapping(AvailabilityDTO availabilityDTO);

    AvailabilityDTO fromAvailabilityMapping(Availability availability);
}
