package com.ordina.ordinaCodeKids.availability;

import com.ordina.ordinaCodeKids.exception.AvailabilityAlreadyExistsException;
import com.ordina.ordinaCodeKids.exception.AvailabilityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/availability")
public class AvailabilityController {

    private final AvailabilityService availabilityService;

    public AvailabilityController(AvailabilityService availabilityService) {
        this.availabilityService = availabilityService;
    }

    @GetMapping()
    public ResponseEntity<List<AvailabilityDTO>> getAvailabilities() {
        List<Availability> availabilities = availabilityService.getAvailabilities();
        List<AvailabilityDTO> availabilityDTOs = availabilities.stream().map(availabilityService::fromAvailabilityMapping).collect(Collectors.toList());
        return ResponseEntity.ok(availabilityDTOs);
    }

    @GetMapping("/employee/{employeeId}")
    public ResponseEntity<List<AvailabilityDTO>> getAvailabilitiesByEmployee(@PathVariable long employeeId) {
        List<Availability> availabilities = availabilityService.getAvailabilitiesByEmployeeId(employeeId);
        List<AvailabilityDTO> availabilityDTOs = availabilities.stream().map(availabilityService::fromAvailabilityMapping).collect(Collectors.toList());
        return ResponseEntity.ok(availabilityDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AvailabilityDTO> getAvailability(@PathVariable long id) {
        try {
            Availability availability = availabilityService.getAvailabilityById(id);
            AvailabilityDTO availabilityDTO = availabilityService.fromAvailabilityMapping(availability);
            return ResponseEntity.ok(availabilityDTO);
        } catch (AvailabilityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public ResponseEntity<AvailabilityDTO> createAvailability(@RequestBody AvailabilityDTO availabilityDTO) {
        Availability availability = availabilityService.toAvailabilityMapping(availabilityDTO);
        try {
            availabilityService.createAvailability(availability);
        } catch (AvailabilityAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        availabilityDTO = availabilityService.fromAvailabilityMapping(availability);

        return ResponseEntity.ok(availabilityDTO);

    }

    @PutMapping("/{id}")
    public ResponseEntity<AvailabilityDTO> updateAvailability(@PathVariable long id, @RequestBody AvailabilityDTO availabilityDTO) {
        try {
            Availability availability = availabilityService.toAvailabilityMapping(availabilityDTO);
            availabilityService.updateAvailability(availability);
            availabilityDTO = availabilityService.fromAvailabilityMapping(availability);
            return ResponseEntity.ok().body(availabilityDTO);
        } catch (AvailabilityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteAvailability(@PathVariable long id) {
        try {
            availabilityService.deleteAvailabilityById(id);
            return ResponseEntity.ok(true);
        } catch (AvailabilityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("")
    public ResponseEntity<Boolean> deleteAvailabilities(@RequestBody List<Long> ids) {

        for(long id : ids) {
            try {
                availabilityService.deleteAvailabilityById(id);
            } catch (AvailabilityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }
        return ResponseEntity.ok(true);
    }



}
